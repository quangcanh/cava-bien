function plumrocketInvitations() {
    $this = this;
    $this.contactList = {};
    $this.contactListInput = null;
    $this.currentInvitationsListType = 'O';
    $this.guestIds = [];
    $this.blockedShare = false;
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
    $this.setData = function (data) {
        $this.data = data;
        return $this;
    }
    $this.goToStep1 = function () {
//        pjQuery.fancybox({href: $this.data.urls.step1, type: 'ajax', maxWidth: 520, autoCenter: true});
        pjQuery.fancybox({
            'href': $this.data.urls.step1,
            'type': 'ajax',
            'autoCenter': true,
            'autoDimensions': false,
            'titleShow': false,
            'maxWidth': 520,
            'height': 450
        });
        return false;
    }
    $this.goToStep2 = function (addressBookId) {
        if (jQuery.inArray(addressBookId, ['1', '2']) != -1) {
            return $this.goToStep1();
        }
        pjQuery.fancybox({href: $this.data.urls.step2 + '?provider_box=' + addressBookId, type: 'ajax', autoCenter: true});
        return false;
    }
    $this.goToStep3 = function (form) {
        if ((pjQuery("#email_box").val().length < 1) || (pjQuery("#password_box").val().length < 1)) {
            $this.showAccessError($this.data.errorMsg.emailOrPasswordMissing);
            return false;
        }
        pjQuery.fancybox.showLoading();
        pjQuery.ajax({type: "POST", cache: false, url: form.attr('action'), data: form.serializeArray(), dataType: 'json', success: function (data) {
            if (data.success) {
                pjQuery.fancybox({content: data.result, maxWidth: 610});
            }
            else {
                var text = '';
                for (var i in data.result)
                    text += data.result[i] + '<br/>';
                $this.showAccessError(text);
            }
            pjQuery.fancybox.hideLoading();
        }, error: function () {
            $this.showAccessError($this.data.errorMsg.connectionError);
            pjQuery.fancybox.hideLoading();
        }});
        return false;
    }
    $this.showAccessError = function (html) {
        pjQuery('.errorMsgTextHld').html(html);
        pjQuery('.errorMsgHld').show();
    }
    $this.providerConnect = function (windowOpen, abook_id) {
        var child = window.open(windowOpen.url, windowOpen.name, windowOpen.params);
        var timer = setInterval(function () {
            if (child.closed) {
                clearInterval(timer);
                pjQuery.fancybox.showLoading();
                pjQuery.ajax({type: "POST", cache: false, url: $this.data.urls.step3 + '?provider_box=' + abook_id, dataType: 'json', success: function (data) {
                    if (data.success) {
                        pjQuery.fancybox({content: data.result, maxWidth: 610, autoCenter: true});
                    }
                    else {
                        var text = '';
                        for (var i in data.result)
                            text += data.result[i] + '<br/>';
                        $this.showAccessError(text);
                    }
                    pjQuery.fancybox.hideLoading();
                }, error: function () {
                    $this.showAccessError($this.data.errorMsg.connectionError);
                    pjQuery.fancybox.hideLoading();
                }});
            }
        }, 500);
    }
    $this.addToContactList = function (connectCode, inviteeName, addressBookId) {
        connectCode = jQuery.trim(connectCode);
        inviteeName = jQuery.trim(inviteeName);
        if (!connectCode)
            return;
        if (!addressBookId)
            addressBookId = 0;
        if (!inviteeName)
            inviteeName = connectCode;
        if ($this.contactList[inviteeName] !== undefined)
            return false;
        connectCode = pjQuery.trim(connectCode);
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (addressBookId == 0 && !emailReg.test(connectCode)) {
            pjQuery('#contact-list').removeTag(inviteeName);
            var val = pjQuery('#contact-list').val();
            pjQuery('#contact-list_tag').addClass("not_valid").val(inviteeName);
            return false;
        }
        $this.contactList[inviteeName] = {'connectCode': connectCode, 'inviteeName': inviteeName, 'addressBookId': addressBookId};
        if (addressBookId != 0)
            pjQuery('#contact-list').addTag(inviteeName, {addressBook: addressBookId});
        return $this;
    }
    $this.addToContactListPopUp = function (connectCode, inviteeName, addressBookId) {
        connectCode = jQuery.trim(connectCode);
        inviteeName = jQuery.trim(inviteeName);
        if (!connectCode)
            return;
        if (!addressBookId)
            addressBookId = 0;
        if (!inviteeName)
            inviteeName = connectCode;
        if ($this.contactList[inviteeName] !== undefined)
            return false;
        connectCode = pjQuery.trim(connectCode);
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (addressBookId == 0 && !emailReg.test(connectCode)) {
            pjQuery('#contact-list-popup').removeTag(inviteeName);
            var val = pjQuery('#contact-list-popup').val();
            pjQuery('#contact-list-popup_tag').addClass("not_valid").val(inviteeName);
            return false;
        }
        $this.contactList[inviteeName] = {'connectCode': connectCode, 'inviteeName': inviteeName, 'addressBookId': addressBookId};
        if (addressBookId != 0)
            pjQuery('#contact-list-popup').addTag(inviteeName, {addressBook: addressBookId});
        return $this;
    }
    $this.removeFromContactList = function (inviteeName) {
        inviteeName = pjQuery.trim(inviteeName);
        delete $this.contactList[inviteeName];
        return $this;
    }
    $this.submitContactList = function () {
        pjQuery('.messages-holder').hide();
        if ($this.validateSubmitContactList()) {
            var form = pjQuery('#invite-friends-form');
            var loader = pjQuery('#invite-friends-holder .invitation-loader');
            var invitationResult = pjQuery('#invitation-result-error');
            loader.show();
            invitationResult.hide();
            $this._generateContactListData();
            pjQuery.ajax({url: form.attr('action'), type: 'POST', data: form.serialize(), dataType: 'json', success: function (data) {
                if (data.success) {
                    $this.contactList = {};
                    if (pjQuery('#contact-list_tagsinput .tag').length)
                        pjQuery('#contact-list').removeAllTags();
                    if (pjQuery('#contact-list-popup_tagsinput .tag').length)
                        pjQuery('#contact-list-popup').removeAllTags();

                    $this.getOpenInvitationsList();
                    pjQuery('.messages-holder ul').html('<li>All friends was invited successfuly.</li>');
                    pjQuery('.messages-holder li:first').removeClass('error-msg').addClass('success-msg');
                    if (!$this.data.popupMessages) {
                        pjQuery('#messages-holder').show();
                    }
                    else {
                        pjQuery.fancybox({href: '#messages', minHeight: 45});
                    }
                }
                if (data.messages) {
                    var html = '';
                    if (data.messages.errors)
                        html += '<li class="error-msg"><ul><li>' + data.messages.errors + '</li></ul></li>';
                    if (data.messages.notices)
                        html += '<li class="notice-msg"><ul><li>' + data.messages.notices + '</li></ul></li>';
                    if (data.messages.successes)
                        html += '<li class="success-msg"><ul><li>' + data.messages.successes + '</li></ul></li>';
                    if (html) {
                        html = '<ul class="messages">' + html + '</ul>';
                        pjQuery('.messages').html(html);
                        if (!$this.data.popupMessages) {
                            pjQuery('.messages-holder').show();
                        }
                        else {
                            pjQuery.fancybox({href: '#messages', minHeight: 45});
                        }
                    }
                }
                loader.hide();
            }, error: function () {
                loader.hide();
            }});
        }
    }
    $this._generateContactListData = function () {
        var e = pjQuery('#contact-list-data');
        var html = '';
        for (var i in $this.contactList) {
            html += '\
    <input type="hidden" name="contacts[' + i + '][connectCode]" value="' + $this.contactList[i]['connectCode'] + '"/>\
    <input type="hidden" name="contacts[' + i + '][inviteeName]" value="' + $this.contactList[i]['inviteeName'] + '"/>\
    <input type="hidden" name="contacts[' + i + '][addressBookId]" value="' + $this.contactList[i]['addressBookId'] + '"/>\
   ';
        }
        e.html(html);
    }
    $this.blockShare = function () {
        $this.blockedShare = true;
        return $this;
    }
    $this.unblockShare = function () {
        $this.blockedShare = false;
        return $this;
    }
    $this.isBlockedShare = function () {
        if ($this.blockedShare) {
            $this.validateSubmitContactList({'#guest_email': false});
        }
        return $this.blockedShare;
    }
    $this.updateReferralLink = function (id) {
        var link = INVITATIONS_REFERRAL_LINK;
        link = link.substr(0, link.lastIndexOf('/') + 1) + id;
        INVITATIONS_REFERRAL_LINK = link;
        pjQuery('#invitelink_text').val(link);
        pjQuery('.addthis_toolbox').attr('addthis:url', link);
        addthis.update('share', 'url', link);
        return $this;
    }
    $this.getGuestId = function (email) {
        if (!email)return 0;
        if (typeof $this.guestIds[email] != 'undefined') {
            return $this.guestIds[email];
        }
        var id = 0;
        pjQuery.ajax({type: "POST", cache: false, url: $this.data.urls.guestId, data: 'email=' + email, async: false, dataType: 'json', success: function (data) {
            id = data.id;
        }, error: function () {
            console.log('Get Guest Id: Unexpected error.')
        }});
        $this.guestIds[email] = id;
        return id;
    }
    $this.validateSubmitContactList = function (errors) {
        if (!errors) {
            var errors = {'#invitation-contacts': false, '#invitation-message': false, '#guest_email': false};
        }
        var error = false;
        var id = '#invitation-contacts';
        if (typeof errors[id] != 'undefined') {
            if ($this._objLength($this.contactList) < 1) {
                errors[id] = true;
                error = true;
            }
        }
//        var id = '#invitation-message';
//        if (typeof errors[id] != 'undefined') {
//            if (pjQuery(id).val().replace(/(^\s+)|(\s+$)/g, "").length < 1) {
//                errors[id] = true;
//                error = true;
//            }
//        }
        var id = '#guest_email';
        if (typeof errors[id] != 'undefined') {
            var E = pjQuery(id);
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (E.length && (!E.val() || !emailReg.test(E.val()))) {
                setTimeout(function () {
                    pjQuery(id).focus();
                }, 200);
                errors[id] = true;
                error = true;
            }
        }
        for (var i in errors) {
            pjQuery(i).removeClass('error-field');
            pjQuery(i + '-error').hide();
        }
        if (!error) {
            return true;
        } else {
            for (var i in errors) {
                if (errors[i]) {
                    pjQuery(i).addClass('error-field');
                    pjQuery(i + '-error').show();
                }
            }
        }
        return false;
    }
    $this._objLength = function (obj) {
        var l = 0;
        for (var i in obj)
            l++;
        return l;
    }
    $this.getOpenInvitationsList = function () {
        $this.getInvitationsList($this.data.urls.openInvitationsList, 'O');
    }
    $this.getAcceptedInvitationsList = function () {
        $this.getInvitationsList($this.data.urls.acceptedInvitationsList, 'A');
    }
    $this.getInvitationsList = function (url, type) {
        pjQuery('.tabs-loader').show();
        pjQuery.ajax({url: url, type: 'POST', dataType: 'json', success: function (data) {
            $this.appendInvitationsListHtml(data, this.url);
            pjQuery('.tabs-loader').hide();
            $this.currentInvitationsListType = type;
            if (type == 'O') {
                pjQuery('#load-open-invitations').addClass('active').removeClass('no-active');
                pjQuery('#load-accepted-invitations').removeClass('active').addClass('no-active');
            }
            else {
                pjQuery('#load-accepted-invitations').addClass('active').removeClass('no-active');
                pjQuery('#load-open-invitations').removeClass('active').addClass('no-active');
            }
        }, error: function () {
            pjQuery('.tabs-loader').hide();
        }});
    }
    $this.appendInvitationsListHtml = function (data, url) {
        if (data.success) {
            pjQuery('#invitations-lists').html(data.result);
            var p = url.indexOf('?');
            if (p != -1)
                url = url.substr(0, p);
            $this.changeInvitationsListPagerLinks(url);
        }
    }
    $this.changeInvitationsListPagerLinks = function (url) {
        var area = pjQuery('#' + $this.data.invitationsListAreaId);
        area.find('.pager a').each(function () {
            pjQuery(this).click(function () {
                var arr = pjQuery(this).attr('href').split('/');
                $this.getInvitationsList(url + arr[arr.length - 1], $this.currentInvitationsListType);
                return false;
            });
        });
        area.find('.pager select').each(function () {
            pjQuery(this).removeAttr('onchange').change(function () {
                var arr = pjQuery(this).val().split('/');
                $this.getInvitationsList(url + arr[arr.length - 1], $this.currentInvitationsListType);
            });
        });
    }
}
var pInvitations = new plumrocketInvitations();