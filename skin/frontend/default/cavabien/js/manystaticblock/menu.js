jQuery(document).ready(function(){
    jQuery('.accordion_content').hide();
    jQuery('.accordion_content').eq('1').show();
    jQuery('.head_accordion').addClass('close');
    jQuery('.head_accordion').eq('1').removeClass('close').addClass('open');
    jQuery('.head_accordion').click(function() {
        if(jQuery(this).hasClass('close')) {//if selected
            jQuery(this).removeClass('close');//slideup
            jQuery(this).next().slideDown();
            jQuery(this).addClass('open');
        } else {
            jQuery(this).removeClass('open');
            jQuery(this).next().slideUp();
            jQuery(this).addClass('close');
        }
    });

    var cms_page_current = jQuery('#cms_page_current').val();
    jQuery('a[href$="'+cms_page_current+'"]').addClass('active');
    jQuery('a[href$="'+cms_page_current+'"]').parent().parent().prev().addClass('active');
    jQuery('.menu_static_header_accordion').addClass('close');

    jQuery('.menu_static_header_accordion').next().slideUp();
    jQuery('.menu_static_header_accordion').each(function(){
        if(jQuery(this).hasClass('active')){
            jQuery(this).next().slideDown();
            jQuery(this).removeClass('close');
            jQuery(this).addClass('open');
        }
    });

    jQuery('.menu_static_header_accordion').click(function(){
        if(jQuery(this).hasClass('close')){
           jQuery('.menu_static_header_accordion').next().slideUp();
           jQuery(this).next().slideDown();
           jQuery('.menu_static_header_accordion').addClass('close');
           jQuery(this).removeClass('close');
           jQuery(this).addClass('open');
        }else{
            jQuery(this).removeClass('open');
            jQuery(this).addClass('close');
            jQuery(this).next().slideUp();
        }
    });

//    if(jQuery('.menu_static_header_accordion').hasClass('active')){
//        jQuery(this).next().slideDown();
//        jQuery(this).removeClass('close');
//        jQuery(this).addClass('open');
//    }

});