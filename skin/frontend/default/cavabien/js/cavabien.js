jQuery(window).load(function() {
      var homeWidth = jQuery("#custommenu > div.menu > .parentMenu.home").width();
    var homeWidthA = jQuery("#custommenu > div.menu > .parentMenu.home > a").width();
    /*Custom Menu*/
    var totalWidth = jQuery("#custommenu").width() - homeWidth;
    var numItems = jQuery("#custommenu > div.menu").length - 1;
    var totalwidthText = 0;
    jQuery('#custommenu > div.menu > .parentMenu > a').each(function(){
        totalwidthText += jQuery(this).width();
    });
    var realWidth = totalwidthText - homeWidthA;
    var padding = (totalWidth - realWidth)/(numItems*2);
    jQuery('#custommenu > div.menu > .parentMenu > a').css('padding-left', padding);
    jQuery('#custommenu > div.menu > .parentMenu > a').css('padding-right', padding);
    jQuery('#custommenu > div.menu > .parentMenu.home > a').css('padding', '0');
    if (jQuery.browser.msie || (!!navigator.userAgent.match(/Trident.*rv\:11\./))) {
        jQuery("#custommenu").width(1310.4);
    }
    if (navigator.userAgent.indexOf('Firefox') > -1) {
        jQuery("#custommenu").width(1310.1);
    }
    /*End Custom Menu*/

});

function enlargeView (a,b,c) {
    jQuery(a).find('.enlarge .item ')
        .mouseenter(function() {
            var enItemOffset =  jQuery(this).offset();
            var posLeftEx = enItemOffset.left - (b - jQuery(this).width())/2;
            var posTopEx = enItemOffset.top - (c - jQuery(this).height())/2;
            if( jQuery('.enlarge-popup .item').text()==''){
                var real =  jQuery(this).find('.enlarge-real');
                jQuery(real).appendTo('.enlarge-popup .item');
                jQuery('.enlarge-popup').css({
                    position: 'absolute',
                    top: posTopEx,
                    left: posLeftEx,
                    'z-index': '6666'
                });
                jQuery('.enlarge-popup .item .item-wrapper').css({
                    width: b,
                    height: c
                });
            }
        });
    jQuery('.enlarge-popup').mouseleave(function() {

        if(jQuery(this).find('.item').text()!=''){
            if(jQuery(this).find('.enlarge-shopthelook').length>0){
                var num = jQuery(this).find('.item-wrapper').attr('idstl');
                jQuery(this).children().children().insertAfter(jQuery('.shop-the-look-slider .item #formstl'+num));
            }else{
                var num = jQuery(this).find('.item-wrapper').attr('id');
                jQuery(this).children().children().insertAfter(jQuery('.enlarge .item #'+num));
            }
        }
        jQuery(this).find('.item').empty();
    });
}

jQuery(document).ready(function(){
    jQuery(".enlarge-fake").hover(
        function() {
            jQuery('.enlarge-real').hide();
            jQuery('.enlarge-fake').show();

            var _id = jQuery(this).attr('id');
            jQuery( '#' + _id ).hide();
            jQuery( '#real-' + _id ).show();
        }, function() {

        }
    );

    jQuery(".enlarge-real").hover(
        function() {

        }, function() {
            var _id = jQuery(this).attr('id');
            jQuery( '#' + _id ).hide();
            _id = _id.replace("real-", "");
            jQuery( '#' + _id ).show();
        }
    );

    //enlargeView ('.stylist-product',265,370);
    //enlargeView ('.homepage-tab',265,490);
    //enlargeView ('.category-products',265,490);
    /*Begin Ajax Search */
    var timer = 0;
    jQuery("#search").on('keyup', function(e){

        if (timer) {
            clearTimeout(timer);
        }
        if (jQuery("#search").val().length >= 3) {
            timer = setTimeout(ajax_search, 400);
        }else{
            jQuery("#search_autocomplete").html("");
            jQuery('#search_autocomplete').css('display','none');
            jQuery("#search").removeClass("loading");
        }


    });
    var search_xhr = null;
    function ajax_search(){
        if(search_xhr && search_xhr.readystate != 4){
            search_xhr.abort();
        }
        jQuery("#search").addClass("loading");
        jQuery("#search_autocomplete").html("");
        search_xhr = jQuery.ajax({
            type: "GET",
            url: "/cavabien/search/result",
            data: jQuery("#search_mini_form").serialize(),
            dataType: "json",
            success: function (response) {
                var data = "<ul>";
                var co=0;
                jQuery.each(response, function (i, item) {
                    co++;
                    data += "<li>";
                    data += "   <a href='" + item.url + "'>";
                    data += "       <img src='" + item.img + "'/>";
                    data += "       <span class='product-name'>" + item.name + "</span>";
                    data += "   </a>";
                    //data += "   <span class='product-desc'>" + item.desc + "</span>";
                    data += "   <span class='product-price'>" + item.price + "</span>";
                    if (item.saleable) {
                        data += "   <button type='button' title='Add to Cart' class='button btn-cart' onclick=\"setLocation('" + item.addtocartURL + "')\"><span><span>Add to Cart</span></span></button>";
                    } else {
                        data += "   <p class='availability out-of-stock'><span>Out of stock</span></p>";
                    }
                    data += "</li>";
                });
                if(co==0){
                    data += "<li style='text-align: center'>No product found!</li>";
                }
                data += "</ul>";
                jQuery("#search_autocomplete").html(data);
                jQuery('#search_autocomplete').css('display','block');
                jQuery("#search").removeClass("loading");
            }
        })
    }
    jQuery('#search_mini_form #cat').change(function(){
        if (jQuery("#search").val().length >= 3 && jQuery("#search").val()!="Search entire store here...") {
            ajax_search();
        }
    });
    jQuery("#search_autocomplete").bind( "clickoutside", function(event){
        jQuery("#search").bind( "clickoutside", function(event){
            if(jQuery('#search_autocomplete').css('display') != 'none'){
                jQuery('#search_autocomplete').hide();
            }
        });
    });

    jQuery('#search').click(function() {
        if(jQuery(this).val()!=""&& jQuery(this).val().length > 2){
            jQuery('#search_autocomplete').css('display','block');
        }
    });

    jQuery('.ul-selectbox').selectBox();
    /*End Ajax Search */

    /*Begin Slider Feature Product */
    var featuredProductCarousel = jQuery('#featured-product-carousel').bxSlider({
        slideWidth: 151,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 12,
        pager: false
    });

    jQuery("#featured-product").hover(function () {
        featuredProductCarousel.reloadSlider();
    });

    /*End Slider Feature Product*/

    /*Live Chat*/
    jQuery('#live-chat').click(function () {
        $zopim(function () {
            $zopim.livechat.window.show();
        });
    });
    /*End Live Chat*/

    // Show invite popup
    jQuery("#invite-popup").dialog({
        autoOpen: false,
        modal: true,
        buttons: false,
        width: "auto"
    });
	jQuery("#invite-popup-login").dialog({
        autoOpen: false,
        modal: true,
        buttons: false,
        width: "auto"
    });
    jQuery("#invite").click(function(){
        jQuery("#invite-popup").dialog("open");
    });
    jQuery("#alogin").click(function(){
        jQuery("#invite-popup-login").dialog("open");
    });
    jQuery("#invite").click(function(){
        jQuery("#invite-popup").dialog("open");
    });
    jQuery("#invite-now").click(function(){
        jQuery("#invite-popup").dialog("open");
    });
    // Close Pop-in If the user clicks anywhere else on the page
//    jQuery('html').bind(
//        'click', function(e){
//            if(!jQuery(e.target).is('.ui-dialog, a') && !jQuery(e.target).closest('.ui-dialog').length)
//            {
//                jQuery('#invite-popup').dialog('close');
//            }
//        }
//    );
    /*End Show Invite Popup */

    var socialSlide = jQuery(".social-button-login-slide").bxSlider({
        slideWidth: 15,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 5,
        pager: false
    });

    //Begin CrossSell Slider
    if(jQuery(".homepage-slider-cross-sell")){
        var crossSellSlide = jQuery(".homepage-slider-cross-sell").bxSlider({
            slideWidth: 100,
            minSlides: 3,
            maxSlides: 3,
            moveSlides: 1,
            slideMargin: 5,
            pager: false
        });

        //jQuery(".cart_box").hover(function () {
        //    if(typeof crossSellSlide != "undefined"){
        //        crossSellSlide.reloadSlider();
        //    }
        //});
    }
    //End CrossSell Slider

    //Begin Open Popup Free Delivery
    jQuery("#free-delivery").click(function(){
        jQuery("#free-delivery-box").dialog("open");
        //jQuery("#free-delivery-box").toggle();
    });

    // Show invite popup
    jQuery("#free-delivery-box").dialog({
        autoOpen: false,
        modal: true,
        buttons: false,
        width: "auto"
    });
    //Begin Open Popup Free Delivery
        jQuery("#cava-popup-link").click(function(){
            jQuery("#cava-popup").dialog("open");
            jQuery('.delivery-content').animate({scrollTop: -500},0);
            //jQuery("#free-delivery-box").toggle();
        });
    // Show invite popup
    jQuery("#cava-popup").dialog({
        autoOpen: false,
        modal: true,
        buttons: false,
        width: 'auto'
    });


    jQuery('#homepage-free-delivery-tab').tabs();
    // Close Pop-in If the user clicks anywhere else on the page
//    jQuery('html').bind(
//        'click', function(e){
//            if(  !jQuery(e.target).is('.ui-dialog, a') && !jQuery(e.target).closest('.ui-dialog').length)
//            {
//                jQuery('#free-delivery-box').dialog('close');
//            }
//        }
//    );
    //End Open Popup Free Delivery

    //Edit image of category new arrival when hover category link
    jQuery(".top-category-new-arrival").on({
        mouseenter: function () {
            $cate_id = jQuery(this).attr('cate_id');
            jQuery('.category-popup-'+$cate_id).css('display', 'block');
            jQuery('.new-arrival-popup').css('display', 'none');
        },
        mouseleave: function () {
            $cate_id = jQuery(this).attr('cate_id');
            jQuery('.category-popup-'+$cate_id).css('display', 'none');
            jQuery('.new-arrival-popup').css('display', 'block');
        }
    });
    //End Edit image of category new arrival when hover category link

    /* Begin Slider Deals of the week*/
    var homepageMultiDealsCarousel = jQuery('#homepage-multi-deals-carousel-1').bxSlider({
        slideWidth: 151,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 12,
        pager: false
    });
    jQuery("#deal-of-week").hover(function () {
        homepageMultiDealsCarousel.reloadSlider();
    });
    /*End Slider Deals of the week */

    /* Begin Special Shop The Look Slider */
    function specialShopTheLookSlider(elem){

        var owl = jQuery("." + elem);
        if(owl){
            owl.owlCarousel({
                items : 7,
                itemsCustom : false,
                itemsDesktop : [1199,3],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                pagination : false,
                navigation: true,
                slideSpeed : 400,
                paginationSpeed : 800,
                rewindSpeed : 1000
            });
        }

    }

    specialShopTheLookSlider('sp-product-slide');
    /* End */

    //init uniform
    jQuery("select").not('.attribute-conf, #cat').uniform({selectAutoWidth: false});


});
function show_style_video(item, url) {
    var myVideo = document.getElementById('video-' + item);
    var myVideo1 = document.getElementById('video1-' + item);
    playVideo(myVideo,url,item);
    playVideo1(myVideo1,url,item);
}
function playVideo(myVideo,url,item){
        if(!myVideo.src){
            myVideo.src = url;
            myVideo.load();
            myVideo.play();
        }


        myVideo.show();
        if (!myVideo.paused){
            myVideo.pause();
        }
        else{
            myVideo.play();
        }
        hideImg(item);
        myVideo.play();
}
function playVideo1(myVideo,url,item){
        if(!myVideo.src){
            myVideo.src = url;
            myVideo.load();
            myVideo.play();
        }
        myVideo.addEventListener("ended", function () {
                myVideo.hide();
                document.getElementById('video-' + item).hide();
                showImg(item);
            },
            true
        );
        myVideo.show();
        if (!myVideo.paused){
            myVideo.pause();
        }
        else{
            myVideo.play();
        }
        hideImg(item);
        myVideo.play();
}
function showImg(item){
    jQuery('.a-image-' + item).show();
    jQuery('.a-image-' + item+' img').show();
}
function hideImg(item){
    jQuery('.a-image-' + item).hide();
    jQuery('.a-image-' + item+' img').hide();

}

jQuery(document).ready(function(){
    var out1 = 0;

    jQuery(".refine-color").bind( "clickoutside", function(event) {
        out1 = 1;
    });
    jQuery("#dropdown_1").bind( "clickoutside", function(event){
        if(jQuery('#dropdown_1').css('display') != 'none' && out1 ==1){
            jQuery('#dropdown_1').hide();
            out1 = 0;
        }
    });

    jQuery("#dropdown_1").bind( "click", function(event){
        jQuery('#dropdown_1').show();
    });

    var out2 = 0;

    jQuery(".refine-price").bind( "clickoutside", function(event) {
        out2 = 1;
    });
    jQuery("#dropdown_2").bind( "clickoutside", function(event){
        if(jQuery('#dropdown_2').css('display') != 'none' && out2 ==1){
            jQuery('#dropdown_2').hide();
            out2 = 0;
        }
    });

    jQuery("#dropdown_2").bind( "click", function(event){
        jQuery('#dropdown_2').show();
    });
    var out3 = 0;

    jQuery(".refine-size").bind( "clickoutside", function(event) {
        out3 = 1;
    });
    jQuery("#dropdown_3").bind( "clickoutside", function(event){
        if(jQuery('#dropdown_3').css('display') != 'none' && out3 ==1){
            jQuery('#dropdown_3').hide();
            out3 = 0;
        }
    });

    jQuery("#dropdown_3").bind( "click", function(event){
        jQuery('#dropdown_3').show();
    });
});
function show_login(){
    jQuery("#invite-popup-login").dialog("open");
}