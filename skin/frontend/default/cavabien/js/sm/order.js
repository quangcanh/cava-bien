

var smOrder = Class.create();
smOrder.prototype = {
    initialize:function (url) {
        this.loadBaseUrl = url;
        this.smContainer = null;
        this.smButton = null;
        document.addEventListener("click", function (e) {
            if (order.smContainer != null && order.smButton != null) {
                if (e.target.id !== order.smContainer.id && e.target.id !== order.smButton.id && e.target.parentNode.id !== order.smContainer.id) {
                    order.smContainer.hide();
                }
            }
        });
    },

    loadArea:function (area, params) {
        var url = this.loadBaseUrl;
        if (area) {
            area.push('messages');
            url += 'block/' + area;
        }
        if (!this.loadingAreas) this.loadingAreas = [];
        this.loadingAreas = area;
        jQuery.fancybox.showLoading();
        $('sm-ajax-load').show();
        new Ajax.Request(url, {
            method:'post',
            parameters:params,
            onSuccess:function (transport) {
                var response = transport.responseText.evalJSON();
                this.loadAreaResponseHandler(response);
                jQuery.fancybox.hideLoading();
                $('sm-ajax-load').hide();
                if (params.type == 'get_store_list') {
                    $('sm-store_list').show();
                    order.smContainer = $('sm-store_list');
                    order.smButton = $($('sm-store_list').getAttribute('sm-store-button'));
                }
            }.bind(this)
        });
    },

    loadAreaResponseHandler:function (response) {
        for (var i = 0; i < this.loadingAreas.length; i++) {
            var id = this.loadingAreas[i];
            if ($(this.getAreaId(id))) {
                if (response[id]) {
                    var wrapper = new Element('div');
                    wrapper.update(response[id] ? response[id] : '');
                    $(this.getAreaId(id)).update(wrapper);
                }
            }
        }
    },

    getAreaId:function (area) {
        return 'sm-' + area;
    },

    validate:function (id) {
        // validate value of inputs
        var elm = $(id);
        var data = elm.value;
        if (data.trim() == '') {
            if ($('advice-required-entry-' + id) == null) {
                var advice = '<div id="advice-required-entry-' + id + '" class="validation-advice" style="">This is a required field.</div>';
                Element.insert(elm.up(), {'after':advice});
            }
            if (!elm.hasClassName('validation-failed')) {
                elm.addClassName('validation-failed');
            }
            return false;
        } else {
            if ($('advice-required-entry-' + id)) {
                $('advice-required-entry-' + id).remove();
            }
            if (elm.hasClassName('validation-failed')) {
                elm.removeClassName('validation-failed');
            }
            return data;
        }
    },

    toolTip:function (container, button) {
        Element.show(container);
        order.smContainer = $(container);
        order.smButton = button;
    },

    removeCoupon:function () {
        this.loadArea(['step1_total', 'cart_totals'], {'type':'remove_coupon'});
    },

    applyCoupon:function (id) {
        if (this.validate(id)) {
            this.loadArea(['step1_total', 'cart_totals'], {'type':'save_coupon', 'order[coupon][code]':this.validate(id)});
        }
    },

    applyGiftCode:function (id) {
        if (this.validate(id)) {
            this.loadArea(['step1_total', 'cart_totals'], {'type':'apply_giftcode', 'order[giftcard][code]':this.validate(id)});
        }
    },

    checkGiftCode:function (id) {
        if (this.validate(id)) {
            this.loadArea(['giftcardaccount_quickcheck'], {'type':'check_giftcode', 'order[giftcard][code]':this.validate(id)});
        }
    },

    removeGiftcode:function (id) {
        this.loadArea(['step1_total', 'cart_totals'], {'type':'remove_giftcode', 'order[giftcard][code]':id});
    },

    applyFacebookCode:function (shared) {
        this.loadArea(['step1_total', 'cart_totals', 'step2_total'], {'type':'apply_facebook_code', 'order[facebook][code]':shared});
    },

    applyRewardPoints:function (points, element) {
        if (element.checked) {
            this.loadArea(['step1_total', 'cart_totals', 'step2_total'], {'type':'apply_reward_points', 'order[points]':points});
        } else {
            this.removeRewardPoints();
        }
    },

    removeRewardPoints:function () {
        this.loadArea(['step1_total', 'cart_totals', 'step2_total'], {'type':'remove_reward_points'});
    },

    getStore:function (id) {
        var zipcode = $(id).value;
        if (zipcode.trim() == '') {
            alert('Please type your post code');
        } else {
            this.loadArea(['store_list'], {'type':'get_store_list', 'order[zipcode]':zipcode});
        }
    },

    deliverySetStore:function(title) {
        var idText = $('sm-store_list').getAttribute('sm-store-text');
        var idMethod = $('sm-store_list').getAttribute('sm-store-method');
        $(idText).value = title;
        $(idMethod).setAttribute('choose-store', true);
        $('sm-store_list').hide();
    }
}
