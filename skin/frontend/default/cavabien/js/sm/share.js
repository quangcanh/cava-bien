// facebook share
function fbShare(link, picture, name, caption, description) {
    FB.ui(
        {
            method:'feed',
            link:link,
            picture:picture,
            name:name,
            caption:caption,
            description:description
        },
        function (response) {
            console.log(response);
            if (response && !response.error_code) {
                // process if something is shared
                facebookAfterShare();
            } else {
                alert('Please click share button to get sale off.');
            }
        });
}

function facebookAfterShare() {
    alert('Thanks for sharing!');
}
// tweet share
twttr.events.bind('tweet', function (event) {
    tweetAfterShare(event);
});

function smTweetShare(id, url, des) {
    var tweetUrl = "https://twitter.com/intent/tweet" +
        "?url=" + encodeURIComponent(url) +
        "&text=" + encodeURIComponent(des);
    document.getElementById(id).href = tweetUrl;
}
function tweetAfterShare(event) {
    if (event.target.id == 'shareLink') {
        // process if the button-clicked is something, example the clicked button has id 'shareLink'
        alert('Thanks for sharing!');
    }
        alert('testing');
}