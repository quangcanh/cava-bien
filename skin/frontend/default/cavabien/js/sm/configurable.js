if (typeof mainProduct == 'undefined') {
    var mainProduct = {};
}

/**************************** CONFIGURABLE PRODUCT **************************/
mainProduct.Config = Class.create();
mainProduct.Config.prototype = {
    initialize: function(config, area, proId){
        this.config     = config;
        this.productId = proId;
        this.taxConfig  = this.config.taxConfig;
        if (config.containerId) {
            this.settings   = $$('#' + config.containerId + ' ' + '.super-attribute-select');
        } else {
            this.settings   = $$('.super-attribute-select');
        }
        this.state      = new Hash();
        this.priceTemplate = new Template(this.config.template);
        this.prices     = config.prices;

        // Init
        this.area = area;
        this.showLabel = false;
        // Ịnit class
        this.containerClass = 'sm-conf-container';
        this.attributeClass = 'sm-conf-attribute';
        this.titleClass = 'sm-conf-title';
        this.qtyClass = 'sm-conf-qty';
        this.ulClass = 'sm-conf-ul';
        this.liClass = 'sm-conf-li';
        this.aClass = 'sm-conf-a';
        this.imgClass = 'sm-conf-img';
        this.spanClass = 'sm-conf-span';
        this.outOfStockOption = 'sm-conf-outofstock';
        this.selectedOption = 'sm-conf-selected';
        this.notSaleableOption = 'sm-conf-not-saleable';

        this.attrIds = this.config.attrIds;
        this.listAttribute = [];
        this.selectedProducts = [];
        this.advice = null;
        this.numberRow = 0;

        // Create list option
//        document.observe("dom:loaded", this.initList.bind(this));

    },

    initList: function (e){
        this.createList(0, 'sm-test', true, false, false, false);
        this.addMore('sm-add-more-container');
    },

    configureForValues: function () {
        if (this.values) {
            this.settings.each(function(element){
                var attributeId = element.attributeId;
                element.value = (typeof(this.values[attributeId]) == 'undefined')? '' : this.values[attributeId];
                this.configureElement(element);
            }.bind(this));
        }
    },

    configure: function(event){
        var element = Event.element(event);
        this.configureElement(element);
    },

    configureElement : function(element) {
        this.reloadOptionLabels(element);
        if(element.value){
            this.state[element.config.id] = element.value;
            if(element.nextSetting){
                element.nextSetting.disabled = false;
                this.fillSelect(element.nextSetting);
                this.resetChildren(element.nextSetting);
            }
        }
        else {
            this.resetChildren(element);
        }
        this.reloadPrice();
    },

    reloadOptionLabels: function(element){
        var selectedPrice;
        if(element.selectedIndex < 0) return;
        if(element.options[element.selectedIndex].config && !this.config.stablePrices){
            selectedPrice = parseFloat(element.options[element.selectedIndex].config.price)
        }
        else{
            selectedPrice = 0;
        }
        for(var i=0;i<element.options.length;i++){
            if(element.options[i].config){
                element.options[i].text = this.getOptionLabel(element.options[i].config, element.options[i].config.price-selectedPrice);
            }
        }
    },

    resetChildren : function(element){
        if(element.childSettings) {
            for(var i=0;i<element.childSettings.length;i++){
                element.childSettings[i].selectedIndex = 0;
                element.childSettings[i].disabled = true;
                if(element.config){
                    this.state[element.config.id] = false;
                }
            }
        }
    },

    fillSelect: function(element){
        var attributeId = element.id.replace(/[a-z]*/, '');
        var options = this.getAttributeOptions(attributeId);
        this.clearSelect(element);
        element.options[0] = new Option('', '');
        element.options[0].innerHTML = this.config.chooseText;

        var prevConfig = false;
        if(element.prevSetting){
            prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
        }

        if(options) {
            var index = 1;
            for(var i=0;i<options.length;i++){
                var allowedProducts = [];
                if(prevConfig) {
                    for(var j=0;j<options[i].products.length;j++){
                        if(prevConfig.config.allowedProducts
                            && prevConfig.config.allowedProducts.indexOf(options[i].products[j])>-1){
                            allowedProducts.push(options[i].products[j]);
                        }
                    }
                } else {
                    allowedProducts = options[i].products.clone();
                }

                if(allowedProducts.size()>0){
                    options[i].allowedProducts = allowedProducts;
                    element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    if (typeof options[i].price != 'undefined') {
                        element.options[index].setAttribute('price', options[i].price);
                    }
                    element.options[index].config = options[i];
                    index++;
                }
            }
        }
    },

    getOptionLabel: function(option, price){
        var price = parseFloat(price);
        if (this.taxConfig.includeTax) {
            var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
            var excl = price - tax;
            var incl = excl*(1+(this.taxConfig.currentTax/100));
        } else {
            var tax = price * (this.taxConfig.currentTax / 100);
            var excl = price;
            var incl = excl + tax;
        }

        if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
            price = incl;
        } else {
            price = excl;
        }

        var str = option.label;
        if(price){
            if (this.taxConfig.showBothPrices) {
                str+= ' ' + this.formatPrice(excl, true) + ' (' + this.formatPrice(price, true) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str+= ' ' + this.formatPrice(price, true);
            }
        }
        return str;
    },

    formatPrice: function(price, showSign){
        var str = '';
        price = parseFloat(price);
        if(showSign){
            if(price<0){
                str+= '-';
                price = -price;
            }
            else{
                str+= '+';
            }
        }

        var roundedPrice = (Math.round(price*100)/100).toString();

        if (this.prices && this.prices[roundedPrice]) {
            str+= this.prices[roundedPrice];
        }
        else {
            str+= this.priceTemplate.evaluate({price:price.toFixed(2)});
        }
        return str;
    },

    clearSelect: function(element){
        for(var i=element.options.length-1;i>=0;i--){
            element.remove(i);
        }
    },

    getAttributeOptions: function(attributeId){
        if(this.config.attributes[attributeId]){
            return this.config.attributes[attributeId].options;
        }
    },

    reloadPrice: function(){
        if (this.config.disablePriceReload) {
            return;
        }
        var price    = 0;
        var oldPrice = 0;
        for(var i=this.settings.length-1;i>=0;i--){
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if(selected.config){
                price    += parseFloat(selected.config.price);
                oldPrice += parseFloat(selected.config.oldPrice);
            }
        }

        optionsPrice.changePrice('config', {'price': price, 'oldPrice': oldPrice});
        optionsPrice.reload();

        return price;

        if($('product-price-'+this.config.productId)){
            $('product-price-'+this.config.productId).innerHTML = price;
        }
        this.reloadOldPrice();
    },

    reloadOldPrice: function(){
        if (this.config.disablePriceReload) {
            return;
        }
        if ($('old-price-'+this.config.productId)) {

            var price = parseFloat(this.config.oldPrice);
            for(var i=this.settings.length-1;i>=0;i--){
                var selected = this.settings[i].options[this.settings[i].selectedIndex];
                if(selected.config){
                    price+= parseFloat(selected.config.price);
                }
            }
            if (price < 0)
                price = 0;
            price = this.formatPrice(price);

            if($('old-price-'+this.config.productId)){
                $('old-price-'+this.config.productId).innerHTML = price;
            }

        }
    },

    createList:function (row, idDiv, showLabelAttr, showLabelOption, showQty, showTitle, showBigImage, longAjax) {
        this.selectedProducts[row] = {'super_attribute': {}, 'qty':1};
        this.listAttribute[row] = JSON.parse(JSON.stringify(this.config.attributes));
        this.listAttribute[row].showLabelAttr = showLabelAttr;
        this.listAttribute[row].showLabelOption = showLabelOption;
        this.listAttribute[row].showQty = showQty;
        this.listAttribute[row].showTitle = showTitle;
        this.listAttribute[row].showBigImage = showBigImage;
        this.listAttribute[row].longAjax = longAjax;

        var attributes = this.listAttribute[row];
        var divContainer = document.createElement('div');
        divContainer.className = this.containerClass;
        divContainer.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-container';

        for (var i = 0; i < this.attrIds.length; i++) {
            this.listAttribute[row][this.attrIds[i]].state = {'id':null, 'position':null};
            var attribute = attributes[this.attrIds[i]];
            var options = attribute.options;

            var divAttribute = document.createElement('div');
            var optionUl = document.createElement('ul');

            //set value
            divAttribute.className = this.attributeClass;
            optionUl.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-ul' + '-attribute_' + this.attrIds[i];
            divAttribute.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i];

            for (var j = 0; j < options.length; j++) {
                options[j].state = 'enable';
                options[j].status = true;
                var optionLi = document.createElement('li');
                var optionA = document.createElement('a');
                var optionImg = document.createElement('img');

                if (attribute.code == 'color' || (options[j].image != null && options[j].image != "" && options[j].image != "undefined")) {
                    // set value
                    optionLi.className = this.liClass;
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionImg.className = this.imgClass;
                    optionImg.src = options[j].image;
                    optionImg.title = options[j].label;
                    optionImg.alt = options[j].label;
                    optionImg.width = '18';
                    optionImg.height = '18';

                    // append
                    optionA.appendChild(optionImg);
                } else if (attribute.code.toString().indexOf('size') > -1) {
                    optionLi.className = this.liClass + " size-product";
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionA.appendChild(document.createTextNode(options[j].label));
                } else {
                    optionLi.className = this.liClass + " size-product";
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionA.appendChild(document.createTextNode(options[j].label));
                }

                if (showLabelOption) {
                    var optionSpan = document.createElement('span');
                    var optionLabel = document.createTextNode(options[j].label);
                    optionSpan.className = this.spanClass;
                    optionSpan.appendChild(optionLabel);
                    optionA.appendChild(optionSpan);
                }

                if (attribute.code == 'color' && showBigImage) {
                    var optionDiv = document.createElement('div');
                    var optionSpan1 = document.createElement('span');
                    var optionSpan2 = document.createElement('span');
                    optionDiv.className = 'image-color-hover';
                    optionSpan1.className = 'color-name';
                    optionSpan2.className = 'desc';
                    optionSpan1.appendChild(document.createTextNode(options[j].label));
                    optionSpan2.appendChild(document.createTextNode('Click to view photo'));
                    optionDiv.appendChild(optionImg.cloneNode(true));
                    optionDiv.appendChild(optionSpan1);
                    optionDiv.appendChild(optionSpan2);
                    optionDiv.style.display = 'none';
                    optionLi.appendChild(optionDiv);
                    optionA.className += ' sm-hover-color';
                }
                optionLi.appendChild(optionA);
                optionUl.appendChild(optionLi);
            }
            if (showLabelAttr) {
                var labelAttribute = document.createElement('label');
                labelAttribute.appendChild(document.createTextNode(attribute.label));
                divAttribute.appendChild(labelAttribute);
            }
            if (showTitle) {
                var titleSpan = document.createElement('span');
                var titleLabel = document.createElement('span');
                titleLabel.appendChild(document.createTextNode(attribute.label));
                titleSpan.className = this.titleClass;
                titleSpan.appendChild(titleLabel);
                divAttribute.appendChild(titleSpan);
            }

            divAttribute.appendChild(optionUl);
            divContainer.appendChild(divAttribute);
        }

        // Create qty
        if (showQty) {
            var divQty = document.createElement('div');
            var qtyUl = document.createElement('ul');
            qtyUl.className = this.ulClass;
            qtyUl.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-qty';
            for (var i = 0; i < 20; i++) {
                var qtyLi = document.createElement('li');
                var qtyA = document.createElement('a');
                var qtyNumber = document.createTextNode(i + 1);
                qtyA.setAttribute('qty', i + 1);
                qtyA.setAttribute('row', row);
                qtyA.href = 'javascript:void(0)';
                qtyA.className = this.aClass;
                qtyLi.className = this.liClass;
                qtyA.appendChild(qtyNumber);
                qtyLi.appendChild(qtyA);
                qtyUl.appendChild(qtyLi);
            }
            if (showTitle) {
                var titleSpan = document.createElement('span');
                var titleLabel = document.createElement('span');
                titleLabel.appendChild(document.createTextNode('Qty'));
                titleSpan.className = this.titleClass;
                titleSpan.appendChild(titleLabel);
                divQty.appendChild(titleSpan);
            } else {
                var qtySpan = document.createElement('span');
                var qtyText = document.createTextNode('Qty');
                qtySpan.appendChild(qtyText);
                divQty.appendChild(qtySpan);
            }
            divQty.appendChild(qtyUl);
            divQty.className = this.qtyClass;
            divContainer.appendChild(divQty);
        }

        $(idDiv).appendChild(divContainer);

        // Register-Event
        var elm = $$('#' + divContainer.id + ' .' + this.attributeClass +' .' + this.aClass);
        elm.each(function (element) {
            Event.observe(element, 'click', this.resetOptions.bind(this));
        }.bind(this));

        if (showQty) {
            var elm = $$('#' + 'sm-conf' + '-area_' + this.area + '-row_' + row + '-qty' + ' .' + this.aClass);
            elm.each(function (element) {
                Event.observe(element, 'click', this.updateQty.bind(this));
            }.bind(this));
        }

        if (showTitle) {
            var elm = $$('#' + divContainer.id + ' .' + this.titleClass);
            elm.each(function (element) {
                element.nextSibling.hide();
                Event.observe(element, 'click', this.eToggle.bind(this));
            }.bind(this));
        }

        if (showBigImage) {
            var elm = $$('#' + divContainer.id + ' .sm-hover-color');
            elm.each(function (element) {
                Event.observe(element, 'mouseover', this.configureMouseoverImage.bind(this));
                Event.observe(element, 'mouseout', this.configureMouseoutImage.bind(this));
            }.bind(this));
        }
    },

    resetOptions:function (event) {
        var attrIds = this.attrIds;
        var optionA = $(Event.element(event));
        if (optionA.getAttribute('attr-id') == null) {
            optionA = optionA.parentNode;
        }
        var attrId = optionA.getAttribute('attr-id');
        var position = optionA.getAttribute('position');
        var row = optionA.getAttribute('row');
        var attribute = this.listAttribute[row][attrId];
        var option = attribute.options[position];

        // remove current selected option and class selected
        if (attribute.state.id) {
            attribute.options[attribute.state.position].state = 'enable';
            if ($('sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + attrId + '-option_' + attribute.state.id).classList.contains(this.selectedOption)) {

                $('sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + attrId + '-option_' + attribute.state.id).classList.remove(this.selectedOption);
            }
        }

        // Change state of option and register selected option
        if (option.id != attribute.state.id) {
            option.state = 'selected';
            this.listAttribute[row][attrId].state = {'id':option.id, 'position':position};
            optionA.classList.add(this.selectedOption);
            this.selectedProducts[row].super_attribute[attrId] = {option_id:option.id,option_position:position};
        } else {
            this.listAttribute[row][attrId].state = {'id':null, 'position':null};
            this.selectedProducts[row].super_attribute[attrId] = {};
        }

        // load different attributes
        for (var i = 0; i < attrIds.length; i++) {
            if (attrIds[i] != attrId) {
                var attrReset = this.listAttribute[row][attrIds[i]];
                var optionReset = attrReset.options;
                // get Allowed product
                var allowedProductIds = this.getAllowedProductIds(row, attrIds[i]);
                for (var j = 0; j < optionReset.length; j++) {
                    if (allowedProductIds == 'allow-all') {
                        if (optionReset[j].state != 'selected') {
                            optionReset[j].state = 'enable';
                        }
                        optionReset[j].status = true;
                    } else {
                        var flag = false;
                        var proIds = [];
                        for(var k=0; k < optionReset[j].products.length; k++){
                            if(allowedProductIds.indexOf(optionReset[j].products[k]) > -1){
                                flag = true;
                                proIds.push(optionReset[j].products[k]);
                            }
                        }
                        if (flag == true) {
                            if (optionReset[j].state != 'selected') {
                                optionReset[j].state = 'enable';
                            }
                        } else {
                            optionReset[j].state = 'disable';
                            if (optionReset[j].state == 'selected') {
                                this.listAttribute[row][attrId].state = {'id':null, 'position':null};
                                this.selectedProducts[row].super_attribute[attrId] = {};
                            }
                        }
                        if (proIds.length > 0 && this.checkIsSaleable(proIds)) {
                            optionReset[j].status = true;
                        } else {
                            optionReset[j].status = false;
                        }
                    }
                }

            }
        }

        // remove old optionList
        this.removeOldList(row);

        // add new optionList
        this.addNewOptionList(row, this.listAttribute[row].showLabelAttr, this.listAttribute[row].showLabelOption, this.listAttribute[row].showQty, this.listAttribute[row].showTitle, this.listAttribute[row].showBigImage);

        // Ajax load image and check status
        if (this.listAttribute[row].longAjax) {
            if (JSON.stringify(this.selectedProducts[row].super_attribute[92]) && JSON.stringify(this.selectedProducts[row].super_attribute[92]) != '{}' ) {
                var optColorVal = this.selectedProducts[row].super_attribute[92].option_id;
            } else {
                var optColorVal = null;
            }
            if (JSON.stringify(this.selectedProducts[row].super_attribute[194]) && JSON.stringify(this.selectedProducts[row].super_attribute[194]) != '{}' ) {
                var optSizeVal = this.selectedProducts[row].super_attribute[194].option_id;
            } else {
                var optSizeVal = null;
            }
            this.longAjax(option.id, attrId, this.productId, optColorVal, optSizeVal);
        }
    },

    removeOldList: function(row) {
        var fc = $('sm-conf' + '-area_' + this.area + '-row_' + row + '-container').firstChild;

        while(fc) {
            $('sm-conf' + '-area_' + this.area + '-row_' + row + '-container').removeChild(fc);
            fc = $('sm-conf' + '-area_' + this.area + '-row_' + row + '-container').firstChild;
        }
    },

    addNewOptionList: function(row, showLabelAttr, showLabelOption, showQty, showTitle, showBigImage) {
        var attributes = this.listAttribute[row];
        var divContainer = $('sm-conf' + '-area_' + this.area + '-row_' + row + '-container');

        for (var i = 0; i < this.attrIds.length; i++) {
            var attribute = attributes[this.attrIds[i]];
            var options = attribute.options;

            var divAttribute = document.createElement('div');
            var optionUl = document.createElement('ul');

            //set value
            divAttribute.className = this.attributeClass;
            optionUl.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-ul' + '-attribute_' + this.attrIds[i];
            divAttribute.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i];

            for (var j = 0; j < options.length; j++) {
                if (options[j].state != 'disable') {
                    var optionLi = document.createElement('li');
                    var optionA = document.createElement('a');
                    var optionImg = document.createElement('img');

                    // set value
                    optionLi.className = this.liClass;
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);
                    if (options[j].state == 'selected') {
                        optionA.classList.add(this.selectedOption);
                    }
                    if (!options[j].status) {
                        optionA.classList.add(this.notSaleableOption);
                    }

                    if(attribute.code == 'color') {
                        optionImg.className = this.imgClass;
                        optionImg.src = options[j].image;
                        optionImg.title = options[j].label;
                        optionImg.alt = options[j].label;
                        optionImg.width = '18';
                        optionImg.height = '18';

                        // append
                        optionA.appendChild(optionImg);
                    } else if(attribute.code.toString().indexOf('size')) {
                        optionLi.className = this.liClass + " size-product";
                        optionA.appendChild(document.createTextNode(options[j].label));
                    } else {
                        optionLi.className = this.liClass + " size-product";
                        optionA.appendChild(document.createTextNode(options[j].label));
                    }

                    if (showLabelOption) {
                        var optionSpan = document.createElement('span');
                        var optionLabel = document.createTextNode(options[j].label);
                        optionSpan.className = this.spanClass;
                        optionSpan.appendChild(optionLabel);
                        optionA.appendChild(optionSpan);
                    }

                    if (attribute.code == 'color' && showBigImage) {
                        var optionDiv = document.createElement('div');
                        var optionSpan1 = document.createElement('span');
                        var optionSpan2 = document.createElement('span');
                        optionDiv.className = 'image-color-hover';
                        optionSpan1.className = 'color-name';
                        optionSpan2.className = 'desc';
                        optionSpan1.appendChild(document.createTextNode(options[j].label));
                        optionSpan2.appendChild(document.createTextNode('Click to view photo'));
                        optionDiv.appendChild(optionImg.cloneNode(true));
                        optionDiv.appendChild(optionSpan1);
                        optionDiv.appendChild(optionSpan2);
                        optionDiv.style.display = 'none';
                        optionLi.appendChild(optionDiv);
                        optionA.className += ' sm-hover-color';
                    }

                    optionLi.appendChild(optionA);
                    optionUl.appendChild(optionLi);
                }
            }
            if (showLabelAttr) {
                var labelAttribute = document.createElement('label');
                labelAttribute.appendChild(document.createTextNode(attribute.label));
                divAttribute.appendChild(labelAttribute);
            }
            if (showTitle) {
                var titleSpan = document.createElement('span');
                if (attribute.state.id) {
                    var optionImg = document.createElement('img');
                    var optionSpan = document.createElement('span');
                    var optionLabel = document.createTextNode(options[attribute.state.position].label);
                    optionImg.className = this.imgClass;
                    optionImg.src = options[attribute.state.position].image;
                    optionImg.title = options[attribute.state.position].label;
                    optionImg.alt = options[attribute.state.position].label;
                    optionImg.width = '18';
                    optionImg.height = '18';
                    optionSpan.className = this.spanClass;
                    optionSpan.appendChild(optionLabel);
                    titleSpan.appendChild(optionImg);
                    titleSpan.appendChild(optionSpan);
                } else {
                    var titleLabel = document.createElement('span');
                    titleLabel.appendChild(document.createTextNode(attribute.label));
                    titleSpan.appendChild(titleLabel);
                }
                titleSpan.className = this.titleClass;
                divAttribute.appendChild(titleSpan);
            }
            divAttribute.appendChild(optionUl);
            divContainer.appendChild(divAttribute);
        }

        // Create qty
        if (showQty) {
            var divQty = document.createElement('div');
            var qtyUl = document.createElement('ul');
            qtyUl.className = this.ulClass;
            qtyUl.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-qty';
            for (var i = 0; i < 20; i++) {
                var qtyLi = document.createElement('li');
                var qtyA = document.createElement('a');
                var qtyNumber = document.createTextNode(i + 1);
                qtyA.setAttribute('qty', i + 1);
                qtyA.setAttribute('row', row);
                qtyA.className = this.aClass;
                qtyA.href = 'javascript:void(0)';
                qtyLi.className = this.liClass;
                qtyA.appendChild(qtyNumber);
                qtyLi.appendChild(qtyA);
                qtyUl.appendChild(qtyLi);
            }
            if (showTitle) {
                var titleSpan = document.createElement('span');
                var titleLabel = document.createElement('span');
                titleLabel.appendChild(document.createTextNode(this.selectedProducts[row].qty));
                titleSpan.className = this.titleClass;
                titleSpan.appendChild(titleLabel);
                divQty.appendChild(titleSpan);
            } else {
                var qtySpan = document.createElement('span');
                var qtyText = document.createTextNode('Qty');
                qtySpan.appendChild(qtyText);
                divQty.appendChild(qtySpan);
            }
            divQty.appendChild(qtyUl);
            divQty.className = this.qtyClass;
            divContainer.appendChild(divQty);
        }

        // Register-Event
        this.elmOptions = $$('#' + divContainer.id + ' .' + this.attributeClass +' .' + this.aClass);
        this.elmOptions.each(function (element) {
            Event.observe(element, 'click', this.resetOptions.bind(this));
        }.bind(this));

        if (showQty) {
            var elm = $$('#' + 'sm-conf' + '-area_' + this.area + '-row_' + row + '-qty' + ' .' + this.aClass);
            elm.each(function (element) {
                Event.observe(element, 'click', this.updateQty.bind(this));
            }.bind(this));
        }

        if (showTitle) {
            var elm = $$('#' + divContainer.id + ' .' + this.titleClass);
            elm.each(function (element) {
                element.nextSibling.hide();
                Event.observe(element, 'click', this.eToggle.bind(this));
            }.bind(this));
        }

        if (showBigImage) {
            var elm = $$('#' + divContainer.id + ' .sm-hover-color');
            elm.each(function (element) {
                Event.observe(element, 'mouseover', this.configureMouseoverImage.bind(this));
                Event.observe(element, 'mouseout', this.configureMouseoutImage.bind(this));
            }.bind(this));
        }
    },

    longAjax: function(optId, attrId, proId, optColorVal, optSizeVal) {
        jQuery.ajax({
            url: getUrl()+'notify/index/ajaxSelectOption',
            type: "POST",
            data: {productOption: optId, attributeId: attrId, productId: proId, optColor: optColorVal,optSize: optSizeVal},
            dataType: "json",
            beforeSend: function(){
                jQuery('#imgChangeOption').show();
            },
            success: function(data) {
                jQuery('#imgChangeOption').hide();
                if(data.mediasub){
                    jQuery('#product_media').replaceWith(data.mediasub);
                }
                if(data.priceNew){
                    jQuery('#price-box-detail').replaceWith(data.priceNew);
                }
                if(data.totalCost){
                    jQuery('#total-cost').replaceWith(data.totalCost);
                }
                if(data.stockColor=='outstock'){
                    jQuery('.popup-color-soldout').show();
                }else if(data.stockSize=='outstock'){
                    jQuery('.popup-size-soldout').show();
                }
            }
        });
    },

    updateQty: function (e) {
        var elmA = $(Event.element(e));
        if (elmA.getAttribute('qty')) {
            var qtyNumber = elmA.getAttribute('qty');
            this.selectedProducts[elmA.getAttribute('row')].qty = elmA.getAttribute('qty');
            while (!elmA.classList.contains(this.qtyClass)) {
                if (elmA.classList.contains(this.ulClass)) {
                    elmA.hide();
                }
                elmA = elmA.parentNode;
            }
            var qtySpan = elmA.firstChild;
            while (qtySpan.firstChild) {
                qtySpan.removeChild(qtySpan.firstChild);
            }
            var newSpan = document.createElement('span');
            newSpan.appendChild(document.createTextNode(qtyNumber));
            qtySpan.appendChild(newSpan);
        }
    },

    genOptionId:function (attrId, optionId, area, row) {
        var id = 'sm-attr' + attrId + '-option' + optionId + '-area_' + area + '-row_' + row;
        return id;
    },

    getAllowedProductIds:function (row, attrId) {
        var attrSelected = this.selectedProducts[row].super_attribute;
        var productIds = null;
        var checkAll = true;
        var i = 0;
        for (id in attrSelected) {
            if (id != attrId && JSON.stringify(attrSelected[id]).length !== 2) {
                var allowedProductIds = [];
                checkAll = false;
                var option = this.listAttribute[row][id].options[attrSelected[id].option_position];
                if (i == 0) {
                    productIds = allowedProductIds = option.products;
                } else {
                    for (var j = 0; j < option.products.length; j++) {
                        if (productIds.indexOf(option.products[j]) > -1) {
                            allowedProductIds.push(option.products[j]);
                        }
                    }
                    productIds = allowedProductIds;
                }
                i++;
            }
        }
        if (checkAll) {
            return 'allow-all';
        }
        return allowedProductIds;
    },

    checkIsSaleable: function(ids) {
        for (var i = 0; i < ids.length; i++) {
            if (this.config.childProducts[ids[i]]['outofstock']){
                return true;
            }
        }
        return false;
    },

    convertInfoForAddToCart: function (rows, type) {
        if (this.validate(rows)) {
            var result = [];
            for (var i = 0; i < rows.length; i++) {
                if (rows[i] == 0) {
                    this.selectedProducts[rows[i]].qty = $('qtyproduct-detail').value;
                }
                var data = this.selectedProducts[rows[i]];
                result[rows[i]] = {qty: data.qty, super_attribute: {}};
                for (var j = 0; j < this.attrIds.length; j++) {
                    result[rows[i]].super_attribute[this.attrIds[j]] = data.super_attribute[this.attrIds[j]].option_id;
                }
            }
            if (type == 'json') {
                return JSON.stringify(result);
            }
            return result;
        }
        return false;
    },

    validate: function (rows) {
        var bool = true;
        for (var i = 0; i < rows.length; i++) {
            var data = this.selectedProducts[rows[i]];
            var result = {};
            if (JSON.stringify(data.super_attribute) == '{}') {
                this.addAdvice(rows[i], this.attrIds);
                bool = false;
                continue;
            }

            for (var j = 0; j < this.attrIds.length; j++) {
                if (typeof JSON.stringify(data.super_attribute[this.attrIds[j]]) == 'undefined' || JSON.stringify(data.super_attribute[this.attrIds[j]]) == '{}') {
                    this.addAdvice(rows[i], [this.attrIds[j]]);
                    bool = false;
                }
            }
        }
        return bool;
    },

    addAdvice: function (row, ids) {
        if (this.advice == null) {
            this.advice = '<div class="validation-advice">This is a required field.</div>';
        }

        for (var i = 0; i < ids.length; i++) {
            var elm = $('sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + ids[i]);
            if(!elm.nextSibling||(elm.nextSibling && elm.nextSibling.className!='validation-advice')){
                Element.insert(elm, {'after':this.advice});
            }
        }
    },

    ajaxAddToCart: function (url, redirectUrl) {
        var params = this.convertInfoForAddToCart([0], 'json');
        if (params) {
            jQuery.fancybox.showLoading();
            $('sm-ajax-load').show();
            new Ajax.Request(url, {
                method:'post',
                parameters:{isSmPro: true, sm_super_attribute: params},
                onSuccess:function (transport) {
                    window.location = redirectUrl;
                }.bind(this)
            });
        }
    },

    addToCart: function (id, formName, number, redirect) {
        var rows = [];
        if (!number) {
            rows.push(0);
        } else {
            for (var i = 1; i <= number; i++) {
                rows.push(i);
            }
        }
        if (redirect == 'addtocart') {
            $('redirect_url').value = 'addtocart';
        } else {
            $('redirect_url').value = 'buynow';
        }
        if (this.addValueForInput(id, rows)) {
            if (formName) {
                var productAddToCartForm = new VarienForm(formName);
                if(formName=='comingsoon_form'){
                    jQuery('#submit-popup-comingsoon').click();
                }else if(formName=='colorsoldout'){
                    jQuery('#submit-popup-colorsoldout').click();
                }else if(formName=='sizesoldout'){
                    jQuery('#submit-popup-sizesoldout').click();
                }else{
                    if(redirect == 'addtocart'){
                        this.addToCartAjaxMin(productAddToCartForm);
                    }else{
                        productAddToCartForm.form.submit();
                    }
                }
            }
        }
    },

    addValueForInput: function (id, rows) {
        var data = this.convertInfoForAddToCart(rows, 'json');
        if (data) {
            if ($(id)) {
                $(id).value = data;
                return true;
            }
        }
        return false;
    },

    addMore: function (container) {
        // Create new row
        this.numberRow += 1;
        this.createList(this.numberRow, container, false, true, true, true);
    },

    eToggle: function (e) {
        var title = $(Event.element(e));
        while (!title.classList.contains(this.titleClass)) {
            title = title.parentNode;
        }
        Effect.toggle(title.nextSibling.id, 'appear');
    },

    configureMouseoverImage: function (e) {
        var elm = $(Event.element(e));
        while (!elm.classList.contains(this.aClass)) {
            elm = elm.parentNode;
        }
        elm.previousSibling.show();
    },

    configureMouseoutImage: function (e) {
        var elm = $(Event.element(e));
        while (!elm.classList.contains(this.aClass)) {
            elm = elm.parentNode;
        }
        elm.previousSibling.hide();
    },
    addToCartAjaxMin : function (form) {
    if (form.validator.validate()) {
        var form = form;
        var url = url_add_to_cart;
        var e = null;
        try {
            var data = form.form.serialize(true);
            new Ajax.Request(url, {
                method: 'post',
                parameters : data,
                evalScripts: true,
                onSuccess: function (transport) {
                    try {
                        if (transport.responseText.isJSON()) {
                            var res = transport.responseText.evalJSON();
                            alert(res.msg);
                            if(res.result){
                                window.location.reload();
                            }
                        }
                    }
                    catch (e) {
                        console.log('Error ajax call function');
                    }
                }
            })
        } catch (e) {
        }
    }
}
}
