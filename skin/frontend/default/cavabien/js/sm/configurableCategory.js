/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Varien
 * @package     js
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (typeof smProduct == 'undefined') {
    var smProduct = {};
}

/**************************** CONFIGURABLE PRODUCT **************************/
smProduct.Config = Class.create();
smProduct.Config.prototype = {
    initialize: function(config, id, area){
        this.config     = config;
        this.area = area + id;
        // Ịnit class
        this.containerClass = 'sm-conf-container';
        this.rowClass = 'sm-conf-row';
        this.liClass = 'sm-conf-li';
        this.aClass = 'sm-conf-a';
        this.imgClass = 'sm-conf-img';
        this.spanClass = 'sm-conf-span';
        this.outOfStockOption = 'sm-conf-outofstock';
        this.selectedOption = 'sm-conf-selected';
        this.notSaleableOption = 'sm-conf-not-saleable';
        this.id = id;
        this.idDefault = 'sm-cat-conf-' + this.area + '-default-color';
        this.divColor = 'sm-cat-conf-' + this.area + '-color';
        this.divSize = 'sm-cat-conf-' + this.area + '-size';

        this.idImage = 'image-' + this.area;

        this.attrIds = this.config.attrIds;
        this.listAttribute = [];
        this.selectedProducts = [];
        this.advice = null;
    },

    run: function () {
        if (typeof this.attrIds != 'undefined') {
            this.createDefaultList();
            this.createList(0);
        }
    },

    createDefaultList: function () {
        for (var i = 0; i < this.attrIds.length; i++) {
            if (this.config.attributes[this.attrIds[i]].code == 'color') {
                var optionUl = document.createElement('ul');
                var options = this.config.attributes[this.attrIds[i]].options;
                for (var j = 0; j < options.length; j++) {
                    var optionLi = document.createElement('li');
                    var optionA = document.createElement('a');
                    var optionImg = document.createElement('img');

                    // set value
                    optionLi.className = this.liClass;
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass + ' sm-conf-option-' + this.area;

                    optionImg.className = this.imgClass;
                    optionImg.src = options[j].image;
                    optionImg.title = options[j].label;
                    optionImg.alt = options[j].label;
                    optionImg.width = '18';
                    optionImg.height = '18';
                    // append
                    optionA.appendChild(optionImg);
                    optionLi.appendChild(optionA);
                    optionUl.appendChild(optionLi);
                }
                if($(this.idDefault))
                $(this.idDefault).appendChild(optionUl);
            }
        }
    },

    createList:function (row) {
        this.selectedProducts[row] = {'super_attribute': {}, 'qty':1};
        this.listAttribute[row] = JSON.parse(JSON.stringify(this.config.attributes));

        var attributes = this.listAttribute[row];

        for (var i = 0; i < this.attrIds.length; i++) {
            this.listAttribute[row][this.attrIds[i]].state = {'id':null, 'position':null};
            var attribute = attributes[this.attrIds[i]];
            var options = attribute.options;

            var rowClass = document.createElement('div');
            var optionUl = document.createElement('ul');

            //set value
            rowClass.className = this.rowClass + ' sm-conf-' + this.area;
            rowClass.id = this.rowClass + '-area_' + this.area + '-row_' + row + '-attrid_' + this.attrIds[i];

            for (var j = 0; j < options.length; j++) {
                options[j].state = 'enable';
                options[j].status = true;
                var optionLi = document.createElement('li');
                var optionA = document.createElement('a');
                var optionImg = document.createElement('img');
                var optionLabel = document.createTextNode(options[j].label);

                if (attribute.code == 'color' || (options[j].image != null && options[j].image != "" && options[j].image != "undefined")) {
                    // set value
                    optionLi.className = this.liClass;
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass + ' sm-conf-option-' + this.area;
                    optionA.id = this.genOptionId(this.attrIds[i], options[j].id, this.area, row);
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionImg.className = this.imgClass;
                    optionImg.src = options[j].image;
                    optionImg.title = options[j].label;
                    optionImg.alt = options[j].label;
                    optionImg.width = '18';
                    optionImg.height = '18';

                // append
                //if (attribute.code == 'color') {
                if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('color') > -1) {
                    optionA.appendChild(optionImg);
                } else {
                    optionA.appendChild(optionLabel);
                }

                }
                else if (attribute.code.toString().indexOf('size') > -1) {
                    optionLi.className = this.liClass + " size-product";
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionA.appendChild(document.createTextNode(options[j].label));
                } else {
                    optionLi.className = this.liClass + " size-product";
                    optionA.href = 'javascript:void(0)';
                    optionA.className = this.aClass;
                    optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                    optionA.setAttribute('attr-id', this.attrIds[i]);
                    optionA.setAttribute('position', j);
                    optionA.setAttribute('row', row);

                    optionA.appendChild(document.createTextNode(options[j].label));
                }
                optionLi.appendChild(optionA);
                optionUl.appendChild(optionLi);
            }
            rowClass.appendChild(optionUl);
            //if (attribute.code == 'color') {
            if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('color') > -1) {
                if($(this.divColor))
                $(this.divColor).appendChild(rowClass);
            }
            if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('size') > -1) {
                if($(this.divColor))
                $(this.divSize).appendChild(rowClass);
            }
        }

        // Register-Event
        this.elmOptions = $$('.' + 'sm-conf-' + this.area + ' .sm-conf-option-' + this.area);
        this.elmOptions.each(function (element) {
            Event.observe(element, 'click', this.resetOptions.bind(this));
        }.bind(this));

    },

    resetOptions:function (event) {
        var attrIds = this.attrIds;
        var optionA = $(Event.element(event));
        if (optionA.getAttribute('attr-id') == null) {
            optionA = optionA.parentNode;
        }
        var attrId = optionA.getAttribute('attr-id');
        var position = optionA.getAttribute('position');
        var row = optionA.getAttribute('row');
        var attribute = this.listAttribute[row][attrId];
        var option = attribute.options[position];

        // remove current selected option and class selected
        if (attribute.state.id) {
            attribute.options[attribute.state.position].state = 'enable';
            if ($(this.genOptionId(attrId, attribute.state.id, this.area, row)).classList.contains(this.selectedOption)) {
                $(this.genOptionId(attrId, attribute.state.id, this.area, row)).classList.remove(this.selectedOption);
            }
        }

        // Change state of option and register selected option
        if (option.id != attribute.state.id) {
            option.state = 'selected';
            this.listAttribute[row][attrId].state = {'id':option.id, 'position':position};
            optionA.classList.add(this.selectedOption);
            this.selectedProducts[row].super_attribute[attrId] = {option_id:option.id,option_position:position};
        } else {
            this.listAttribute[row][attrId].state = {'id':null, 'position':null};
            this.selectedProducts[row].super_attribute[attrId] = {};
        }

        // load different attributes
        for (var i = 0; i < attrIds.length; i++) {
            if (attrIds[i] != attrId) {
                var attrReset = this.listAttribute[row][attrIds[i]];
                var optionReset = attrReset.options;
                // get Allowed product
                var allowedProductIds = this.getAllowedProductIds(row, attrIds[i]);
                for (var j = 0; j < optionReset.length; j++) {
                    if (allowedProductIds == 'allow-all') {
                        if (optionReset[j].state != 'selected') {
                            optionReset[j].state = 'enable';
                        }
                        optionReset[j].status = true;
                    } else {
                        var flag = false;
                        var proIds = [];
                        for(var k=0; k < optionReset[j].products.length; k++){
                            if(allowedProductIds.indexOf(optionReset[j].products[k]) > -1){
                                flag = true;
                                proIds.push(optionReset[j].products[k]);
                            }
                        }
                        if (flag == true) {
                            if (optionReset[j].state != 'selected') {
                                optionReset[j].state = 'enable';
                            }
                        } else {
                            optionReset[j].state = 'disable';
                            if (optionReset[j].state == 'selected') {
                                this.listAttribute[row][attrId].state = {'id':null, 'position':null};
                                this.selectedProducts[row].super_attribute[attrId] = {};
                            }
                        }
                        if (proIds.length > 0 && this.checkIsSaleable(proIds)) {
                            optionReset[j].status = true;
                        } else {
                            optionReset[j].status = false;
                        }
                    }
                }

            }
        }

        // update image
        //if (attribute.code == 'color') {
        if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('color') > -1) {
            if (this.config.childProducts[option.products[0]].image){
                this.changeImage(this.config.childProducts[option.products[0]].image);
            }
        }

        // remove old optionList
        this.removeOldList(row);

        // add new optionList
        this.addNewOptionList(row);
    },

    removeOldList: function(row) {
        var fcColor = $(this.divColor).firstChild;
        var fcSize = $(this.divSize).firstChild;

        while(fcColor) {
            $(this.divColor).removeChild(fcColor);
            fcColor = $(this.divColor).firstChild;
        }
        while(fcSize) {
            $(this.divSize).removeChild(fcSize);
            fcSize = $(this.divSize).firstChild;
        }
    },

    addNewOptionList: function(row) {
        var attributes = this.listAttribute[row];

        for (var i = 0; i < this.attrIds.length; i++) {
            var attribute = attributes[this.attrIds[i]];
            var options = attribute.options;

            var rowClass = document.createElement('div');
            var optionUl = document.createElement('ul');

            //set value
            rowClass.className = this.rowClass + ' sm-conf-' + this.area;
            rowClass.id = this.rowClass + '-area_' + this.area + '-row_' + row + '-attrid_' + this.attrIds[i];

            for (var j = 0; j < options.length; j++) {
                if (options[j].state != 'disable') {
                    var optionLi = document.createElement('li');
                    var optionA = document.createElement('a');
                    var optionImg = document.createElement('img');
                    var optionSpan = document.createElement('label');
                    if (attribute.code == 'color' || (options[j].image != null && options[j].image != "" && options[j].image != "undefined")) {
                        // set value
                        optionLi.className = this.liClass;
                        optionA.href = 'javascript:void(0)';
                        optionA.className = this.aClass + ' sm-conf-option-' + this.area;
                        optionA.id = this.genOptionId(this.attrIds[i], options[j].id, this.area, row);
                        optionA.setAttribute('attr-id', this.attrIds[i]);
                        optionA.setAttribute('position', j);
                        optionA.setAttribute('row', row);
                        if (options[j].state == 'selected') {
                            optionA.classList.add(this.selectedOption);
                        }
                        if (!options[j].status) {
                            optionA.classList.add(this.notSaleableOption);
                        }

                        optionImg.className = this.imgClass;
                        optionImg.src = options[j].image;
                        optionImg.title = options[j].label;
                        optionImg.alt = options[j].label;
                        optionImg.width = '18';
                        optionImg.height = '18';

                        optionSpan.className = this.spanClass;

                        // append
                        optionA.appendChild(optionImg);
                    }
                    else if (attribute.code.toString().indexOf('size') > -1) {
                        optionLi.className = this.liClass + " size-product";
                        optionA.href = 'javascript:void(0)';
                        optionA.className = this.aClass;
                        optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                        optionA.setAttribute('attr-id', this.attrIds[i]);
                        optionA.setAttribute('position', j);
                        optionA.setAttribute('row', row);

                        optionA.appendChild(document.createTextNode(options[j].label));
                    } else {
                        optionLi.className = this.liClass + " size-product";
                        optionA.href = 'javascript:void(0)';
                        optionA.className = this.aClass;
                        optionA.id = 'sm-conf' + '-area_' + this.area + '-row_' + row + '-attribute_' + this.attrIds[i] + '-option_' + options[j].id;
                        optionA.setAttribute('attr-id', this.attrIds[i]);
                        optionA.setAttribute('position', j);
                        optionA.setAttribute('row', row);

                        optionA.appendChild(document.createTextNode(options[j].label));
                    }
                    optionLi.appendChild(optionA);
                    optionUl.appendChild(optionLi);
                }
            }
            rowClass.appendChild(optionUl);
            //if (attribute.code == 'color') {
            if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('color') > -1) {
                $(this.divColor).appendChild(rowClass);
            }
            if (attribute.code != "" && attribute.code != null && attribute.code.indexOf('size') > -1) {
                $(this.divSize).appendChild(rowClass);
            }
        }

        // Register-Event
        this.elmOptions = $$('.' + 'sm-conf-' + this.area + ' .sm-conf-option-' + this.area);
        this.elmOptions.each(function (element) {
            Event.observe(element, 'click', this.resetOptions.bind(this));
        }.bind(this));
    },

    genOptionId:function (attrId, optionId, area, row) {
        var id = 'sm-attr' + attrId + '-option' + optionId + '-area_' + area + '-row_' + row;
        return id;
    },

    getAllowedProductIds:function (row, attrId) {
        var attrSelected = this.selectedProducts[row].super_attribute;
        var productIds = null;
        var checkAll = true;
        var i = 0;
        for (id in attrSelected) {
            if (id != attrId && JSON.stringify(attrSelected[id]).length !== 2) {
                var allowedProductIds = [];
                checkAll = false;
                var option = this.listAttribute[row][id].options[attrSelected[id].option_position];
                if (i == 0) {
                    productIds = allowedProductIds = option.products;
                } else {
                    for (var j = 0; j < option.products.length; j++) {
                        if (productIds.indexOf(option.products[j]) > -1) {
                            allowedProductIds.push(option.products[j]);
                        }
                    }
                    productIds = allowedProductIds;
                }
                i++;
            }
        }
        if (checkAll) {
            return 'allow-all';
        }
        return allowedProductIds;
    },

    checkIsSaleable: function(ids) {
        for (var i = 0; i < ids.length; i++) {
            if (this.config.childProducts[ids[i]]['outofstock']){
                return true;
            }
        }
        return false;
    },

    convertInfoForAddToCart: function (rows) {
        if (this.validate(rows)) {
            var result = [];
            for (var i = 0; i < rows.length; i++) {
                var data = this.selectedProducts[rows[i]];
                result[rows[i]] = {qty: data.qty, super_attribute: {}};
                for (var j = 0; j < this.attrIds.length; j++) {
                    result[rows[i]].super_attribute[this.attrIds[j]] = data.super_attribute[this.attrIds[j]].option_id;
                }
            }
            return JSON.stringify(result);
        }
        return false;
    },

    validate: function (rows) {
        var bool = true;
        for (var i = 0; i < rows.length; i++) {
            var data = this.selectedProducts[rows[i]];
            var result = {};
            if (JSON.stringify(data.super_attribute) == '{}') {
                this.addAdvice(rows[i], this.attrIds);
                bool = false;
                continue;
            }

            for (var j = 0; j < this.attrIds.length; j++) {
                if (typeof JSON.stringify(data.super_attribute[this.attrIds[j]]) == 'undefined' || JSON.stringify(data.super_attribute[this.attrIds[j]]) == '{}') {
                    this.addAdvice(rows[i], [this.attrIds[j]]);
                    bool = false;
                }
            }
        }
        return bool;
    },

    addAdvice: function (row, ids) {
        if (this.advice == null) {
            this.advice = '<div class="validation-advice">This is a required field.</div>';
        }

        for (var i = 0; i < ids.length; i++) {
            var elm = $(this.rowClass + '-area_' + this.area + '-row_' + row + '-attrid_' + ids[i]);
            Element.insert(elm, {'after':this.advice});
        }
    },

    addToCart: function (url, redirectUrl) {
        var params = this.convertInfoForAddToCart([0]);
        if (params) {
            jQuery.fancybox.showLoading();
            $('sm-ajax-load').show();
            new Ajax.Request(url, {
                method:'post',
                parameters:{isCat: true, data: params},
                onSuccess:function (transport) {
                    window.location = redirectUrl;
                }.bind(this)
            });
        }
    },

    changeImage: function (url) {
        if ($(this.idImage)){
            $(this.idImage).src = url;
        }
        $(id + this.id)
    }
}
