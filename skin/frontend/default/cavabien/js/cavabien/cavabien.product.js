(function ($) {
    $(document).ready(function(){


        function slideSettings (slideType) {

            var settings1 = {
                slideWidth: 210,
                minSlides: 1,
                maxSlides: 1,
                mode: 'horizontal'
            };
            var settings2 = {
                slideWidth: 180,
                minSlides: 2,
                maxSlides: 2,
//            moveSlides: 1,
                slideMargin: 60,
                mode: 'horizontal'
            };
            var settings3 = {
                slideWidth: 180,
                minSlides: 2,
                maxSlides: 2,
//            moveSlides: 1,
                slideMargin: 60,
                mode: 'horizontal'
            };
            var settings4 = {
							slideWidth: 200,
                minSlides: 3,
                maxSlides: 3,
//            moveSlides: 1,
                slideMargin: 31
            };
            var settings5 = {
                slideWidth: 210,
                minSlides: 4,
                maxSlides: 4,
//            moveSlides: 1,
                slideMargin: 57,
                mode: 'horizontal'
            };
            if(slideType == "wide") // The slide with full wide screen
            {
                settings5 = {
                    slideWidth: 238,
                    minSlides: 5,
                    maxSlides: 5,
//                moveSlides: 1,
                    slideMargin: 30,
                    pager: false,
                    mode: 'horizontal'
                };
                settings4 = {
                    slideWidth: 220,
                    minSlides: 4,
                    maxSlides: 4,
//                moveSlides: 1,
                    slideMargin: 20,
                    pager: false,
                    mode: 'horizontal'
                };
                settings3 = {
                    slideWidth: 220,
                    minSlides: 3,
                    maxSlides: 3,
//                moveSlides: 1,
                    slideMargin: 88,
                    pager: false,
                    mode: 'horizontal'
                };
            }
            if(jQuery(window).width()<1025 && jQuery(window).width() > 959) {
                return settings4;
            } else if(jQuery(window).width() < 960 && jQuery(window).width() > 767 ) {
                return settings3;
            } else if(jQuery(window).width() < 768 && jQuery(window).width() > 439 ) {
                return settings2;
            } else if(jQuery(window).width() < 440) {
                return settings1;
            } else {
                return settings5;
            }
        }

        /**
         * TODO: update "slideWidth" option for all sliders.
         */

        /**
         * Set owl carousel for slider
         * @param elem : class name of slider
         */
        function mySlider(elem){

            var owl = jQuery("." + elem);
            if(owl){
                owl.owlCarousel({
                    items : 5,
                    itemsCustom : false,
                    itemsDesktop : [1199,4],
                    itemsDesktopSmall : [980,4],
                    itemsTablet: [768,3],
                    itemsTabletSmall: false,
                    itemsMobile : [479,1],
                    pagination : false,
                    navigation: true,
                    slideSpeed : 400,
                    paginationSpeed : 800,
                    rewindSpeed : 1000
                });
            }

        }

				function theLookSlider(elem){
					
            var owl = jQuery("." + elem);
            if(owl){
                owl.owlCarousel({
                    items : 3,
                    itemsCustom : false,
                    itemsDesktop : [1199,3],
                    itemsDesktopSmall : [980,3],
                    itemsTablet: [768,2],
                    itemsTabletSmall: false,
                    itemsMobile : [479,1],
                    pagination : false,
                    navigation: true,
                    slideSpeed : 400,
                    paginationSpeed : 800,
                    rewindSpeed : 1000
                });
            }

        }
				
        mySlider('customers-also-bought-slider');
        mySlider('recently-viewed-slider');
        mySlider('style-gallery-slider');
        theLookSlider('tab-the-look-slider');
    });
})(jQuery)

/**
 * Move js from app\design\frontend\default\cavabien\template\catalog\product\view\tabs.phtml.
 * Purpose: avoid repeating call this script for multiple tabs in product detail page
 */
Varien.Tabs = Class.create();
Varien.Tabs.prototype = {
    initialize: function (selector) {
        var self = this;
        $$(selector + ' a').each(this.initTab.bind(this));
        var productTabCollection = $$(selector);
        for (var i = 0; i < productTabCollection.length; i++) {
            this.showContent(productTabCollection[i].down('a'));
        }
    },

    initTab: function (el) {
        el.href = 'javascript:void(0)';
        el.observe('click', this.showContent.bind(this, el));
    },

    showContent: function (a) {
        if (typeof a !== 'undefined') {
            var li = $(a.parentNode), ul = $(li.parentNode);
            if (typeof li !== 'undefined' && typeof ul !== 'undefined') {
                ul.select('li', 'ol').each(function (el) {
                    var contents = $(el.id + '_contents');
                    if (el == li) {
                        el.addClassName('active');
                        contents.show();
                    } else {
                        el.removeClassName('active');
                        contents.hide();
                    }
                });
            }
        }
    }
}


if (typeof Product == 'undefined') {
    var Product = {};
}
Product.Common = Class.create();
//Product.Common.prototype = {
//    initialize: function() {
//        this.changeQuantity();
//    },
//
//    changeQuantity: function() {
//        var subtractQtyElement = $$('.qty-subtract');
//        var addQtyElement = $$('.qty-add');
//
//        subtractQtyElement.each(function (element) {
//            element.stopObserving('click');
//            element.observe('click', function(event){
//                var element = Event.element(event);
//                var qtyWrapper = element.up('.quantity-container');
//                var qty = qtyWrapper.down('.input-text-qty');
//                if(parseInt(qty.getValue()) == 1) {
//                    alert('Quantity must be greater than or equal 1');
//                } else {
//                    qty.setValue(parseInt(qty.getValue()) - 1);
//                }
//            });
//        });
//
//        addQtyElement.each(function (element) {
//            element.stopObserving('click');
//            element.observe('click', function(event){
//                var element = Event.element(event);
//                var qtyWrapper = element.up('.quantity-container');
//                var qty = qtyWrapper.down('.input-text-qty');
//                qty.setValue(parseInt(qty.getValue()) + 1);
//            });
//        });
//    }
//}

jQuery(window).load(function () {
    new Varien.Tabs('.product-tabs');
    new Product.Common();
});

