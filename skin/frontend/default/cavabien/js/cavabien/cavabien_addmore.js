if (typeof Product == 'undefined') {
    var Product = {};
}

Product.Addmore = Class.create();
Product.Addmore.prototype = {
    initialize: function (config) {
        this.config = config;

        // Put events to check select reloads
        this.settings = $$('.addmore-save');
        this.settings.each(function (element) {
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));

        // Init settings dropdown
        var childSettings = [];
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var prevSetting = this.getPrevElement(this.settings[i]) ? this.getPrevElement(this.settings[i]) : false;
            var nextSetting = this.getNextElement(this.settings[i]) ? this.getNextElement(this.settings[i]) : false;
            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevSetting = prevSetting;
            $(this.settings[i]).nextSetting = nextSetting;
            childSettings.push(this.settings[i]);
        }

        var addMoreRowButton = $$('.add-more-option')[0];
        Event.observe(addMoreRowButton, 'click', this.addMoreOptionRow.bind(this));
    },

    getPrevElement: function (currentElement) {
        var prevElement = currentElement.previous('select', 0);
        return prevElement;
    },

    getNextElement: function (currentElement) {
        var nextElement = currentElement.next('select', 0);
        return nextElement;
    },

    configure: function (event) {
        var element = Event.element(event);
        this.configureElement(element);
    },

    configureElement: function (element) {
        if (this.getNextElement(element)) {
            this.fillSelect(element, this.getNextElement(element));
            this.resetChildren(this.getNextElement(element));
        }
    },

    fillSelect: function (prevElement, element) {
        var elementId = element.id;
        var attributeId = elementId.split('_')[1];
        var options = this.getAttributeOptions(attributeId);
        this.clearSelect(element);
        element.options[0] = new Option(this.config.chooseText, '');
        var prevConfig = false;
        if (prevElement) {
            prevConfig = prevElement.options[prevElement.selectedIndex];
            var prevAttributeId = prevElement.id.split('_')[1];
            var prevAllowedProducts = this.getAllowedProducts(prevAttributeId, prevConfig.value);
        }

        if (options) {
            var index = 1;
            for (var i = 0; i < options.length; i++) {
                var allowedProducts = [];
                if (prevAllowedProducts) {
                    allowedProducts = this.getAllowedProducts(attributeId, options[i].id);
                    if (this.checkArrayIntersect(prevAllowedProducts, allowedProducts)) {
                        element.options[index] = new Option(options[i].label, options[i].id);
                        index++;
                    }
                }
            }
        }
    },

    getAttributeOptions: function (attributeId) {
        if (this.config.attributes[attributeId]) {
            return this.config.attributes[attributeId].options;
        }
    },

    getAllowedProducts: function (attributeId, optionId) {
        var allowedProducts = {};
        var attributeOptions = this.getAttributeOptions(attributeId);
        if (attributeOptions) {
            for (var index = 0, length = attributeOptions.length; index < length; index++) {
                if (attributeOptions[index].id == optionId) {
                    allowedProducts = attributeOptions[index].products;
                    return allowedProducts;
                }
            }
        }
    },

    checkArrayIntersect: function (array1, array2) {
        for (var i = 0; i < array1.length; i++) {
            if (array2.indexOf(array1[i]) > -1) {
                return true;
            }
        }
        return false;
    },

    resetChildren: function (element) {
        if (element.childSettings) {
            for (var i = 0; i < element.childSettings.length; i++) {
                element.childSettings[i].selectedIndex = 0;
                element.childSettings[i].disabled = true;
                if (element.config) {
                    this.state[element.config.id] = false;
                }
            }
        }
    },

    clearSelect: function (element) {
        for (var i = element.options.length - 1; i >= 0; i--) {
            element.remove(i);
        }
    },

    addMoreOptionRow: function () {
        var optionWrapper = $$('.option-wrapper')[0];
        var numberCurrentRows = $$('.dropdown-option-item').length;
        var holderDiv = document.createElement('div');
        holderDiv = $(holderDiv); // fix for IE
        holderDiv.addClassName('dropdown-option-item');
        var productId = this.config.productId;
        var attributes = this.config.attributes;
        var counter = 0;
        for (var i in attributes) {
            var newSelect = document.createElement('select');
            var className = 'addmore-save supper_attribute_select_' + productId + '_row' + (numberCurrentRows + 1);
            newSelect.addClassName(className);
            var idElement = 'attribute_' + attributes[i].id + '_' + productId + '_row' + (numberCurrentRows + 1);
            newSelect.id = idElement;
            newSelect.name = 'super_attribute[' + attributes[i].id + '_row' + (numberCurrentRows + 1)+']';
            newSelect.options[0] = new Option(attributes[i].label, '');
            if (counter === 0) {
                var firstAttributeOptions = this.getAttributeOptions(attributes[i].id);
                var index = 1;
                for (var j = 0; j < firstAttributeOptions.length; j++) {
                    newSelect.options[index] = new Option(firstAttributeOptions[j].label, firstAttributeOptions[j].id);
                    index++;
                }
            }

            newSelect.observe('change', this.configure.bind(this));

            holderDiv.appendChild(newSelect);
            counter++;
        }

        var qtyHolderDiv = document.createElement('div');
        qtyHolderDiv = $(qtyHolderDiv); // fix for IE
        qtyHolderDiv.addClassName('quantity-container');
        var qtyHolderDivHtml = '<span class="qty-subtract">-</span>';
        qtyHolderDivHtml += '<input type="text" value="1" title="Qty" class="input-text-qty" name="qty['+this.config.productId+'_row'+(numberCurrentRows + 1)+']" />';
        qtyHolderDivHtml += '<span class="qty-add">+</span>';
        qtyHolderDiv.update(qtyHolderDivHtml);

        holderDiv.appendChild(qtyHolderDiv);
        optionWrapper.appendChild(holderDiv);
        Product.Common.prototype.changeQuantity();
        return false;
    }
}