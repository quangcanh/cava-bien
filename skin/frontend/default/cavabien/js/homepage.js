jQuery(document).ready(function () {

    /*Begin Slider Block Get The Look in Homepage*/

    jQuery('#home-get-the-look-slider').bxSlider({
        slideWidth: 246,
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 1,
        slideMargin: 10
    });


    /*End Slider Block Get The Look in Homepage*/

    /* End Slider Multi Deals Tab */

    jQuery('#homepage-product-tab').tabs();

    // set options bxSlider function
    function slideSettings (slideType) {

        var settings1 = {
            slideWidth: 210,
            minSlides: 1,
            maxSlides: 1
        };
        var settings2 = {
            slideWidth: 180,
            minSlides: 2,
            maxSlides: 2,
//            moveSlides: 1,
            slideMargin: 60
        };
        var settings3 = {
            slideWidth: 180,
            minSlides: 2,
            maxSlides: 2,
//            moveSlides: 1,
            slideMargin: 60
        };
        var settings4 = {
            slideWidth: 200,
            minSlides: 3,
            maxSlides: 3,
//            moveSlides: 1,
            slideMargin: 31
        };
        var settings5 = {
            slideWidth: 230,
            minSlides: 4,
            maxSlides: 4,
//            moveSlides: 1,
            slideMargin: 20
        };
        if(slideType == "wide") // The slide with full wide screen
        {
            settings5 = {
                slideWidth: 238,
                minSlides: 5,
                maxSlides: 5,
//                moveSlides: 1,
                slideMargin: 30,
                pager: false
            };
            settings4 = {
                slideWidth: 220,
                minSlides: 4,
                maxSlides: 4,
//                moveSlides: 1,
                slideMargin: 20,
                pager: false
            };
            settings3 = {
                slideWidth: 220,
                minSlides: 3,
                maxSlides: 3,
//                moveSlides: 1,
                slideMargin: 88,
                pager: false
            };
        }
        if(jQuery(window).width()<1025 && jQuery(window).width() > 959) {
            return settings4;
        } else if(jQuery(window).width() < 960 && jQuery(window).width() > 767 ) {
            return settings3;
        } else if(jQuery(window).width() < 768 && jQuery(window).width() > 439 ) {
            return settings2;
        } else if(jQuery(window).width() < 440) {
            return settings1;
        } else {
            return settings5;
        }
    }

//    var stylistProductCarousel      = jQuery('#stylist-product-carousel').bxSlider(slideSettings("wide"));

//    jQuery(window).resize(function() {
//        if(stylistProductCarousel){
//            stylistProductCarousel.reloadSlider(slideSettings("wide"));
//        }
//    });


    //Ow Carousel Slider

    function mySlider(elem, number, pagination, auto){

        var owl = jQuery("#" + elem);
        if(owl){
            owl.owlCarousel({
                items : number,
                itemsCustom : false,
                itemsDesktop : [1199,number],
                itemsDesktopSmall : [980,number],
                itemsTablet: [768,3],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                pagination : pagination,
                navigation: true,
                paginationNumbers: true,
                slideSpeed : 400,
                paginationSpeed : 800,
                rewindSpeed : 1000,
                auto : 0
            });
        }

    }

    mySlider("homepage-best-selling-carousel", 4, true);
    mySlider("new-arrivals-block-carousel", 4, true);
    mySlider("homepage-multi-deals-carousel", 4, true);
    mySlider("back-in-stock-carousel", 4, true);
    mySlider("stylist-product-carousel", 5, false);
});



