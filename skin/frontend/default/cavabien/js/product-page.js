jQuery(document).ready(function(){
    jQuery('a#attachfile_mesure').click(function(){
        jQuery(this).prev().click();
    })
    jQuery("#attach-measurement").change(function(){
        jQuery('#attachfile_mesure').next().text(jQuery(this).val());
    });
    jQuery('.add-more-save').click(function(){
        jQuery('.addmore-tier').slideToggle();
    })
    jQuery('.qty-add').click(function(){
        var valInput =  jQuery(this).prev('input#qtyproduct-detail').val();
        if(!/^[0-9]+$/.test(valInput)){
            jQuery(this).prev('input#qtyproduct-detail').val(1);
        }else{
            jQuery(this).prev('input#qtyproduct-detail').val(parseInt(jQuery(this).prev('input#qtyproduct-detail').val())+1);
        }
    })
    jQuery('.qty-subtract').click(function(){
        if(parseInt(jQuery(this).next('input#qtyproduct-detail').val())>1){
            jQuery(this).next('input#qtyproduct-detail').val(parseInt(jQuery(this).next('input#qtyproduct-detail').val())-1);
        }else{
            jQuery(this).next('input#qtyproduct-detail').val(1);
        }
    })

//    // Shop the look slider
//    var shopTheLookSliderConfig;
//    if(jQuery(window).width() > 1024) {
//        shopTheLookSliderConfig = {
//            pager: false,
//            minSlides: 3,
//            maxSlides: 3,
//            mode: 'vertical',
//            slideMargin: 5
//        }
//    }
//
//    var shopTheLookSlider;
//    if (jQuery('.shop-the-look-slider').length > 0) {
//        shopTheLookSlider = jQuery('.shop-the-look-slider').bxSlider(shopTheLookSliderConfig);
//        jQuery(window).resize(function() {
//            if(jQuery(window).width() > 1024) {
//                shopTheLookSliderConfig = {
//                    pager: false,
//                    minSlides: 3,
//                    maxSlides: 3,
//                    mode: 'vertical',
//                    slideMargin: 20
//                }
//            }
//            //shopTheLookSlider.reloadSlider(shopTheLookSliderConfig);
//            //customersAlsoBoughtSlider.reloadSlider(slideSettings('wide'));
//        });
//    }
//    jQuery('.shop-the-look-slider').rcarousel({
//        orientation: "vertical"
//    });
//
//    jQuery( "#ui-carousel-next" )
//        .add( "#ui-carousel-prev" );
    jQuery('#shopthelookslider').ulslide({
        width: 120,
        height: 225,
        effect: {
            type: 'carousel', // slide or fade
            axis: 'y',        // x, y
            showCount: 3
        },
        nextButton: '#slide_next',
        prevButton: '#slide_prev',
        duration: 800
        /*autoslide: 2000,*/
    });

    jQuery('#delivery-destination').text(jQuery('#delivery_checked').text());
    jQuery('#delivery-show-options').click(function(){
        jQuery('.delivery-form').toggle();
    })
    jQuery('#delivery-choose').click(function(){
        var priceChosen;
        if(jQuery('input[name="shipping_method"]:checked').parent().next().next().find('span').text()!=''){
            priceChosen = jQuery('input[name="shipping_method"]:checked').parent().next().next().find('span').text();
            priceChosen = parseFloat(priceChosen.replace( /^\D+/g, ''));
        }else{
            priceChosen = 'Free';
        }
        var deliverySpeed =  jQuery('input[name="shipping_method"]:checked').next().text();
        var oldPrice;
        if(jQuery('#price-box-detail .sm-wrap-price .regular-price').length!=0){
            oldPrice = parseFloat(jQuery('#price-box-detail .sm-wrap-price .regular-price').text().trim().substring(1));
        }else{
            oldPrice = parseFloat(jQuery('#price-box-detail .sm-wrap-price .special-price').text().trim().substring(1));
        }
        if(priceChosen=='Free'){
            jQuery('#delivery-select').text(priceChosen+'('+deliverySpeed+')');
            jQuery('#total-cost').text(getCurrencySymbol()+oldPrice);
            jQuery('.delivery-form').hide();
        }else if(priceChosen>0 && priceChosen!=null){
            jQuery('#delivery-select').text(getCurrencySymbol()+priceChosen+'('+deliverySpeed+')');
            jQuery('#total-cost').text(getCurrencySymbol()+(oldPrice+priceChosen));
            jQuery('.delivery-form').hide();
        }else{
            alert('Please choose shipping speed');
        }
    })

    jQuery('input#photo').change(function(){
        if(this.files.length>6 || this.files[0].size/1024/1024>5){
            alert('You may upload a maximum of 6 photos, 5MB max size per image');
            jQuery(this).replaceWith(jQuery(this).val('').clone(true));
        }
    });

    function showHideTooltip(elmClick,elmShowHide){
        jQuery('#'+elmClick).click(function(){
            jQuery('#'+elmShowHide).toggle();
        })
    };
})