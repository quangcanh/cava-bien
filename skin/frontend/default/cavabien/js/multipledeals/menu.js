jQuery(document).ready(function(){
    /* handle url */
    var cat_current = jQuery('#cat_current').val();
    var page_type = jQuery('#page_type').val();

    /* change ahref dynamic */
    var i;
    var url = [];
    var url_horizontal = [];
    for (i = 0; i < 6; i++) {
        url[i] = jQuery('#menu_multipledeals_vertical_'+i+'').attr('href');
        jQuery('#menu_multipledeals_vertical_'+i+'').attr('href', url[i]+'/deals/'+page_type);
        url_horizontal[i] = jQuery('#menu_multipledeals_horizontal_'+i+'').attr('href');
        jQuery('#menu_multipledeals_horizontal_'+i+'').attr('href', url_horizontal[i]+'/deals/'+page_type);
    }

    /* add active */
    jQuery('a[href*="'+cat_current+'/deals/'+page_type+'"]').addClass('active');
    jQuery('a[href*="'+cat_current+'/deals/'+page_type+'"]').parent().addClass('active');

    /*accordion menu*/

    var plus = jQuery('<span class="deals_plus_menu"><a href="javascript:void(0)">+  </a></span>');
    var subtract = jQuery('<span class="deals_subtract_menu"><a href="javascript:void(0)">-  </a></span>');

    jQuery('.click_to_slide').html(plus);

    jQuery('.menu_static_header_accordion').parent().next().hide();
    jQuery('.menu_static_header_accordion').each(function(){
        if(jQuery(this).children().hasClass('active')){
            jQuery(this).parent().next().show();
            jQuery(this).find('.click_to_slide').html(subtract);
        }
    });

      //check if click then hasClass
       jQuery('.click_to_slide').click(function(){
             if(jQuery(this).children().hasClass('deals_plus_menu')){ 
                 jQuery('.menu_static_header_accordion').parent().next().hide();
                 jQuery(this).closest('h3').next().show();
                 jQuery('.click_to_slide').html(plus);
                 jQuery(this).html(subtract);
             }
             else{
               jQuery('.menu_static_header_accordion').parent().next().hide();
               jQuery('.click_to_slide').html(plus);
             }

        });


});

