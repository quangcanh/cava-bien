jQuery(document).ready(function(){
    jQuery('.bxslider').bxSlider();
    //jQuery(".toolbar select").uniform();
	function accordion() {
        jQuery(".block dl dt").addClass("show");
        jQuery(".block dl dt").click(function(){
			var dtClass = jQuery(".block dl dt").attr("class");
			if(jQuery(this).hasClass("show")) {
				jQuery(this).siblings().slideUp();
				jQuery(this).removeClass("show");
			} else {
				jQuery(this).siblings().slideDown();
				jQuery(this).addClass("show");
			}
		})
	};
	//accordion();
    jQuery('.filter-content-toggle').on('click', function(){
        var $id = '#advancednavigation-filter-content-' + jQuery(this).data('id');
        var $ddToggle = jQuery(this).parent().find($id);
        $ddToggle.toggle();
    });

    jQuery('.filter-content-cancel').on('click', function(){
        var $ddToggle = jQuery(this).parent();
        $ddToggle.toggle();
    });
    jQuery('.slide-banner').bxSlider({controls: false});
    jQuery('.top-cate-banner').bxSlider({
        pagerCustom: '#top-pager',
        controls: false


    });
    //hover on color swatch
    jQuery("#amconf-block .amconf-image-container img").mouseover(function(){
        jQuery(this).trigger('click');
//        jQuery(this).removeClass('amconf-image-selected');
    }).mouseout(function(){
        jQuery(this).removeClass('amconf-image-selected');
    });
});
