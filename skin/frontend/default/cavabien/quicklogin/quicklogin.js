var checkurl = URL + "customer/account/signupformpopup/";
/* URL is define in header.phtml */

var showPass = true;
function showForgotSection(it, box) {
    var vis = showPass ? "block" : "none";
    document.getElementById(it).style.display = vis;
    showPass = !showPass;
}
if ($('alogin')) {
    $('alogin').onclick = function () {
        TINY.box.show({url: checkurl, width: 620, height: 100, opacity: 20, topsplit: 10});

    }
}
else if ($('aclose')) {
    $('aclose').onclick = function () {
        TINY.box.alpha(p, -1, 0, 3)
    }
}

/*Ajax Login Function */
function loginAjax() {

    var valid = new Validation('login-form');
    if (valid.validate()) {
        var request = new Ajax.Request(
            URL + "customer/account/ajaxLogin",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        alert('Successfully Loggedin');
                        redirectTime = "1";
                        redirectURL = URL;
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            alert(response.message);
                        }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('login-form')
            }
        );
    } else {

        return false;
    }

}

/*Ajax Login Function for other form */
function loginAjax2() {
    jQuery('#login-form-2 .signup-loader').css('display', 'block');
    jQuery('#login-form-2 #send2').css('display', 'none');
    var valid = new Validation('login-form-2');
    if (valid.validate()) {
        var request = new Ajax.Request(
            URL + "customer/account/ajaxLogin",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        //alert('Successfully Loggedin');
                        redirectTime = "1";
                        redirectURL = URL;
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            alert(response.message);
                        }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('login-form-2')
            }
        );
    } else {
        jQuery('#login-form-2 .signup-loader').css('display', 'none');
        jQuery('#login-form-2 #send2').css('display', 'block');
        return false;
    }

}
/*Ajax Login Function for other form */
function loginAjax3() {
    jQuery('#login-form-3 .signup-loader').css('display', 'block');
    jQuery('#login-form-3 #send3').css('display', 'none');
    var valid = new Validation('login-form-3');

    if (valid.validate()) {

        var request = new Ajax.Request(
            URL + "customer/account/ajaxLogin",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        //alert('Successfully Loggedin');
                        redirectTime = "1";
                        redirectURL = URL;
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            jQuery('#login-form-3 .signup-loader').css('display', 'none');
                            jQuery('#login-form-3 #send3').css('display', 'block');
                            alert(response.message);
                         }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('login-form-3')
            }
        );
    } else {
        jQuery('#login-form-3 .signup-loader').css('display', 'none');
        jQuery('#login-form-3 #send3').css('display', 'block');
        return false;
    }

}
/*Ajax Register Customer Function */
function registerAjax() {

    var valid = new Validation('regis-form');
    if (valid.validate()) {
        var request = new Ajax.Request(
            URL + "customer/account/ajaxCreate",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        alert('Successfully Registered');
                        redirectTime = "1";
                        redirectURL = URL;
                        //redirectURL = URL + "customer/account";
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            alert(response.message);
                        }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('regis-form')
            }
        );
    } else {

        return false;
    }

}

/*Ajax Register Customer Function */
function registerAjax2() {
    jQuery('#regis-form-2 .signup-loader').css('display', 'block');
    jQuery('#regis-form-2 #create-account').css('display', 'none');
    regemail = jQuery('#regis-form-2 #email_address').val();
    regemail_arr = new Array();
    if (regemail != "" && regemail != null) {
        regemail_arr = regemail.split('@');
        jQuery('#regis-form-2 #firstname').val(regemail_arr[0]);
        jQuery('#regis-form-2 #lastname').val(regemail_arr[0]);
        jQuery('#regis-form-2 #confirmation').val(jQuery('#regis-form-2 #password').val());
    }
    var valid = new Validation('regis-form-2');
    if (valid.validate()) {
        var request = new Ajax.Request(
            URL + "customer/account/ajaxCreate",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        //alert('Successfully Registered');
                        redirectTime = "1";
                        redirectURL = URL;
                        //redirectURL = URL + "customer/account";
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            jQuery('#regis-form-2 .signup-loader').css('display', 'none');
                            jQuery('#regis-form-2 #create-account').css('display', 'block');
                            alert(response.message);
                        }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('regis-form-2')
            }
        );
    } else {
        jQuery('#regis-form-2 .signup-loader').css('display', 'none');
        jQuery('#regis-form-2 #create-account').css('display', 'block');
        return false;
    }

}

/*Ajax Register Customer Function */
function registerAjax3() {
    jQuery('#regis-form-3 .signup-loader').css('display', 'block');
    jQuery('#regis-form-3 #create-account').css('display', 'none');
    regemail = jQuery('#regis-form-3 #email_address').val();
    regemail_arr = new Array();
    if (regemail != "" && regemail != null) {
        regemail_arr = regemail.split('@');
        jQuery('#regis-form-3 #firstname').val(regemail_arr[0]);
        jQuery('#regis-form-3 #lastname').val(regemail_arr[0]);
        jQuery('#regis-form-3 #confirmation').val(jQuery('#regis-form-3 #password').val());
    }
    var valid = new Validation('regis-form-3');
    if (valid.validate()) {
        var request = new Ajax.Request(
            URL + "customer/account/ajaxCreate",
            {
                method: 'post',
                onComplete: function () {

                },
                onSuccess: function (transport) {
                    if (transport && transport.responseText) {
                        try {
                            response = eval('(' + transport.responseText + ')');
                        }
                        catch (e) {
                            response = {};
                        }
                    }

                    if (response.success) {
                        //alert('Successfully Registered');
                        redirectTime = "1";
                        redirectURL = URL;
                        //redirectURL = URL + "customer/account";
                        setTimeout("location.href = redirectURL;", redirectTime);
                    } else {
                        if ((typeof response.message) == 'string') {
                            jQuery('#regis-form-3 .signup-loader').css('display', 'none');
                            jQuery('#regis-form-3 #create-account').css('display', 'block');
                            alert(response.message);
                        }
                        return false;
                    }
                },
                onFailure: function () {
                    alert("Failed");
                },
                parameters: Form.serialize('regis-form-3')
            }
        );
    } else {
        jQuery('#regis-form-3 .signup-loader').css('display', 'none');
        jQuery('#regis-form-3 #create-account').css('display', 'block');
        return false;
    }

}
/*Forget Password Function */
function forgetpass() {
    var req2 = new Ajax.Request(URL + "customer/account/ajaxForgotPassword/",
        {
            method: 'post',
            parameters: $('forgot-form').serialize(true),
            onSuccess: function (transport) {
                var response = eval('(' + transport.responseText + ')');
                if (response.success) {
                    alert(response.message);
                    TINY.box.hide();


                } else {
                    alert(response.message);
                }
            },
            onFailure: function () {
                alert('Something went wrong...')
            }
        });

}
/*Forget Password Function */
function forgetpass2() {
    var req2 = new Ajax.Request(URL + "customer/account/ajaxForgotPassword/",
        {
            method: 'post',
            parameters: $('forgot-form-2').serialize(true),
            onSuccess: function (transport) {
                var response = eval('(' + transport.responseText + ')');
                if (response.success) {
                    alert(response.message);
                    TINY.box.hide();


                } else {
                    alert(response.message);
                }
            },
            onFailure: function () {
                alert('Something went wrong...')
            }
        });

}
/*Forget Password Function */
function forgetpass3() {
    var req3 = new Ajax.Request(URL + "customer/account/ajaxForgotPassword/",
        {
            method: 'post',
            parameters: $('forgot-form-3').serialize(true),
            onSuccess: function (transport) {
                var response = eval('(' + transport.responseText + ')');
                if (response.success) {
                    alert(response.message);
                    TINY.box.hide();


                } else {
                    alert(response.message);
                }
            },
            onFailure: function () {
                alert('Something went wrong...')
            }
        });

}