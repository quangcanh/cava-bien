<?php

class Pensio_Payment_Block_Onepage_Failed extends Mage_Checkout_Block_Onepage_Abstract {

	protected function _construct() {
		parent::_construct();
	}

	public function getSessionPensioPaymentRedirectUrl()
	{
		return Mage::getSingleton('core/session')->getData('pensio_payment_request_url');
	}

	public function getErrorMessage()
	{
		return Mage::getSingleton('core/session')->getData('pensio_error_message');
	}

	public function getPaymentSchemeName()
	{
		return Mage::getSingleton('core/session')->getData('pensio_payment_scheme_name');
	}
}