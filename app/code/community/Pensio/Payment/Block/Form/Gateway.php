<?php

class Pensio_Payment_Block_Form_Gateway extends Mage_Payment_Block_Form_Cc
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('pensio/payment/form/gateway.phtml');
    }
    

}