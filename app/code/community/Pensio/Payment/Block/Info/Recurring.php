<?php

class Pensio_Payment_Block_Info_Recurring extends Mage_Payment_Block_Info
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/recurring.phtml');
    }    
    
}