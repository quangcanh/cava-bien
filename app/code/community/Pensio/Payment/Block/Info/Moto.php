<?php

class Pensio_Payment_Block_Info_Moto extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/moto.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/moto.phtml');
		return $this->toHtml();
    }
}