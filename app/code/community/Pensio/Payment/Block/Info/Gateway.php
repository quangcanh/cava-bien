<?php

class Pensio_Payment_Block_Info_Gateway extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/gateway.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/gateway.phtml');
		return $this->toHtml();
    }
    
    
}