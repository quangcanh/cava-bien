<?php

class Pensio_Payment_Block_Info_Gateway4 extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/gateway4.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/gateway4.phtml');
		return $this->toHtml();
    }
    
    
}
