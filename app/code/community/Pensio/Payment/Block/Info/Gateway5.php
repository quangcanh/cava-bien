<?php

class Pensio_Payment_Block_Info_Gateway5 extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/gateway5.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/gateway5.phtml');
		return $this->toHtml();
    }
    
    
}
