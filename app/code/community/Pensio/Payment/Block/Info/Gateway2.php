<?php

class Pensio_Payment_Block_Info_Gateway2 extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/gateway2.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/gateway2.phtml');
		return $this->toHtml();
    }
    
    
}
