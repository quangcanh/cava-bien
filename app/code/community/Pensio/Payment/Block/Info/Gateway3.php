<?php

class Pensio_Payment_Block_Info_Gateway3 extends Mage_Payment_Block_Info_Cc
{
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('pensio/payment/info/gateway3.phtml');
    }

    public function toPdf()
    {
        $this->setTemplate('pensio/payment/pdf/gateway3.phtml');
		return $this->toHtml();
    }
    
    
}
