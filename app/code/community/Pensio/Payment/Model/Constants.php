<?php

class Pensio_Payment_Model_Constants {
	// configuration paths
	const CONF_PATH_API_INSTALLATION = 'pensio_general/api_installation';
	const CONF_PATH_API_USERNAME     = 'pensio_general/api_username';
	const CONF_PATH_API_PASSWORD     = 'pensio_general/api_password';
	
	const CONF_PATH_MOTO_TERMINAL    = 'payment/pensio_moto/terminal';
	
	const CONF_PATH_GATEWAY_TERMINAL    = 'payment/pensio_gateway/terminal';
	
	const CONF_PATH_GATEWAY2_TERMINAL =  'payment/pensio_gateway2/terminal';

	const CONF_PATH_GATEWAY3_TERMINAL =  'payment/pensio_gateway3/terminal';
	
	const CONF_PATH_GATEWAY4_TERMINAL =  'payment/pensio_gateway4/terminal';
	
	const CONF_PATH_GATEWAY5_TERMINAL =  'payment/pensio_gateway5/terminal';
	
	// payment types
	const ACTION_AUTHORIZE         = 'payment';
	const ACTION_AUTHORIZE_CAPTURE = 'paymentAndCapture';
	const ACTION_RECURRING         = 'recurring';
}
