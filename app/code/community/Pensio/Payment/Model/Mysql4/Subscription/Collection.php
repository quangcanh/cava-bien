<?php
class Pensio_Payment_Model_Mysql4_Subscription_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Initialize resources
     *
     */
    public function _construct()
    {
		$this->_init('pensiopayment/subscription');
    }

	public function addAttributeToSort($attribute, $dir='asc')
    {
        $this->addOrder($attribute, $dir);
        return $this;
    }

}