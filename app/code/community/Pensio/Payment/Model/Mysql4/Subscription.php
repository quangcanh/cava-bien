<?php
/**
 * @author emanuel
 *
 */
class Pensio_Payment_Model_Mysql4_Subscription extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct()
	{
		$this->_init('pensiopayment/subscription', 'id');
	}
}