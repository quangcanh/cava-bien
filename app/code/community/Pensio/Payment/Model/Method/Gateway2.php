<?php

class Pensio_Payment_Model_Method_Gateway2 extends Pensio_Payment_Model_Method_Gateway {
	
	protected $_code = 'pensio_gateway2';
    protected $_formBlockType = 'pensiopayment/form_gateway';

	protected function getPensioTerminal()
	{
		return Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_GATEWAY2_TERMINAL);
	}
}
