<?php

class Pensio_Payment_Model_Method_Gateway5 extends Pensio_Payment_Model_Method_Gateway {
	
	protected $_code = 'pensio_gateway5';
    protected $_formBlockType = 'pensiopayment/form_gateway';

	protected function getPensioTerminal()
	{
		return Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_GATEWAY5_TERMINAL);
	}
}
