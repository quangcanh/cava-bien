<?php

class Pensio_Payment_Model_Method_Gateway3 extends Pensio_Payment_Model_Method_Gateway {
	
	protected $_code = 'pensio_gateway3';
    protected $_formBlockType = 'pensiopayment/form_gateway3';

	protected function getPensioTerminal()
	{
		return Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_GATEWAY3_TERMINAL);
	}
}
