<?php

require_once(dirname(__FILE__).'/../Api/PensioMerchantAPI.class.php');

abstract class Pensio_Payment_Model_Method_Abstract extends Mage_Payment_Model_Method_Abstract {
	
	protected function getPensioModel(Varien_Object $payment = null)
	{
		return new Pensio_Payment_Model_Pensio($this->getStoreId($payment));
	}
	
	protected function getStoreId(Varien_Object $payment = null)
	{
		if(is_null($payment))
		{
			return Pensio_Payment_Helper_Utilities::guessStoreIdBasedOnParameters();
		}
		else
		{
			return $payment->getOrder()->getStore()->getId();
		}
	}
	
	protected function getPensioPaymentType($configPath, $storeId = null)
	{
		$type = Mage::getStoreConfig($configPath, $storeId);
		if($type == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE){
			return Pensio_Payment_Model_Constants::ACTION_AUTHORIZE;
		}else{
			return Pensio_Payment_Model_Constants::ACTION_AUTHORIZE_CAPTURE;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see code/core/Mage/Payment/Model/Method/Mage_Payment_Model_Method_Abstract#capture()
	 */
	public function capture(Varien_Object $payment, $amount) {
		$session_authTransactionId = Mage::getSingleton('core/session')->getData('pensio_auth_transaction_id');
		Mage::getSingleton('core/session')->unsetData('pensio_auth_transaction_id');
		$session_requiresCapture = Mage::getSingleton('core/session')->getData('pensio_requires_capture');
		Mage::getSingleton('core/session')->unsetData('pensio_requires_capture');

		if ($session_requiresCapture == 'false')
		{
			// payment has already been captured by Pensio
			return $this;
		}

		/* @var $payment Mage_Sales_Model_Order_Payment */
		$invoice = $this->_getInvoice($payment);

		$invoiceData = $invoice->__toArray();
		$salesTax = number_format($invoiceData['tax_amount'], 2,'.','');
		$amount = number_format($invoiceData['grand_total'], 2,'.',''); // To get the amount in the correct currency
		
		$orderLines = $this->_createOrderLinesFromInvoice($invoice);

		$authTrans = $payment->getAuthorizationTransaction();

		if (empty($authTrans))
		{
			/**
			 * We hit this case when our OnepageController->successAction method is run. During the
			 * execution the quote is stored, but while saving, this method is called and that means
			 * that we will never find the authorization transaction (because it has not been saved yet).
			 */
			$doCapture = true;
			$transaction_id = $session_authTransactionId;
		}
		else
		{
			$transaction_id = $authTrans->getTxnId();
			$authResponse = @unserialize($authTrans->getAdditionalInformation('pensio_response'));
			$doCapture = $authResponse === false || $authResponse->getPrimaryPayment()->mustBeCaptured();
		}
		
		// Find out if we need to capture this payment
		if($doCapture)
		{
			$response = $this->getPensioModel($payment)->captureReservation($transaction_id, $amount, $orderLines, $salesTax);
			$payment->setTransactionAdditionalInfo('pensio_response', serialize($response));
	
			if($response->wasSuccessful())
			{
				$payment->setIsTransactionClosed(false);
				$payment->setTransactionId($response->getPrimaryPayment()->getLastReconciliationIdentifier()->getId());
				
				return $this;
			}
			else
			{
				Mage::throwException($response->getMerchantErrorMessage());
			}
		}
		else
		{
			$payment->setIsTransactionClosed(false);
			$payment->setTransactionId($authTrans->getTxnId().'-captured');
			
			return $this;
		}
	}

	protected function _createOrderLinesFromInvoice(Mage_Sales_Model_Order_Invoice $invoice)
	{
		$orderLines = array();
		foreach($invoice->getAllItems() as $item) /* @var $item Mage_Sales_Model_Order_Invoice_Item */
		{
			$data = $item->__toArray();

			$orderLines[] = array(
				'description'=>$data['name'],
				'itemId'=>$data['sku'],
				'quantity'=>$data['qty'],
				'taxAmount'=>number_format(bcsub($data['price_incl_tax'], $data['price'], 2), 2, '.', ''),
				'unitCode'=>'pcs', // TODO: Nice this up
				'unitPrice'=>round($data['price'], 2, PHP_ROUND_HALF_DOWN),
				'discount'=>0, // There is no such thing on each row, only a total on the invoice....
				'goodsType'=>'item',
			);
		}

		$shipping = $invoice->__toArray();
		if($shipping['shipping_amount'] > 0)
		{
			$orderLines[] = array(
				'description'=>$invoice->getOrder()->getData('shipping_description'),
				'itemId'=>$invoice->getOrder()->getData('shipping_method'),
				'quantity'=>1,
				'taxAmount'=>0,
				'unitCode'=>'pcs', // TODO: Nice this up
				'unitPrice'=>$invoice->getData('shipping_amount'),
				'discount'=>0,
				'goodsType'=>'shipment',
			);
		}
		return $orderLines;
	}


	/**
	 * This will dig out the invoice that we are paying for right now As Magento does not tell us we have
	 * to investigate the invoices and make a qualified guess (as this is the best we can do). 
	 * 
	 * @return Mage_Sales_Model_Order_Invoice
	 */
	private function _getInvoice(Mage_Sales_Model_Order_Payment $payment)
	{
		$invoice = Pensio_Payment_Model_Observer::getInvoiceBeingPayedFor();
		if(is_null($invoice))
		{
			Mage::throwException("Could not find the invoice for which the payment is being made");
		}
		return $invoice;
	}

	/**
	 * (non-PHPdoc)
	 * @see code/core/Mage/Payment/Model/Method/Mage_Payment_Model_Method_Abstract#refund()
	 */
	public function refund(Varien_Object $payment, $amount) {
		/* @var $payment Mage_Sales_Model_Order_Payment */
		$creditmemo = $payment->getCreditmemo();
		
		$creditmemoData = $creditmemo->__toArray();
		$amount = number_format($creditmemoData['grand_total'], 2,'.',''); // To get the amount in the correct currency

		/**
		 * @var $collection Mage_Sales_Model_Mysql4_Order_Payment_Transaction_Collection
		 *
		 * $payment->getAuthorizationTransaction() returns the capture transaction
		 * and if we use the transaction id from that, the refund fails in magento
		 */
		$collection = Mage::getModel('sales/order_payment_transaction')->getCollection()
			->setOrderFilter($payment->getOrder())
			->addPaymentIdFilter($payment->getId())
			->addTxnTypeFilter(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);

		if (count($collection) < 1)
		{
			throw Mage::exception('Pensio - refund', 'I was unable to find any authorization transactions for order '.$payment->getOrder()->getId());
		}

		if (count($collection) > 1)
		{
			throw Mage::exception('Pensio - refund', 'I found multiple authorization transactions for order '.$payment->getOrder()->getId().'. I do not know which one to use.');
		}

		$authTrans = $collection->getFirstItem();

		$transaction_id = $authTrans->getTxnId();

		$response = $this->getPensioModel($payment)->refundCapturedReservation($transaction_id, $amount);
		$payment->setTransactionAdditionalInfo('pensio_response', serialize($response));
		
		if($response->wasSuccessful())
		{
			$payment->setShouldCloseParentTransaction(false);
			$payment->setTransactionId($response->getPrimaryPayment()->getLastReconciliationIdentifier()->getId());
			
			return $this;
		}
		else
		{
			Mage::throwException($response->getMerchantErrorMessage());
		}
	}

	/**
	 * This will dig out the creditmemo that we are refunding for right now As Magento does not tell us we have
	 * to investigate the credit memos and make a qualified guess (as this is the best we can do).
	 *
	 * @return Mage_Sales_Model_Order_Creditmemo
	 */
	private function _getCreditmemo(Mage_Sales_Model_Order_Payment $payment)
	{
		$creditmemo = Pensio_Payment_Model_Observer::getCreditmemoBeingRefunded();
		if(is_null($creditmemo))
		{
			Mage::throwException("Could not find the creditmemo for which the refund is being made");
		}
		return $creditmemo;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see code/core/Mage/Payment/Model/Method/Mage_Payment_Model_Method_Abstract#void()
	 */
    public function void(Varien_Object $payment)
    {
		$authTrans = $payment->getAuthorizationTransaction();
		$transaction_id = $authTrans->getTxnId();

		$response = $this->getPensioModel($payment)->releaseReservation($transaction_id);
		$payment->setTransactionAdditionalInfo('pensio_response', serialize($response));
		
		$payment->setIsTransactionClosed(false);
		$payment->setTransactionId($transaction_id.'-void');

		if($response->wasSuccessful())
		{
			return $this;
		}
		else
		{
			Mage::throwException($response->getMerchantErrorMessage());
		}
    }
}