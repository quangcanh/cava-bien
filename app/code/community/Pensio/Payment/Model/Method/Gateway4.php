<?php

class Pensio_Payment_Model_Method_Gateway4 extends Pensio_Payment_Model_Method_Gateway {
	
	protected $_code = 'pensio_gateway4';
    protected $_formBlockType = 'pensiopayment/form_gateway4';

	protected function getPensioTerminal()
	{
		return Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_GATEWAY4_TERMINAL);
	}
}
