<?php
/**
 *
 * Payment Action Dropdown source
 *
 * @author Emanuel Holm Greisen <eg@pensio.com>
 */
class Pensio_Payment_Model_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE,
                'label' => Mage::helper('pensiopayment')->__('Authorize Only')
            ),
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('pensiopayment')->__('Authorize and Capture')
            ),
        );
    }
}
