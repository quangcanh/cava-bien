<?php

require_once(dirname(__FILE__).'/Api/PensioMerchantAPI.class.php');

class Pensio_Payment_Model_Pensio {

	/**
	 * @var PensioMerchantAPI
	 */
	private $_merchantApi;
	private $_merchantApiLogger;
	private $_cachedTerminalList = null;
	private $_error_ = array();
	protected $_varien_response = null;
	private $_canSavePayment = false;

	/* PUBLIC METHODS */

	private $storeId;
	
	public function __construct($storeId=null) 
	{
		$this->storeId = $storeId;
	}
	
	private function setupApi()
	{
		if(is_null($this->_merchantApi))
		{
			Mage::log('Create Pensio Merchant API for store: '.$this->storeId, Zend_Log::INFO, 'pensio.log', true);

			$this->_merchantApiLogger = new Pensio_Payment_Helper_Logger();
			$this->_merchantApi = new PensioMerchantAPI(
				Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_API_INSTALLATION,$this->storeId),
				Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_API_USERNAME,$this->storeId),
				Mage::helper('core')->decrypt(Mage::getStoreConfig(Pensio_Payment_Model_Constants::CONF_PATH_API_PASSWORD,$this->storeId)),
				$this->_merchantApiLogger
			);
		}
	}

	public function getActionType($type) {
		if ($type == Pensio_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
			return Pensio_Payment_Model_Constants::ACTION_AUTHORIZE_CAPTURE;
		} else {
			return Pensio_Payment_Model_Constants::ACTION_AUTHORIZE;
		}
	}
	
	public function getURI() {
		return $this->_uri;
	}

	public function getURIMode() {
		return $this->_mode;
	}

	public function setUriMode($mode) {
		$this->_mode = $mode;
	}

	public function setMethod($pensio_payment_method) {
		$this->_method = $pensio_payment_method;
	}

	public function getMethod() {
		return $this->_method;
	}
	 
	public function getInstallation() {
		return $this->_installation;
	}
	 
	public function getUserName() {
		return $this->_user_name;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function getErrorMessage() {
		if (isset($this->_error_['message'])) {
			return $this->_error_['message'];
		} else {
			return false;
		}
	}

	public function getErrorType() {
		if (isset($this->_error_['type'])) {
			return $this->_error_['type'];
		} else {
			return false;
		}
	}

	public function getPensioMerchantAPI() {
		$this->setupApi();
		if (!$this->_merchantApi->isConnected()) {
			$this->_merchantApi->login();
		}
		return $this->_merchantApi;
	}
	 
	public function authenticate() {
		return $this->getPensioMerchantAPI()->login();
	}
	 
	public function createPaymentRequest(
		$terminal,
		$orderid,
		$amount,
		$currencyCode,
		$paymentType,
		array $customerInfo=array(),
		$cookie=null,
		$language=null,
		array $config = array(),
		array $transactionInfo = array(),
		array $orderLines = array())
	{
		return $this->getPensioMerchantAPI()->createPaymentRequest(
			  $terminal
			, $orderid
			, $amount
			, $currencyCode
			, $paymentType
			, $customerInfo
			, $cookie
			, $language
			, $config
			, $transactionInfo
			, $orderLines);
	}

	/**
	 * This will capture using the Pensio API.
	 * TODO: Support order_lines for invoices
	 * 
	 * @return PensioCaptureResponse
	 */
	public function captureReservation($paymentId, $amount, $orderLines, $salesTax)
	{
		return $this->getPensioMerchantAPI()->captureReservation($paymentId, $amount, $orderLines, $salesTax);
	}
	
	/**
	 * This will capture using the Pensio API.
	 * TODO: Support order_lines for invoices
	 * 
	 * @return PensioReleaseResponse
	 */
	public function releaseReservation($paymentId, $amount)
	{
		return $this->getPensioMerchantAPI()->releaseReservation($paymentId, $amount);
	}
	

	/**
	 * This will capture using the Pensio API.
	 * TODO: Support order_lines for invoices
	 * 
	 * @return PensioRefundResponse
	 */
	public function refundCapturedReservation($paymentId, $amount) {
		return $this->getPensioMerchantAPI()->refundCapturedReservation($paymentId, $amount);
	}

	/**
	 * @return PensioGetTerminalsResponse
	 */
	public function getTerminals() {
		if(is_null($this->_cachedTerminalList))
		{
			$this->_cachedTerminalList = $this->getPensioMerchantAPI()->getTerminals();
		}
		return $this->_cachedTerminalList;
	}

	/**
	 * @return PensioReservationResponse
	 */
	public function reservationOfFixedAmountMOTO(
		  $terminal
		, $shop_orderid
		, $amount
		, $currency
		, $cc_num
		, $cc_expiry_year
		, $cc_expiry_month
		, $cvc) {
		return $this->getPensioMerchantAPI()->reservationOfFixedAmount(
			  $terminal
			, $shop_orderid
			, $amount
			, $currency
			, $cc_num
			, $cc_expiry_year
			, $cc_expiry_month
			, $cvc
			, 'moto');
	}
	
	/**
	 * This will make a recurring capture based on the subscription obtained via the "Recurring"-method.
	 *
	 * @return PensioCaptureRecurringResponse
	 */
	public function captureRecurring($subscriptionId, $amount)
	{
		return $this->getPensioMerchantAPI()->captureRecurring($subscriptionId, $amount);
	}
	
	/**
	 * This will make a recurring capture based on the subscription obtained via the "Recurring"-method.
	 *
	 * @return PensioPreauthRecurringResponse
	 */
	public function recurringReservation($subscriptionId, $amount = null)
	{
		return $this->getPensioMerchantAPI()->preauthRecurring($subscriptionId, $amount);
	}
	
	
}