<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';
require_once(dirname(__FILE__).'/../Model/Api/PensioMerchantAPI.class.php');


class Pensio_Payment_OnepageController extends Mage_Checkout_OnepageController {
	
	public function failureAction() {

		$errorMessage = 'Internal error';
		$paymentScheme = 'N/A';

		try
		{
			if ($this->getRequest()->has('xml'))
			{
				$reservationResponse = new PensioReservationResponse(new SimpleXmlElement($this->getRequest()->getParam('xml')));

				$errorMessage = $reservationResponse->getCardHolderErrorMessage();

				$payments = $reservationResponse->getPayments();
				$paymentScheme = $payments[0]->getPaymentSchemeName();
			}
		}
		catch (Exception $ex)
		{
			// do something?
		}

		Mage::getSingleton('core/session')->setData('pensio_error_message', $errorMessage);
		Mage::getSingleton('core/session')->setData('pensio_payment_scheme_name', $paymentScheme);

		$this->loadLayout();+
		$this->renderLayout();
	}

	public function redirectAction() {
		$this->renderLayout();
	}
	
	public function formAction() {
		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
		$this->renderLayout();
	}	

	/**
	 * This method is called by Pensio, and the result will be displayed to the customer.
	 */
	public function successAction()
	{
		$reservationResponse = new PensioReservationResponse(new SimpleXmlElement($this->getRequest()->getParam('xml')));

		/**
		 * Store the authorization transaction id in the session, in order
		 * for it to be available in Payment->capture (which is called by Magento
		 * when saving the quote)
		 */
		Mage::getSingleton('core/session')->setData('pensio_auth_transaction_id', $reservationResponse->getPrimaryPayment()->getId());
		Mage::getSingleton('core/session')->setData('pensio_requires_capture', $this->getRequest()->getParam('require_capture'));

		$this->storeOrderAndPayment($reservationResponse, 'success');
	}

	/**
	 * This method is called by Pensio, and the result will be displayed to the customer.
	 */
	public function openAction()
	{
		$reservationResponse = new PensioReservationResponse(new SimpleXmlElement($this->getRequest()->getParam('xml')));
	
		$this->storeOrderAndPayment($reservationResponse, 'open');
	}

	/**
	 * This method is called by Pensio's gateway without the user being there to see the result.
	 * For this reason we print out some things which will be visible in the logs in Pensio.
	 */
	public function notificationAction()
	{
		$reservationResponse = new PensioReservationResponse(new SimpleXmlElement($this->getRequest()->getParam('xml')));

		// Find the order
		$orderId = $reservationResponse->getPrimaryPayment()->getShopOrderId();
		$order = $this->_loadOrder($orderId);

		$status = $this->getRequest()->getParam('status');
		if(!is_null($order))
		{
			print("OrderState: ".$order->getState()."\n");
		}
		else
		{
			$merchantErrorMessage = $this->getRequest()->getPost('merchant_error_message', '');

			if ($merchantErrorMessage == 'Declined')
			{
				// we are ok with not finding the payment
				return;
			}

			if($status == 'success' || $status == 'succeeded')
			{
				// create an order etc.
				$this->successAction();
			}
			else
			{
				throw new Exception("Could not find order: ".$orderId);
			}
		}

		// Handle the actual notification
		if($status == 'success' || $status == 'succeeded')
		{
			if($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)
			{
				if(bccomp($reservationResponse->getPrimaryPayment()->getReservedAmount(), $order->getTotalDue(), 2) == 0)
				{
					// The notification is a payment-notification
					$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, 'The payment is no longer "open" at Pensio, we can start processing the order', false);
				}
				else
				{
					print('The payment is most likely still "open" because the reserved amount and the amount due do not match: '.$reservationResponse->getPrimaryPayment()->getReservedAmount().' and '.$order->getTotalDue()."\n");
					$order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true, 'The payment is most likely still "open" because the reserved amount and the amount due do not match: '.$reservationResponse->getPrimaryPayment()->getReservedAmount().' and '.$order->getTotalDue(), false);
				}
				$order->save();
			}
			else if($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING)
			{
				print("The order is already processing, hmm..\n");
			}
		}
		else if($status == 'failed')
		{
			if($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)
			{
				// Cancel the order (as the payment was declined)
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'The payment was declined', false);
				$order->save();
			}
		}
		else
		{
			throw new Exception("An unknown notification was sent: ".$status);
		}
	}
	
	/**
	 * This method is called when the user chooses to use an already existing subscription.
	 * 
	 * We should:
	 *  - Find the subscription and verify that it belongs to the user.
	 *  - Attempt to make a recurring preauth/capture to cover the order.
	 */
	public function recurringPaymentAction()
	{
		$params = $this->getRequest()->getParams();
		
		if(!isset($params['subscription_id']) || $params['subscription_id'] == 'new')
		{
			Mage::getSingleton('checkout/session')->setErrorMessage("No subscription provided");
			$this->_redirect('checkout/onepage/failure');
		}
		else
		{
			$subscription = Mage::getModel('pensiopayment/subscription')->load($params['subscription_id']);
			if($subscription->getCustomerId() != $this->getCustomer()->getId())
			{
				Mage::getSingleton('checkout/session')->setErrorMessage("This subscription does not belong to you");
				$this->_redirect('checkout/onepage/failure');
			}
			
			if($subscription->getCurrencyCode() != $this->getOnepage()->getQuote()->getQuoteCurrencyCode())
			{
				Mage::getSingleton('checkout/session')->setErrorMessage("This subscription is in the wrong currency");
				$this->_redirect('checkout/onepage/failure');
			}
			else
			{
				$this->processRecurringPayment($subscription);
			}
		}
	}
	
	/**
	 * This method is called when we have successfully made a subscription.
	 * 
	 * We should:
	 *  - Store the subscription for later use 
	 *  - Attempt to make a recurring preauth/capture to cover the order.
	 */
	public function recurringSuccessAction()
	{
		// Store the subscription
		$subscription = $this->storeSubscription($this->getRequest()->getParam('xml'));
		
		$this->processRecurringPayment($subscription);
	}
	
	private function storeOrderAndPayment($reservationResponse, $successType='success')
	{
        // Clear the basket and save the order (including some info about how the payment went)
        $this->getOnepage()->getQuote()->collectTotals();
        $this->getOnepage()->getQuote()->getPayment()->setAdditionalInformation('successType', $successType);
        $orderId = $this->getOnepage()->saveOrder()->getLastOrderId();
        $this->getOnepage()->getQuote()->save();

        // Store the information from Pensio
        {
            $order = $this->_loadOrder($orderId);

            $payment = $order->getPayment();
            $payment->setTransactionId($reservationResponse->getPrimaryPayment()->getId());
            if($reservationResponse->getPrimaryPayment()->getPaymentNature() == 'CreditCard')
            {
                $payment->setData('cc_last4', substr('****'.$reservationResponse->getPrimaryPayment()->getMaskedPan(), -4));
                $payment->setData('cc_number_enc', $reservationResponse->getPrimaryPayment()->getMaskedPan());
                $payment->setData('cc_trans_id', $reservationResponse->getPrimaryPayment()->getId());
            }
            $payment->setAdditionalData(serialize($reservationResponse));
            $payment->setAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $this->getRequest()->getParams());

            $payment->save();

            $transaction = $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
            $transaction->setAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $this->getRequest()->getParams());
            $transaction->setAdditionalInformation('pensio_response', serialize($reservationResponse));
            $transaction->setTxnId($reservationResponse->getPrimaryPayment()->getId());
            $transaction->setIsClosed(false);
            $transaction->save();

            if($successType == 'open')
            {
                $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, true, 'The payment is "open" at Pensio, we must wait for a notification before it will be processing', false);
                $order->save();
            }
        }

        // Redirect to success page
        //$this->_redirect('checkout/onepage/success');

        // Render Meta-Redirect success page (could be skipped)
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
	}
	
	private function processRecurringPayment($subscription)
	{
		$totals         = $this->getOnePage()->getQuote()->getTotals();
		$grandTotal     = $totals['grand_total']->getValue();
		$amount	        = number_format($grandTotal, 2,'.','');
		
		$storeId = $this->getOnePage()->getQuote()->getStoreId();
		$pensioModel = new Pensio_Payment_Model_Pensio($storeId);
		$reservationResponse = $pensioModel->recurringReservation($subscription->getSubscriptionId(), $amount);
		
		if($reservationResponse->wasSuccessful())
		{
			$this->storeOrderAndPayment($reservationResponse, 'success');
		}
		else
		{
			Mage::getSingleton('checkout/session')->setErrorMessage($reservationResponse->getCardHolderErrorMessage());
			$this->_redirect('checkout/onepage/failure');
		}
	}
	
	private function storeSubscription($xml)
	{
		$subscriptionResponse = new PensioReservationResponse(new SimpleXmlElement($xml));
		
		$currencyMapper = new Pensio_Payment_Helper_CurrencyMapper();
		
		/* @var Mage_Customer_Model_Customer $customer */
		$customer = $this->getOnepage()->getCustomerSession()->getCustomer();
		$subscription = Mage::getModel('pensiopayment/subscription');
		$subscription->setSubscriptionId($subscriptionResponse->getPrimaryPayment()->getId());
		$subscription->setCustomerId($customer->getId());
		$subscription->setMaskedPan($subscriptionResponse->getPrimaryPayment()->getMaskedPan());
		$subscription->setCardToken($subscriptionResponse->getPrimaryPayment()->getCreditCardToken());
		$subscription->setCurrencyCode($currencyMapper->getAlpha($subscriptionResponse->getPrimaryPayment()->getCurrency()));
		$subscription->save();
		
		return $subscription;
	}
	
	/**
	 * @return Mage_Sales_Model_Order
	 **/
	private function _loadOrder($orderId)
	{
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		if($order->getIncrementId() == $orderId)
		{
			return $order;
		}
		return null;
	}
	
	/**
	 * @return Mage_Customer_Model_Customer
	 */
	private function getCustomer()
	{
		return $this->getOnepage()->getCustomerSession()->getCustomer();
	}
	
}
