<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please 
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Invitations
 * @copyright   Copyright (c) 2012 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */
?>
<?php

class Plumrocket_Invitations_IndexController extends Mage_Core_Controller_Front_Action
{
	private $_customer = NULL;
	public function preDispatch()
    {
		parent::preDispatch();

		$openActions = array('accept');
		if (Mage::getModel('invitations/config')->getGuestsCanMakeInvites()){
			$openActions[] = 'send';
		}
		
		$_helper = Mage::helper('invitations');
		if (!($_helper->moduleEnabled())){
			$this->_toBaseUrl();
		}
		 
		$customer = $_helper->getCurrentCustomer();
			
		if (in_array($this->getRequest()->getActionName(), $openActions)){
			$this->_customer = $customer;
		} elseif ($customer && $customer->getId()){
			$this->_customer = $customer;
		} else {
			$this->_toBaseUrl();
		}
    }
	
	private function _toBaseUrl()
	{
		 header('Location: '.Mage::getUrl('customer/account/login'));
		 exit();
	}
	
	public function indexAction() {
	 	$this->loadLayout();
		$this->getRequest()->setParam('customer', $this->_customer);
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('invitations')->__('My Invitations'));
        }
      	$this->renderLayout();
    }

	
	public function sendAction()
	{
		$request			= $this->getRequest();
		$contacts			= $request->getParam('contacts');
		$helper				= Mage::helper('invitations');
		$modelInvitations	= Mage::getModel('invitations/invitations');
		$modelConfig		= Mage::getModel('invitations/config');
		
		$websiteId			= Mage::app()->getWebsite()->getId();
		$currentCustomerId 	= $helper->getCurrentCustomerId();
		
		$errors = array();
		$notices = array();
		$successes = array();
		
		if (!$currentCustomerId && $modelConfig->getGuestsCanMakeInvites()){
			$guestEmail = $request->getParam('guest_email');
			if (!$guestEmail || !Zend_Validate::is($guestEmail, 'EmailAddress')){
				$errors[] = $helper->__('Please enter a valid email address. For example johndoe@domain.com.');
				return $this->_setSendResponse($successes, $errors, $notices);
			} else {
				$cId = $helper->getCustomerByEmail($guestEmail)->getId();

				if ($cId){
					$errors[] = $helper->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.',
						Mage::helper('customer')->getLoginUrl());
					return $this->_setSendResponse($successes, $errors, $notices);
				}
				
				$customerInfo = Mage::getModel('customer/customer')
					->setFirstName($this->__('Your'))
					->setLastName($this->__('Friend'))
					->setEmail($guestEmail)
					->setGuestId(Mage::getModel('invitations/guest')->getGuestId($guestEmail));
			}
		} else {
			$customerInfo = $this->_customer;
		}

			
		$emails = array();
		foreach($contacts as $key => $contact){
			if (!empty($contact['connectCode'])){
				if ($contact['connectCode'] == $customerInfo->getEmail()){
					continue;
				}
				
				if (Zend_Validate::is($contact['connectCode'], 'EmailAddress')){
					$contacts[$key]['connectCode'] = strtolower($contacts[$key]['connectCode']);
					$emails[] = $contacts[$key]['connectCode'];
				} else {
					unset($contacts[$key]);
					$notices[] = $helper->__('"%s" is invalid email.',$contact['connectCode']);
				}	
			} else {
				unset($contacts[$key]);
			}
		}
		
		
		$regEmails = array();
		if ($emails)
		{	
			$regCustomers = Mage::getModel('customer/customer')
				->getCollection()
				->addFieldToFilter('email', array('in' => $emails))
				->addFieldToFilter('website_id', $websiteId);
				
			foreach ($regCustomers as $customer){
				$regEmails[$customer->getEmail()] = true;	
			}
		}
		
		foreach($contacts as $key => $contact){
			if (!empty($regEmails[$contact['connectCode']])){
				unset($contacts[$key]);
				$notices[] = $helper->__('"%s" is already a member.',$contact['connectCode']);
			}
		}
				
		if (empty($contacts)){
			//$errors[] = $helper->__('No one valid contact given.');
		} else {
			
			$emailTemplateVariables = array(
				'referral_link' => $helper->getRefferalLink(true, $customerInfo->getGuestId()),
				'customer_name' => $customerInfo->getName(),
			);

			$emailTemplateCode = $modelConfig->getInviteEmailTemplate();
			if (!$modelConfig->editInviteeText()){
				$emailTemplateVariables['text'] = nl2br($helper->getFilteredInviteeText($emailTemplateVariables, false));
			} else {
				$emailTemplateVariables['text'] = nl2br(htmlspecialchars($request->getParam('message')));
			}

			$emailTemplateVariables['subject'] = $subject = $helper->getFilteredInviteeSubject($emailTemplateVariables);

			$aBooks = Mage::getModel('invitations/addressbooks')->getEnabled();
			$aBooksById = array();
			
			foreach($aBooks as $aBook)
				$aBooksById[$aBook->getId()] = $aBook;
			
			$aCounts = array(); //availableCounts
			$sendedEmails = array();
			
			
			foreach($contacts as $key => $contact){
				$send = false;
				$contact['connectCode'] = trim($contact['connectCode']);
				
				if (isset($sendedEmails[$contact['connectCode']])){
					continue;
				}

				if (
					   !empty($contact['connectCode'])
					&& !empty($contact['inviteeName'])
					&& isset($contact['addressBookId']) 
				)
				{
					$send = $this->_mail($customerInfo, $contact['connectCode'], $contact['inviteeName'], $subject, $emailTemplateCode, $emailTemplateVariables);
					$sendedEmails[$contact['connectCode']] = true;
				}
					
				if ($send)
				{
					$modelInvitations->invitee($contact['connectCode'], $contact['inviteeName'], $contact['addressBookId'], $currentCustomerId, $customerInfo->getGuestId(), $websiteId);
					$successes[] = $helper->__('"%s" invited successfully.',$contact['connectCode']);
				}
				else
				{
					$notices[] = $helper->__('You can not invite '. htmlspecialchars($contact['inviteeName']));
				}
			}	
		}
		$successes = array_reverse($successes);
		$this->_setSendResponse($successes, $errors, $notices);		
	}
	
	protected function _setSendResponse($successes, $errors, $notices)
	{
		$result = array('success' => empty($errors), 'messages' => array(
			'errors' => (empty($errors)) ? false : implode('<br/>', $errors), //false,
			'successes' => (empty($successes)) ? false : implode('<br/>', $successes),
			'notices' => (empty($notices)) ? false : implode('<br/>', $notices),
		));
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		
		return $this;
	}
	

	public function acceptedInvitationsAction()
	{
		$data = array(
			'success'	=> true,
			'result'	=> $this->getLayout()->createBlock('invitations/accepted')->setData('customerId', $this->_customer->getId())->toHtml(),
		);
		$this->getResponse()->setBody(json_encode($data));
	}
	
	public function openInvitationsAction()
	{
		$data = array(
			'success'	=> true,
			'result'	=> $this->getLayout()->createBlock('invitations/open')->setData('customerId', $this->_customer->getId())->toHtml(),
		);
		$this->getResponse()->setBody(json_encode($data));
	}
	
	public function deleteOpenInvitationsAction()
	{
		$invitationIds = $this->getRequest()->getParam('invitation');
		$modelInvitations = Mage::getModel('invitations/invitations');
		$modelInvitations->deleteInvites($invitationIds, $this->_customer->getId());
		
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('invitations/open')->setData('customerId', $this->_customer->getId())->toHtml()
		);
	}
	
	public function acceptAction()
	{
		$key = $this->_getAcceptKey();
		if (!empty($key) && !($this->_customer && $this->_customer->getId()))
		{
//			$guest = Mage::getSingleton('invitations/invitations')->load($key);
//            $guest->setData('deactivated', '1')->save();
		}
		$this->_redirect('customer/account/login');
	}
	
	protected function _getAcceptKey()
	{
		$cUrl = Mage::helper('core/url')->getCurrentUrl();
		$items = explode('/',$cUrl);
		$count = count($items);
		if ($items[$count - 1])
			return $items[$count - 1];
		elseif ($count > 2)
			return $items[$count - 2];
		else
			return false;
	}

	
	private function _mail($customerInfo, $toEmail, $toName, $subject, $emailTemplateCode, $emailTemplateVariables)
	{
		if (is_numeric($emailTemplateCode))
			$emailTemplate  = Mage::getModel('core/email_template')->load($emailTemplateCode); 
		else
			$emailTemplate  = Mage::getModel('core/email_template')->loadDefault($emailTemplateCode); 
									
        $emailTemplate->getProcessedTemplate($emailTemplateVariables);
        $emailTemplate->setTemplateSubject($subject);
		
		if (Mage::getModel('invitations/config')->getEmailFromStore())
		{
			$emailTemplate
				->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'))
				->setSenderName($customerInfo->getName())
				->setReplyTo($customerInfo->getEmail());
		}
		else
		{
			$emailTemplate
				->setSenderEmail($customerInfo->getEmail())
				->setSenderName($customerInfo->getName());
		}
            
		return $emailTemplate->send($toEmail, $toName, $emailTemplateVariables);   
	}
    
/*
    private function _getStoreName(){
        if (empty($this->_data['store_name'])) {
            $this->_data['store_name'] = Mage::app()->getStore()->getName();
        }
        return $this->_data['store_name'];
    }

    private function _getStoreLogoUrl(){
        if (empty($this->_data['store_logo_url'])) {
            $this->_data['store_logo_url'] = $this->getLayout()->createBlock('invitations/default')->getSkinUrl(Mage::getStoreConfig('design/header/logo_src'));
        }
        return $this->_data['store_logo_url'];
    }
    
    private function _getStoreDescription(){
        if (empty($this->_data['store_description'])) {
            $this->_data['store_description'] = Mage::getStoreConfig('design/head/default_description');
        }
        return $this->_data['store_description'];
    }
*/
}
