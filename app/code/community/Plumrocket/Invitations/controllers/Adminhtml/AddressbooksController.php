<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please 
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Invitations
 * @copyright   Copyright (c) 2012 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */
?>
<?php

class Plumrocket_Invitations_Adminhtml_AddressbooksController extends Mage_Adminhtml_Controller_Action
{
	public function preDispatch()
    {
		parent::preDispatch();
		Mage::helper('invitations')->checkAdminAccess('invitations/address_books');
    }

	public function IndexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
    }
	
	public function enableAction(){
		$this->_changeStatus('enable');
	}
	
	public function disableAction(){
		$this->_changeStatus('disable');
	}
	
	private function _changeStatus($status)
	{
		if ($status == 'disable')
		{
			$statusValue = Plumrocket_Invitations_Model_Addressbooks::STATUS_DISABLED;
			$statusStr = 'disabled';
		}
		else
		{
			$statusValue = Plumrocket_Invitations_Model_Addressbooks::STATUS_ENABLED;
			$statusStr = 'enabled';
		}
		
		$helper = Mage::helper('invitations');
		$helper->admCheckAccess('addressbooks');
		if($ids = $this->getRequest()->getParam('id'))
		{
			$modelAddressBooks = Mage::getModel('invitations/addressbooks');
			foreach($ids as $id)
			{
				if ( ($addressBook = $modelAddressBooks->load($id)) && $addressBook->getId() )
					$addressBook->addData(array('status' => $statusValue))->setId($id)->save();
			}
			$helper->admResultSuccess('Address book(s) '.$statusStr.' successfully.');
		}
		else
			$helper->admResultError('Address book(s) wasn\'t '.$statusStr.'.');
	}
	
	public function EditAction()
    {
		$result = false;
		if (
			($id = $this->getRequest()->getParam('id'))
			&& ($addressBook = Mage::getModel('invitations/addressbooks')->load($id))
			&& $addressBook->getId()
		)
		{
			Mage::register('current_invitee_addressbook', $addressBook);
			$this->loadLayout();
		    $this->renderLayout();
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('invitations')->__('This address book no longer exists.'));
			$this->_redirect('*/*/');
		}
    }
    
    public function saveAction()
	{
		$result = false;
		if (
			($id = $this->getRequest()->getParam('id'))
			&& ($addressBook = Mage::getModel('invitations/addressbooks')->load($id))
			&& ($addressBook->getId())
		)
		{
			$data = $this->getRequest()->getParams();
			$settings = $data['settings'];
			$originalSettings = @json_decode($addressBook->getSettings(), true);
			
			if (!empty($settings) && is_array($settings) && $originalSettings)
			{
				foreach($settings as $name => $value)
				{
					if (!empty($originalSettings[$name]))
					{
						if ($originalSettings[$name]['type'] != 'password' || $value != $addressBook->getDefaultHiddenValue())
						{
							$originalSettings[$name]['value'] = $value;		
						}
					}
				}
				$data['settings'] = json_encode($originalSettings);
			}
			else
				$data['settings'] = '';
			
			$data['key'] = $addressBook->getKey();
				
			$addressBook->setData($data)->save();
			Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Saving was success.'));
		    $this->_redirect('*/*');
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError($this->__('Having trouble during saving.'));
			$this->_redirectReferer();
		}	
	}
	
}
