<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Shipping
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  * Webshopapps Shipping Module
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Open Software License (OSL 3.0)
  * that is bundled with this package in the file LICENSE.txt.
  * It is also available through the world-wide-web at this URL:
  * http://opensource.org/licenses/osl-3.0.php
  * If you did not receive a copy of the license and are unable to
  * obtain it through the world-wide-web, please send an email
  * to license@magentocommerce.com so we can send you a copy immediately.
  *
  * DISCLAIMER
  *
  * Do not edit or add to this file if you wish to upgrade Magento to newer
  * versions in the future. If you wish to customize Magento for your
  * needs please refer to http://www.magentocommerce.com for more information.
  *
  * Shipping MatrixRates
  *
  * @category   Webshopapps
  * @package    Webshopapps_Matrixrate
  * @copyright  Copyright (c) 2010 Zowta Ltd (http://www.webshopapps.com)
  * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  * @author     Karen Baker <sales@webshopapps.com>
*/

/**
 * Shipping data helper
 */
class Webshopapps_Matrixrate_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function convertRate($rate){
        $price = trim($rate);
        if($price==0 || $price == NULL){
            return 0;
        }else{
            $check = strpos($price,'otherwise');
            $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
            if($check)
            {
                $price1 = floatval(substr($price,0,$check));
                $price2 = floatval(substr($price,$check+9,10));
                $priceNew = Mage::helper('directory')->currencyConvert($price1, $baseCurrencyCode, $currentCurrencyCode);
                $totalPrice = Mage::getSingleton('checkout/cart')->getQuote()->getGrandTotal();
                if($totalPrice>$priceNew){
                    $priceNew = Mage::helper('directory')->currencyConvert($price2, $baseCurrencyCode, $currentCurrencyCode);
                    return $priceNew;
                }else{
                    return 0;
                }
            }
            return  Mage::helper('directory')->currencyConvert($price, $baseCurrencyCode, $currentCurrencyCode);
        }
    }
    public function getRateCorrect($rate)
    {
        $price = trim($rate);
        if($price==0 || $price == NULL || $price =='Not Available'){
            return 0;
        }else{
            $check = strpos($price,'otherwise');
            if($check)
            {
                $totalPrice = Mage::getSingleton('checkout/cart')->getQuote()->getGrandTotal();
                $price1 = floatval(substr($price,0,$check));
                $price2 = floatval(substr($price,$check+9,10));
                if($totalPrice>$price1){
                    return floatval($price2);
                }else{
                    return 0;
                }
            }
            return floatval($price);
            }
    }
}
