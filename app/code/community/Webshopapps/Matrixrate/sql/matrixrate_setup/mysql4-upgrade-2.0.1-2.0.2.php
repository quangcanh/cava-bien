<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('shipping_matrixrate')};
CREATE TABLE IF NOT EXISTS {$this->getTable('shipping_matrixrate')} (
  pk int(10) unsigned NOT NULL auto_increment,
  website_id int(11) NOT NULL default '0',
  dest_country_id varchar(4) NOT NULL default '0',
  dest_region_id int(10) NOT NULL default '0',
  dest_city varchar(30) NOT NULL default '',
  dest_zip varchar(10) NOT NULL default '',
  dest_zip_to varchar(10) NOT NULL default '',
  condition_name varchar(20) NOT NULL default '',
  condition_from_value decimal(12,4) NOT NULL default '0.0000',
  condition_to_value decimal(12,4) NOT NULL default '0.0000',
  price varchar(50) NOT NULL default '0.0000',
  cost decimal(12,4) NOT NULL default '0.0000',
  delivery_type varchar(255) NOT NULL default '',
  delivery_desc varchar(255) NOT NULL default '',
  PRIMARY KEY(`pk`),
  UNIQUE KEY `dest_country` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_city`,`dest_zip`,`dest_zip_to`,`condition_name`,`condition_from_value`,`condition_to_value`,`delivery_type`,`delivery_desc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ");
try {
    $_query = "
	INSERT INTO `directory_country` (`country_id`, `iso2_code`, `iso3_code`) VALUES
	('BQ', 'BQ', 'BQA'),
	('XK', 'XK', 'XKX');
	INSERT INTO `directory_country_region` (`country_id`, `code`, `default_name`) VALUES
	('ZW', 'ZW', 'Zimbabwe'),
	('BQ', 'BQ', 'Bonaire'),
	('XK', 'XK', 'Kosovo');
";
    $installer->run($_query);
} catch (Exception $e) {

}
$installer->endSetup();


