<?php

$installer = $this;

$installer->startSetup();

try {
    $_query = "
	INSERT INTO `directory_country` (`country_id`, `iso2_code`, `iso3_code`) VALUES
	('CW', 'CW', 'CUW'),
	('SS', 'SS', 'SSD'),
	('SX', 'SX', 'SXM');
	INSERT INTO `directory_country_region` (`country_id`, `code`, `default_name`) VALUES
	('CW', 'CW', 'Curacao'),
	('SS', 'SS', 'South Sudan'),
	('SX', 'SX', 'Sint Maarten');
";
    $installer->run($_query);
} catch (Exception $e) {

}
$installer->endSetup();


