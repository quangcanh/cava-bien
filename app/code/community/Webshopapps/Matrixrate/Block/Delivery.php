<?php
class Webshopapps_Matrixrate_Block_Delivery extends Mage_Checkout_Block_Cart_Shipping
{
    public $countryId;
    public $addressId;
    public function _construct()
    {
        parent::_construct();
        $this->setCountryId();
    }
    public function setAddressId($addressId='')
    {
        if($addressId=='')
        {
            $this->addressId=Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate')->getAddressId();
        }else
        $this->addressId=$addressId;
    }
    public function setCountryId($value=''){
        if($value=='')
        {
            $this->countryId = Mage::getBlockSingleton('directory/data')->getCountryId();
        }
        else
        $this->countryId=$value;
    }
    public function getAddressId()
    {
        return $this->addressId;
    }
    public function getCountryId()
    {
        return $this->countryId;
    }
    public function getHtmlCountry($defValue=null){
        Varien_Profiler::start('DELIVERY: '.__METHOD__);
        if (is_null($defValue)) {
            $defValue = $this->getCountryId();
        }
        $options =  Mage::getBlockSingleton('directory/data')->getCountryCollection()->toOptionArray();
        Varien_Profiler::stop('DELIVERY: '.__METHOD__);
        return $options;
    }
    public function getDelivery($countryId='')
    {
        if($countryId=='')
        {
            $countryId=$this->getCountryId();
        }
            $result = Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate')->getNewDelivery($countryId);
        if(sizeof($result)!=0){
            $this->setAddressId();
            $i=0;
            foreach($result as $res){
                $result[$i]['address_id']=$this->getAddressId();
                $i++;
            }
            Mage::getModel('matrixrate/carrier_matrixrate')->createRate($result);
        }

        return $result;
    }
}