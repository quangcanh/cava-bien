<?php
class PWS_ProductQA_IndexController extends Mage_Core_Controller_Front_Action
{
   
   const CONFIG_SEND_NOTIFICATION_EMAIL = 'pws_productqa/general/send_notification';
   const CONFIG_SEND_NOTIFICATION_EMAIL_TO = 'pws_productqa/general/notification_email';
   
   const XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY  = 'default/pws_productqa/emails/email_identity';
   const XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE  = 'product_qa_notification';

   const XML_PATH_EMAIL_PRODUCT_QUESTION_TEMPLATE  = 'product_qa_answer';
   const CONFIG_SEND_USER_EMAIL = 'pws_productqa/general/send_email';
   
   public function addQuestionAction()
   {
        $post = $this->getRequest()->getPost();
        if ($post) {
           
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if((isset($post['firstname'])&&$post['firstname']!='')||(isset($post['lastname'])&&$post['lastname']!='')){
                     $post['name'] = $post['firstname'].' '.$post['lastname'];
                }

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['question']) , 'NotEmpty')) {
                    $error = true;
                }

//                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
//                    $error = true;
//                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }
                $fileNames = '';
                if (isset($_FILES['photo']['name'])) {
                    $count = 0;
                    foreach ($_FILES['photo']['name'] as $f => $name) {
                        if ($_FILES['photo']['error'][$f] == 4) {
                            continue; // Skip file if any error found
                        }
                        if ($_FILES['photo']['error'][$f] == 0) {
                                try {
                                    $fileName       = $name;
                                    $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
                                    $fileNamewoe    = rtrim($fileName, $fileExt);
                                    $fileName       = $fileNamewoe . time() . '.' . $fileExt;
                                    $fileNames      .= $fileName.' ';
                                    $uploader       = new Varien_File_Uploader( array(
                                        'name' => $_FILES['photo']['name'][$f],
                                        'type' => $_FILES['photo']['type'][$f],
                                        'tmp_name' => $_FILES['photo']['tmp_name'][$f],
                                        'error' => $_FILES['photo']['error'][$f],
                                        'size' => $_FILES['photo']['size'][$f]
                                    ));
                                    $uploader->setAllowedExtensions(array('png','jpg','gif'));
                                    $uploader->setAllowRenameFiles(false);
                                    $uploader->setFilesDispersion(false);
                                    $path = Mage::getBaseDir('media') . DS . 'productqa';
                                    if(!is_dir($path)){
                                        mkdir($path, 0777, true);
                                    }
                                    $uploader->save($path . DS, $fileName );
                                } catch (Exception $e) {
                                    $error = true;
                                }
                                    $count++; // Number of successfully uploaded file
                        }
                    }
                }
                if ($error) {
                    throw new Exception();
                }
                if(isset($post['email_answer'])&&$post['email_answer']=='on'){
                     $post['email_answer'] = 1;
                }

                if(isset($post['publish_question'])&&$post['publish_question']=='on'){
                    $client = Mage::getSingleton('inchoo_socialconnect/facebook_client');

                    $customer = Mage::getSingleton('customer/session')->getCustomer();

                    if(($socialconnectFid = $customer->getInchooSocialconnectFid()) &&
                        ($socialconnectFtoken = $customer->getInchooSocialconnectFtoken())) {
                        $client->setAccessToken($socialconnectFtoken);
                        try{
                            $client->api(
                                '/me/feed',
                                'POST',
                                array(
                                    'link' => Mage::helper('core/http')->getHttpReferer(),
                                    'message' => $post['question']
                                )
                            );
                        }catch (Exception $e) {
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }
                    }
                }


                $post['photo'] = trim($fileNames);
                $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $productqa = Mage::getModel('pws_productqa/productqa');
                $productqa->setData($post);
                $productqa->save();

                //  --------------------- send email when someone comments on my question

                $sendEmailToPoster = Mage::getStoreConfig(self::CONFIG_SEND_USER_EMAIL);

                if ($sendEmailToPoster) {
                    if(isset($post['parentqa_id'])&&$post['parentqa_id']!=''){
                        $parentQa =  Mage::getModel('pws_productqa/productqa')->load($post['parentqa_id']);
                        if($parentQa->getEmailAnswer()==1 && $parentQa->getEmail()!=''){
                            $product = Mage::getModel('catalog/product')->load($productqa->getProductId());
                            $emailData = array();
                            $emailData['to_email'] = $parentQa->getEmail();
                            $emailData['to_name'] = Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY);
                            $emailData['email'] = array(
                                'product_name' => $product->getName(),
                                'store_id' =>$productqa->getStoreId(),
                                'store_name' => $productqa->getStoreName(),
                                'question' => $parentQa->getQuestion(),
                                'answer' => $post['question'],
                                'customer_name' => $parentQa->getName(),
                                'date_posted' => Mage::helper('core')->formatDate($productqa->getCreatedOn(), 'long'),
                            );
                            $result = $this->sendEmail($emailData, self::XML_PATH_EMAIL_PRODUCT_QUESTION_TEMPLATE);

                            if(!$result) {
                                Mage::throwException($this->__('Cannot send email'));
                            }
                        }
                    }
                }
                
                // --------------------- SEND NOTIFICATION EMAIL
                
                $sendNotificationEmail = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL);
               
                               
                if ($sendNotificationEmail && $post['email']!='' && isset($post['email_post'])&&$post['email_post']=='on') {
                        $product = Mage::getModel('catalog/product')->load($productqa->getProductId());

                        $emailData = array();
                        $emailData['to_email'] = $post['email'];
                        $emailData['to_name'] =  Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY);
                        $emailData['email'] = array(
                            'product_name' => $product->getName(),
                            'store_id' => $productqa->getStoreId(),
                            'question' => $productqa->getQuestion(),
                            'date_posted' => Mage::helper('core')->formatDate($productqa->getCreatedOn(), 'long'),
                        );
                        $result = $this->sendEmail($emailData,self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE);
		            if(!$result) {
		                Mage::throwException($this->__('Cannot send email'));
		            }
		        }      
                

                Mage::getSingleton('catalog/session')->addSuccess(Mage::helper('pws_productqa')->__('Thank You! We\'ll reply to your question as soon as possible '));
                
                return $this->_redirectReferer();
            } catch (Exception $e) {               

                Mage::getSingleton('catalog/session')->addError(Mage::helper('pws_productqa')->__('Unable to submit your question. Please, try again later'.$e->getMessage()));
                
                return $this->_redirectReferer();
            }

        } else {
            return $this->_redirectReferer();
        }
    }
    

    private function sendEmail($data, $template)
	{	
		
		$storeID = $data['email']['store_id'];
		
		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
        
        $result->sendTransactional(
                $template,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        //echo $result->getProcessedTemplate($data);       

        $translate->setTranslateInline(true);
        
        return $result;
	}

    public function qaHelpfullAction(){
        $post = $this->getRequest()->getPost();
        if ($post) {
            $qaId = $post['id'];
            $isHelpfull = $post['status'];
            $result['success'] = false;
            $result['answer'] = '';
            $qa = Mage::getModel('pws_productqa/productqa')->load($qaId);
            $visitorData = Mage::getSingleton('core/session')->getVisitorData();
            $customerId = $visitorData['customer_id'];
            $visitorId = $visitorData['visitor_id'];
            $customerExist = false;
            $visitorExist = false;
            if(isset($customerId)&& $customerId!=null){
                $colectionQahelpfull =  Mage::getModel('pws_productqa/productqahelpfull')->getCollection()
                    ->addFieldToFilter('productqa_id', array('eq' => $qaId))
                    ->addFieldToFilter('customer_id', array('eq' => $customerId));
                if(count($colectionQahelpfull)>0){
                    $customerExist = true;
                    $visitorExist = true;
                }
            }else{
                $colectionQahelpfull =  Mage::getModel('pws_productqa/productqahelpfull')->getCollection()
                    ->addFieldToFilter('productqa_id', array('eq' => $qaId))
                    ->addFieldToFilter('is_new_visitor', array('eq' => $visitorId));
                if(count($colectionQahelpfull)>0){
                    $visitorExist = true;
                }
            }

            $helpfull = (int)($qa->getHelpfull());
            $newHelpfull = '';
            $noHelpfull = (int)($qa->getNoHelpfull());
            $newNoHelpfull = '';
            if($isHelpfull=='yes'){
                $newHelpfull = $helpfull+1;
                if($visitorExist==false){
                    $qa->setHelpfull($newHelpfull)->save();
                    $helpfullData = array(
                        'productqa_id'=>$qaId,
                        'customer_id'=>$customerId,
                        'review_status'=>$isHelpfull,
                        'is_new_visitor'=>$visitorId
                    );
                    Mage::getModel('pws_productqa/productqahelpfull')->setData($helpfullData)->save();
                    $result['answer'] = 'yes';
                    $result['success'] = true;
                }else if($visitorExist==true){
                    if($noHelpfull>0){
                         $newNoHelpfull = $noHelpfull - 1;
                    }else{
                         $newNoHelpfull = '';
                    }
//                    Mage::getModel('pws_productqa/productqahelpfull')->load($visitorId,'is_new_visitor')->setReviewStatus('yes')->save();
                    if(isset($customerId)&& $customerId!=null){
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('customer_id', array('eq' => $customerId))->walk('delete');
                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('customer_id', array('eq' => $customerId))->getFirstItem()->setReviewStatus('yes')->save();
                    }else{
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->walk('delete');
                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->getFirstItem()->setReviewStatus('yes')->save();
                    }
                    $qa->setHelpfull($newHelpfull)->save();
                    $qa->setNoHelpfull($newNoHelpfull)->save();
                    $result['answer'] = 'yes';
                    $result['success'] = true;
                }
            }else if($isHelpfull=='no'){
                $newNoHelpfull = $noHelpfull + 1;
                if($visitorExist==true){
                    if($helpfull>0){
                        $newHelpfull = $helpfull-1;
                        $qa->setHelpfull($newHelpfull)->save();
                    }
                    if(isset($customerId)&& $customerId!=null){
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('customer_id', array('eq' => $customerId))->walk('delete');
                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('customer_id', array('eq' => $customerId))->getFirstItem()->setReviewStatus('no')->save();
                    }else{
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->walk('delete');
                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->getFirstItem()->setReviewStatus('no')->save();
                    }
                }else if($visitorExist==false){
                    $helpfullData = array(
                        'productqa_id'=>$qaId,
                        'customer_id'=>$customerId,
                        'review_status'=>$isHelpfull,
                        'is_new_visitor'=>$visitorId
                    );
                    Mage::getModel('pws_productqa/productqahelpfull')->setData($helpfullData)->save();
                }
                $qa->setNoHelpfull($newNoHelpfull)->save();
                $result['answer'] = 'no';
                $result['success'] = true;
            }
            $result['newHelpfull'] = $newHelpfull;
            $result['newNoHelpfull'] = $newNoHelpfull;
            $response = $this->getResponse();
            $response->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function filterQaAction(){
        $post = $this->getRequest()->getPost();
        if ($post) {
            $result['success'] = false;
            $collection = '';
            $filter = $post['val'];
            $productId = $post['productId'];
            Mage::register('product', Mage::getModel('catalog/product')->load($productId));
            Mage::register('current_product', Mage::getModel('catalog/product')->load($productId));
            $qaCollection = Mage::getModel('pws_productqa/productqa')->getCollection()
                ->addFieldToFilter('product_id', array('eq' => $productId));
            switch($filter){
                case 0:
                    $collection = Mage::getModel('pws_productqa/productqa')->getCollection()
                        ->addFieldToFilter('status', 'public')
                        ->addFieldToFilter('parentqa_id', array('null'=>true))
                        ->addFieldToFilter('product_id', Mage::registry('product')->getId())
                        ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
                        ->setOrder('created_on','DESC')
                    ;
                    $result['success'] = true;
                    break;
                case 1:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT DISTINCT parentqa_id FROM pws_productqa
                            WHERE helpfull >= ALL (SELECT helpfull FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL) AND parentqa_id IS NOT NULL";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';

                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 2:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT productqa_id FROM pws_productqa
                            WHERE created_on IN (SELECT max(created_on) FROM pws_productqa
                            WHERE parentqa_id IS NULL
                            )";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 3:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT productqa_id FROM pws_productqa
                            WHERE created_on IN (SELECT min(created_on) FROM pws_productqa
                            WHERE parentqa_id IS NULL)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 4:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT productqa_id FROM
                            (SELECT productqa_id, created_on FROM pws_productqa
                            WHERE parentqa_id IS NULL AND
                            productqa_id IN
                            (SELECT parentqa_id FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL
                            )) as tbl
                            WHERE tbl.created_on IN (
                            SELECT max(created_on) FROM pws_productqa
                            WHERE parentqa_id IS NULL AND
                            productqa_id IN
                            (SELECT parentqa_id FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL ))";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 5:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT productqa_id FROM
                            (SELECT productqa_id, created_on FROM pws_productqa
                            WHERE parentqa_id IS NULL AND
                            productqa_id IN
                            (SELECT parentqa_id FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL
                            )) as tbl
                            WHERE tbl.created_on IN (
                            SELECT min(created_on) FROM pws_productqa
                            WHERE parentqa_id IS NULL AND
                            productqa_id IN
                            (SELECT parentqa_id FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL ))";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 6:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT parentqa_id FROM
                            (SELECT parentqa_id, count(*) as c FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL
                            GROUP BY parentqa_id) as tbl
                            WHERE tbl.c >= ALL
                            (SELECT count(*) FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL
                            GROUP BY parentqa_id)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;
                case 7:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT productqa_id FROM pws_productqa
                            WHERE productqa_id NOT IN
                            (SELECT parentqa_id FROM pws_productqa
                            WHERE parentqa_id IS NOT NULL)  AND parentqa_id IS NULL";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $qaCollection->getSelect()->where('productqa_id IN'.$arrDetailId);
                    $collection =  $qaCollection;
                    $result['success'] = true;
                    break;

            }
            $questionForm = $this->getLayout()->createBlock('core/template')->setTemplate('pws_productqa/form.phtml');
            $answerForm = $this->getLayout()->createBlock('core/template')->setTemplate('pws_productqa/answerform.phtml');
            $qaList = $this->getLayout()->createBlock('pws_productqa/list','filterqa')->setEntries($collection)->setTemplate('catalog/product/view/tabs/bottom/question_answer_filter.phtml');
            $result['afterfilter'] =  $qaList->toHtml();
            $response = $this->getResponse();
            $response->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
   
}
