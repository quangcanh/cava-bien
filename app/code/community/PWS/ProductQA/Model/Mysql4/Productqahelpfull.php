<?php

class PWS_ProductQA_Model_Mysql4_Productqahelpfull extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('pws_productqa/productqahelpfull', 'productqahelpfull_id');
    }
}