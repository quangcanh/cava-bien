<?php

/**
 * Class PWS_ProductQA_Model_Mysql4_Productqahelpfull_Collection
 */
class PWS_ProductQA_Model_Mysql4_Productqahelpfull_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('pws_productqa/productqahelpfull');
    }
}