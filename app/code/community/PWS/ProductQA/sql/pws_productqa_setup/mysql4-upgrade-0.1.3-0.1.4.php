<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'parentqa_id',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'helpfull',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'parentqa_id',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'main_question',      //column name
    'text'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'city',      //column name
    'varchar(255)'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'photo',      //column name
    'varchar(255)'  //datatype definition
);

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'video_caption',      //column name
    'varchar(255)'  //datatype definition
);

$installer->endSetup();