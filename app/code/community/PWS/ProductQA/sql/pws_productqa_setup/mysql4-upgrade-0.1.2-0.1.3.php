<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('pws_productqa/productqahelpfull')};
CREATE TABLE IF NOT EXISTS {$this->getTable('pws_productqa/productqahelpfull')} (
      `productqahelpfull_id` int(10) unsigned NOT NULL auto_increment,
      `productqa_id` int(10),
      `customer_id` int(10),
      `review_status` varchar(255),
      `is_new_visitor` varchar(255),
      PRIMARY KEY  (`productqahelpfull_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->endSetup();