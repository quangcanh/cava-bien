<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Add new column to the 'pws_productqa' table
$installer->getConnection()->addColumn(
    $this->getTable('pws_productqa/productqa'), //table name
    'video',      //column name
    'text'  //datatype definition
);

$installer->endSetup();