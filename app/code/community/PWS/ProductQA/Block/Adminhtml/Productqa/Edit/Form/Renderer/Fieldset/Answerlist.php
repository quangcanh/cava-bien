<?php
    /**
     * File path:
     * magento_root/app/code/local/Youramespace/Yourextensionname/Block/Adminhtml/Entityname/Edit/Form/Renderer/Fieldset/Customtype.php
     */
class PWS_ProductQA_Block_Adminhtml_Productqa_Edit_Form_Renderer_Fieldset_Answerlist
        extends Varien_Data_Form_Element_Abstract
{
    protected $_element;

 public function getElementHtml()
 {
 /**
  * You can do all necessary customisations here
  *
  * You can use parent::getElementHtml() to get original markup
  * if you are basing on some other type and if it is required
  *
  * Use $this->getData('desired_data_key') to extract the desired data
  * E.g. $this->getValue() or $this->getData('value') will return form elements value
  **/
  $parentqa = $this->getParent();
  $parentqaId = $parentqa['id'];
  $colectionQa =  Mage::getModel('pws_productqa/productqa')->getCollection()
         ->addFieldToFilter('parentqa_id', array('eq' => $parentqaId));

  if(count($colectionQa)){
      $outHtml = '<ul>';
      foreach($colectionQa as $c){
          $outHtml .= '<li><b>Name:</b> '.$c->getName().'</br>'.$c->getQuestion().'</li>';
      }
      $outHtml .= '</ul>';
  }else{
      $outHtml = 'No answers';
  }
  return $outHtml;
  }

}