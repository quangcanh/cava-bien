<?php

class WP_CustomMenu_Block_Filter extends Mage_Core_Block_Template
{
    private $_category;
    private $_categoryInstance;

    public function __construct() {
        parent::_construct();
    }

    public function setCategory($category) {
        if ($category instanceof Mage_Catalog_Model_Category) {
            $this->_category = $category;
        } else {
            $this->_category = $this->_getCategoryInstance()
                ->setData($category->getData());
        }
        return $this;
    }

    protected function _getCategoryInstance()
    {
        if (is_null($this->_categoryInstance)) {
            $this->_categoryInstance = Mage::getModel('catalog/category');
        }
        return $this->_categoryInstance;
    }

    public function getCategory() {
        return $this->_category;
    }

    public function getProductCollection() {
        if ($this->getCategory()) {
            return $this->getCategory()->getProductCollection();
        }
        return false;
    }

    public function getAttributes()
    {
        $setIds = $this->_getSetIds();
        if (!$setIds) {
            return array();
        }
        /** @var $collection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
        $collection = Mage::getResourceModel('catalog/product_attribute_collection');
        $collection
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->setAttributeSetFilter($setIds)
            ->addStoreLabel(Mage::app()->getStore()->getId())
            ->setOrder('position', 'ASC');
        $collection->addIsFilterableFilter();
        $collection->load();

        return $collection;
    }

    protected function _getSetIds()
    {
//        $key = $this->getStateKey().'_SET_IDS';
//        $setIds = $this->getAggregator()->getCacheData($key);
//
//        if ($setIds === null) {
            if ($this->getProductCollection() === false) return false;
            $setIds = $this->getProductCollection()->getSetIds();
//            $this->getAggregator()->saveCacheData($setIds, $key, $this->getStateTags());
//        }

        return $setIds;
    }

    public function getOptions($attributeCode)
    {
        if ($this->getAttribute($attributeCode)) {
            return $this->getAttribute($attributeCode)->getSource()->getAllOptions();
        }
        return false;
    }

    public function getAttribute($attributeCode) {
        foreach ($this->getAttributes() as $attribute) {
            if ($attribute->getAttributeCode() === $attributeCode) {
                return $attribute;
            }
        }
        return false;
    }
}
