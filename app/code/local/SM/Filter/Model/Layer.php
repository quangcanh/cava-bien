<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogSearch
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SM_Filter_Model_Layer extends Mage_Catalog_Model_Layer
{
    const NEW_ARRIVAL   = 'new_arrival';
    const BACK_IN_STOCK = 'back_in_stock';
    const COMING_SOON   = 'coming_soon';
    const TOP_RATED     = 'top_rate';
    const ALMOST_GONE   = 'almost_gone';
    const IN_STOCK      = 'in_stock';
    const OUT_OF_STOCK      = 'out_of_stock';
    const SM_FILTER_TOP_RATED_PRODUCT_RATING = 'sm_filter/top_rated/product_rating';
    /**
     * Get current layer product collection
     *
     * @return Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
            $collection = Mage::getResourceModel('catalog/product_collection');
            $this->prepareProductCollection($collection);
            $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
        }
        return $collection;
    }

    /**
     * Prepare product collection: filter
     *
     * @param Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection $collection
     * @return Mage_Catalog_Model_Layer
     */
    public function prepareProductCollection($collection)
    {
        $params = Mage::app()->getRequest()->getParams();
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite()
        ;

        switch ($params['status']) {
            case self::NEW_ARRIVAL:
                $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`is_in_stock`=1', array('qty', 'is_in_stock'));
                $collection->addAttributeToFilter('news_from_date', array('lteq' => date('Y/m/d')))
                    ->addAttributeToFilter('news_from_date', array('gteq' => date('Y/m/d', strtotime('-7 days'))));
                /*$timeNow = date(Varien_Date::DATETIME_PHP_FORMAT, Mage::getModel('core/date')->gmtTimestamp());
                $collection->getSelect()->where("
                    TIMESTAMPDIFF(MINUTE, `e`.`created_at`, '" . $timeNow . "') BETWEEN 0 AND 2880
                ");*/
                break;
            case self::BACK_IN_STOCK:
                $todayStartOfDayDate  = Mage::app()->getLocale()->date()
                    ->setTime('00:00:00')
                    ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=1', array('qty', 'is_in_stock'));
                //$collection->addAttributeToFilter('back_to_stock_from',  array('date' => true, 'from' => $todayStartOfDayDate));
                $collection->addAttributeToFilter('back_to_stock_from', array('gteq' => date('Y/m/d', strtotime('-7 days'))))
                    ->addAttributeToFilter('back_to_stock_from', array('lteq' => date('Y/m/d')));
                break;
            case self::COMING_SOON:
                $collection->addAttributeToFilter('coming_soon', 1);
                $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=0', array('qty', 'is_in_stock'));
                break;
            case self::TOP_RATED:
                $minProductRate = Mage::getStoreConfig(self::SM_FILTER_TOP_RATED_PRODUCT_RATING);
                $collection->getSelect()->join(array('rv' => Mage::getSingleton('core/resource')->getTableName('review/review_aggregate')), '`e`.`entity_id` = `rv`.`entity_pk_value` AND `rv`.`store_id` = '. Mage::app()->getStore()->getId() . ' AND rv.rating_summary >= ' . $minProductRate, array('rating_summary'));
                break;
            case self::ALMOST_GONE:
                $minQty = Mage::getStoreConfig(Cavabien_Theme_Helper_Conf_Data::PRODUCT_LEFT);
                $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=1 AND (`ci`.`qty` > 0 AND `ci`.`qty` < ' . $minQty . ')', array('qty', 'is_in_stock'));
                break;

        }
        
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $this;
    }

    protected function _prepareAttributeCollection($collection){

        $collection = parent::_prepareAttributeCollection($collection);

        $collection->addIsFilterableFilter();

        $tableAlias = 'gomage_nav_attr';
        $collection->getSelect()->joinLeft(
            array($tableAlias => Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute')),
            "`main_table`.`attribute_id` = `{$tableAlias}`.`attribute_id`",
            array('filter_type',
                'inblock_type',
                'round_to',
                'show_currency',
                'image_align',
                'image_width',
                'image_height',
                'show_minimized',
                'show_image_name',
                'visible_options',
                'show_help',
                'show_checkbox',
                'popup_width',
                'popup_height',
                'filter_reset',
                'is_ajax',
                'inblock_height',
                'max_inblock_height',
                'filter_button',
                'category_ids_filter',
                'range_options',
                'range_manual',
                'range_auto',
                'attribute_location')
        );

        $tableAliasStore = 'gomage_nav_attr_store';

        $collection->getSelect()->joinLeft(
            array($tableAliasStore => Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute_store')),
            "`main_table`.`attribute_id` = `{$tableAliasStore}`.`attribute_id` and `{$tableAliasStore}`.store_id = " . Mage::app()->getStore()->getStoreId(),
            array('popup_text')
        );

        $connection = Mage::getSingleton('core/resource')->getConnection('read');
        $table = Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute_option');

        foreach($collection as $attribute){

            $option_images = array();

            $attribute_id = $attribute->getId();

            $_option_images = $connection->fetchAll("SELECT `option_id`, `filename` FROM {$table} WHERE `attribute_id` = {$attribute_id};");

            foreach($_option_images as $imageInfo){

                $option_images[$imageInfo['option_id']] = $imageInfo['filename'];

            }

            $attribute->setOptionImages($option_images);


        }


        return $collection;
    }

    /**
     * Get layer state key
     *
     * @return string
     */
    public function getStateKey()
    {
        if ($this->_stateKey === null) {
            $this->_stateKey = 'Q_' . Mage::helper('catalogsearch')->getQuery()->getId()
                . '_' . parent::getStateKey();
        }
        return $this->_stateKey;
    }

    /**
     * Get default tags for current layer state
     *
     * @param   array $additionalTags
     * @return  array
     */
    public function getStateTags(array $additionalTags = array())
    {
        $additionalTags = parent::getStateTags($additionalTags);
        $additionalTags[] = Mage_CatalogSearch_Model_Query::CACHE_TAG;
        return $additionalTags;
    }
}
