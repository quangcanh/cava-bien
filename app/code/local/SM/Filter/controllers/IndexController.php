<?php
/**
 * SM Filter Controller
 */
class SM_Filter_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $params = Mage::app()->getRequest()->getParams();
        $title = 'Cavabien Filter';
        $this->loadLayout();
        switch ($params['status']) {
            case 'new_arrival':
               $title = 'NEW ARRIVALS';
                break;
            case 'back_in_stock':
                $title = 'BACK IN STOCK';
                break;
            case 'coming_soon':
                $title = 'COMING SOON';
                break;
            case 'top_rate':
                $title = 'TOP RATED';
                break;
            case 'almost_gone':
                $title = 'ALMOST GONE';
                break;
            default:
                break;
        }
        $this->getLayout()->getBlock('head')->setTitle($title);
        $this->getLayout()->getBlock('sm_filter.result')->setTitle($title);
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }
}
