<?php

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

try {
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'estimated_ship_day', array(
        'group' => 'General',
        'input' => 'text',
        'type' => 'varchar',
        'label' => 'Estimated Ship Day',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend' => '',
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => true,
        'filterable' => true,
        'comparable' => true,
        'default' => NULL,
        'visible_on_front' => true,
        'used_in_product_listing' => true,
        'visible_in_advanced_search' => true,
        'is_html_allowed_on_front' => '0',
        'used_in_product_listing' => true,
        'is_configurable' => true,
        'apply_to' => 'simple,grouped,configurable'
    ));
} catch (Exception $e) {
}
$installer->endSetup();