<?php
$installer = $this;

$installer->startSetup();

try {
    $_query = "
    DROP TABLE IF EXISTS {$this->getTable('sm_poll')};
	CREATE TABLE {$this->getTable('sm_poll')} (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `poll_title` varchar(36) NOT NULL,
	  `votes_count` INT(10) NOT NULL,
      `updated_time` timestamp NOT NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	INSERT INTO `sm_poll` (`id`, `poll_title`, `votes_count`) VALUES (NULL, 'Vote 0', '')
";
    for ($i = 1; $i <= 10; $i++) {
        $_query .= ", (NULL, 'Vote $i', '')";
    }
    $_query .= ";";
    $installer->run($_query);
} catch (Exception $e) {
}

$installer->endSetup();
