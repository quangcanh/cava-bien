<?php
$installer = $this;

$installer->startSetup();

try {
    $_query = "
    DROP TABLE IF EXISTS {$this->getTable('sm_store')};
	CREATE TABLE {$this->getTable('sm_store')} (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `store_code` VARCHAR(50) NOT NULL,
	  `title` VARCHAR(100) NOT NULL,
      `description` VARCHAR(255) NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	DROP TABLE IF EXISTS {$this->getTable('sm_zipcode')};
	CREATE TABLE {$this->getTable('sm_zipcode')} (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `zip_code` VARCHAR(50) NOT NULL,
      `description` VARCHAR(255) NULL,
      `store_ids` VARCHAR(100) NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
    $installer->run($_query);
} catch (Exception $e) {
}

$installer->endSetup();
