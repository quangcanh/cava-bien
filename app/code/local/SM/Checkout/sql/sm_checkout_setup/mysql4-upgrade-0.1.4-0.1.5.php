<?php
$installer = $this;

$installer->startSetup();

try {
    $installer->updateAttribute('customer_address', 'telephone', 'is_required', 1);
    $id = Mage::getModel('eav/entity_attribute')->loadByCode('customer_address', 'telephone')->getId();
    $tableName = Mage::getSingleton('core/resource')->getTableName('customer_eav_attribute');
    $rule = 'a:2:{s:15:"max_text_length";i:255;s:15:"min_text_length";i:1;}';
    $installer->run("UPDATE `$tableName` SET `validate_rules` = '$rule' WHERE `attribute_id` = $id;");

    $html = '{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}<br/>
{{depend company}}{{var company}}<br />{{/depend}}
{{if street1}}{{var street1}}<br />{{/if}}
{{depend street2}}{{var street2}}<br />{{/depend}}
{{depend street3}}{{var street3}}<br />{{/depend}}
{{depend street4}}{{var street4}}<br />{{/depend}}
{{if city}}{{var city}},  {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}<br/>
{{var country}}<br/>
{{depend telephone}}Phone: {{var telephone}}{{/depend}}
{{depend fax}}<br/>F: {{var fax}}{{/depend}}
{{depend vat_id}}<br/>VAT: {{var vat_id}}{{/depend}}';
    Mage::getConfig()->saveConfig('customer/address_templates/html', $html);
} catch (Exception $e) {
}

$installer->endSetup();
