<?php
$installer = $this;

$installer->startSetup();

$content = <<<EOD
    <div class="sm_step1_promotions">
        <span>Earn 3% Reward on Every Purchase!</span>
    </div>
EOD;
$staticBlock = array(
    'title' => 'Checkout Promotions',
    'identifier' => 'sm_checkout_promotions',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(1)
);

try {
    $block = Mage::getModel('cms/block')->load('sm_checkout_promotions');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($content)->save();
    }
} catch (Exception $e) {
}

$installer->endSetup();
