<?php
$installer = $this;

$installer->startSetup();

try {
    $installer->run("
       ALTER TABLE  `" . $this->getTable('sales/quote_address') . "` ADD  `facebook_share_amount` DECIMAL( 10, 2 ) NULL;
       ALTER TABLE  `" . $this->getTable('sales/quote_address') . "` ADD  `base_facebook_share_amount` DECIMAL( 10, 2 ) NULL;
       ALTER TABLE  `" . $this->getTable('sales/quote') . "` ADD  `facebook_is_apply_amount` INT(1) NULL;

       ALTER TABLE  `" . $this->getTable('sales/order') . "` ADD  `facebook_share_amount` DECIMAL( 10, 2 ) NULL;
       ALTER TABLE  `" . $this->getTable('sales/order') . "` ADD  `base_facebook_share_amount` DECIMAL( 10, 2 ) NULL;
       ALTER TABLE  `" . $this->getTable('sales/order') . "` ADD  `facebook_is_apply_amount` INT(1) NULL;
");

} catch (Exception $e) {
}

$installer->endSetup();
