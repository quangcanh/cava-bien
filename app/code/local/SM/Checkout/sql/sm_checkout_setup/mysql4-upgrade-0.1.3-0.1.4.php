<?php
$installer = $this;

$installer->startSetup();

try {
    $installer->updateAttribute('customer_address', 'telephone', 'is_required', 0);
    $id = Mage::getModel('eav/entity_attribute')->loadByCode('customer_address', 'telephone')->getId();
    $installer->run("UPDATE `customer_eav_attribute` SET `validate_rules` = '' WHERE `attribute_id` = $id;");
} catch (Exception $e) {
}

$installer->endSetup();
