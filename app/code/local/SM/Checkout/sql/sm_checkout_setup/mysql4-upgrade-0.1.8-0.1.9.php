<?php

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

try {
    $attributes = array(
        'attribute_codes' => 'Attribute Code To Filter',
    );
    foreach ($attributes as $code => $label) {
        $installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, $code, array(
            'group'         => 'Attributes Setting',
            'input'         => 'text',
            'type'          => 'varchar',
            'label'         => $label,
            'backend'       => '',
            'visible'       => true,
            'required'      => false,
            'visible_on_front' => true,
            'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        ));
    }

} catch (Exception $e) {
}
$installer->endSetup();