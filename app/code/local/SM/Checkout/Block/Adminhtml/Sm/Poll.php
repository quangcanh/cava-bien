<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/1/14
 * Time: 4:53 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Poll extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sm_poll';
        $this->_blockGroup = 'sm_checkout';
        $this->_headerText = Mage::helper('sm_checkout')->__("Recommendations");
        parent::__construct();
        $this->_removeButton('add');
    }
}
