<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 4:42 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Store_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct() {
        parent::__construct();
        $this->setId('sm_store_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sm_checkout')->__('SM Store'));
    }

    protected function _beforeToHtml() {
        $this->addTab('general_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_store_edit_tabs_info')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}