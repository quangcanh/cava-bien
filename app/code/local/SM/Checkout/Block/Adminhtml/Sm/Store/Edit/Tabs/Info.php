<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 2:04 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Store_Edit_Tabs_Info extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        // initial form
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('sm_store', array('legend' => Mage::helper('sm_checkout')->__('SM Store Information')));

        $fieldset->addField('id', 'hidden', array(
            'label' => Mage::helper('sm_checkout')->__('SM Store ID'),
            'readonly' => true,
            'name' => 'id',
        ));

        $fieldset->addField('store_code', 'text', array(
            'label' => Mage::helper('sm_checkout')->__('Store Code'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'store_code',
        ));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('sm_checkout')->__('Store Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('description', 'textarea', array(
            'label' => Mage::helper('sm_checkout')->__('Description'),
            'class' => 'required-entry',
            'required' => false,
            'name' => 'description',
        ));

        if (Mage::registry('sm_store_data')) {
            $form->setValues(Mage::registry('sm_store_data')->getData());
        }

        return parent::_prepareForm();

    }
}