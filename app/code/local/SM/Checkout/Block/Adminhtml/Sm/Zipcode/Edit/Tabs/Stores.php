<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 2:04 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Zipcode_Edit_Tabs_Stores extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sm_zipcode_store');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setTemplate('sm/checkout/zipcode/grid.phtml');
    }

    public function getZipcode() {
        return Mage::registry('sm_zipcode_data');
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in zip code flag
        if ($column->getId() == 'in_zipcode') {
            $storeIds = $this->_getSelectedStores();
            if (empty($storeIds)) {
                $storeIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('id', array('in'=>$storeIds));
            }
            elseif(!empty($storeIds)) {
                $this->getCollection()->addFieldToFilter('id', array('nin'=>$storeIds));
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
        if ($this->getZipcode() && $this->getZipcode()->getId()) {
            $this->setDefaultFilter(array('in_zipcode'=>1));
        }
        $collection = Mage::getModel('sm_checkout/store')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_zipcode', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_category',
            'values'    => $this->_getSelectedStores(),
            'align'     => 'center',
            'index'     => 'id'
        ));
        $this->addColumn('id', array(
            'header'    => Mage::helper('sm_checkout')->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  => 'number',
        ));
        $this->addColumn('store_code', array(
            'header'    => Mage::helper('sm_checkout')->__('Store Code'),
            'width'     => '150',
            'index'     => 'store_code'
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('sm_checkout')->__('Title'),
            'width'     => '250',
            'index'     => 'title'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/store', array('_current'=>true));
    }

    protected function _getSelectedStores()
    {
        $storeIds = $this->getRequest()->getPost('selected_stores');
        if (is_null($storeIds) && $this->getZipcode()) {
            $storeIds = explode(',', $this->getZipcode()->getStoreIds());
            return $storeIds;
        }
        return $storeIds;
    }

    public function getStoresJson() {
        $store = array_flip($this->_getSelectedStores());
        if (!empty($store)) {
            return Mage::helper('core')->jsonEncode($store);
        }
        return '{}';
    }
}