<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 4:42 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Zipcode_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'sm_checkout';
        $this->_controller = 'adminhtml_sm_zipcode';

        $this->_updateButton('save', 'label', Mage::helper('sm_checkout')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('sm_checkout')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = '
            function saveAndContinueEdit(){
                editForm.submit($("edit_form").action+"back/edit/");
            }
        ';
    }

    public function getHeaderText()
    {
        if( Mage::registry('sm_zipcode_data') && Mage::registry('sm_zipcode_data')->getId() ) {
            return Mage::helper('sm_checkout')->__("Edit Zip code '%s'", Mage::registry('sm_zipcode_data')->getZipCode());
        } else {
            return Mage::helper('sm_checkout')->__('Add Zip code');
        }
    }
}