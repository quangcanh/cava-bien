<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 2:04 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Zipcode_Edit_Tabs_Info extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        // initial form
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('sm_zipcode', array('legend' => Mage::helper('sm_checkout')->__('SM Zip Code Information')));

        $fieldset->addField('id', 'hidden', array(
            'label' => Mage::helper('sm_checkout')->__('SM Zip Code ID'),
            'readonly' => true,
            'name' => 'id',
        ));

        $fieldset->addField('zip_code', 'text', array(
            'label' => Mage::helper('sm_checkout')->__('Zip Code'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'zip_code',
        ));

        $fieldset->addField('description', 'textarea', array(
            'label' => Mage::helper('sm_checkout')->__('Description'),
            'class' => 'required-entry',
            'required' => false,
            'name' => 'description',
        ));

        if (Mage::registry('sm_zipcode_data')) {
            $form->setValues(Mage::registry('sm_zipcode_data')->getData());
        }

        return parent::_prepareForm();

    }
}