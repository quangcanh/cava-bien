<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 4:42 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Store_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'sm_checkout';
        $this->_controller = 'adminhtml_sm_store';

        $this->_updateButton('save', 'label', Mage::helper('sm_checkout')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('sm_checkout')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = '
            function saveAndContinueEdit(){
                editForm.submit($("edit_form").action+"back/edit/");
            }
        ';
    }

    public function getHeaderText()
    {
        if( Mage::registry('sm_store_data') && Mage::registry('sm_store_data')->getId() ) {
            return Mage::helper('sm_checkout')->__("Edit Store '%s'", Mage::registry('sm_store_data')->getTitle());
        } else {
            return Mage::helper('sm_checkout')->__('Add Store');
        }
    }
}