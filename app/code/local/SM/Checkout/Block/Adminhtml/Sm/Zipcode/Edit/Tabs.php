<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/7/14
 * Time: 4:42 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Zipcode_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct() {
        parent::__construct();
        $this->setId('sm_zipcode_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sm_checkout')->__('SM Zip Code'));
    }

    protected function _beforeToHtml() {
        $this->addTab('general_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_edit_tabs_info')->toHtml(),
        ));
        $this->addTab('store_section', array(
            'label' => $this->__('Store List'),
            'title' => $this->__('Store List'),
            'content' => $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_edit_tabs_stores')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}