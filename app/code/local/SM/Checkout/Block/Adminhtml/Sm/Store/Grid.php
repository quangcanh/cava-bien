<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:42 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Store_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('storeGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sm_checkout/store')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('sm_checkout')->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  => 'number',
        ));
        $this->addColumn('store_code', array(
            'header'    => Mage::helper('sm_checkout')->__('Store Code'),
            'width'     => '150',
            'index'     => 'store_code'
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('sm_checkout')->__('Title'),
            'width'     => '250',
            'index'     => 'title'
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sm_checkout')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sm_checkout')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('sm_store_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('sm_checkout')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('sm_checkout')->__('Are you sure?')
        ));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=> true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id'=>$row->getId()));
    }
}