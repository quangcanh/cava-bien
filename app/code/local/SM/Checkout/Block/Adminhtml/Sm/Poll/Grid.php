<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 4/10/14
 * Time: 2:58 PM
 */

class SM_Checkout_Block_Adminhtml_Sm_Poll_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('pollGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sm_checkout/poll')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('sm_checkout')->__('Poll ID'),
            'align' => 'center',
            'width' => '10px',
            'index' => 'id',
        ));

        $this->addColumn('poll_title', array(
            'header' => Mage::helper('sm_checkout')->__('Poll Title'),
            'align' => 'left',
            'index' => 'poll_title',
            'width' => '250px',
        ));

        $this->addColumn('votes_count', array(
            'header' => Mage::helper('sm_checkout')->__('Votes Count'),
            'width' => '200px',
            'index' => 'votes_count',
        ));

        $this->addColumn('updated_time', array(
            'header' => Mage::helper('sm_checkout')->__('Updated Time'),
            'width' => '200px',
            'type'  => 'datetime',
            'index' => 'updated_time',
        ));

        return parent::_prepareColumns();
    }

}
