<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:40 PM
 */
class SM_Checkout_Block_Adminhtml_Sm_Store extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sm_store';
        $this->_blockGroup = 'sm_checkout';
        $this->_headerText = Mage::helper('sm_checkout')->__("SM Stores");
        parent::__construct();
    }
}