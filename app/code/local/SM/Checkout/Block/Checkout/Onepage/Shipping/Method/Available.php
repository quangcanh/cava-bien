<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/9/14
 * Time: 11:30 AM
 */ 
class SM_Checkout_Block_Checkout_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available {
    public function getAllShippingMethods()
    {
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
        $options = array();

        foreach($methods as $_code => $_method)
        {
            if(!$_title = Mage::getStoreConfig("carriers/$_code/title"))
                $_title = $_code;
            $options[] = array('value' => $_code . '_' .  $_code, 'label' => $_title . " ($_code)");
        }

        return $options; // This array will have all the active shipping methods
    }
}