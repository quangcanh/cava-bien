<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/30/14
 * Time: 11:50 AM
 */ 
class SM_Checkout_Block_Checkout_Onepage_Success extends Mage_Checkout_Block_Onepage_Success {

    public function getOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        $order = Mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            return $order;
        }
        return false;
    }

    public function __construct() {
        parent::__construct();
    }
}