<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/30/14
 * Time: 1:54 PM
 */
class SM_Checkout_Block_Checkout_Onepage_Success_Info extends SM_Checkout_Block_Checkout_Onepage_Success
{
    protected function _prepareLayout()
    {
        $this->setChild(
            'payment_info',
            $this->helper('payment')->getInfoBlock($this->getOrder()->getPayment())
        );
    }

    public function getPaymentInfoHtml()
    {
        return $this->getChildHtml('payment_info');
    }

    public function getPointsCurrent(){
        $store_id = Mage::app()->getStore()->getId();
        $customerId = Mage::getModel('customer/session')->getCustomerId();
        if (Mage::getStoreConfig('rewardpoints/default/flatstats', $store_id)){
            $reward_flat_model = Mage::getModel('rewardpoints/flatstats');
            return $reward_flat_model->collectPointsCurrent($customerId, $store_id)+0;
        }

        $reward_model = Mage::getModel('rewardpoints/stats');
        return $reward_model->getPointsCurrent($customerId, $store_id)+0;
    }
}