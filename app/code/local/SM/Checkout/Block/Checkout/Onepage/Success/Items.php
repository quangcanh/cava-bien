<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/30/14
 * Time: 1:54 PM
 */
class SM_Checkout_Block_Checkout_Onepage_Success_Items extends SM_Checkout_Block_Checkout_Onepage_Success
{
    public function gerItemOptions($item){
        $result = array();
        if ($options = $item->getProductOptions()) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }
}