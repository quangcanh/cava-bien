<?php
/**
 * Created by   : JetBrains PhpStorm.
 * @project     : ${PROJECT_NAME}
 * @author      : DUC THANG
 * @Date        : 7/9/14
 * @Time        : 7:12 AM
 * @copyright  Copyright (c) 2014
 *
 */
class SM_Checkout_Block_Checkout_Onepage extends Mage_Checkout_Block_Onepage
{
    /**
     * Get active step
     *
     * @return string
     */
    public function getActiveStep()
    {
//        return $this->isCustomerLoggedIn() ? 'billing' : 'login';
        return 'delivery_detail';
    }

    /**
     * Get checkout steps codes
     *
     * @return array
     */
    protected function _getStepCodes()
    {
        //return array('login', 'billing', 'shipping', 'shipping_method', 'payment', 'review');
        return array('delivery_detail','delivery_option','payment_option');
    }
}