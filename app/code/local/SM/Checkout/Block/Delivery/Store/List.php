<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/11/14
 * Time: 11:13 AM
 */
class SM_Checkout_Block_Delivery_Store_List extends Mage_Core_Block_Template
{
    public function getZipCode()
    {
        $data = $this->getRequest()->getPost();
        if (isset($data['order']['zipcode'])) {
            return $data['order']['zipcode'];
        }
        return false;
    }

    public function getList()
    {
        return Mage::getModel('sm_checkout/zipcode')->getStoresList($this->getZipCode());
    }
}