<?php
/**
 * Created by   : JetBrains PhpStorm.
 * @project     : magento18
 * @author      : DUC THANG
 * @Date        : 7/8/14
 * @Time        : 11:34 PM
 * @copyright  Copyright (c) 2014
 *
 */
class SM_Checkout_Block_Delivery_Detail extends Mage_Checkout_Block_Onepage_Abstract
{
    /**
     * Initialize billing address step
     *
     */
    protected function _construct()
    {
        $this->getCheckout()->setStepData('delivery_detail', array(
            'label'     => Mage::helper('checkout')->__('Address Detail...'),
            'is_show'   => $this->isShow(),
            'allow'     => true
        ));
        parent::_construct();
    }

}