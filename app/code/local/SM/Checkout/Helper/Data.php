<?php
/**
 * Created by   : JetBrains PhpStorm.
 * @project     : ${PROJECT_NAME}
 * @author      : DUC THANG
 * @Date        : 7/8/14
 * @Time        : 10:56 PM
 * @copyright  Copyright (c) 2014
 *
 */
class SM_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ENABLE_FACEBOOK_DISCOUNT   = 'sm_facebook/facebook_setting/enable_facebook_discount';
    const FACEBOOK_DISCOUNT_LABEL = 'sm_facebook/facebook_setting/facebook_discount_label';
    const FACEBOOK_DISCOUNT_TYPE   = 'sm_facebook/facebook_setting/facebook_discount_type';
    const FACEBOOK_DISCOUNT_AMOUNT   = 'sm_facebook/facebook_setting/facebook_discount_amount';

    public function getFacebookSetting() {
        $result = null;
        $result['enable'] = Mage::getStoreConfig(self::ENABLE_FACEBOOK_DISCOUNT, Mage::app()->getStore()->getId());
        $result['label'] = Mage::getStoreConfig(self::FACEBOOK_DISCOUNT_LABEL, Mage::app()->getStore()->getId());
        $result['type'] = Mage::getStoreConfig(self::FACEBOOK_DISCOUNT_TYPE, Mage::app()->getStore()->getId());
        $result['amount'] = Mage::getStoreConfig(self::FACEBOOK_DISCOUNT_AMOUNT, Mage::app()->getStore()->getId());
        return $result;
    }
}