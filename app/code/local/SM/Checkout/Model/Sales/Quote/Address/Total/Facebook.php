<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/22/14
 * Time: 3:23 PM
 */
class SM_Checkout_Model_Sales_Quote_Address_Total_Facebook extends Mage_Sales_Model_Quote_Address_Total_Abstract{
    protected $_code = 'facebook';

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this; //this makes only address type shipping to come through
        }

        $quote = $address->getQuote();
        $setting = Mage::helper('sm_checkout')->getFacebookSetting();
        if($quote->getFacebookIsApplyAmount() && $setting['enable']){
            $amountF = (int) $setting['amount'];
            if ($setting['type'] === 'by_percent') {
                if ($amountF > 100) {
                    $amountF = 100;
                }
                $balance = ($amountF / 100) * (int) $address->getSubtotal();
            } else {
                $balance = $amountF;
                if ($amountF > (int) $address->getSubtotal()){
                    $balance = (int) $address->getSubtotal();
                }

            }
            $address->setFacebookShareAmount($balance);
            $address->setBaseFacebookShareAmount($balance);

            $quote->setFacebookShareAmount($balance);

            $address->setGrandTotal($address->getGrandTotal() - $address->getFacebookShareAmount());
            $address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getBaseFacebookShareAmount());
        }
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        if ($address->getFacebookShareAmount() && $address->getQuote()->getFacebookShareAmount()) {
            $address->addTotal(array(
                'code'  => $this->getCode(),
                'title' => Mage::helper('sm_checkout')->__('Facebook code'),
                'value' => '-' . $address->getFacebookShareAmount()
            ));
        }
        return $this;
    }
}