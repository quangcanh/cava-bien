<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:03 PM
 */
class SM_Checkout_Model_Zipcode extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_checkout/zipcode');
    }

    /**
     * Reset all model data
     *
     * @return SM_Checkout_Model_Zipcode
     */
    public function reset()
    {
        $this->setData(array());
        $this->setOrigData();
        $this->_attributes = null;

        return $this;
    }

    public function getStoresList($zipCode)
    {
        $zipModel = $this->getCollection()->addFieldToFilter('zip_code', $zipCode)->getFirstItem();
        if ($zipModel->getId()) {
            $storeIds = explode(',', $zipModel->getStoreIds());
            $col = Mage::getModel('sm_checkout/store')->getCollection()
                ->addFieldToFilter('id', array('in' => $storeIds));
            return $col;
        }
        return false;
    }
}