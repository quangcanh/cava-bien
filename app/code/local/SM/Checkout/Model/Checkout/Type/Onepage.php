<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/2/14
 * Time: 4:45 PM
 */ 
class SM_Checkout_Model_Checkout_Type_Onepage extends Mage_Checkout_Model_Type_Onepage {
    /**
     * Save checkout shipping address
     *
     * @param   array $data
     * @param   int $customerAddressId
     * @return  Mage_Checkout_Model_Type_Onepage
     */
    protected  $_customerEmailExistsMessage;
    public function saveShipping($data, $customerAddressId)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }
        $address = $this->getQuote()->getShippingAddress();

        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm    = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
            ->setEntityType('customer_address')
            ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        if (!empty($customerAddressId)) {
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {
                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }

                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
                $addressForm->setEntity($address);
                $addressErrors  = $addressForm->validateData($address->getData());
                if ($addressErrors !== true) {
                    return array('error' => 1, 'message' => $addressErrors);
                }
            }
        } else {
            $addressForm->setEntity($address);
            // emulate request object
            $addressData    = $addressForm->extractData($addressForm->prepareRequest($data));
            $addressErrors  = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                return array('error' => 1, 'message' => $addressErrors);
            }
            $addressForm->compactData($addressData);
            // unset shipping address attributes which were not shown in form
            foreach ($addressForm->getAttributes() as $attribute) {
                if (!isset($data[$attribute->getAttributeCode()])) {
                    $address->setData($attribute->getAttributeCode(), NULL);
                }
            }

            $address->setCustomerAddressId(null);
            // Additional form data, not fetched by extractData (as it fetches only attributes)
            $address->setSaveInAddressBook(empty($data['save_in_address_book']) ? 0 : 1);
            $address->setSameAsBilling(empty($data['same_as_billing']) ? 0 : 1);
        }

        $address->implodeStreetAddress();
        $address->setCollectShippingRates(true);

        if (($validateRes = $address->validate())!==true) {
            return array('error' => 1, 'message' => $validateRes);
        }

        $this->getQuote()->collectTotals()->save();

        $this->getCheckout()
            ->setStepData('shipping', 'complete', true)
            ->setStepData('shipping_method', 'allow', true);

        return array();
    }
//    public function saveShipping($data, $customerAddressId)
//    {
//        if (empty($data)) {
//            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
//        }
//        $address = $this->getQuote()->getShippingAddress();
//
//        /* @var $addressForm Mage_Customer_Model_Form */
//        $addressForm    = Mage::getModel('customer/form');
//        $addressForm->setFormCode('customer_address_edit')
//            ->setEntityType('customer_address')
//            ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());
//
//        if (!empty($customerAddressId)) {
//            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
//            if ($customerAddress->getId()) {
//                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
//                    return array('error' => 1,
//                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
//                    );
//                }
//
//                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
//                $addressForm->setEntity($address);
//                $addressErrors  = $addressForm->validateData($address->getData());
//                if ($addressErrors !== true) {
//                    return array('error' => 1, 'message' => $addressErrors);
//                }
//            }
//        } else {
//            $addressForm->setEntity($address);
//            // emulate request object
//            $addressData    = $addressForm->extractData($addressForm->prepareRequest($data));
//            $addressErrors  = $addressForm->validateData($addressData);
//            if ($addressErrors !== true) {
//                return array('error' => 1, 'message' => $addressErrors);
//            }
//            $addressForm->compactData($addressData);
//            // unset shipping address attributes which were not shown in form
//            foreach ($addressForm->getAttributes() as $attribute) {
//                if (!isset($data[$attribute->getAttributeCode()])) {
//                    $address->setData($attribute->getAttributeCode(), NULL);
//                }
//            }
//
//            $address->setCustomerAddressId(null);
//            // Additional form data, not fetched by extractData (as it fetches only attributes)
//            $address->setSaveInAddressBook(empty($data['save_in_address_book']) ? 0 : 1);
//            $address->setSameAsBilling(empty($data['same_as_billing']) ? 0 : 1);
//        }
//
//        $address->implodeStreetAddress();
//        $address->setCollectShippingRates(true);
//
//        if (($validateRes = $address->validate())!==true) {
//            return array('error' => 1, 'message' => $validateRes);
//        }
//        if (!$this->getQuote()->getCustomerId() && self::METHOD_REGISTER == $this->getQuote()->getCheckoutMethod()) {
//            if ($this->_customerEmailExists($data['email'], Mage::app()->getWebsite()->getId())) {
//                return array('error' => 1, 'message' => $this->_customerEmailExistsMessage);
//            }
//        }
//
//        $this->getQuote()->collectTotals()->save();
//
//        $this->getCheckout()
//            ->setStepData('shipping', 'complete', true)
//            ->setStepData('shipping_method', 'allow', true);
//
//        return array();
//    }
    public function saveShippingMethod($shippingMethod,$shippingDesc)
    {
        if (empty($shippingMethod)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid shipping method.'));
        }
        $this->getQuote()->getShippingAddress()
            ->setShippingMethod($shippingMethod)
            ->setShippingDescription($shippingDesc);
        Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate')->unsetAddress();
        $this->getCheckout()
            ->setStepData('shipping_method', 'complete', true)
            ->setStepData('payment', 'allow', true);

        return array();
    }
}