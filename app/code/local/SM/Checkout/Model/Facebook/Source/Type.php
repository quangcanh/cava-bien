<?php

class SM_Checkout_Model_Facebook_Source_Type
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'by_percent', 'label'=>Mage::helper('sm_checkout')->__('By Percentage of the Original Price')),
            array('value' => 'by_fixed', 'label'=>Mage::helper('sm_checkout')->__('By Fixed Amount')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'by_percent' => Mage::helper('sm_checkout')->__('By Percentage of the Original Price'),
            'by_fixed' => Mage::helper('sm_checkout')->__('By Fixed Amount'),
        );
    }

}