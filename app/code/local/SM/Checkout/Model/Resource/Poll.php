<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/1/14
 * Time: 3:37 PM
 */
class SM_Checkout_Model_Resource_Poll extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_checkout/poll', 'id');
    }
}