<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:03 PM
 */
class SM_Checkout_Model_Resource_Zipcode extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_checkout/zipcode', 'id');
    }
}