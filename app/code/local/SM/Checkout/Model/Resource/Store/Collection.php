<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:05 PM
 */
class SM_Checkout_Model_Resource_Store_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_checkout/store');
    }
}