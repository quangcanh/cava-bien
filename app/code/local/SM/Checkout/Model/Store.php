<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:03 PM
 */
class SM_Checkout_Model_Store extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_checkout/store');
    }

    /**
     * Reset all model data
     *
     * @return SM_Checkout_Model_Store
     */
    public function reset()
    {
        $this->setData(array());
        $this->setOrigData();
        $this->_attributes = null;

        return $this;
    }
}