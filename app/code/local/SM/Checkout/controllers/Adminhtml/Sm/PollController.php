<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/1/14
 * Time: 4:49 PM
 */
class SM_Checkout_Adminhtml_Sm_PollController extends Mage_Adminhtml_Controller_action
{
    /**
     * Init layout, menu and breadcrumb
     *
     * @return SM_Checkout_AdminHtml_Sm_Poll
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Recommendations'), $this->__('Recommendations'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Sales'))
            ->_title($this->__('SM Checkout'))
            ->_title($this->__('Recommendations'));
        $this->_initAction()
            ->renderLayout();
    }
}