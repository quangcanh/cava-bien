<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:49 PM
 */
class SM_Checkout_Adminhtml_Sm_ZipcodeController extends Mage_Adminhtml_Controller_action
{
    /**
     * Init layout, menu and breadcrumb
     *
     * @return SM_Checkout_Adminhtml_Sm_ZipcodesController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/sm/zipcodes')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Manage Zipcodes'), $this->__('Manage Zipcodes'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('SM Checkout'))
            ->_title($this->__('Store Locator'))
            ->_title($this->__('Manage Zipcodes'));
        $this->_initAction()
            ->renderLayout();
    }

    public function massDeleteAction()
    {
        $zipcodeIds = $this->getRequest()->getParam('sm_zipcode_id');
        if(!is_array($zipcodeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select zip code(s).'));
        } else {
            try {
                $zipcode = Mage::getModel('sm_checkout/zipcode');
                foreach ($zipcodeIds as $zipcodeId) {
                    $zipcode->reset()
                        ->load($zipcodeId)
                        ->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were deleted.', count($zipcodeIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'sm_zipcode.csv';
        $content    = $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_grid')
            ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName   = 'sm_zipcode.xml';
        $content    = $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_grid')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Sm_ZipCode edit action
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sm_checkout/zipcode')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('sm_zipcode_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('sm_checkout/index');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('SM Zip Code Manager'), Mage::helper('adminhtml')->__('SM Zip Code Manager'));

            $this->_addContent($this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_edit'))
                ->_addLeft($this->getLayout()->createBlock('sm_checkout/adminhtml_sm_zipcode_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_checkout')->__('Zip Code does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Create new sm_zipcode action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    // save action
    public function saveAction() {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('sm_checkout/zipcode');
                if ($this->getRequest()->getParam('zipcode_id')) {
                    $model->load($this->getRequest()->getParam('zipcode_id'));
                }
                $model->setZipCode($postData['zip_code']);
                $model->setDescription($postData['description']);
                $model->setTitle($postData['store_ids']);
                // Set product ids for customer
                if (isset($postData['zipcode_stores'])) {
                    $stores = array();
                    parse_str($postData['zipcode_stores'], $stores);
                    $stores = implode(',', array_keys($stores));
                    $model->setData('store_ids', $stores);
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sm_checkout')->__('SM Zip Code was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($postData);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_checkout')->__('Unable to find zip code to save'));
        $this->_redirect('*/*/');

    }

    // delete action
    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $id = $this->getRequest()->getParam('id');
                $model = Mage::getModel('sm_checkout/zipcode')->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Zip Code was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function storeAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $model = Mage::getModel('sm_checkout/zipcode')->load($id);
            Mage::register('sm_zipcode_data', $model);
        }

        $this->loadLayout();
        $this->renderLayout();
    }
}