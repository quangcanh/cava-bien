<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 8/6/14
 * Time: 4:49 PM
 */
class SM_Checkout_Adminhtml_Sm_StoreController extends Mage_Adminhtml_Controller_action
{
    /**
     * Init layout, menu and breadcrumb
     *
     * @return SM_Checkout_Adminhtml_Sm_StoresController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/sm/store')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Manage Stores'), $this->__('Manage Stores'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('SM Checkout'))
            ->_title($this->__('Store Locator'))
            ->_title($this->__('Manage Stores'));
        $this->_initAction()
            ->renderLayout();
    }

    public function massDeleteAction()
    {
        $storeIds = $this->getRequest()->getParam('sm_store_id');
        if(!is_array($storeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select store(s).'));
        } else {
            try {
                $store = Mage::getModel('sm_checkout/store');
                foreach ($storeIds as $storeId) {
                    $store->reset()
                        ->load($storeId)
                        ->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were deleted.', count($storeIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'sm_stores.csv';
        $content    = $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_store_grid')
            ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName   = 'sm_stores.xml';
        $content    = $this->getLayout()->createBlock('sm_checkout/adminhtml_sm_store_grid')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Sm_Store edit action
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('sm_checkout/store')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('sm_store_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('sm_checkout/index');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('SM Store Manager'), Mage::helper('adminhtml')->__('SM Store Manager'));

            $this->_addContent($this->getLayout()->createBlock('sm_checkout/adminhtml_sm_store_edit'))
                ->_addLeft($this->getLayout()->createBlock('sm_checkout/adminhtml_sm_store_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_checkout')->__('Store does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     * Create new sm_store action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    // save action
    public function saveAction() {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $vendorsModel = Mage::getModel('sm_checkout/store');
                if ($this->getRequest()->getParam('id')) {
                    $vendorsModel->load($this->getRequest()->getParam('id'));
                }
                $vendorsModel->setStoreCode($postData['store_code']);
                $vendorsModel->setTitle($postData['title']);
                $vendorsModel->setDescription($postData['description']);
                $vendorsModel->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sm_checkout')->__('SM Store was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $vendorsModel->getId()));
                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($postData);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_checkout')->__('Unable to find store to save'));
        $this->_redirect('*/*/');

    }

    // delete action
    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $id = $this->getRequest()->getParam('id');
                $model = Mage::getModel('sm_checkout/store')->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Store was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
}