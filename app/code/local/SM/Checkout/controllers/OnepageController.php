<?php
/**
 * Created by JetBrains PhpStorm.
 * User: DucThang
 * Date: 7/9/14
 * Time: 10:54 AM
 */
require_once(Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php');
class SM_Checkout_OnepageController extends Mage_Checkout_OnepageController
{
    public $shipping_method;
    public $shipping_desc;
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    private function _processData()
    {
        $result = false;
        $type = $this->getRequest()->getPost('type');
        switch ($type) {
            case 'save_coupon':
                $this->_saveCoupon(true);
                break;
            case 'remove_coupon':
                $this->_saveCoupon(false);
                break;
            case 'apply_giftcode':
                $this->_applyGiftcode();
                break;
            case 'check_giftcode':
                $this->_checkGiftcode();
                break;
            case 'remove_giftcode':
                $this->_removeGiftcode();
                break;
            case 'apply_facebook_code':
                $result = $this->_applyFacebookCode();
                break;
            case 'apply_reward_points':
                $this->_applyRewardPoints();
                break;
            case 'remove_reward_points':
                $this->_removeRewardPoints();
                break;
            default:
                break;
        }
        return $result;
    }

    private function _saveCoupon($flag)
    {
        /**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getQuote()->getItemsCount()) {
            return;
        }
        $data = $this->getRequest()->getPost('order');
        if ($flag === false) {
            $couponCode = '';
        } elseif (isset($data) && isset($data['coupon']['code'])) {
            $couponCode = trim($data['coupon']['code']);
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                } else {
                    $this->_getSession()->addError(
                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }
    }

    /**
     * Add Gift Card to current quote
     *
     */
    private function _applyGiftcode()
    {
        $data = $this->getRequest()->getPost('order');
        if (isset($data) && isset($data['giftcard']['code'])) {
            $code = trim($data['giftcard']['code']);
            try {
                if (strlen($code) > Enterprise_GiftCardAccount_Helper_Data::GIFT_CARD_CODE_MAX_LENGTH) {
                    Mage::throwException(Mage::helper('enterprise_giftcardaccount')->__('Wrong gift card code.'));
                }
                Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
                    ->loadByCode($code)
                    ->addToCart();
                $this->_getQuote()->collectTotals()
                    ->save();
                $this->_getSession()->addSuccess(
                    $this->__('Gift Card "%s" was added.', Mage::helper('core')->escapeHtml($code))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::dispatchEvent('enterprise_giftcardaccount_add', array('status' => 'fail', 'code' => $code));
                $this->_getSession()->addError(
                    $e->getMessage()
                );
            } catch (Exception $e) {
                $this->_getSession()->addException($e, $this->__('Cannot apply gift card.'));
            }
        }
    }

    /**
     * Check a gift card account availability
     *
     */
    private function _checkGiftcode()
    {
        $data = $this->getRequest()->getPost('order');
        $giftcardCode = '';
        if (isset($data) && isset($data['giftcard']['code'])) {
            $giftcardCode = trim($data['giftcard']['code']);
        }
        /* @var $card Enterprise_GiftCardAccount_Model_Giftcardaccount */
        $card = Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
            ->loadByCode($giftcardCode);
        Mage::register('current_giftcardaccount', $card);

        if (!$card->getId()) {
            $this->_getSession()->addError($this->__('Wrong or expired Gift Card Code.'));
        }

        try {
            $card->isValid(true, true, true, false);
        } catch (Mage_Core_Exception $e) {
            $card->unsetData();
        }
    }

    private function _removeGiftcode()
    {
        $data = $this->getRequest()->getPost('order');
        if (isset($data) && isset($data['giftcard']['code'])) {
            $code = trim($data['giftcard']['code']);
            try {
                Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
                    ->loadByCode($code)
                    ->removeFromCart();
                $this->_getSession()->addSuccess(
                    $this->__('Gift Card "%s" was removed.', Mage::helper('core')->escapeHtml($code))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError(
                    $e->getMessage()
                );
            } catch (Exception $e) {
                $this->_getSession()->addException($e, $this->__('Cannot remove gift card.'));
            }
        }
    }

    /*
     * Apply facebook-code for current order
     */
    protected function _applyFacebookCode()
    {
        $data = $this->getRequest()->getPost('order');
        $shared = false;
        if (isset($data) && isset($data['facebook']['code'])) {
            $shared = $data['facebook']['code'];
        }
        $quote = $this->_getQuote();
        if ($shared && !$quote->getFacebookIsApplyAmount()) {
            try {
                $quote->setFacebookIsApplyAmount(1);
                $quote->collectTotals()
                    ->save();
                return false;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot apply the facebook code.'));
                Mage::logException($e);
            }
        } elseif (!$shared) {
            $this->_getSession()->addError($this->__('Please click share button to get sale off.'));
            return true;
        } elseif ($quote->getFacebookIsApplyAmount()) {
            $this->_getSession()->addError($this->__('Your already get sale off.'));
            return true;
        }
    }

    /*
     * Apply reward points
     */
    private function _applyRewardPoints()
    {
        $pointsValue = 0;
        $data = $this->getRequest()->getPost('order');
        if (isset($data) && isset($data['points'])) {
            $pointsValue = $data['points'];
        }
        if (Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId())) {
            if ((int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId()) < $pointsValue) {
                $pointsMax = (int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId());
                $this->_getSession()->addError($this->__('You tried to use %s loyalty points, but you can use a maximum of %s points per shopping cart.', $pointsValue, $points_max));
                $pointsValue = $pointsMax;
            }
        }
        try {
            $quote = $this->_getQuote();
            Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
            Mage::getSingleton('rewardpoints/session')->setShippingChecked(0);
            Mage::helper('rewardpoints/event')->setCreditPoints($pointsValue);
            $quote->setRewardpointsQuantity($pointsValue)
                ->collectTotals()
                ->save();
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
    }

    /*
     *
     * */

    private function _removeRewardPoints()
    {
        Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
        Mage::helper('rewardpoints/event')->setCreditPoints(0);
        $this->_getQuote()
            ->setRewardpointsQuantity(NULL)
            ->setRewardpointsDescription(NULL)
            ->setBaseRewardpoints(NULL)
            ->setRewardpoints(NULL)
            ->collectTotals()
            ->save();
    }

    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->getOnepage()->getQuote();
    }

    /**
     * Checkout page
     * Init message of 'checkout/session'
     */
    public function indexAction()
    {
        if (!Mage::helper('checkout')->canOnepageCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
            $this->_redirect('checkout/cart');
            return;
        }
        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
        $this->getOnepage()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session')
            ->_initLayoutMessages('checkout/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
        $this->renderLayout();
    }

    /**
     * DeliveryOption save action
     */
    public function saveDeliveryOptionAction()
    {
        // save shipping method
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $data2 = $this->getRequest()->getPost('desc_'.$data, '');
            Mage::getSingleton('core/session')->setShipping($data);
            Mage::getSingleton('core/session')->setShippingDesc($data2);
            $result = $this->getOnepage()->saveShippingMethod($data,$data2);
            // $result will contain error data if shipping method is empty
            if (!$result) {
                Mage::dispatchEvent(
                    'checkout_controller_onepage_save_shipping_method',
                    array(
                        'request' => $this->getRequest(),
                        'quote' => $this->getOnepage()->getQuote()));
                $this->getOnepage()->getQuote()->collectTotals();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

                $result['goto_section'] = 'payment_option';
                $result['update_section'] = array(
                    'name' => 'payment-method',
                    'html' => $this->_getPaymentMethodsHtml()
                );


            }

            $this->getOnepage()->getQuote()->collectTotals()->save();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * DeliveryDetail save action
     */
    public function saveDeliveryDetailAction()
    {
        // save billing address
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());

            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['allow_new_account'])) {
                $method = 'guest';
                if ($data['allow_new_account'] === 'on') {
                    $method = 'register';
                }

                $resultNo = $this->getOnepage()->saveCheckoutMethod($method);
                if ($resultNo['error']) {
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($resultNo));
                }
            }
           // $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                if (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
                    $result['goto_section'] = 'delivery_option';
                    $result['allow_sections'] = array('delivery_shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 0 ) {
                    $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
                    $dataship = $this->getRequest()->getPost('shipping', array());
                    $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
                    if($dataship && $dataship['telephone'] != null ) {
                        $result = $this->getOnepage()->saveShipping($dataship, $customerAddressId);
                    }

                    if($data['anorther_address'] == 0) {
                        $result['goto_section'] = 'delivery_option';

                    }


                }
            }
            if ($this->getRequest()->getPost('delivery_email')) {
                $resultNo = $this->_saveDeliveryNotification($this->getRequest()->getPost('delivery_email'), $this->getRequest()->getPost('delivery_phone'));
                if ($resultNo['error']) {
                    $result = $resultNo;
                }
            }


            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }


    private  function _saveDeliveryNotification($email, $phone_number = null)
    {
        $result['error'] = false;
        $session            = Mage::getSingleton('core/session');
        $customerSession    = Mage::getSingleton('customer/session');
        try {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $result['error'] = true;
                $result['message'] = $this->__('Please enter a valid email address.');
            }

            if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                !$customerSession->isLoggedIn()
            ) {
                $result['error'] = true;
                $result['message'] = $this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl());
            }

            $ownerId = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email)
                ->getId();
            if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                $result['error'] = true;
                $result['message'] = $this->__('This email address is already assigned to another user.');
            }

            $status = Mage::getModel('newsletter/subscriber')->subscribe($email);

            // Save phone number
            $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
            $subscriber->setSubscriberPhone($phone_number)
                ->save();

        } catch (Mage_Core_Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('There was a problem with the subscription.'));
        }

        return $result;
    }

    /**
     * PaymentOption save action
     */

    public function savePaymentOptionAction()
    {
        // payment method
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }


            if (!isset($result['error'])) {
                try {
                    $data = $this->getRequest()->getPost('payment', array());
                    $result = $this->getOnepage()->savePayment($data);

                    // get section and redirect data
                    $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
                    if ($redirectUrl) {
                        $result['redirect'] = $redirectUrl;
                    }
                } catch (Mage_Payment_Exception $e) {
                    if ($e->getFields()) {
                        $result['fields'] = $e->getFields();
                    }
                    $result['error'] = $e->getMessage();
                } catch (Mage_Core_Exception $e) {
                    $result['error'] = $e->getMessage();
                } catch (Exception $e) {
                    Mage::logException($e);
                    $result['error'] = $this->__('Unable to set Payment Method.');
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_ajaxRedirectResponse();
            return;
        }
    }


    /**
     *  Load block and update Order
     */
    public function loadBlockAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $onlyMsg = $this->_processData();

        $result = array();
        $blocks = $this->getRequest()->getParam('block');
        $blocks = explode(',', $blocks);
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        foreach ($blocks as $name) {
            $update->load('checkout_onepage_load_' . $name);
        }
        $layout->generateXml();
        $layout->generateBlocks();
        $this->_initLayoutMessages('customer/session')
            ->_initLayoutMessages('checkout/session');
        $allBlock = $layout->getAllBlocks();
        if ($onlyMsg) {
            $result['messages'] = $allBlock['messages']->toHtml();
        } else {
            foreach ($allBlock as $blockName => $block) {
                if ($block->toHtml()) {
                    $result[$blockName] = $block->toHtml();
                } else {
                    $result[$blockName] = '<span style="display: none">There are no messages</span>';
                }

            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function savePollAction()
    {
        if ($this->getRequest()->getPost('sm-poll-choose') !== '') {
            $id = (int)$this->getRequest()->getPost('sm-poll-choose') + 1;
            $poll = Mage::getModel('sm_checkout/poll')->load($id);
            if ($poll->getId()) {
                $poll->setVotesCount((int)$poll->getVotesCount() + 1);
                $poll->save();
            }
        }
        $this->_redirectUrl(Mage::getBaseUrl());
    }
    /**
     * Create order action
     */
    public function saveOrderAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');
            return;
        }

        if ($this->_expireAjax()) {
            return;
        }

        $result = array();
        try {
            $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
            if ($requiredAgreements) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                $diff = array_diff($requiredAgreements, $postedAgreements);
                if ($diff) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            $data = $this->getRequest()->getPost('payment', array());
            if ($data) {
                $data['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                    | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                    | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                    | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
                $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }
            $this->callBackSaveShipping(true);
            $this->getOnepage()->saveOrder();

            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if (!empty($message)) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'payment';
            $result['update_section'] = array(
                'name' => 'payment-method',
                'html' => $this->_getPaymentMethodsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            $gotoSection = $this->getOnepage()->getCheckout()->getGotoSection();
            if ($gotoSection) {
                $result['goto_section'] = $gotoSection;
                $this->getOnepage()->getCheckout()->setGotoSection(null);
            }
            $updateSection = $this->getOnepage()->getCheckout()->getUpdateSection();
            if ($updateSection) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepage()->getCheckout()->setUpdateSection(null);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $this->getOnepage()->getQuote()->save();
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /*
    *
    * Load delivery option ajax
    */
    public function getDeliveryAction()
    {
        $this->loadLayout('delivery_getdelivery');
        $block = $this->getLayout()->getBlock('delivery.option');
        $countryId='';
        $countryId=$this->_request->getParam('countryId');
        $block->setCountryId($countryId);
        $data= array('result'=>$block->toHtml());
        $json=Mage::helper('core')->jsonEncode($data);
        $this->getResponse()->setBody($json);
    }
    public function callBackSaveShipping($addShippingDesc=false)
    {
        $shipping_method=Mage::getSingleton('core/session')->getShipping();
        $shipping_desc=Mage::getSingleton('core/session')->getShippingDesc();
        if($addShippingDesc)
            $shipping_desc=Mage::getSingleton('core/session')->getShippingDescNew();
        $this->getOnepage()->saveShippingMethod($shipping_method,$shipping_desc);
        $this->getOnepage()->getQuote()->collectTotals()->save();
    }
}