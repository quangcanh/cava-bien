<?php
require_once(Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php');
class SM_Checkout_CartController extends Mage_Checkout_CartController
{
    public $response_Html = array();

    /**
     * @return array
     */
    public function getResponseHtml()
    {
        return $this->response_Html;
    }
    public function setResponseHtml($response)
    {
        $this->response_Html = $response;
    }

    public function addAction()
    {
        $response = array(
            'result' => false,
            'msg' => $this->__('Cannot add the item to shopping cart.')
        );
        $this->setResponseHtml($response);
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }
            if ($this->getRequest()->getParam('isSmPro')) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach (Mage::helper('core')->jsonDecode($this->getRequest()->getParam('sm_super_attribute')) as $k => $row) {
                    if ($row) {
                        $product = $this->_initProduct();
                        if (isset($row['qty'])) {
                            $row['qty'] = $filter->filter($row['qty']);
                        }
                        $params['super_attribute'] = $row['super_attribute'];
                        $params['qty'] = $row['qty'];
                        $cart->addProduct($product, $params);
                    }
                }
            }  else {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }
                $cart->addProduct($product, $params);
            }
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponseHtml())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', $product->getName());
//                    $this->_getSession()->addSuccess($message);
                    $response['result'] = true;
                    $response['msg'] = $message;
                    $this->setResponseHtml($response);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
//                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                $response['result'] = false;
                $response['msg'] = $e->getMessage();
                $this->setResponseHtml($response);
                $this->_goBack();
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
//                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                    $response['result'] = false;
                    $response['msg'] .= $message;
                }
            }
            $this->setResponseHtml($response);
        } catch (Exception $e) {
//            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            $response['result'] = false;
            $response['msg'] .= $this->__('Cannot add the item to shopping cart.');
            $this->setResponseHtml($response);
            Mage::logException($e);
            $this->_goBack();
        }
    }

    protected function _goBack()
    {
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($this->getResponseHtml()));
    }
}
