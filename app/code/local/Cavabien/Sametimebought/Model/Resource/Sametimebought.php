<?php

/**
 * Class Cavabien_SametimeBought_Model_Resource_Sametimebought
 */
class Cavabien_Sametimebought_Model_Resource_Sametimebought extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_sametimebought/sametimebought', 'sametimebought_id');
    }
}