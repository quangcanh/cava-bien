<?php

/**
 * Class Cavabien_SametimeBought_Model_Resource_Sametimebought_Collection
 */
class Cavabien_Sametimebought_Model_Resource_Sametimebought_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_sametimebought/sametimebought');
    }
}