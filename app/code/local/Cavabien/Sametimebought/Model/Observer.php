<?php

/**
 * Class Cavabien_SametimeBought_Model_Observer
 */
class Cavabien_Sametimebought_Model_Observer
{
    /**
     * Save same time bought records when customer order more than 2 different products
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveSametimeBought(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $itemCollection = $order->getItemsCollection();
        $numberItems = count($itemCollection);

        // Checking customer buy at least 2 products
        if ($numberItems >= 2) {
            $productIds = array();
            foreach ($itemCollection as $item) {
                $productIds[] = $item->getProductId();
            }

            // Preparing sametime bought data from list products ordered
            for ($i = 0; $i < $numberItems; $i++) {
                for ($j = $i + 1; $j < $numberItems; $j++) {
                    $dataToSave[] = array(
                        'main_product_id'      => $productIds[$i],
                        'secondary_product_id' => $productIds[$j],
                    );
                }
            }

            // Save sametime bought records into database
            /** @var $saveTimeBought Cavabien_SametimeBought_Model_Sametimebought */
            $saveTimeBought = Mage::getModel('cavabien_sametimebought/sametimebought');
            foreach ($dataToSave as $data) {
                $saveTimeBought->add($data);
            }
        }
    }
}