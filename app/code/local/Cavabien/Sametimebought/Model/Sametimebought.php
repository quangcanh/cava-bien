<?php

/**
 * Class Cavabien_SametimeBought_Model_Sametimebought
 */
class Cavabien_Sametimebought_Model_Sametimebought extends Mage_Core_Model_Abstract
{
    /**
     * Resource model initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_sametimebought/sametimebought');
    }

    /**
     * Insert ignore sametime bought record into database
     *
     * @param $data
     */
    public function add($data)
    {
        $coreResource = Mage::getSingleton('core/resource');
        $tableName = $coreResource->getTableName('cavabien_sametimebought/sametimebought');
        $writer = $coreResource->getConnection('core_write');
        $sql = "INSERT IGNORE INTO {$tableName}
                (main_product_id, secondary_product_id)
                VALUES ({$data['main_product_id']}, {$data['secondary_product_id']}),
                ({$data['secondary_product_id']}, {$data['main_product_id']});";
        $writer->query($sql);
    }
}