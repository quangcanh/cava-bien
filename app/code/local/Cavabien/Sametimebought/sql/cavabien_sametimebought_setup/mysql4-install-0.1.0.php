<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_sametimebought/sametimebought')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_sametimebought/sametimebought')} (
      `sametimebought_id` int(10) unsigned NOT NULL auto_increment,
      `main_product_id` int(10) unsigned NOT NULL,
      `secondary_product_id` varchar(255),
      PRIMARY KEY  (`sametimebought_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('cavabien_sametimebought/sametimebought')} ADD INDEX (`main_product_id`, `secondary_product_id`);

ALTER TABLE {$this->getTable('cavabien_sametimebought/sametimebought')} ADD CONSTRAINT main_secondary UNIQUE (`main_product_id`, `secondary_product_id`);
"
);

$installer->endSetup();