<?php

/**
 * Class Cavabien_Sametimebought_Block_List
 */
class Cavabien_Sametimebought_Block_List extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Get sametime bought products list
     *
     * @param $productIds
     *
     * @return mixed
     */
    public function getSametimeBoughtList($productIds)
    {
        $result = array();
        $productIds = implode(', ', $productIds);
        $coreResource = Mage::getSingleton('core/resource');
        $tableName = $coreResource->getTableName('cavabien_sametimebought/sametimebought');
        $reader = $coreResource->getConnection('core_read');
        $sql = "SELECT secondary_product_id FROM {$tableName}
                WHERE main_product_id IN ({$productIds});";
        $productIdsFetched = $reader->fetchCol($sql);
        if (!empty ($productIdsFetched)) {
            foreach ($productIdsFetched as $productId) {
                $product = Mage::getModel('catalog/product');
                $result[] = $product->load($productId);
            }
        }

        return $result;
    }
}
 