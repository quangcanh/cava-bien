<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 24/07/2014
 * Time: 10:34
 * To change this template use File | Settings | File Templates.
 */ 
class Cavabien_Groupedconfigurable_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View {

    public function getAssociatedProducts()
    {
        return $this->getProduct()->getTypeInstance(true)
            ->getAssociatedProducts($this->getProduct());
    }

    /**
     * Set preconfigured values to grouped associated products
     *
     * @return Mage_Catalog_Block_Product_View_Type_Grouped
     */
    public function setPreconfiguredValue() {
        $configValues = $this->getProduct()->getPreconfiguredValues()->getSuperGroup();
        if (is_array($configValues)) {
            $associatedProducts = $this->getAssociatedProducts();
            foreach ($associatedProducts as $item) {
                if (isset($configValues[$item->getId()])) {
                    $item->setQty($configValues[$item->getId()]);
                }
            }
        }
        return $this;
    }

}