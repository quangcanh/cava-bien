<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 28/07/2014
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
require_once 'Mage/Checkout/controllers/CartController.php';


class Cavabien_Groupedconfigurable_CartController extends Mage_Checkout_CartController {

    public function addgroupedAction()
    {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getPost('data');
        $groupedProductName = $this->getRequest()->getPost('productName');
        try {
            foreach ($params as $productsParam) {
                $paramAttr = array();
                $subArr = array();
                foreach ($productsParam as $param) {
                    if(strpos($param['name'], 'super_attribute') !== false) {
                        $lengthAttr = strlen('super_attribute');
                        $superSubAttr = substr($param['name'], $lengthAttr + 1, -1);
                        $subArr[$superSubAttr] = $param['value'];

                    } else {
                        $paramAttr[$param['name']] = $param['value'];
                    }
                    $paramAttr['super_attribute'] = $subArr;
                }
                $productId = (int) $paramAttr['product'];
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($productId);
                if($product->isSaleable()) {
                    $cart->addProduct($product, $paramAttr);
                }

            }
            $cart->save();
            $this->_getSession()->setCartWasUpdated(true);

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($groupedProductName));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }


    }

    /**
     * Add product to shopping cart action
     *
     * @return Mage_Core_Controller_Varien_Action
     * @throws Exception
     */
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }
            if ($this->getRequest()->getParam('isSmPro')) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach (Mage::helper('core')->jsonDecode($this->getRequest()->getParam('sm_super_attribute')) as $k => $row) {
                    if ($row) {
                        $product = $this->_initProduct();
                        if (isset($row['qty'])) {
                            $row['qty'] = $filter->filter($row['qty']);
                        }
                        $params['super_attribute'] = $row['super_attribute'];
                        $params['qty'] = $row['qty'];
                        $cart->addProduct($product, $params);
                    }
                }
            }  else {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }
                $cart->addProduct($product, $params);
            }

            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if ($params['redirect_url'] == 'buynow') {
                $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                $this->_getSession()->addSuccess($message);
                $this->getResponse()->setRedirect(Mage::getUrl('checkout/onepage'));
            } else {
                if (!$this->_getSession()->getNoCartRedirect(true)) {
                    if (!$cart->getQuote()->getHasError()) {
                        $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                        $this->_getSession()->addSuccess($message);
                    }
                    if (!$this->getRequest()->getParam('isCat')) {
                        $this->_goBack();
                    }
                }
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }
}