<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 9/8/14
 * Time: 11:00 AM
 */ 
class Cavabien_Invitation_Block_Inviter_Step3 extends Cavabien_Invitations_Block_Inviter_Abstract {
    public function __construct(){
        parent::__construct();
        $this->setTemplate('invitations/inviter/step3/default.phtml');
    }

    public function getCurentAddressBook(){
        return $this->getAddressBook();
    }
}