<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 9/8/14
 * Time: 11:41 AM
 */ 
class Cavabien_Invitations_Block_Page extends Plumrocket_Invitations_Block_Page {

    public function getConfig()
    {
        if (is_null($this->_config)){
            $this->_config = Mage::getModel('invitations/config');
        }

        return $this->_config;
    }
}