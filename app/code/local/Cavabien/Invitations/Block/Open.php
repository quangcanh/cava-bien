<?php
/**
 * Created by PhpStorm.
 * User: Leonardo
 * Date: 9/8/14
 * Time: 11:10 AM
 */ 
class Cavabien_Invitations_Block_Open extends Plumrocket_Invitations_Block_Open {
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('myaccount/invitations/open.phtml');
    }

    protected function _beforeToHtml()
    {
        $fbAddressBook = Mage::getModel('invitations/addressbooks')->getByKey('facebook');
        $customerId = $this->getData('customerId');
        $this->_collection = Mage::getModel('invitations/invitations')
            ->getCustomerOpenInvitations($customerId)
            ->addFieldToFilter('addressbook_id', array('neq' => $fbAddressBook->getId()))
//			->addFieldToFilter('deactivated', 0)
            ->addFieldToFilter('website_id', Mage::app()->getWebsite()->getId());

        $this->_pagerBlock = $this->getLayout()->createBlock('page/html_pager');
        $this->_pagerBlock->setCollection($this->_collection);

        $this->setData('invitations', $this->_collection);

        return parent::_beforeToHtml();
    }
}