<?php

/**
 * Class Cavabien_PageNavigation_Block_PageNavigation
 */
class Cavabien_PageNavigation_Block_PageNavigation extends Mage_Core_Block_Template
{
    /**
     * Next type.
     */
    const NEXT = 'next';
    /**
     * Previous type.
     */
    const PREVIOUS = 'previous';
    /**
     * Xml path config enable module
     */
    const XML_PATH_PAGE_NAVIGATION_ENABLE = 'product_page_navigation/configs/enable';
    /**
     * Xml path config number item
     */
    const XML_PATH_PAGE_NAVIGATION_NUMBER_ITEMS = 'product_page_navigation/configs/number_items';

    /**
     * @var Mage_Catalog_Model_Category
     */
    protected $_currentCategory;

    protected $_numberItems;

    /**
     * Class constructor
     * Set current category
     */
    public function _construct()
    {
        $this->_currentCategory = Mage::registry('current_category');
        $this->_numberItems = (int)Mage::getStoreConfig(self::XML_PATH_PAGE_NAVIGATION_NUMBER_ITEMS) ? (int)Mage::getStoreConfig(self::XML_PATH_PAGE_NAVIGATION_NUMBER_ITEMS) : 6 ;
    }

    /**
     * Return true if module is enabled otherwise
     *
     * @return mixed
     */
    public function isEnable()
    {
        return Mage::getStoreConfig(self::XML_PATH_PAGE_NAVIGATION_ENABLE);
    }

    /**
     * Factory method that get next or previous product url
     *
     * @param $productId
     * @param $type
     *
     * @return bool
     */
    public function getPreNextProductUrl($productId, $type)
    {
        $currentProductId = $productId ? $productId : Mage::registry('current_product')->getId();
        $positionData = $this->getProductsPositionData();
        if ($positionData) {
            $productIdToPosition = $positionData['productId_to_position'];
            $positionToProductId = $positionData['position_to_productId'];

            $prevNextPosition = null;
            if ($type === self::PREVIOUS) {
                $prevNextPosition = $productIdToPosition[$currentProductId] - 1;
            } else {
                if ($type === self::NEXT) {
                    $prevNextPosition = $productIdToPosition[$currentProductId] + 1;
                } else {
                    return false;
                }
            }

            if (array_key_exists($prevNextPosition, $positionToProductId)) {
                $prevNextProductId = $positionToProductId[$prevNextPosition];
            }

            $product = Mage::getModel('catalog/product');
            if (!empty($prevNextProductId)) {
                $product->load($prevNextProductId);

                return $product->getProductUrl();
            }

            return false;
        }

        return false;
    }

    /**
     * Get previous product url
     *
     * @param null $currentProductId
     *
     * @return bool
     */
    public function getPreviousProductUrl($currentProductId = null)
    {
        $currentProductId = $currentProductId ? $currentProductId : Mage::registry('current_product')->getId();

        return $this->getPreNextProductUrl($currentProductId, self::PREVIOUS);
    }

    /**
     * Get next product url
     *
     * @param null $currentProductId
     *
     * @return bool
     */
    public function getNextProductUrl($currentProductId = null)
    {
        $currentProductId = $currentProductId ? $currentProductId : Mage::registry('current_product')->getId();

        return $this->getPreNextProductUrl($currentProductId, self::NEXT);
    }

    /**
     * Get group that contain current productId
     *
     * @param null $currentProductId
     *
     * @return array|bool
     */
    public function getCurrentGroup($currentProductId = null)
    {
        $currentProductId = $currentProductId ? $currentProductId : Mage::registry('current_product')->getId();
        $positionData = $this->getProductsPositionData();
        $productIdToPosition = $positionData['productId_to_position'];
        $catArray = Mage::registry('current_category');
        if ($catArray) {
            $catArray = $catArray->getProductsPosition();
            $array = array_keys($catArray);
            $groupSize = ($positionData['number_of_products'] >= $this->_numberItems) ? $this->_numberItems : $positionData['number_of_products'];
            $groups = array_chunk($array, $groupSize, true);
            $currentGroup = array();

            foreach ($groups as $group) {
                if (array_key_exists($productIdToPosition[$currentProductId], $group)) {
                    $currentGroup = $group;
                    break;
                }
            }

            return $currentGroup;
        }

        return false;
    }

    /**
     * Get products position in current category
     */
    public function getProductsPositionData()
    {
        if ($this->_currentCategory) {
            $positionData = $this->_currentCategory->getProductsPosition();

            return array(
                'productId_to_position' => array_flip(array_keys($positionData)),
                'position_to_productId' => array_keys($positionData),
                'number_of_products'    => count($positionData),
            );
        }else{
            foreach(Mage::registry('current_product')->getCategoryIds() as $catId){
                $this->_currentCategory =  Mage::getModel('catalog/category')->load($catId);
                break;
            }
            $positionData = $this->_currentCategory->getProductsPosition();

            return array(
                'productId_to_position' => array_flip(array_keys($positionData)),
                'position_to_productId' => array_keys($positionData),
                'number_of_products'    => count($positionData),
            );
        }

        return false;
    }

    /**
     * Get data for navigation block
     *
     * @return array|bool
     */
    public function getNavigationData()
    {
        $result = array();
        $currentProductId = Mage::registry('current_product')->getId();
        $positionData = $this->getProductsPositionData();
        $productIdToPosition = $positionData['productId_to_position'];
        $positionToProductId = $positionData['position_to_productId'];
        $currentGroup = $this->getCurrentGroup();
        if (count($currentGroup) < $this->_numberItems) {
            // Get 6 last items form products list
            $offset = $positionData['number_of_products'] - $this->_numberItems - 1;
            $currentGroup = array_slice($positionToProductId, $offset, $this->_numberItems);
        }

        foreach ($currentGroup as $productId) {
            $product = Mage::getModel('catalog/product');
            $result['products'][] = $product->load($productId);
        }

        $result['previous'] = $productIdToPosition[$currentProductId] == 0 ? null : $this->getPreviousProductUrl();
        $result['next'] = $productIdToPosition[$currentProductId] == ($positionData['number_of_products']
            - 1) ? null : $this->getNextProductUrl();
        $result['current_product']  = $currentProductId;
        $result['current_category'] = $this->_currentCategory;

        return $result;
    }
}