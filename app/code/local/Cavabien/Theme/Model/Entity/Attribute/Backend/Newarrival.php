<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Model_Entity_Attribute_Backend_Newarrival extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Prepare data for save
     *
     * @param Varien_Object $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
     */
    public function beforeSave($object)
    {
        $object->setData('new_arrival', $object->getData('created_at'));
        return parent::beforeSave($object);
    }
}