<?php

/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cavabien_Theme_Model_Layer_Filter_Category extends GoMage_Navigation_Model_Layer_Filter_Category
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setData('show_category_filter',true);
        $this->_requestVar = 'supercat';
    }

    protected function _getItemsData()
    {
        $key = $this->getLayer()->getStateKey() . '_SUBCATEGORIES';
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $_productCollection = $this->getLayer()->getProductCollection();
            $catArr = array();
            foreach($_productCollection as $_product){
                $categoryIds = $_product->getCategoryIds();
                if(is_array($categoryIds)){
                    foreach($categoryIds as $catId){
                        if(!in_array($catId,$catArr)){
                            array_push($catArr,$catId);
                        }
                    }
                }
            }
            $cats_ids_str = $this->getFilterableCategoriesIds();
            $cats_ids_arr = explode(',',$cats_ids_str);
            foreach($catArr as $key => $catIdFilter){
                if(!in_array($catIdFilter,$cats_ids_arr)){
                    unset($catArr[$key]);
                }
            }
            $cat_ids = implode(',',$catArr);
            $categories = Mage::getResourceModel('catalog/category_collection');
            /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
            $categories->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('all_children')
                ->addAttributeToSelect('level')
                ->addAttributeToSelect('is_anchor')
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToSelect('filter_image')
                ->addIdFilter($cat_ids)
                ->joinUrlRewrite()
                ->load();

            $category_list_ids = array();
            foreach ($categories as $category) {
                $category_list_ids[$category->getId()] = $category;
            }

//            $category = $this->getLayer()->getCurrentCategory();
            $cat = Mage::app()->getStore()->getRootCategoryId();
            $category = Mage::getModel('catalog/category')->load($cat);
            $categories = $category->getChildrenCategories();

            $categories->addAttributeToSelect('*');
            foreach ($categories as $_category) {
                $this->_renderCategoryList($_category, $category_list_ids);
            }

            $selected = array();

            if ($value = Mage::app()->getFrontController()->getRequest()->getParam($this->_requestVar)) {
                $selected = array_merge($selected, explode(',', $value));
            }

            $data = array();

            $filter_mode = Mage::helper('gomage_navigation')->isGomageNavigation();

            if (count($this->category_list) > 0) {

                if (Mage::helper('gomage_navigation')->isEnterprise()) {
                    $isCatalog = is_null(Mage::app()->getFrontController()->getRequest()->getParam('q'));

                    $helper = Mage::helper('enterprise_search');
                    if (!$isCatalog && $helper->isThirdPartSearchEngine() && $helper->getIsEngineAvailableForNavigation($isCatalog) && Mage::helper('gomage_navigation')->isGomageNavigation()) {
                        $productCollection = $this->getLayer()->getProductCollection();
                        $category_count = $productCollection->getFacetedData('category_ids');
                    } else {
                        $category_count = $this->_getResource()->getCount($this, $this->category_list);
                    }
                } else {
                    $category_count = $this->_getResource()->getCount($this, $this->category_list);
                }

                foreach ($this->category_list as $category) {

                    if ($category->getIsActive()) {

                        if (in_array($category->getId(), $selected) && !$filter_mode) {
                            continue;
                        }

                        if (Mage::getStoreConfig('gomage_navigation/category/hide_empty') && !isset($category_count[$category->getId()])) {
                            continue;
                        }

                        if (in_array($category->getId(), $selected) && $filter_mode) {

                            $active = true;

                            $value = $category->getId();

                        } else {

                            $active = false;

                            if (!empty($selected)) {

                                $value = $this->_prepareRequestValue($selected, $category);
                                $value = implode(',', $value);

                            } else {

                                $value = $category->getId();

                            }

                        }

                        $data[] = array(
                            'label' => Mage::helper('core')->htmlEscape($category->getName()),
                            'value' => $value,
                            'count' => isset($category_count[$category->getId()]) ? $category_count[$category->getId()] : 0,
                            'active' => $active,
                            'image' => $category->getFilterImage(),
                            'level' => $category->getLevel(),
                            'haschild' => $category->getChildren(),
                        );
                    }
                }


            }

            $tags = $this->getLayer()->getStateTags();

            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);


        }
        return $data;
    }

    /**
     * @return string
     */
    protected function getFilterableCategoriesIds()
    {
        $cat = Mage::app()->getStore()->getRootCategoryId();

        $category = Mage::getModel('catalog/category')->load($cat);

        if (Mage::getStoreConfigFlag('gomage_navigation/category/show_allsubcats')) {

            $cats_ids = array_diff($category->getAllChildren(true), array($category->getId()));

            if (count($cats_ids)) {
                $cats_ids_str = implode(',', $cats_ids);
            } else {
                $cats_ids_str = '0';
            }

        } else {
            $cats_ids = array();
            if ($category->getChildren()) {
                $cats_ids = explode(',', $category->getChildren());
            }

            $cats_ids_str = '0';

            foreach ($cats_ids as $_id) {
                $cats_ids_str .= ',' . $this->_addChildsCategory($_id, explode(',', Mage::app()->getFrontController()->getRequest()->getParam($this->_requestVar)));
            }

            foreach (explode(',', Mage::app()->getFrontController()->getRequest()->getParam($this->_requestVar)) as $_id) {
                $_cat = Mage::getModel('catalog/category')->load($_id);
                $cats_ids_str .= ',' . $_cat->getId();
                if ($_cat->getChildren()) {
                    $cats_ids_str .= ',' . $_cat->getChildren();
                }
                $_parent_cat = Mage::getModel('catalog/category')->load($_cat->getParentId());
                if ($_parent_cat->getChildren()) {
                    $cats_ids_str .= ',' . $_parent_cat->getChildren();
                }
                while ($category->getLevel() < $_parent_cat->getLevel()) {
                    $cats_ids_str .= ',' . $_parent_cat->getId();
                    $_parent_cat = Mage::getModel('catalog/category')->load($_parent_cat->getParentId());
                }
            }
        }

        return $cats_ids_str;

    }
}