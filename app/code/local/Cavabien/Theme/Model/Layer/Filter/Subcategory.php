<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Model_Layer_Filter_Subcategory extends GoMage_Navigation_Model_Layer_Filter_Category
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setData('show_sub_category_filter',true);
        $this->_requestVar = 'subcat';
    }
}