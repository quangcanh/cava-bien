<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cavabien_Theme_Model_Conf_Observer extends Amasty_Conf_Model_Observer
{

    public function onListBlockHtmlBefore($observer)//core_block_abstract_to_html_after
    {
        if (($observer->getBlock() instanceof Mage_Catalog_Block_Product_List) && Mage::getStoreConfig('amconf/list/enable_list')) {
            $html = $observer->getTransport()->getHtml();
            preg_match_all("/product-price-([0-9]+)/", $html, $productsId) ;
            if(!$productsId[0]){
                preg_match_all("/price-including-tax-([0-9]+)/", $html, $productsId) ;
            }

            $productsId[1] = array_unique($productsId[1]);

            foreach ($productsId[1] as $key => $productId){
                $_product = Mage::getModel('catalog/product')->load($productId);
                // @see Mage_Catalog_Block_Product_Abstract::getProduct()
                if (!is_null(Mage::registry('product')))
                {
                    Mage::unregister('product');
                }
                Mage::register('product', $_product);
                if($_product->isSaleable() && $_product->isConfigurable()){
                    $addButton = (Mage::getStoreConfig('amconf/product_image_size/have_button'))?"(.*?)/button>":"";
                    $template = '@(product-'.$productId.'">(.*?)div>)' . $addButton . '@s';
                    preg_match_all($template, $html, $res);
                    if(!$res[0]){
                        $template = '@(price-including-tax-'.$productId.'">(.*?)div>)' . $addButton . '@s';
                        preg_match_all($template, $html, $res);
                        if(!$res[0]){
                            $template = '@(price-excluding-tax-'.$productId.'">(.*?)div>)' . $addButton . '@s';
                            preg_match_all($template, $html, $res);
                        }
                    }
                    if($res[1]){
                        $replace =  Mage::helper('amconf')->getHtmlBlock($_product, $res[1][0]);
                        unset($_product);
                        if(strpos($html, $replace) === false) {
                            $html= str_replace($res[1][0], $replace, $html);
                        }
                        unset($replace);
                    }
                }
            }
            $observer->getTransport()->setHtml($html);
        }
    }
}