<?php

/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cavabien_Theme_Model_Observer extends Devinc_Multipledeals_Model_Observer
{

    const VIEW_ALL_LABEL = 'VIEW ALL';

    public function afterReindexProcessCatalogProductAttribute()
    {
        // Update category has discounts-and-offers in url_key
        $this->_updateFilterForCategory('discount_offer', 'discounts-and-offers');
        // Update category has stylist-picks in url_key
        $this->_updateFilterForCategory('stylist_pick', 'stylist-picks');
    }

    /**
     * This will find all category has url key like %urlKey%,
     * look up its child categories and compare those child categories
     * name with attribute options then update that attribute
     * for all product inside each child categories
     *
     * @param $attributeCode string
     * @param $urlKey string
     */
    protected function _updateFilterForCategory($attributeCode, $urlKey)
    {

        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
        /** @var $attribute Mage_Eav_Model_Entity_Attribute */
        $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setAttributeFilter($attributeModel->getId())
            ->setStoreFilter(0, false); // admin store

        // Contain all attribute option
        $result = array();
        foreach ($valuesCollection as $valueItem) {
            $result[$valueItem->getOptionId()] = $valueItem->getValue();
        }

        // Get all product which belong to category
        $categoryCollection = Mage::getModel('catalog/category')->getCollection();
        $categoryCollection->addFieldToFilter('url_key', array('like' => '%' . $urlKey . '%'));

        /**@var $category Mage_Catalog_Model_Category */
        $allValues = array_search(self::VIEW_ALL_LABEL, $result) ? array_search(self::VIEW_ALL_LABEL, $result) : 0;

        foreach ($categoryCollection as $category) {
            if ($category->hasChildren()) {
                foreach ($category->getChildrenCategories() as $subCategory) {
                    $id = array_search($subCategory->getName(), $result);

                    if (!empty($id)) {
                        $productCollection = $subCategory->getProductCollection();
                        $productCollection->addAttributeToSelect($attributeCode);

                        foreach ($productCollection as $product) {
                            $discountOffer = $product->getData($attributeCode);
                            if (!empty($discountOffer)) {
                                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array($attributeCode => $discountOffer . ',' . $id), 0);
                            } else {
                                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array($attributeCode => $id . ',' . $allValues), 0);
                            }
                        }
                    }
                }
            }
        }
    }

    /***
     * Rebuild toprate data for product
     *
     */
    public function buildToprateData()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $storeCollection = Mage::getModel('core/store')->getCollection();
        foreach ($productCollection as $product) {
            foreach ($storeCollection as $store) {
                $summaryData = Mage::getModel('review/review_summary')
                    ->setStoreId($store->getId())
                    ->load($product->getId());
                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('top_rate' => $summaryData->getData('rating_summary')), $store->getId());
            }
        }
    }

    public function buildBestsellerData()
    {
        $resource = Mage::getSingleton('core/resource');
        $zennDb = $this->_getZendDb();
        $select = $zennDb->select()
            ->from($resource->getTableName('sales/order_item'), array('product_id', 'store_id', 'sell_qty' => 'sum(qty_ordered)'));
        $select->group('product_id')
            ->group('store_id');
        $stmt = $zennDb->query($select);
        $result = $stmt->fetchAll();

        foreach ($result as $item) {
            $item = new Varien_Object($item);
            Mage::getSingleton('catalog/product_action')->updateAttributes(array($item->getData('product_id')), array('top_rate' => $item->getData('sell_qty')), $item->getData('store_id'));
        }
    }

    public function _getZendDb()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');
        $zendDb = Zend_Db::factory('Pdo_Mysql', $writeAdapter->getConfig());
        return $zendDb;
    }
    public function setCollectionFinalPrice($observer)
    {
        $products = $observer->getEvent()->getCollection();
//        $currentDateTime = Mage::helper('multipledeals')->getCurrentDateTime(0);
        foreach ($products as $product) {
            $helper = Mage::helper('multipledeals');
            $deal = $helper->getDealByProduct($product);

            if ($helper->isEnabled() && $deal) {
                $setPriceForType = array('simple', 'virtual', 'downloadable', 'configurable');
//                if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
                    if (in_array($product->getTypeId(), $setPriceForType)) {
                        $price = $this->_applyTierPrice($product, null, $deal->getDealPrice());
                        $product->setFinalPrice($price);
                    }
//                } else {
//                    Mage::getModel('multipledeals/multipledeals')->refreshDeal($deal);
//                }
            }
        }
    }
}