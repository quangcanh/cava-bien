<?php
class Cavabien_Theme_Block_Multipledeals_Page extends Mage_Catalog_Block_Product_List
{
    const STATUS_RUNNING = Devinc_Multipledeals_Model_Source_Status::STATUS_RUNNING;
    const STATUS_DISABLED = Devinc_Multipledeals_Model_Source_Status::STATUS_DISABLED;
    const STATUS_ENDED = Devinc_Multipledeals_Model_Source_Status::STATUS_ENDED;
    const STATUS_QUEUED = Devinc_Multipledeals_Model_Source_Status::STATUS_QUEUED;
    protected $_dealsCollection;

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getDealProductCollection($loadRecent = false, $cat)
    {
        $pageTypeDeals = Mage::registry('pageTypeDeals');
        if (!$this->_dealsCollection) {
            $dealStatus = ($loadRecent) ? self::STATUS_ENDED : self::STATUS_RUNNING;
            if(isset($pageTypeDeals) && $pageTypeDeals == 'comesoon'){
                $dealStatus = ($loadRecent) ? self::STATUS_ENDED : self::STATUS_QUEUED;
            }


            //load active deal ids
            $dealCollection = Mage::getModel('multipledeals/multipledeals')->getCollection()->addFieldToFilter('status', array('eq'=>$dealStatus))->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');
//            foreach($dealCollection as $row){
//                echo $row->getDatetime_to();
//                echo $row->getDatetime_from();
//            }
            $productIds = array(0);

            if (count($dealCollection)) {
                $dealIds = array();
                foreach ($dealCollection as $deal) {
                    if (Mage::helper('multipledeals')->runOnStore($deal) && !array_key_exists($deal->getProductId(), $dealIds)) {
                        $dealIds[$deal->getProductId()] = $deal->getId();
                        $productIds[] = $deal->getProductId();
                    }
                }

                if (count($dealIds)) {
                    $dealIdsString = implode(',', $dealIds);
                } else {
                    $dealIdsString = 0;
                }
            } else {
                $dealIdsString = 0;
            }

            //load product collection + deals info
            $resource = Mage::getSingleton('core/resource');
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToFilter('entity_id', array('in', $productIds))
                ->addAttributeToFilter('status', array('in'=>array(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)))
                ->addStoreFilter()
                ->joinField('category_id', 'catalog/category_product',
                    'category_id', 'product_id = entity_id', null, 'left')
                ->joinTable($resource->getTableName('multipledeals'),'product_id=entity_id', array('multipledeals_id' => 'multipledeals_id', 'position' => 'position', 'deal_qty' => 'deal_qty', 'datetime_to' => 'datetime_to', 'datetime_from' => 'datetime_from'), '{{table}}.multipledeals_id IN ('.$dealIdsString.')','left');
            $collection->addAttributeToFilter('category_id', $cat);
            //deals end soon
            if(isset($pageTypeDeals) && $pageTypeDeals == 'endsoon'){
                $today = time();
                $tomorrow = $today + (60*60*24);
                $from = date("Y-m-d h:i:s", $today);
                $to = date("Y-m-d h:i:s", $tomorrow);
                //2014-08-29 23:50:00 => format in table multiple deals
                $collection->addAttributeToFilter('datetime_to', array('to' => $to));
                $collection->addAttributeToFilter('datetime_to', array('from' => $from));
            }

            //set collection order
            if ($loadRecent) {
                $collection->setOrder('datetime_to', 'DESC');
            } else {
                $collection->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');
            }
            //$collection->printLogQuery(true); die;
            $configNumberDeals = Mage::getStoreConfig('multipledeals/sidebar_configuration/sidedeals_number');
            $collection->setPageSize($configNumberDeals);//get item's number in config
            Mage::getModel('review/review')->appendSummary($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

            $this->_dealsCollection = $collection;
        }
        return $this->_dealsCollection;

    }

}