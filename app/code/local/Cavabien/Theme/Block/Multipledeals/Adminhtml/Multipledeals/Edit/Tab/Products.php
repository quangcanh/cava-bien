<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 9/17/14
 * Time: 10:04 AM
 */ 
class Cavabien_Theme_Block_Multipledeals_Adminhtml_Multipledeals_Edit_Tab_Products extends Devinc_Multipledeals_Block_Adminhtml_Multipledeals_Edit_Tab_Products {


    protected function _prepareCollection()
    {
        $store = $this->_getStore();
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            // filter category deals of weeks
            ->joinField('category_id', 'catalog/category_product',
                'category_id', 'product_id = entity_id', null, 'left')
            // filter category deals of weeks
            ->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');

        if ($store->getId()) {
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            $collection->joinAttribute('name', 'catalog_product/name', 'entity_id', null, 'inner', $adminStore);
            $collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
        }
        else {
            $collection->addAttributeToSelect('price');
            $collection->addAttributeToSelect('status');
            $collection->addAttributeToSelect('visibility');
        }

        $visibility = array(2, 4);
        $collection->addAttributeToFilter('visibility', $visibility);

        // filter category deals of weeks
        // get category 'deals of weeks' from admin config
        $catId = Mage::getStoreConfig('multipledeals_cat_config/category_root_id/multipledeals_cat_root_id');
        $subCatDeals = Mage::helper('multipledeals')->getSubCatDeals($catId);
        //print_r($subCatDeals);print_r(array(1,3,5));die;
        $collection->addAttributeToFilter('category_id', array('in'=>$subCatDeals));
        // filter category deals of weeks

        $this->setCollection($collection);

        //parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();

        return $this;
    }

}