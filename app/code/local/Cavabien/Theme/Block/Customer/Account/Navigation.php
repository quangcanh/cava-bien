<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 8/19/14
 * Time: 2:25 PM
 */ 
class Cavabien_Theme_Block_Customer_Account_Navigation extends Mage_Customer_Block_Account_Navigation {

    public function removeLinkByName($name) {
        unset($this->_links[$name]);
    }
}