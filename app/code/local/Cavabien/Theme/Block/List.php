<?php

/**
 * Class Cavabien_Theme_Block_List
 */
class Cavabien_Theme_Block_List extends Mage_Catalog_Block_Product_Abstract
{

    /**
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        parent::__construct();
    }

    /**
     * Get all categories for search function
     * @return array $cat_list
     */
    public function getCategories()
    {
        $category = Mage::getModel('cavabien_theme/system_config_source_listcategory');
        $cat_list = $category->toOptionArray(true);
        return $cat_list;
    }

    /**
     * Get search result URL
     * @param $obj
     * @return mixed
     */
    public function getSearchUrl($obj)
    {
        $url = Mage::getModel('core/url');
        /*
        * url encoding will be done in Url.php http_build_query
        * so no need to explicitly called urlencode for the text
        */
        $url->setQueryParam('q', $obj->getName());
        return $url->getUrl('catalogsearch/result');
    }
}



