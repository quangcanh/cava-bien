<?php
class Cavabien_Theme_Block_Navigation extends GoMage_Navigation_Block_Navigation
{
    protected function _prepareLayout()
    {
        $this->setTemplate('catalog/category_navigation.phtml');
    }
}