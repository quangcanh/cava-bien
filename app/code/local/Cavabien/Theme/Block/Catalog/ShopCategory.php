<?php
class Cavabien_Theme_Block_Catalog_ShopCategory extends Mage_Catalog_Block_Category_View
{
    protected function _prepareLayout()
    {
        $this->setTemplate('catalog/sub_category_banner.phtml');
    }

    public function getSubcategories()
    {
        return $this->getCurrentCategory()->getChildrenCategories();
    }
}