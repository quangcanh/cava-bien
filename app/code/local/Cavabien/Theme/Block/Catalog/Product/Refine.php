<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Block_Catalog_Product_Refine extends Mage_Core_Block_Template
{

    /**
     * @param $attributeCode
     * @return bool
     */
    public function getFilter($attributeCode)
    {
        /**@var $block GoMage_Navigation_Block_Layer_View */
        $block = $this->getLayout()->getBlock('catalog.leftnav');
        foreach($block->getFilters() as $filter) {
            if ($filter->getAttributeModel() && $filter->getAttributeModel()->getAttributeCode() == $attributeCode) return $filter;
        }
        return false;
    }

    public function getFilterHtml($attributeCode)
    {
        $filter = $this->getFilter($attributeCode);
        if ($filter) {
            $filterClone = clone $filter;
            if($attributeCode == "color"){
                $filterClone->setTemplate('catalog/product/list/refine/image.phtml');
            }else{
                $filterClone->setTemplate('catalog/product/list/refine/default.phtml');
            }

            return $filterClone->toHtml();
        }
        return '';
    }

    public function getClearUrl(){
        $attributeFilter = array('color','size','price');
        $helper = Mage::helper('gomage_navigation');
        $requestVarValue = Mage::app()->getFrontController()->getRequest()->getParams();
        foreach($attributeFilter as $attributeCode){
            if(array_key_exists($attributeCode,$requestVarValue)){
                $requestVarValue[$attributeCode] = null;
            }
        }

        $params['_nosid']       = true;
        $params['_current']     = true;
        $params['_use_rewrite'] = true;
        $params['_query']       = $requestVarValue;
        $params['_escape']      = true;

        return Mage::getUrl('*/*/*', $params);

    }
}