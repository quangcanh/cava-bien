<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Block_Layer_Filter_Subcategory extends GoMage_Navigation_Block_Layer_Filter_Category
{
    public function __construct()
    {
        parent::__construct();
        $this->setData('show_sub_category_filter', true);
        $this->_filterModelName = 'cavabien_theme/layer_filter_subcategory';
        $this->_template = ('cavabien/navigation/layer/filter/subcategory/default.phtml');
    }

    public function setCustomTemplate()
    {
        $this->_template = ('cavabien/navigation/layer/filter/subcategory/default.phtml');
    }
}