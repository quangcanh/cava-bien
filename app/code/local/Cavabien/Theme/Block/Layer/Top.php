<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Block_Layer_Top extends Mage_Core_Block_Template
{

    const NEW_ARRIVAL_CAT       = 'homepage/cat_block/new_arrival_cat';
    const BACK_IN_STOCK_CAT     = 'homepage/cat_block/back_in_stock_cat';
    const COMING_SOON_CAT       = 'homepage/cat_block/coming_soon_cat';
    const TOPRATE_CAT           = 'homepage/cat_block/top_rate_cat';
    const ALMOST_GONE_CAT       = 'homepage/cat_block/almost_gone_cat';

    public function _construct()
    {
        /**
         * construct TOP link's URL
        */

        if ($catId = Mage::getStoreConfig(self::NEW_ARRIVAL_CAT)) {
            $cat = Mage::getModel('catalog/category')->load($catId);

            $this->setNewArrivalUrl($cat->getUrl());
        }

        if ($catId = Mage::getStoreConfig(self::BACK_IN_STOCK_CAT)) {
            $cat = Mage::getModel('catalog/category')->load($catId);
            $this->setBackInStockUrl($cat->getUrl());
        }

        if ($catId = Mage::getStoreConfig(self::COMING_SOON_CAT)) {
            $cat = Mage::getModel('catalog/category')->load($catId);
            $this->setComingSoonUrl($cat->getUrl());
        }

        if ($catId = Mage::getStoreConfig(self::TOPRATE_CAT)) {
            $cat = Mage::getModel('catalog/category')->load($catId);
            $this->setTopRateUrl($cat->getUrl());
        }
        if ($catId = Mage::getStoreConfig(self::ALMOST_GONE_CAT)) {
            $cat = Mage::getModel('catalog/category')->load($catId);
            $this->setAlmostGoneUrl($cat->getUrl());
        }
    }

    /**
     * @param $attributeCode
     * @return bool
     */
    public function getFilter($attributeCode)
    {
        /**@var $block GoMage_Navigation_Block_Layer_View */
        $block = $this->getLayout()->getBlock('catalog.leftnav');
        foreach($block->getFilters() as $filter) {
            if ($filter->getAttributeModel() && $filter->getAttributeModel()->getAttributeCode() == $attributeCode) return $filter;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getDiscountAndOfferFilterHtml()
    {
        if ($this->getCategory()->getDiscountOffer()) {
            $filter = $this->getFilter($this->getCategory()->getDiscountOffer());
            if (!$filter) return '';
            $blockView = $this->getLayout()->getBlock('catalog.leftnav');
            return $this->getLayout()
                ->createBlock('core/template','discount.offer')
                ->setFilter($filter)
                ->setBlockView($blockView)
                ->setTemplate('catalog/layer/filter/template.phtml')
                ->setCustomLabel('Discounts & Offers')
                ->toHtml();
        }
    }

    /**
     * @return string
     */
    public function getStylistPickHtml()
    {
        if ($this->getCategory()->getStylistPicks()) {
            $filter = $this->getFilter($this->getCategory()->getStylistPicks());
            if (!$filter) return '';
            $blockView = $this->getLayout()->getBlock('catalog.leftnav');
            return $this->getLayout()
                ->createBlock('core/template','stylist.picks')
                ->setFilter($filter)
                ->setBlockView($blockView)
                ->setTemplate('catalog/layer/filter/template.phtml')
                ->setCustomLabel('Stylist Picks')
                ->toHtml();
        }
    }

    /**
     * @return string
     */
    public function getTopStyleHtml()
    {
        $filter = $this->getFilter('top_style');
        if (!$filter) return '';
        $blockView = $this->getLayout()->getBlock('catalog.leftnav');
        return $this->getLayout()
            ->createBlock('core/template')
            ->setFilter($filter)
            ->setBlockView($blockView)
            ->setTemplate('catalog/layer/filter/template.phtml')
            ->toHtml();
    }

    /**
     * @return array
     */
    public function getSubcategories()
    {
        /** @var $currentCategory Mage_Catalog_Model_Category */
        $currentCategory = Mage::registry('current_category');
        if (isset($currentCategory)) {
            return $currentCategory->getChildrenCategories();
        }
        return array();
    }

    /**
     * Return Category Filter Html
     *
     * @return string
    */

    public function getCategoryHtml()
    {
        $block = $this->getLayout()->getBlock('catalog.leftnav');
        foreach($block->getFilters() as $filterBlock) {
            if ($filterBlock->getFilter()->getData('show_category_filter')) {
                return $this->getLayout()
                    ->createBlock('core/template')
                    ->setFilter($filterBlock)
                    ->setBlockView($block)
                    ->setTemplate('cavabien/catalog/layer/filter/category/template.phtml')
                    ->toHtml();
            }
        }
        return '';
    }
    /***
     * get subcategory filter for level 3 category
     *
     * @return string
     */
    public function getSubCategoryFilter()
    {
        $block = $this->getLayout()->getBlock('catalog.leftnav');
        $filter = $block->getCategoryFilter();
        return $filter->getHtml();
    }

    public function getStyleHtml()
    {
        if ($this->getCategory()->getStyleAttribute()) {
            $filter = $this->getFilter($this->getCategory()->getStyleAttribute());
            if (!$filter) return '';
            $blockView = $this->getLayout()->getBlock('catalog.leftnav');
            return $this->getLayout()
                ->createBlock('core/template','style.filter')
                ->setFilter($filter)
                ->setBlockView($blockView)
                ->setTemplate('cavabien/catalog/layer/filter/style.phtml')
                ->setCustomLabel('Style')
                ->setCustomClass('style-attribute-left')
                ->toHtml();
        }
        return '';
    }

    /**
     * Get title of the category filter
     *
     * @return string
     */
    public function getTitle()
    {
        $currentCategory = Mage::registry('current_category');
        switch($currentCategory->getId()) {
            case Mage::getStoreConfig(self::NEW_ARRIVAL_CAT):
                return $this->__('See Newest In');
                break;
            case Mage::getStoreConfig(self::BACK_IN_STOCK_CAT):
                return $this->__('See Back In Stock In');
                break;
            case Mage::getStoreConfig(self::COMING_SOON_CAT):
                return $this->__('See Comming soon In');
                break;
            case Mage::getStoreConfig(self::TOPRATE_CAT):
                return $this->__('See Top Rate In');
                break;
            case Mage::getStoreConfig(self::ALMOST_GONE_CAT):
                return $this->__('See Almost Gone In');
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Check if current category is specials categories or not
     *
     * @return bool
     */
    public function showCategoryFilter()
    {
        if (!$this->getCategory()) return false;
        $catId = $this->getCategory()->getId();
        $specialCategory = array(
            Mage::getStoreConfig(self::NEW_ARRIVAL_CAT),
            Mage::getStoreConfig(self::BACK_IN_STOCK_CAT),
            Mage::getStoreConfig(self::COMING_SOON_CAT),
            Mage::getStoreConfig(self::TOPRATE_CAT),
            Mage::getStoreConfig(self::ALMOST_GONE_CAT)
        );
        if (in_array($catId, $specialCategory)) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function showSubCategory()
    {
        $catId = $this->getCategory()->getId();
        $rootCat = Mage::app()->getStore()->getRootCategoryId();
        $rootCat = Mage::getModel('catalog/category')->load($rootCat);
        $categories = $rootCat->getChildrenCategories();
        foreach($categories as $category) {
            if ($category->getId() == $catId) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return Mage::registry('current_category');
    }

    public function getNewArrivalId(){
        return Mage::getStoreConfig(self::NEW_ARRIVAL_CAT);
    }
    public function getBackInStockId(){
        return Mage::getStoreConfig(self::BACK_IN_STOCK_CAT);
    }
    public function getComingSoonId(){
        return Mage::getStoreConfig(self::COMING_SOON_CAT);
    }
    public function getAlmostGoneId(){
        return Mage::getStoreConfig(self::ALMOST_GONE_CAT);
    }
    public function getTopRateId(){
        return Mage::getStoreConfig(self::TOPRATE_CAT);
    }
}