<?php
class Cavabien_Theme_Block_Layer_View extends GoMage_Navigation_Block_Layer_View
{
    public function getFilters()
    {
        $filters = array();
        if ($categoryFilter = $this->_getCategoryFilter()) {
            $filters[] = $categoryFilter;
        }

        return $filters;
    }
}