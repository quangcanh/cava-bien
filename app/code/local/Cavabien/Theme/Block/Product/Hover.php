<?php
/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Cavabien_Theme_Block_Product_Hover extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        $this->setTemplate('catalog/product/hover.phtml');
        return parent::_toHtml();
    }

}