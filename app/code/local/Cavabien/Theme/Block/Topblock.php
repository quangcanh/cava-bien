<?php

/**
 * Class Cavabien_Theme_Block_topBlock
 */
class Cavabien_Theme_Block_Topblock extends Mage_Core_Block_Template
{
    /**
     * Block:           Deal of the week
     * Position:        below top menu
     * Function desc:   Get get Monthly Pick product on right hand in block
     * @return 1 products object
     */
    public function getMonthlyPick()
    {
        $cus = Mage::getSingleton("customer/customer");
        $storeId = Mage::app()->getStore()->getId();
        $monthlyPickId = Mage::getStoreConfig('homepage/header/monthly_pick_id');
        $category = Mage::getModel('catalog/category')->load($monthlyPickId);
        $_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addCategoryFilter($category)
            ->addAttributeToFilter('type_id', array('eq' => 'grouped'))
            ->addAttributeToFilter('status', array('eq' => 1))
            ->load();

        foreach ($_products as $_product) {
            return $_product;
        }
        return null;
    }

    public function getIdsFromGroup($_product)
    {
        if (!$_product) return null;

        $childIds = Mage::getModel('catalog/product_type_grouped')
            ->getChildrenIds($_product->getId());
        $ids = "";
        //echo "<pre>";var_dump($childIds);
        foreach ($childIds as $key => $childId) {
            foreach ($childId as $child){
                $ids .= $child.",";
            }
        }
        return $ids;
    }

    /**
     * Block:           Most wanted
     * Position:        below top menu
     * Function desc:   Get categories show in most Wanted block(Below top menu)
     * @return n products array
     */
    public function getMostWanted()
    {
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
        $topCategories = Mage::getModel('catalog/category')->load($rootCategoryId)->getChildren();

        foreach (explode(',', $topCategories) as $topCategory) {
            $topIds[] = $topCategory;

        }
        $allCategories = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('parent_id', $topIds)
            ->load();

        $mostWantedCategory = array();
        foreach ($allCategories as $category) {
            if ($this->getBestSellerByCategory($category->getId()) != null) {
                $mostWantedCategory[] = $category;
            }
        }
        $sub_category_number = Mage::getStoreConfig('homepage/header/sub_category_number');
        if (count($mostWantedCategory)) {
            if (count($mostWantedCategory) < $sub_category_number) {
                return $mostWantedCategory;
            } else {
                $mostWanted_keys = array_rand($mostWantedCategory, $sub_category_number);
                $mostWanted_array = array();
                foreach ($mostWanted_keys as $mostWanted_key) {
                    $mostWanted_array[] = $mostWantedCategory[$mostWanted_key];
                }
                return $mostWanted_array;
            }
        } else {
            return null;
        }

    }

    /**
     * Block:           Most wanted
     * Position:        below top menu
     * Function desc:   Get best seller product foreach category (shown in most wanted block)
     * @param int $categoryId
     * @return n products array
     */
    public function getBestSellerByCategory($categoryId = 0)
    {
        if ($categoryId == 0) {
            return null;
        }
//        $category = Mage::getModel('catalog/category')->load($categoryId);
//        $visibility = Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds();
//        $storeId = Mage::app()->getStore()->getId();
//        $attributesToSelect = array('*');
//        $productFlatTable = Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId);
//        try {
//            $resourceCollection = Mage::getResourceSingleton('reports/product_collection')
//                ->addOrderedQty();
//            if (Mage::helper('catalog/product_flat')->isEnabled()) {
//                $resourceCollection->joinTable(array('flat_table' => $productFlatTable), 'entity_id=entity_id', $attributesToSelect);
//            } else {
//                $resourceCollection->addAttributeToSelect($attributesToSelect);
//            }
//            $resourceCollection
//                /** Use the set visibility method instead of addAttributeToFilter  */
//                ->setVisibility($visibility)
//                /** Add store filter! */
//                ->addStoreFilter($storeId)
//                ->addCategoryFilter($category)
//                ->setOrder('ordered_qty', 'desc')
//                ->setPageSize(0, 1);
//
//
//            foreach($resourceCollection as $resource){
//                return $resource;
//            }
//
//            return null;
//        } catch (Exception $e) {
//            Mage::logException($e);
//        }

        $storeId = (int)Mage::app()->getStore()->getId();
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $resourceCollection = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
            ->addCategoryFilter($category)
            ->addAttributeToFilter('status', array('eq' => 1))

            ->setOrder('ordered_qty', 'desc')
            ->setPageSize(1);

        foreach ($resourceCollection as $resource):
            return $resource;
        endforeach;

        return null;

    }

    /**
     * Block:           Most wanted
     * Position:        below top menu
     * Function desc:   Get most sold product in the right hand of block (shown in most wanted block)
     * @return n products array
     */

    public function getMostSoldProduct()
    {
//        $storeId = (int)Mage::app()->getStore()->getId();
//        // Date
//        $weekStart = strtotime('last Sunday', time());
//        $weekCurrent = time();
//        $from = date("Y-m-d H:i:s", $weekStart);
//        $to = date("Y-m-d H:i:s", $weekCurrent);
//        $resourceCollection = Mage::getResourceModel('catalog/product_collection')
//            ->addAttributeToSelect('*')
//            ->setPageSize(1);
//        $resourceCollection->getSelect()
//            ->joinLeft(
//                array('aggregation' => $resourceCollection->getResource()->getTable('sales/bestsellers_aggregated_monthly')),
//                "e.entity_id = aggregation.product_id AND aggregation.store_id={$storeId} AND aggregation.period BETWEEN '{$from}' AND '{$to}'",
//                array('SUM(aggregation.qty_ordered) AS sold_quantity')
//            )
//            ->group('e.entity_id')
//            ->order(array('sold_quantity DESC', 'e.created_at'));
//        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($resourceCollection);
//        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($resourceCollection);
//        foreach ($resourceCollection as $resource):
//            return $resource;
//        endforeach;
//
//        return null;


        $weekStart = strtotime('last Sunday', time());
        $weekCurrent = time();
        $from = date("Y-m-d H:i:s", $weekStart);
        $to = date("Y-m-d H:i:s", $weekCurrent);
        $storeId = (int)Mage::app()->getStore()->getId();
        $resourceCollection = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
            ->addOrderedQty($from, $to)
            ->addAttributeToFilter('status', array('eq' => 1))
            ->setPageSize(1)
            ->setOrder('ordered_qty', 'DESC');
        $resourceCollection->getSelect()->limit(1);
        foreach ($resourceCollection as $resource):
            return $resource;
        endforeach;

        return null;

    }

    public function getMostSoldProduct_bak()
    {
        $weekStart = strtotime('last Sunday', time());
        $weekCurrent = time();
        $from = date("Y-m-d 00:00:01", $weekStart);
        $to = date("Y-m-d H:i:s", $weekCurrent);

        $visibility = Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds();
        $storeId = Mage::app()->getStore()->getId();
        $attributesToSelect = array('*');
        $productFlatTable = Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId);
        try {
            $resourceCollection = Mage::getResourceSingleton('reports/product_collection');
            //->addOrderedQty();
            if (Mage::helper('catalog/product_flat')->isEnabled()) {
                $resourceCollection->joinTable(array('flat_table' => $productFlatTable), 'entity_id=entity_id', $attributesToSelect);
            } else {
                $resourceCollection->addAttributeToSelect($attributesToSelect);
            }
            $resourceCollection
                /** Use the set visibility method instead of addAttributeToFilter  */
                //->setVisibility($visibility)
                /** Add store filter! */
                ->addStoreFilter($storeId)
                ->addOrderedQty($from, $to, true)
                ->setOrder('ordered_qty', 'desc')
                ->setPageSize(0, 1);

            if (count($resourceCollection)) {
                var_dump($resourceCollection->getData());
                return $resourceCollection;
            } else
                return null;
        } catch (Exception $e) {
            //Mage::logException($e);
        }


    }

    /**
     * Block:           Featured product
     * Position:        Below top menu
     * Function desc:   Get random n=5 featured product to show on Featured product left hand
     * @return n featured products array | null
     */
    public function getFeaturedProduct()
    {
        // Featured Product: ID 17 ; Name: features-products
        $featured_product_menu_id = Mage::getStoreConfig('homepage/header/featured_product_menu_id');
        $featured_product_number = Mage::getStoreConfig('homepage/header/featured_product_number');
        $category = Mage::getModel('catalog/category')->load($featured_product_menu_id);
        $_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addCategoryFilter($category)
            ->addAttributeToFilter('status', array('eq' => 1))
            ->load();
        $productsArray = array();
        $productsKeys = array();
        $featuredProducts = array();
        if ($_products->count()) {
            foreach ($_products as $_product) {
                $productArray[] = $_product;
            }
            //If selected product smaller than number of product needed
            if ($featured_product_number > count($productArray)) {
                return $productArray;
            }
            //Get random 5 keys
            $featuredProducts = array();
            $productsKeys = array_rand($productArray, $featured_product_number);
            foreach ($productsKeys as $_productsKey) {
                $featuredProducts[] = $productArray[$_productsKey];
            }
            return $featuredProducts;
        }
        return null;
    }

    /**
     * Block:           Featured product
     * Position:        Below top menu
     * Function desc:   get most loved (Product attribute) in current month, show on right hand
     * @return null | 1 product object
     */
    public function getMostLoved()
    {
        $monthStart = strtotime('first day of this month', time());
        $monthCurrent = time();
        $from = date("Y-m-d 00:00:01", $monthStart);
        $to = date("Y-m-d 23:59:59", $monthCurrent);

        $loveIt = Mage::getModel('cavabien_loveit/loveit')->getCollection();
        $loveIt->addFieldToFilter('loved_at_time', array('gteq' => $from));
        $loveIt->addFieldToFilter('loved_at_time', array('lteq' => $to));
        $loveIt->getSelect()->group('product_id')->columns('COUNT(product_id) as c')->order('c DESC')->limit(1);
        if (count($loveIt)) {
            foreach ($loveIt as $loveItem) {
                $_productId = $loveItem->getProductId();
                $_product = Mage::getModel('catalog/product')->load($_productId);
                if($_product->getName() && ($_product->getStatus() ==  1)){

                    return $_product;

                }
            }
        }

        return null;
    }

    /**
     * Block:           Stylist pick
     * Position:        in homepage
     * Function desc:   get n grouped products from Stylist Pick Categories
     * @return null | n grouped product object
     */
    public function getStylistPick()
    {
        $stylistProductNumber = Mage::getStoreConfig('homepage/header/stylist_product_number');
        $stylistPickId = Mage::getStoreConfig('homepage/header/stylist_pick_id');
        $stylistPickCategory = Mage::getModel('catalog/category')->load($stylistPickId);
        $_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            //->addAttributeToFilter('type_id', array('eq' => 'grouped'))
            ->addCategoryFilter($stylistPickCategory)
            ->addAttributeToFilter('status', array('eq' => 1));

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_products);
        $_products->setPageSize($stylistProductNumber?$stylistProductNumber:10);

        if ($_products->count()) {
            return $_products;
        }
        return null;
    }


    /**
     * Block:           Get the look
     * Position:        in homepage tabs below
     * Function desc:   get random n products of Get the look categories
     * @return n products array |null
     */
    public function getTheLook()
    {
        // Get the Look Category: ID 529 ; Name: GET THE LOOK
        $getLookId = Mage::getStoreConfig('homepage/homepage_block/get_look_id');
        $getLookNumber = Mage::getStoreConfig('homepage/homepage_block/get_look_number');
        $category = Mage::getModel('catalog/category')->load($getLookId);
        $_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addCategoryFilter($category)
            ->addAttributeToFilter('status', array('eq' => 1));
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_products);


        $productArray = array();
        if ($_products->count()) {
            foreach ($_products as $_product) {
                $productArray[] = $_product;
            }
            //If selected product smaller than number of product needed
            if ($getLookNumber > count($productArray)) {
                return $productArray;
            }

            // Random 5 products from
            $getTheLookArray = array();
            $getTheLook_keys = array_rand($productArray, $getLookNumber);
            foreach ($getTheLook_keys as $getTheLookKey) {
                $getTheLookArray[] = $productArray[$getTheLookKey];
            }
            return $getTheLookArray;
        }
        return null;
    }


    /**
     * Block:           Best selling
     * Position:        in homepage tabs below
     * Function desc:   get random n products of most wanted category
     * @return  n products array | null
     */
    public function bestSelling()
    {
        //MOST WANTED Category: ID: 14; Name: MOST WANTED
//        $mostWantedCategoryId = Mage::getStoreConfig('homepage/homepage_block/most_wanted_category_id');
//        $mostWantedNumber = Mage::getStoreConfig('homepage/homepage_block/most_wanted_number');
//        $category = Mage::getModel('catalog/category')->load($mostWantedCategoryId);
//        $_products = Mage::getModel('catalog/product')
//            ->getCollection()
//            ->addAttributeToSelect('*')
//            ->addCategoryFilter($category)
//            ->addAttributeToFilter('status', array('eq' => 1))
//            ->load();
        $storeId = (int)Mage::app()->getStore()->getId();
        $mostWantedNumber = Mage::getStoreConfig('homepage/homepage_block/most_wanted_number');
        $_products = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
             ->addAttributeToFilter('status', array('eq' => 1))
            ->setOrder('ordered_qty', 'desc')
            ->setPageSize($mostWantedNumber);
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_products);
        $productArray = array();
        foreach ($_products as $_product) {
            $productArray[] = $_product;
        }
        if (count($_products)) {
            //If selected product smaller than number of product needed
            if ($mostWantedNumber > count($productArray)) {
                return $productArray;
            }
            $mostWantedArray = array();
            $mostWantedKeys = array_rand($productArray, $mostWantedNumber);
            foreach ($mostWantedKeys as $mostWantedKey) {
                $mostWantedArray[] = $productArray[$mostWantedKey];
            }
            return $mostWantedArray;
        }
        return null;
    }

    /**
     * Block:           New Arrivals
     * Position:        in homepage tabs below
     * Function desc:   get random n products of new Arrivals category
     * @return n products array|null
     */
    public function newArrival()
    {
        //New Arrivals Category: ID: 30; Name: NEW ARRIVALS
        $newArrivalId = Mage::getStoreConfig('homepage/homepage_block/new_arrival_id');
        $newArrivalNumber = Mage::getStoreConfig('homepage/homepage_block/new_arrival_number');
        //$category = Mage::getModel('catalog/category')->load($newArrivalId);
        $_collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            //->addCategoryFilter($category)
            ->addAttributeToFilter('news_from_date', array('lteq' => date('Y/m/d')))
            ->addAttributeToFilter('news_from_date', array('gteq' => date('Y/m/d', strtotime('-7 days'))))
            ->addAttributeToFilter('status', array('eq' => 1));
        $_collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`is_in_stock`=1', array('qty', 'is_in_stock'));
            //->addAttributeToFilter('is_in_stock',array('eq' => 1))
            //->load();
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_collection);

        $_products = $_collection->load();

        $productArray = array();
        if ($_products->count()) {
            foreach ($_products as $_product) {
                $productArray[] = $_product;
            }
            if ($newArrivalNumber > count($productArray)) {
                return $productArray;
            }
            $newArrivalArray = array();
            $newArrivalKeys = array_rand($productArray, $newArrivalNumber);
            foreach ($newArrivalKeys as $newArrivalKey) {
                $newArrivalArray[] = $productArray[$newArrivalKey];
            }
            return $newArrivalArray;
        }
        return null;
    }

    /**
     * Block:           Back in stock
     * Position:        in homepage tabs below
     * Function desc:   get random n products of back InStock category
     * @return n products array|null
     */
    public function backInStock()
    {
        //New Arrivals Category: ID: 27; Name: NEW ARRIVALS
        $backInStockId = Mage::getStoreConfig('homepage/homepage_block/back_in_stock_id');
        $backInStockNumber = Mage::getStoreConfig('homepage/homepage_block/back_in_stock_number');
        //$category = Mage::getModel('catalog/category')->load($backInStockId);
        $_collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            //->addCategoryFilter($category)
            ->addAttributeToFilter('back_to_stock_from', array('gteq' => date('Y/m/d', strtotime('-7 days'))))
            ->addAttributeToFilter('back_to_stock_from', array('lteq' => date('Y/m/d')))
            ->addAttributeToFilter('status', array('eq' => 1));
        $_collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=1', array('qty', 'is_in_stock'));
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_collection);
        $_products = $_collection->load();

        $productArray = array();
        if ($_products->count()) {
            foreach ($_products as $_product) {
                $productArray[] = $_product;
            }

            if ($backInStockNumber > count($productArray)) {
                return $productArray;
            }
            $backInStockArray = array();
            $backInStockKeys = array_rand($productArray, $backInStockNumber);
            foreach ($backInStockKeys as $backInStockKey) {
                $backInStockArray[] = $productArray[$backInStockKey];
            }
            return $backInStockArray;

        }
        return null;
    }

    /**
     * Method:          Get price of grouped product
     * Function desc:   Get price of the grouped product by calculator
     * @param $groupedProduct
     * @param bool $incTax
     * @return int price value
     */
    public function getGroupedProductPrice($groupedProduct, $incTax = true)
    {
        $_taxHelper = $this->helper('tax');
        $aProductIds = $groupedProduct->getTypeInstance()->getChildrenIds($groupedProduct->getId());
        $prices = array();
        foreach ($aProductIds as $ids) {
            foreach ($ids as $id) {
                $aProduct = Mage::getModel('catalog/product')->load($id);
                if ($incTax) {
                    $prices[] = $_taxHelper->getPrice($aProduct, $aProduct->getPriceModel()->getFinalPrice(null, $aProduct, true), true);
                } else {
                    $prices[] = $aProduct->getPriceModel()->getFinalPrice(null, $aProduct, true);
                }
            }
        }
        $productPrice = 0;
        foreach ($prices as $price) {
            $productPrice += $price;
        }
        //asort($prices);array_shift($prices);
        return $productPrice;
    }

    public function fullWorkingBestSeller()
    {
        $visibility = Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds();
        $storeId = Mage::app()->getStore()->getId();

        $attributesToSelect = array('*');

        $productFlatTable = Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId);
        try {

            $resourceCollection = Mage::getResourceSingleton('reports/product_collection')

                ->addOrderedQty();

            if (Mage::helper('catalog/product_flat')->isEnabled()) {

                $resourceCollection->joinTable(array('flat_table' => $productFlatTable), 'entity_id=entity_id', $attributesToSelect);
            } else {

                $resourceCollection->addAttributeToSelect($attributesToSelect);
            }

            $resourceCollection
                /** Use the set visibility method instead of addAttributeToFilter  */
                ->setVisibility($visibility)
                /** Add store filter! */
                ->addStoreFilter($storeId)
                ->setPageSize(0, 1)
                ->setOrder('ordered_qty', 'desc');
            return $resourceCollection;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }


    public function  getFacebookGiveaway(){
        $facebookGiveawayId = Mage::getStoreConfig('homepage/homepage_block/facebook_giveaway_id');
        $category = Mage::getModel('catalog/category')->load($facebookGiveawayId);
        $_products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addCategoryFilter($category)
            ->addAttributeToFilter('status', array('eq' => 1))
            ->setPage(0,1)
            ->load();

        if(count($_products)){
            foreach($_products as $_product){
                return $_product;

            }
        }
        return null;
    }


}