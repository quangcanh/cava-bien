<?php

/**
 * Class Cavabien_Theme_Block_topBlock
 */

class Cavabien_Theme_Block_Sidebar extends Mage_Checkout_Block_Cart_Sidebar
{
    /**
     * Function desc:   Get Cross-SellProduct of Cart
     * @return array|bool
     */
    public function getCrossSellProducts()
    {
        $result = array();
        $crossSellProducts = array();
        $_products = $this->getRecentItems();
        if (count($_products) === 0) {
            return false;
        }

        foreach ($_products as $key => $_product) {
            $temp = Mage::getModel('catalog/product')
                ->load($_product->getProductId())
                ->getCrossSellProductIds();
            $result = array_unique(array_merge($result, $temp));
        }

        foreach ($result as $key => $productId) {
            $crossSellProducts[] = Mage::getModel('catalog/product')
                ->load($productId);
                //->getData();
        }
        return $crossSellProducts;
    }

    public function addToCartProduct($_product)
    {
        Mage::log(print_r($_product, true), null, "nghiahc_debug.log");
        exit();
        $qty = '1'; // Replace qty with your qty
        $cart = Mage::getModel('checkout/cart');
        $cart->init();
        $cart->addProduct($_product, array('qty' => $qty));
        $cart->save();
        Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
    }
}

