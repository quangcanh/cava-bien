<?php

/**
 * Class Cavabien_Theme_Block_Myaccount
 */
class Cavabien_Theme_Block_Myaccount extends Mage_Customer_Block_Account_Dashboard
{
    /**
     * Block:           Multi block used
     * Position:        Multi position
     * Function desc:   Get Balance total of current logged in customer
     * @return number amount
     */
    public function getBalance()
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $amount = Mage::getModel('enterprise_customerbalance/balance')
            ->setCustomerId($customerId)
            ->loadByCustomer()
            ->getAmount();
        return $amount;
    }

    /**
     * Page:            My Account
     * Block:
     * Position:        Below top page tab block
     * Function desc:   Get total reward points of current logged in customer
     * @return number of reward points
     */
    public function getRewardPoints()
    {
        $storeId = Mage::app()->getStore()->getId();
        $customerId = Mage::getModel('customer/session')->getCustomerId();
        if (Mage::getStoreConfig('rewardpoints/default/flatstats', $storeId)) {
            $rewardFlatModel = Mage::getModel('rewardpoints/flatstats');
            return (int)$rewardFlatModel->collectPointsCurrent($customerId, $storeId);
        }

        $rewardModel = Mage::getModel('rewardpoints/stats');
        return (int)$rewardModel->getPointsCurrent($customerId, $storeId);
    }

    /**
     * Page:            My Account > My loved items
     * Block:
     * Position:        content
     * Function desc:   Get all loved it record by current logged in customer
     * @return          loveit Collection | null
     */
    public function getLoveItProducts()
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $loveit = Mage::getModel('cavabien_loveit/loveIt')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId);

        $arr_temp = array();
        if (count($loveit)) {
            foreach($loveit as $item_loveit){
//                $love = Mage::getModel('catalog/product')
//                        ->getCollection()
//                        ->addFieldToFilter('entity_id', array('eq' => $item_loveit->getProductId()));
                $sql = "SELECT * FROM `catalog_product_entity` WHERE entity_id = ".(int)$item_loveit->getProductId();

                $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $results = $connection->fetchAll($sql);
                if ($results){
                    $arr_temp[] = $item_loveit;
                }
            }
            if (!count($arr_temp)) return null;
            return $arr_temp;
        }
        return null;
    }
    public function getAddToCartUrl($product, $additional = array())
    {
        $addUrlKey = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
        $addUrlValue = Mage::getUrl('*/*/*', array('_use_rewrite' => true, '_current' => true));
        $additional[$addUrlKey] = Mage::helper('core')->urlEncode($addUrlValue);
        return $this->helper('checkout/cart')->getAddUrl($product, $additional);

    }
    public function getItemConfigureUrl($product)
    {

        if ($product instanceof Mage_Catalog_Model_Product) {
            $id = $product->getLoveitId();
        } else {
            $id = $product->getId();
        }
        $params = array('id' => $id);

        return $this->getUrl('cavabien/myaccount/loveit/', $params);
    }
}