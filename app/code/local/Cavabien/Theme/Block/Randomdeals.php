<?php

/**
 * Class Cavabien_Theme_Block_Randomdeals
 */
class Cavabien_Theme_Block_Randomdeals extends Devinc_Multipledeals_Block_List
{
    /**
     * return random n deals
     * @param bool $loadRecent
     * @return mixed
     */
    public function getRandomDeals($loadRecent = false)
    {
        if (!$this->_dealsCollection) {
            $dealStatus = ($loadRecent) ? self::STATUS_ENDED : self::STATUS_RUNNING;

            //load active deal ids
            $dealCollection = Mage::getModel('multipledeals/multipledeals')->getCollection()->addFieldToFilter('status', array('eq' => $dealStatus))->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');
            $productIds = array(0);

            if (count($dealCollection)) {
                $dealIds = array();
                foreach ($dealCollection as $deal) {
                    if (Mage::helper('multipledeals')->runOnStore($deal) && !array_key_exists($deal->getProductId(), $dealIds)) {
                        $dealIds[$deal->getProductId()] = $deal->getId();
                        $productIds[] = $deal->getProductId();
                    }
                }
                $productRandomIds = array();

                $dealOfTheWeekNumber = Mage::getStoreConfig('homepage/homepage_block/deal_of_week_number');

                if (count($productIds) > $dealOfTheWeekNumber) {
                    $productRandomKeys = array_rand($productIds, $dealOfTheWeekNumber);
                    foreach ($productRandomKeys as $productRandomKey) {
                        $productRandomIds[] = $productIds[$productRandomKey];
                    }
                    $productIds = $productRandomIds;
                }


                if (count($dealIds)) {

                    $dealIdsString = implode(',', $dealIds);
                } else {
                    $dealIdsString = 0;
                }
            } else {
                $dealIdsString = 0;
            }

            //load product collection + deals info
            $resource = Mage::getSingleton('core/resource');
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToFilter('entity_id', array('in', $productIds))
                ->addAttributeToFilter('status', array('in' => array(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)))
                ->addStoreFilter()
                ->joinTable($resource->getTableName('multipledeals'), 'product_id=entity_id', array('multipledeals_id' => 'multipledeals_id', 'position' => 'position', 'deal_qty' => 'deal_qty', 'datetime_to' => 'datetime_to'), '{{table}}.multipledeals_id IN (' . $dealIdsString . ')', 'left');

            //set collection order
            if ($loadRecent) {
                $collection->setOrder('datetime_to', 'DESC');
            } else {
                $collection->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');
            }

            Mage::getModel('review/review')->appendSummary($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

            $this->_dealsCollection = $collection;
        }
        return $this->_dealsCollection;
    }
}

