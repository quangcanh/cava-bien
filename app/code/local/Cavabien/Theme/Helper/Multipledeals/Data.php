<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 7/22/14
 * Time: 2:54 PM
 */ 
class Cavabien_Theme_Helper_Multipledeals_Data extends Devinc_Multipledeals_Helper_Data {

    public function getSubCatDeals($catId)
    {
        $cat = Mage::getModel('catalog/category')->load($catId);
        $subcats = $cat->getChildren();
        $arrCats = explode(',', $subcats);
        $num = count($arrCats);
        $subCatIdChild = array();
        for($i=0;$i<$num;$i++)
        {
            $subCatId = Mage::getModel('catalog/category')->load($arrCats[$i]);
            $subCatIdChild[] = $subCatId->getChildren();
        }
        $stringSubCat = implode(',',$subCatIdChild);
        $arrDealsReturn =  explode(',',$stringSubCat);
        return $arrDealsReturn;
    }

    public function getSubCatDealsMenu($cat)
    {
        $cat = Mage::getModel('catalog/category')->load($cat);
        $subcats = $cat->getChildren();
        $arrCats = explode(',', $subcats);
        return $arrCats;
    }

    public function getCatNameById($id)
    {
        $cat = Mage::getModel('catalog/category');
        $cat->load($id);
        return $cat->getName();
    }

    public function getUrlCatById($id)
    {
        $cat = Mage::getModel('catalog/category');
        $cat->load($id);
        return $cat->getUrl();
    }

    public function getRatingReviewDeals($productCollection)
    {
        $storeId = Mage::app()->getStore()->getId();
        $reviewData = Mage::getModel('review/review_summary')->setStoreId($storeId)  ->load($productCollection->getId());
        return $reviewData;
    }

    /**
     * get deal by product come soon
     */

    public function getDealByProductComeSoon(Varien_Object $_product) {
        $dealCollection = Mage::getModel('multipledeals/multipledeals')->getCollection()->addFieldToFilter('product_id', array('eq'=>$_product->getId()))->addFieldToFilter('status', array('eq'=>self::STATUS_QUEUED))->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');

        if (count($dealCollection)) {
            foreach ($dealCollection as $deal) {
                if ($this->runOnStore($deal)) {
                    return $deal;
                }
            }
        }

        return false;
    }

    /**
     * get time countdown for comesoon product
     */

    public function getProductCountdownComeSoon(Varien_Object $_product, $_params = false, $_timeLeftText = true)
    {
        $deal = $this->getDealByProductComeSoon($_product);

        $html = '';
        if (Mage::helper('multipledeals')->isEnabled() && $deal) {
            $toDate = $deal->getDatetimeFrom();/*IMPORTANT - day come to deal*/
            $currentDateTime = Mage::helper('multipledeals')->getCurrentDateTime(0);
            if ($currentDateTime<$deal->getDatetimeFrom()) { /* is it come soon ? */
                $finished = false;
            }
            $html .= ($_timeLeftText) ? '<b>'.$this->__('Time come to buy:').'</b>' : '';
            $html .= $this->getCountdown($toDate, $_product->getId(), $finished, $_params);
        }

        return $html;
    }

    public function getFirstSubCatId($catRootId)
    {
        $cat = Mage::getModel('catalog/category')->load($catRootId);
        $subcats = $cat->getChildren();
        $arrCats = explode(',', $subcats);
        $firstSubCatId = $arrCats[0];
        return $firstSubCatId;
    }

    public function getLinkShopMoreByCatId($id)
    {
        $cat = Mage::getModel('catalog/category');
        $cat->load($id);
        return $cat->getData('link_multipledeals_shop_more');
    }
    //if the product is a deal and the deal is running under the current store, returns the deal data
    public function getDealByProduct(Varien_Object $_product) {
        $dealCollection = Mage::getModel('multipledeals/multipledeals')->getCollection()->addFieldToFilter('product_id', array('eq'=>$_product->getId()))
            ->addFieldToFilter('status',
                array('in' => array(self::STATUS_RUNNING, self::STATUS_QUEUED, self::STATUS_ENDED)
                ))
            ->setOrder('position', 'ASC')->setOrder('multipledeals_id', 'DESC');
        if (count($dealCollection)) {
            foreach ($dealCollection as $deal) {
                if ($this->runOnStore($deal)) {
                        return $deal;
                }
            }
        }

        return false;
    }

}