<?php

/**
 * Cavabien_Theme
 *
 * @category    Cavabien
 * @package     Cavabien_Theme
 * @copyright   Copyright (c) 2014 SmartOSC Inc. (http://www.smartosc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cavabien_Theme_Helper_Conf_Data extends Amasty_Conf_Helper_Data
{

    const PRODUCT_LEFT = 'homepage/homepage_block/almost_out_of_stock';

    public function getHtmlBlock($_product, $html)
    {
        $blockForForm = Mage::app()->getLayout()->createBlock('amconf/catalog_product_view_type_configurablel', 'amconf.catalog_product_view_type_configurable', array('template' => "amasty/amconf/configurable.phtml"));
        $blockForForm->setProduct($_product);
        $blockForForm->setNameInLayout('product.info.options.configurable');
        $submitUrl = $blockForForm->getSubmitUrl($_product);
        $html .= '<div id="insert" style="display:none;"></div><div id="amconf-block">' . $blockForForm->toHtml() . '</div>';
        $attributes = $blockForForm->getAttributes();

        return $html;
    }

    /**
     * This will return button html
     *
     * @param $_product Mage_Catalog_Model_Product
     * @param $label string
     * @return string
     */
    public function getAddToCartBt($_product, $label)
    {
        $html = '';
        $blockForForm = Mage::app()->getLayout()->createBlock('amconf/catalog_product_view_type_configurablel');
        $blockForForm->setProduct($_product);
        $blockForForm->setNameInLayout('product.info.options.configurables');
        $submitUrl = $blockForForm->getSubmitUrl($_product);
        $blockForForm->getJsonConfig();
        $attributes = $blockForForm->getAttributes();
        $onClick = "formSubmit(this,'" . $submitUrl . "', '" . $_product->getId() . "', " . $attributes . ")";
        $amConf = "createForm('" . $submitUrl . "', '" . $_product->getId() . "', " . $attributes . ")";
        $html .= '
                      <button type="button" title="' . $this->__('Add to Cart') . '" class="button btn-cart" onclick="' . $onClick . '"  amconf="' . $amConf . '">
                            <span>
                                <span>' . $this->__($label) . '</span>
                            </span>
                      </button>';
        return $html;
    }

    /*
     * Get Status of Product
     * */
    /*
   public function getProductStatus($_product){
       $productStatus = array();
       if($_product instanceof Mage_Catalog_Model_Product){
           $stock = Mage::getModel('cataloginventory/stock_item')
               ->loadByProduct($_product);
           if ($stock->getIsInStock()) {
               $createAt = new DateTime($_product->getCreatedAt());
               $now = new DateTime(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
               $newArrive = $now->diff($createAt);
               $newArrive = $newArrive->format("%y:%m:%d:%H");
               $newArrive = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $newArrive);
               sscanf($newArrive, "%d:%d:%d:%d",$years,$months,$days, $hours);
               $time_hours = $years*24*365 + $months*30*24 +$days*24 + $hours;
               if($time_hours<=24){
                   $productStatus[] = array(
                       'status'     => SM_Filter_Model_Layer::NEW_ARRIVAL,
                       'label'    => 'New Today'
                   );
               }elseif($time_hours<=48){
                   $productStatus[] = array(
                       'status'     => SM_Filter_Model_Layer::NEW_ARRIVAL,
                       'label'    => $this->__('New Yesterday')
                   );
               }
               if ($_product->getTypeId() != 'configurable' && $_product->getTypeId() != 'grouped') {
                   $minQty = Mage::getStoreConfig(self::PRODUCT_LEFT);
                   if ($stock->getQty() > 0 && $stock->getQty() < $minQty) {
                       $productStatus[] = array(
                           'status'     => SM_Filter_Model_Layer::ALMOST_GONE,
                           'label'    => $this->__('Only %s left!', (int) $stock->getQty())
                       );
                   }
               }
               if (empty($productStatus)) {
                   $productStatus[] = array(
                       'status'     => SM_Filter_Model_Layer::IN_STOCK,
                       'label'    => $this->__('In Stock')
                   );
               }
               $productStatus['review_star'] = $productStatus['review_number'] = true;
               $productStatus['button_label'] = $this->__('SHOP NOW');
           } else {
               if ($_product->getComingSoon()) {
                   $productStatus[] = array(
                       'status'     => SM_Filter_Model_Layer::COMING_SOON,
                       'label'    => $this->__('Coming Soon')
                   );
                   $productStatus['review_star'] = $productStatus['review_number'] = false;
               } else {
                   $productStatus[] = array(
                       'status'     => SM_Filter_Model_Layer::OUT_OF_STOCK,
                       'label'    => $this->__('Sold Out!')
                   );
                   $productStatus['review_star'] = true;
                   $productStatus['review_number'] = false;
               }
               $productStatus['button_label'] = $this->__('PRE-ORDER NOW');
           }
       }

       return $productStatus;
   }*/

    public function getProductStatus($_product)
    {
        $productStatus = array();
        if ($_product instanceof Mage_Catalog_Model_Product) {
            $stock = Mage::getModel('cataloginventory/stock_item')
                ->loadByProduct($_product);
            $now = strtotime(date('Y/m/d'));

            if ($stock->getIsInStock()) {
                if ($_product->getNewsFromDate()) {
                    $newArrivalTime = strtotime(str_replace('-', '/', $_product->getNewsFromDate()));
                    if ($now == $newArrivalTime) {
                        $productStatus[] = array(
                            'status' => SM_Filter_Model_Layer::NEW_ARRIVAL,
                            'label' => 'New Today'
                        );
                    } elseif (date('d', $now) != date('d', $newArrivalTime) && $now > $newArrivalTime && $now - $newArrivalTime <= 24 * 3600) {
                        $productStatus[] = array(
                            'status' => SM_Filter_Model_Layer::NEW_ARRIVAL,
                            'label' => 'New Yesterday'
                        );
                    }
                }
                if (empty($productStatus) && $_product->getBackToStockFrom()) {
                    $stockTime = strtotime(str_replace('-', '/', $_product->getBackToStockFrom()));

                    if ($now == $stockTime) {
                        $productStatus[] = array(
                            'status' => SM_Filter_Model_Layer::BACK_IN_STOCK,
                            'label' => $this->__('Back Today')
                        );
                    } elseif (date('d', $now) != date('d', $stockTime) && $now > $stockTime && $now - $stockTime <= 24 * 3600) {
                        $productStatus[] = array(
                            'status' => SM_Filter_Model_Layer::BACK_IN_STOCK,
                            'label' => $this->__('Back Yesterday')
                        );
                    }
                }
                if ($_product->getTypeId() != 'configurable' && $_product->getTypeId() != 'grouped') {
                    $minQty = Mage::getStoreConfig(self::PRODUCT_LEFT);
                    if ($stock->getQty() > 0 && $stock->getQty() < $minQty) {
                        $productStatus[] = array(
                            'status' => SM_Filter_Model_Layer::ALMOST_GONE,
                            'label' => $this->__('Only %s left!', (int)$stock->getQty())
                        );
                    }
                }

                if (empty($productStatus)) {
                    $productStatus[] = array(
                        'status' => SM_Filter_Model_Layer::IN_STOCK,
                        'label' => $this->__('In Stock')
                    );
                }
                $productStatus['review_star'] = $productStatus['review_number'] = true;
                $productStatus['button_label'] = $this->__('SHOP NOW');
            } else {
                if ($_product->getComingSoon() && strtotime(str_replace('-', '/', $_product->getComingSoonFrom())) > $now) {
                    $productStatus[] = array(
                        'status' => SM_Filter_Model_Layer::COMING_SOON,
                        'label' => $this->__('Coming Soon')
                    );
                    $productStatus['review_star'] = $productStatus['review_number'] = false;
                } else {
                    $productStatus[] = array(
                        'status' => SM_Filter_Model_Layer::OUT_OF_STOCK,
                        'label' => $this->__('Sold Out!')
                    );
                    $productStatus['review_star'] = true;
                    $productStatus['review_number'] = false;
                }
                $productStatus['button_label'] = $this->__('PRE-ORDER NOW');
            }
        }

        return $productStatus;
    }
}