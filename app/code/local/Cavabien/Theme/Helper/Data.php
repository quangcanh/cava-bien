<?php

/**
 * Class Cavabien_Theme_Helper_Data
 */
class Cavabien_Theme_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get param name for search action
     *
     * @return mixed
     */
    public function getCategoryParamName()
    {
        return Mage::getModel('catalog/layer_filter_category')->getRequestVar();
    }

    /**
     * get search result URL
     *
     * @return string
     */
    public function getResultUrl()
    {
        return $this->_getUrl(
                    'cavabien/search/result', array(
                            '_secure' => Mage::app()->getFrontController()->getRequest()->isSecure()
                        )
        );
    }

    /**
     * Check product's conditions that able to display special price
     *
     * @param $product
     *
     * @return bool
     */
    public function ableToDisplaySpecialPrice($product)
    {
        $price = $product->getPrice();
		$specialPrice = $product->getSpecialPrice();
        if (!$product || is_null($product->getSpecialPrice())) {
            return false;
        }
        if((float)$specialPrice>(float)$price){
            return false;
        }

        /** @var $dateModel Mage_Core_Model_Date */
        $dateModel = Mage::getModel('core/date');
        $specialToDateTimeStamp = new DateTime($product->getSpecialToDate());
        $specialToDateTimeStamp->modify('+1 day');
        $specialFromDateTimeStamp = $dateModel->timestamp($product->getSpecialFromDate());
        $specialToDateTimeStamp = $specialToDateTimeStamp->getTimestamp();
        $now = $dateModel->timestamp(time());

        return ($specialFromDateTimeStamp <= $now && $now <= $specialToDateTimeStamp) ? true : false;
    }

    /**
     * Get time remain that valid special price
     *
     * @param $product
     *
     * @return bool|string
     */
    public function getTimeRemain($product)
    {
        if ($this->ableToDisplaySpecialPrice($product)) {
            $specialToDateTimeStamp = new DateTime($product->getSpecialToDate());
            $now = new DateTime();
            $timeRemain = $specialToDateTimeStamp->diff($now);

            return $timeRemain->format('%Hh:%im:%ss');
        }

        return false;
    }

    /**
     * Get time remain that valid special price
     *
     * @param $product
     *
     * @return bool|string
     */
    public function getTimeRemainNew($product)
    {
        if ($this->ableToDisplaySpecialPrice($product)) {
            $specialToDateTimeStamp = new DateTime($product->getSpecialToDate());
            $specialToDateTimeStamp->modify('+1 day');
            $now = new DateTime(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
            $timeRemain = $specialToDateTimeStamp->diff($now);

            return $timeRemain->format("%y:%m:%d:%H:%i:%s");
        }

        return false;
    }

    /**
     * Get html of configurable product options block
     *
     * @param $product
     *
     * @return string
     */
    public function getConfigurableOptionsBlock($product)
    {
        $optionsBlock = Mage::app()->getLayout()
                            ->createBlock('amconf/catalog_product_view_type_configurablel', 'amconf.catalog_product_view_type_configurable', array('template' => "amasty/amconf/configurable.phtml"));
        $optionsBlock->setProduct($product);
        $optionsBlock->setNameInLayout('product.info.options.configurable');
        $html =
            '<div id="insert" style="display:none;"></div><div id="amconf-block">' . $optionsBlock->toHtml() . '</div>';

        return $html;
    }

    /**
     * Return html of 'ADD TO CART' button base on product is 'out stock' or 'coming soon'
     *
     * @param $product
     *
     * @param $blockCssId
     *
     * @param $label
     *
     * @return string
     */
    public function getAddToCartButtonHtml($product, $blockCssId, $label = "Add to cart")
    {
        $html = '';
        $buttonTitle = $label;
        if ($this->isOutOfStockProduct($product)) {
            $buttonTitle = 'Notify me on Restock';
        } elseif ($this->isComingSoonProduct($product)) {
            $buttonTitle = 'Notify Me';
        }

        $optionsBlock = Mage::app()->getLayout()->createBlock('amconf/catalog_product_view_type_configurablel');
        $optionsBlock->setProduct($product);
        $optionsBlock->setNameInLayout('product.info.options.configurables');
        $submitUrl = $optionsBlock->getSubmitUrl($product);
        $optionsBlock->getJsonConfig();
        $attributes = $optionsBlock->getAttributes();
        if( $buttonTitle == 'Notify Me' && $blockCssId=='product-detail'){
            $onClick = "jQuery('.popup-comingsoon').show();return false;";
        }else{
            $onClick = "formSubmit(this,'" . $submitUrl . "', '" . $product->getId().$blockCssId . "', " . $attributes . ", '".$blockCssId."')";
        }
        $amConf = "createForm('" . $submitUrl . "', '" . $product->getId().$blockCssId . "', " . $attributes . ")";
        $html .=
            '<button type="button" title="' . $this->__($buttonTitle) . '" class="button btn-cart" onclick="' . $onClick
            . '"  amconf="' . $amConf . '">
                <span><span>' . $this->__($buttonTitle) . '</span></span>
            </button>';

        return $html;
    }

    /**
     * Return html of 'BUY NOW' button base on product is 'out stock' or 'coming soon'
     *
     * @param $product
     *
     * @param $blockCssId
     *
     * @return string
     */
    public function getBuyNowButtonHtml($product, $blockCssId)
    {
        $html = '';
        if ($this->isOutOfStockProduct($product)) {
            $buttonTitle = 'Order Now';
        } elseif ($this->isComingSoonProduct($product)) {
            $buttonTitle = 'Pre-order Now';
        } else if($blockCssId=='shopnow'){
            $buttonTitle = 'Shop Now';
        }else{
            $buttonTitle = 'Buy Now';

        }

        $optionsBlock = Mage::app()->getLayout()->createBlock('amconf/catalog_product_view_type_configurablel');
        $optionsBlock->setProduct($product);
        $optionsBlock->setNameInLayout('product.info.options.configurables');
        $submitUrl = $optionsBlock->getSubmitUrl($product);
        $optionsBlock->getJsonConfig();
        $attributes = $optionsBlock->getAttributes();
        $onClick = "formSubmit(this,'" . $submitUrl . "', '" . $product->getId().$blockCssId . "', " . $attributes . ")";
        $amConf = "createForm('" . $submitUrl . "', '" . $product->getId().$blockCssId . "', " . $attributes . ")";
        $html .=
            '<button type="button" title="' . $this->__($buttonTitle) . '" class="button btn-cart" onclick="' . $onClick
            . '"  amconf="' . $amConf . '">
                <span><span>' . $this->__($buttonTitle) . '</span></span>
            </button>';

        return $html;
    }

    /**
     * Return true if product is out of stock
     *
     * @param $product
     *
     * @return bool
     */
    public function isOutOfStockProduct($product)
    {
        return !($product->isAvailable());
    }

    /**
     * Return true if product is coming soon product
     *
     * @param $product
     */
    public function isComingSoonProduct($product)
    {
        return $product->getComingSoon();
    }

    /**
     * Get currency icon by currency code
     *
     * @param $currencyCode
     *
     * @return string
     */
    public function getCurrencyIcon($currencyCode)
    {
        return Mage::getDesign()->getSkinUrl() . 'images/currency/' . $currencyCode . '.gif';
    }

    /**
     * Return html of product options block
     *
     * @param $_product
     *
     * @param $blockCssId
     *
     * @return string
     */
    public function getProductOptionsBlock($_product, $blockCssId)
    {
        $blockForForm = Mage::app()->getLayout()
                            ->createBlock('amconf/catalog_product_view_type_configurablel', 'amconf.catalog_product_view_type_configurable', array('template' => "amasty/amconf/configurable.phtml"));
        $blockForForm->setData('block_id', $blockCssId);
        $blockForForm->setProduct($_product);
        $blockForForm->setNameInLayout('product.info.options.configurable');
        $html = '<div id="insert" style="display:none;"></div><div class="amconf-block">'
            . $blockForForm->toHtml() . '</div>';

        return $html;
    }

    public function globalCategory($catId)
    {
        $specialCategory = array(
            Mage::getStoreConfig(Cavabien_Theme_Block_Layer_Top::NEW_ARRIVAL_CAT),
            Mage::getStoreConfig(Cavabien_Theme_Block_Layer_Top::BACK_IN_STOCK_CAT),
            Mage::getStoreConfig(Cavabien_Theme_Block_Layer_Top::COMING_SOON_CAT),
            Mage::getStoreConfig(Cavabien_Theme_Block_Layer_Top::TOPRATE_CAT),
            Mage::getStoreConfig(Cavabien_Theme_Block_Layer_Top::ALMOST_GONE_CAT)
        );
        if (in_array($catId, $specialCategory)) {
            return true;
        }
        return false;
    }

    function getOffersByCategoryId($categoryId){
        //DISCOUNTS & OFFERS
        $offerName = Mage::getStoreConfig('homepage/header/discount-offer-sub-category-name');
        if(!$offerName) return null;
        $collection = Mage::getModel('catalog/category')->getCollection();
        $collection->addAttributeToSelect('*')
            ->addAttributeToFilter('parent_id',$categoryId)
            ->addAttributeToFilter('name',$offerName);
        $collection->getFirstItem();
        foreach($collection as $category){
            $cat = Mage::getModel('catalog/category')->load($category->getId());
            return $cat->getURL();
        }
        return null;

    }

    function getCertificateByCategoryId($categoryId){
        //GIFT CERTIFICATE
        $certificateName = Mage::getStoreConfig('homepage/header/gift-certificate-sub-category-name');
        if(!$certificateName) return null;
        $collection = Mage::getModel('catalog/category')->getCollection();
        $collection->addAttributeToSelect('*')
            ->addAttributeToFilter('parent_id',$categoryId)
            ->addAttributeToFilter('name',$certificateName);
        $collection->getFirstItem();
        foreach($collection as $category){
            $cat = Mage::getModel('catalog/category')->load($category->getId());
            return $cat;
        }
        return null;

    }

    /**
     * return stock status of product
     * @param $product
     * @return string
     */
    public function checkNewArrive($product){
        $createAt = new DateTime($product->getCreatedAt());
        $now = new DateTime(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
        $newArrive = $now->diff($createAt);
        $newArrive = $newArrive->format("%y:%m:%d:%H");
        $newArrive = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $newArrive);
        sscanf($newArrive, "%d:%d:%d:%d",$years,$months,$days, $hours);
        $time_hours = $years*24*365 + $months*30*24 +$days*24 + $hours;
        if($time_hours<=24){
            $status = "New Today";
        }elseif($time_hours<=48){
            $status = "New Yesterday";
        }else{
            $status = "In stock";
        }
        return $status;
    }

}