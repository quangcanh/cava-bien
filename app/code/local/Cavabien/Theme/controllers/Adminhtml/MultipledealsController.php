<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 9/25/14
 * Time: 4:29 PM
 */
require_once 'Devinc/Multipledeals/controllers/Adminhtml/MultipledealsController.php';


class Cavabien_Theme_Adminhtml_MultipledealsController extends Devinc_Multipledeals_Adminhtml_MultipledealsController {

    public function saveAction() {

        if ($data = $this->getRequest()->getPost()) {
            //format dates
            $data['datetime_from'] = Mage::getModel('multipledeals/entity_attribute_backend_datetime')->formatDate($data['datetime_from']);
            $data['datetime_to'] = Mage::getModel('multipledeals/entity_attribute_backend_datetime')->formatDate($data['datetime_to']);

            if (!isset($data['deal_qty'])) $data['deal_qty'] = 0;
            if (!isset($data['deal_price'])) $data['deal_price'] = 0;
            if (!isset($data['position']) || $data['position']=='') $data['position'] = 0;
            if (isset($data['stores'])) $data['stores'] = implode(',',$data['stores']);

            $model = Mage::getModel('multipledeals/multipledeals');
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));

            try {
                $model->save();
                $productId = $model->getProductId();

                //verify if product is enabled for at least one website
                $productStatus = 2;
                $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
                $statusId = $eavAttribute->getIdByCode('catalog_product', 'status');

                $resource = Mage::getSingleton('core/resource');
                $connection = $resource->getConnection('core_read');

                $select = $connection->select()
                    ->from($resource->getTableName('catalog_product_entity_int'))
                    ->where('store_id IN (?)', '0,'.$model->getStores())
                    ->where('attribute_id = ?', $statusId)
                    ->where('value = ?', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                    ->where('entity_id = ?', $model->getProductId());

                $rows = $connection->fetchAll($select);

                if (count($rows)>0) {
                    $productStatus = 1;
                }

                //verify if product is in stock and if deal qty is higher than 0
                $product = Mage::getModel('catalog/product')->load($productId);
                $dealQtyValidationTypes = array('simple', 'virtual', 'downloadable');
                $stockItem = $product->getStockItem();
                $productInStockQty = $stockItem->getQty();

                if ($stockItem->getIsInStock()) {
                    if (in_array($product->getTypeId(), $dealQtyValidationTypes)) {
                        $inStock = ($model->getDealQty()>0 && $model->getDealQty() < $productInStockQty) ? true : false;
                    }
                    else {
                        $inStock = true;
                    }
                } else {
                    $inStock = false;
                }

                //disable deal if product is disabled or not in stock
                if ($inStock && $productStatus==1) {
                    $this->_getSession()->addSuccess(Mage::helper('multipledeals')->__('Deal was successfully saved'));
                } elseif($model->getDealQty() > $productInStockQty){
                    $this->_getSession()->addError(Mage::helper('multipledeals')->__('Deal quantity is higher than product qty in stock and deal was disabled'));
                    $model->setStatus(self::STATUS_DISABLED)->save();
                }
                elseif ($productStatus!=1) {
                    $this->_getSession()->addError(Mage::helper('multipledeals')->__('Deal was saved & disabled because the product is disabled.'));
                    $model->setStatus(self::STATUS_DISABLED)->save();
                } elseif (!$inStock) {
                    if ($stockItem->getIsInStock()) {
                        $this->_getSession()->addError(Mage::helper('multipledeals')->__('Deal was saved & disabled because the deal\'s qty is 0.'));
                    }
                    else {
                        $this->_getSession()->addError(Mage::helper('multipledeals')->__('Deal was saved & disabled because the product is out of stock.'));
                    }
                    $model->setStatus(self::STATUS_DISABLED)->save();
                }

                $this->_getSession()->setFormData(false);

                //update deal status
                $model->setDealJustSaved(true);
                Mage::getModel('multipledeals/multipledeals')->refreshDeal($model);

                if ($this->getRequest()->getParam('back')) {
                    //get the page number for the "Select a Product" tab on the deal edit page
                    $pageNr = Mage::helper('multipledeals')->getProductPage($productId);

                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'page' => $pageNr));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);

                //get the page number for the "Select a Product" tab on the deal edit page
                if (($productId = $model->getProductId())>0) {
                    $pageNr = Mage::helper('multipledeals')->getProductPage($productId);
                } else {
                    $pageNr = 1;
                }

                $this->_redirect('*/*/edit', array('id' => $model->getId(), 'page' => $pageNr));
                return;
            }
        }
        $this->_getSession()->addError(Mage::helper('multipledeals')->__('Unable to find deal to save.'));
        $this->_redirect('*/*/');
    }
}