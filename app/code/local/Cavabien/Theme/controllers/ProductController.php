<?php

class Cavabien_Theme_ProductController extends Mage_Core_Controller_Front_Action {
    /**
     * Load product model with data by passed id.
     * Return false if product was not loaded or has incorrect status.
     *
     * @param int $productId
     * @return bool|Mage_Catalog_Model_Product
     */
    protected function _loadProduct($productId)
    {
        if (!$productId) {
            return false;
        }

        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productId);
        /* @var $product Mage_Catalog_Model_Product */
        if (!$product->getId() || !$product->isVisibleInCatalog() || !$product->isVisibleInSiteVisibility()) {
            return false;
        }

        Mage::register('current_product', $product);
        Mage::register('product', $product);

        return $product;
    }

    /**
     * Initialize and check product
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        Mage::dispatchEvent('review_controller_product_init_before', array('controller_action'=>$this));
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id');

        $product = $this->_loadProduct($productId);
        if (!$product) {
            return false;
        }

        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            Mage::register('current_category', $category);
        }

        try {
            Mage::dispatchEvent('review_controller_product_init', array('product'=>$product));
            Mage::dispatchEvent('review_controller_product_init_after', array(
                'product'           => $product,
                'controller_action' => $this
            ));
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return false;
        }

        return $product;
    }

    /**
     * Submit new review action
     *
     */
    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            // returns to the product item page
            $this->_redirectReferer();
            return;
        }

        if ($data = Mage::getSingleton('review/session')->getFormData(true)) {
            $rating = array();
            if (isset($data['ratings']) && is_array($data['ratings'])) {
                $rating = $data['ratings'];
            }
        } else {
            $data   = $this->getRequest()->getPost();
            $rating = $this->getRequest()->getParam('ratings', array());
        }

        if (($product = $this->_initProduct()) && !empty($data)) {
            $session    = Mage::getSingleton('core/session');
            /* @var $session Mage_Core_Model_Session */
            $review     = Mage::getModel('review/review')->setData($data);
            /* @var $review Mage_Review_Model_Review */

            $validate = $review->validate();

            if ($validate === true) {
                try {
                    $review->setEntityId($review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE))
                        ->setEntityPkValue($product->getId())
                        ->setStatusId(Mage_Review_Model_Review::STATUS_PENDING)
                        ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->setStores(array(Mage::app()->getStore()->getId()))
                        ->save();
										
                    $fileName = '';
                    if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                        try {
                            $fileName       = $_FILES['image']['name'];
                            $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
                            $fileNamewoe    = rtrim($fileName, $fileExt);
                            $fileName       = $fileNamewoe . time() . '.' . $fileExt;

                            $uploader       = new Varien_File_Uploader('image');
                            $uploader->setAllowedExtensions(array('png','jpg','gif'));
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(false);
                            $path = Mage::getBaseDir('media') . DS . 'review';
                            if(!is_dir($path)){
                                mkdir($path, 0777, true);
                            }
                            $uploader->save($path . DS, $fileName );
                        } catch (Exception $e) {
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }
                    }
                    $i=0;
                    $s=0;
                    foreach ($rating as $ratingId => $optionId) {
                        $i++;
                        $s+= $this->changePercent($optionId);
                    }

                    $height = '';
                    $heightInches = '';
                    $bra = '';
                    $braCup = '';
                    $shoe = '';
                    $shoeWidth = '';
                    $bust = '';
                    $waist = '';
                    $hip = '';
                    $dress = '';
                    $customerSesstion = Mage::getSingleton('customer/session');
										$customer = $customerSesstion->getCustomer();
                    if($data['height']!=''){
						$height = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_height_ft')
                            ->getSource()->getOptionText($data['height']);
                        if($customerSesstion->isLoggedIn()){
							$customer->setCustomerHeightFt($data['height']);
						}
                    }

                    if($data['height_inches']!=''){
                        $heightInches = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_height_inches')
                            ->getSource()->getOptionText($data['height_inches']);
						if($customerSesstion->isLoggedIn()){
							$customer->setCustomerHeightInches($data['height_inches']);
						}
                    }

                    if($data['bra']!=''){
                        $bra = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_bra_size_band')
                            ->getSource()->getOptionText($data['bra']);
						if($customerSesstion->isLoggedIn()){
							$customer->setCustomerBraSizeBand($data['bra']);
						}
                    }

                    if($data['bra_size_cup']!=''){
                        $braCup = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_bra_size_cup')
                            ->getSource()->getOptionText($data['bra_size_cup']);
						if($customerSesstion->isLoggedIn()){
							$customer->setBraSizeCup($data['bra_size_cup']);
						}
                    }

                    if($data['shoe']!=''){
                        $shoe = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_shoe_size')
                            ->getSource()->getOptionText($data['shoe']);
						if($customerSesstion->isLoggedIn()){
							$customer->setShoe($data['shoe']);
						}
                    }

                    if($data['shoe_width']!=''){
                        $shoeWidth = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_shoe_width')
                            ->getSource()->getOptionText($data['shoe_width']);
						if($customerSesstion->isLoggedIn()){
							$customer->setCustomerShoeWidth($data['shoe_width']);
						}
                    }

                    if($data['bust']!=''){
                        $bust = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_bust')
                            ->getSource()->getOptionText($data['bust']);
						if($customerSesstion->isLoggedIn()){
							$customer->setBust($data['bust']);
						}
                    }

                    if($data['waist']!=''){
                        $waist = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_waist')
                            ->getSource()->getOptionText($data['waist']);
						if($customerSesstion->isLoggedIn()){
							$customer->setWaist($data['waist']);
						}
                    }

                    if($data['hip']!=''){
                        $hip = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_hips')
                            ->getSource()->getOptionText($data['hip']);
						if($customerSesstion->isLoggedIn()){
							$customer->setHip($data['hip']);
						}
                    }

                    if($data['dress']!=''){
                        $dress = Mage::getResourceSingleton('customer/customer')
                            ->getAttribute('customer_dress_size')
                            ->getSource()->getOptionText($data['dress']);
						if($customerSesstion->isLoggedIn()){
							$customer->setDress($data['dress']);
						}
                    }
					if($customerSesstion->isLoggedIn()){
						$customer->save();
					}

                    $lastReview = Mage::getModel('review/review')->getCollection()->getLastItem();
                    $lastReview->setImage($fileName)
                        ->setItemSize($data['item_size'])
                        ->setAge($data['age'])
                        ->setHeight($height)
                        ->setHeightInches($heightInches)
                        ->setBra($bra)
                        ->setBraCup($braCup)
                        ->setShoe($shoe)
                        ->setShoeWidth($shoeWidth)
                        ->setBust($bust)
                        ->setWaist($waist)
                        ->setHip($hip)
                        ->setDress($dress)
                        ->setRatingPercent($s/$i)
                        ->save();

                    foreach ($rating as $ratingId => $optionId) {
                        Mage::getModel('rating/rating')
                            ->setRatingId($ratingId)
                            ->setReviewId($review->getId())
                            ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
                            ->addOptionVote($optionId, $product->getId());
                    }

                    $review->aggregate();
                    Mage::getSingleton('core/session')->setReviewSuccess('true');
                    $session->addSuccess($this->__('Your review has been accepted for moderation.'));
                }
                catch (Exception $e) {
								die('aa');
                    $session->setFormData($data);
                    $session->addError($this->__('Unable to post the review.'));
                }
            }
            else {
                $session->setFormData($data);
                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                }
                else {
                    $session->addError($this->__('Unable to post the review.'));
                }
            }
        }

        if ($redirectUrl = Mage::getSingleton('review/session')->getRedirectUrl(true)) {
            $this->_redirectUrl($redirectUrl);
            return;
        }
        $this->_redirectReferer();
    }

    public function changePercent($r){
        switch ($r){
            case 1:
                return 20;
            case 2:
                return 40;
            case 3:
                return 60;
            case 4:
                return 80;
            case 5:
                return 100;
            case 6:
                return 20;
            case 7:
                return 40;
            case 8:
                return 60;
            case 9:
                return 80;
            case 10:
                return 100;
            case 11:
                return 20;
            case 12:
                return 40;
            case 13:
                return 60;
            case 14:
                return 80;
            case 15:
                return 100;
            default:
                return false;
        }
    }

}