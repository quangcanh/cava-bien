<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 7/22/14
 * Time: 9:43 AM
 */
require_once 'Devinc/Multipledeals/controllers/IndexController.php';


class Cavabien_Theme_IndexController extends Devinc_Multipledeals_IndexController {


    public function pageAction()
    {
        $params = $this->getRequest()->getParams();
        $page_type = isset($params['deals']) ? $params['deals'] : Mage::registry('pageTypeDeals');
        $catCurrentId = Mage::getStoreConfig('multipledeals_cat_config/category_root_id/multipledeals_cat_current_deal_id');
        $catComingId = Mage::getStoreConfig('multipledeals_cat_config/category_root_id/multipledeals_cat_coming_soon_id');
        $catEndingId = Mage::getStoreConfig('multipledeals_cat_config/category_root_id/multipledeals_cat_endingsoon_id');
        $catId = 0;
        if (!$page_type || $page_type == 'current') {
            $catId = $catCurrentId;
        }
        if ($page_type == 'comesoon') {
            $catId = $catComingId;
        }
        if ($page_type == 'endsoon') {
            $catId = $catEndingId;
        }

        //url : multipledeals/index/page/cat/647/deals/current
        //647 is first sub category of 'deals of week' category
        // get category "deal of week" id
        //$catId = Mage::getStoreConfig('multipledeals_cat_config/category_root_id/multipledeals_cat_root_id');
        $firstSubCatId = Mage::helper('multipledeals')->getFirstSubCatId($catId);

        $params = $this->getRequest()->getParams();
        //print_r($params);echo $params['deals'];echo $params['cat'];
        if(isset($params['cat'])){
            Mage::register('cat_id',$params['cat']);
        }else{
            Mage::register('cat_id',$firstSubCatId);
            $params['cat'] = $firstSubCatId;
        }

        if(isset($params['deals'])){
             Mage::register('pageTypeDeals', $params['deals']);
        }else{
            Mage::register('pageTypeDeals', 'current');
            $params['deals'] = 'current';
        }

        $arrSubCats = Mage::helper('multipledeals')->getSubCatDealsMenu($params['cat']);
        //print_r($arrSubCats);

        if (Mage::helper('multipledeals')->isEnabled()) {
            //Mage::getModel('multipledeals/multipledeals')->refreshDeals();
            $this->loadLayout();
//            if (Mage::helper('multipledeals')->getMagentoVersion()>1411 && Mage::helper('multipledeals')->getMagentoVersion()<1800) {
//                $this->initLayoutMessages(array('catalog/session', 'checkout/session'));
//            }

            //load block deal of day
            $blockDealOfDay = $this->getLayout()->createBlock('cavabien_theme/multipledeals_sidedealofday','block_deal_of_day',
                array('template'=>'multipledeals/dealOfDay.phtml'));
            $this->getLayout()->getBlock('content')->append($blockDealOfDay);

            //load block menu horizon
            $blockMenuHorizon = $this->getLayout()->createBlock('core/template','block_menu_horizon',
                  array('template'=>'multipledeals/blockMenuHorizon.phtml'));
            $this->getLayout()->getBlock('content')->append($blockMenuHorizon);

            //load deals from category
            $num = count($arrSubCats);
            for($i=0;$i<$num;$i++){
                if($arrSubCats[$i]>0 && $arrSubCats[$i]!=""){
                $nameBlock = 'multipledeals_list'.$i;
                $block = $this->getLayout()
                    ->createBlock('cavabien_theme/multipledeals_page',
                        $nameBlock,
                        array('template' => 'multipledeals/page.phtml'))
                    ->assign('data',$arrSubCats[$i]);
                $this->getLayout()->getBlock('content')->append($block);
                }
            }

            $blockJs = $this->getLayout()->createBlock('core/template','block_js',
                array('template'=>'multipledeals/blockjs.phtml'));
            $this->getLayout()->getBlock('content')->append($blockJs);

            $this->renderLayout();
        } else {
            $this->_redirect('no-route');
        }
    }

    public function reviewHelpfullAction(){
        $post = $this->getRequest()->getPost();
        if ($post) {
            $reviewId = $post['id'];
            $isHelpfull = $post['status'];
            $result['success'] = false;
            $result['answer'] = '';
            $review = Mage::getModel('review/review')->load($reviewId);
            $visitorData = Mage::getSingleton('core/session')->getVisitorData();
            $customerId = $visitorData['customer_id'];
            $visitorId = $visitorData['visitor_id'];
            $customerExist = false;
            $visitorExist = false;
            if(isset($customerId)&& $customerId!=null){
                $colectionReviewhelpfull =  Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()
                  ->addFieldToFilter('review_id', array('eq' => $reviewId))
                  ->addFieldToFilter('customer_id', array('eq' => $customerId));
                if(count($colectionReviewhelpfull)>0){
                    $customerExist = true;
                    $visitorExist = true;
                }
            }else{
                $colectionReviewhelpfull =  Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()
                    ->addFieldToFilter('review_id', array('eq' => $reviewId))
                    ->addFieldToFilter('is_new_visitor', array('eq' => $visitorId));
                if(count($colectionReviewhelpfull)>0){
                    $visitorExist = true;
                }
            }
            $helpfull = (int)($review->getHelpfull());
            $newHelpfull = '';
            $noHelpfull = (int)($review->getNoHelpfull());
            $newNoHelpfull = '';
            if($isHelpfull=='yes'){
                $newHelpfull = $helpfull+1;
                if($visitorExist==false){
                    $review->setHelpfull($newHelpfull)->save();
                    $helpfullData = array(
                        'review_id'=>$reviewId,
                        'customer_id'=>$customerId,
                        'review_status'=>$isHelpfull,
                        'is_new_visitor'=>$visitorId
                    );
                    Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->setData($helpfullData)->save();
                }else if($visitorExist==true){
                    if($noHelpfull>0){
                        $newNoHelpfull = $noHelpfull - 1;
                    }else{
                        $newNoHelpfull = '';
                    }
//                    Mage::getModel('pws_productqa/productqahelpfull')->load($visitorId,'is_new_visitor')->setReviewStatus('yes')->save();
                    if(isset($customerId)&& $customerId!=null){
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('customer_id', array('eq' => $customerId))->walk('delete');
                        Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()->addFieldToFilter('review_id', array('eq' => $reviewId))->addFieldToFilter('customer_id', array('eq' => $customerId))->getFirstItem()->setReviewStatus('yes')->save();
                    }else{
//                        Mage::getModel('pws_productqa/productqahelpfull')->getCollection()->addFieldToFilter('productqa_id', array('eq' => $qaId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->walk('delete');
                        Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()->addFieldToFilter('review_id', array('eq' => $reviewId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->getFirstItem()->setReviewStatus('yes')->save();
                    }
                    $review->setHelpfull($newHelpfull)->save();
                    $review->setNoHelpfull($newNoHelpfull)->save();
                }
                $result['answer'] = 'yes';
                $result['success'] = true;
            }else if($isHelpfull=='no'){
                $newNoHelpfull = $noHelpfull + 1;
                if($visitorExist==true){
                    if($helpfull>0){
                        $newHelpfull = $helpfull-1;
                        $review->setHelpfull($newHelpfull)->save();
                    }
                    if(isset($customerId)&& $customerId!=null){
                        Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()->addFieldToFilter('review_id', array('eq' => $reviewId))->addFieldToFilter('customer_id', array('eq' => $customerId))->getFirstItem()->setReviewStatus('no')->save();
                    }else{
                        Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->getCollection()->addFieldToFilter('review_id', array('eq' => $reviewId))->addFieldToFilter('is_new_visitor', array('eq' => $visitorId))->addFieldToFilter('customer_id', array('null' => true))->getFirstItem()->setReviewStatus('no')->save();
                    }
                }else if($visitorExist==false){
                    $helpfullData = array(
                        'review_id'=>$reviewId,
                        'customer_id'=>$customerId,
                        'review_status'=>$isHelpfull,
                        'is_new_visitor'=>$visitorId
                    );
                    Mage::getModel('cavabien_cavabienreview/reviewhelpfull')->setData($helpfullData)->save();
                }
                $review->setNoHelpfull($newNoHelpfull)->save();
                $result['answer'] = 'no';
                $result['success'] = true;
            }
            $result['newHelpfull'] = $newHelpfull;
            $result['newNoHelpfull'] = $newNoHelpfull;
            $response = $this->getResponse();
            $response->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function filterReviewAction(){
        $post = $this->getRequest()->getPost();
        if ($post) {
            $result['success'] = false;
            $collection = '';
            $filter = $post['val'];
            $productId = $post['productId'];
            $reviewCollection = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                ->addEntityFilter('product', $productId)
                ->setDateOrder();
            switch($filter){
                case 0:

                    $result['success'] = true;
                    break;
                case 1:
                    $customerIdArray = array();
                    foreach($reviewCollection as $review){
                        if($review->getCustomerId()!=null){
                            array_push($customerIdArray,$review->getCustomerId());
                        }
                    }
                    $customerIdArrayUnique = array_unique($customerIdArray);
                    $cavaStaffArray = '(';
                    foreach($customerIdArrayUnique as $customerId){
                        $cus = Mage::getModel('customer/customer')->load($customerId);
                        if($cus->getCavabienStaff()==1){
                            $cavaStaffArray .= $customerId.',';
                        }
                    }
                    $cavaStaffArray = substr($cavaStaffArray,0,-1).')';
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');
                    $query = "SELECT detail_id FROM review_detail
                            INNER JOIN review
                            ON review_detail.review_id = review.review_id
                            WHERE customer_id IN ".$cavaStaffArray;

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);

                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection = Mage::getModel('review/review')->getCollection()
                        ->addStoreFilter(Mage::app()->getStore()->getId())
                        ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                        ->addEntityFilter('product', $productId)
                        ->setDateOrder();
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 2:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM review_detail
                                WHERE customer_id IN
                                (SELECT customer_id FROM
                                (SELECT customer_id,count(customer_id) as c
                                FROM `review_detail`
                                WHERE customer_id IS NOT NULL
                                GROUP BY customer_id ORDER BY c DESC) as tbl
                                WHERE tbl.c >= ALL
                                (SELECT count(customer_id) as c
                                FROM `review_detail`
                                WHERE customer_id IS NOT NULL
                                GROUP BY customer_id ORDER BY c DESC) )";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 3:
                    $collection = $reviewCollection->setPageSize(1);
                    $result['success'] = true;
                    break;
                case 4:
                    $reviewCollection->getSelect()->order('detail_id ASC')->limit(1);
                    $collection = $reviewCollection;
                    $result['success'] = true;
                    break;
                case 5:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` INNER JOIN `review` ON `review_detail`.review_id = `review`.review_id WHERE rating_percent = (SELECT MAX(rating_percent) FROM `review`)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);

                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 6:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` INNER JOIN `review` ON `review_detail`.review_id = `review`.review_id WHERE rating_percent = (SELECT MIN(rating_percent) FROM `review`)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);

                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 7:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` INNER JOIN `review` ON `review_detail`.review_id = `review`.review_id WHERE helpfull = (SELECT MAX(helpfull) FROM `review`)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 8:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` INNER JOIN `review` ON `review_detail`.review_id = `review`.review_id WHERE helpfull = (SELECT MIN(helpfull) FROM `review`)";

                    /**
                     * Execute the query and store the results in $results                                                                           \
                     *
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 9:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` WHERE LENGTH(detail) = (SELECT MAX(LENGTH(detail)) FROM `review_detail`)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
                case 10:
                    /**
                     * Get the resource model
                     */
                    $resource = Mage::getSingleton('core/resource');

                    /**
                     * Retrieve the read connection
                     */
                    $readConnection = $resource->getConnection('core_read');

                    $query = "SELECT detail_id FROM `review_detail` WHERE LENGTH(detail) = (SELECT MIN(LENGTH(detail)) FROM `review_detail`)";

                    /**
                     * Execute the query and store the results in $results
                     */
                    $results = $readConnection->fetchAll($query);
                    $arrDetailId = '(';
                    foreach($results as $res){
                        foreach($res as $k=>$v){
                            $arrDetailId.=$v.',';
                        }
                    }
                    $arrDetailId = substr($arrDetailId,0,-1).')';
                    $reviewCollection->getSelect()->where('detail_id IN'.$arrDetailId);
                    $collection =  $reviewCollection;
                    $result['success'] = true;
                    break;
            }
//            $pager = $this->getLayout()->createBlock('page/html_pager')->setTemplate('page/html/pager-review.phtml');
//            $toolbar = $this->getLayout()->createBlock('catalog/product_list_toolbar')->setCollection($collection)->setChild('pager_after_filter',$pager)->setTemplate('catalog/product/list/toolbar.phtml');
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            Mage::register('product', $product);
            $block = $this->getLayout()->createBlock('review/product_view_list','filterreview')->setProductId($productId)->setFilterReview($collection)->setTemplate('catalog/product/view/tabs/bottom/review-filter.phtml');
            $result['afterfilter'] =  $block->toHtml();
            $response = $this->getResponse();
            $response->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function reviewSubscribeAction(){
        $post = $this->getRequest()->getPost();
        if ($post) {
            $check = $post['check'];
            $customerEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
            if(isset($customerEmail)&&$customerEmail!=null){
                if($check=='checked'){
                    $ownerId = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($customerEmail)
                        ->getId();
                    $customerSession    = Mage::getSingleton('customer/session');
                    if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                        Mage::throwException($this->__('This email address is already assigned to another user.'));
                    }
                    Mage::getModel('newsletter/subscriber')->subscribe($customerEmail);
                    $result['customerSubscribe'] = true;
                    $result['guest'] = false;
                }else if($check=='uncheck'){
                    Mage::getModel('newsletter/subscriber')->loadByEmail($customerEmail)
                        ->unsubscribe();
                    $result['customerSubscribe'] = false;
                    $result['guest'] = false;
                }
            }else{
                $result['guest'] = true;
            }
            $result['success'] = true;
            $response = $this->getResponse();
            $response->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function IscustomerEmailExists($email, $websiteId = null){
        $customer = Mage::getModel('customer/customer');

        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            return $customer->getId();
        }
        return false;
    }

    public function createAction()
    {
        $data   = $this->getRequest()->getPost();
        $customer = Mage::getModel('customer/customer');

        $firstName = $data['firstname'];
        $lastName = $data['lastname'];
        $email = $data['email'];
        $password = $data['password'];
        $securityQuestion = $data['customer_security_question'];
        $securityAnswer = $data['customer_security_answer'];
        $dob = $data['dob'];

        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);
        //Zend_Debug::dump($customer->debug()); exit;

        $websiteId = Mage::app()->getWebsite()->getId();
        $custExist = $this->IscustomerEmailExists($email,$websiteId);

        if($custExist)
        {
            //echo 'email exist';
            $message = 'email exist';
            Mage::getSingleton('core/session')->addError($message);
            Mage::getSingleton('core/session')->setCustomerDataSession($data);
            $this->_redirectUrl('/customer/account/login');
        }
        else
        {
            //echo 'email not exist';
            if(!$customer->getId()) {
                $customer->setFirstname($firstName);
                $customer->setLastname($lastName);
                $customer->setEmail($email);
                $customer->setPassword($password);
                $customer->setData('customer_security_question', $securityQuestion);
                $customer->setData('customer_security_answer', $securityAnswer);
                $customer->setData('dob',date($dob));
            }

            try {
                $customer->save();
                $customer->setConfirmation(null);
                $customer->save();
                Mage::getSingleton('core/session')->unsCustomerDataSession();
                //Make a "login" of new customer
                Mage::getSingleton('customer/session')->loginById($customer->getId());
                $this->_redirectUrl('/customer/account');
            }
            catch (Exception $ex) {
                Zend_Debug::dump($ex->getMessage());
            }

        }



    }

}