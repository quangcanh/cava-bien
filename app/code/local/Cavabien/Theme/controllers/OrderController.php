<?php
/**
 * Created by PhpStorm.
 * User: t
 * Date: 9/26/14
 * Time: 11:51 AM
 */
require_once 'Mage/Sales/controllers/OrderController.php';


class Cavabien_Theme_OrderController extends Mage_Sales_OrderController {

    public function historyAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

//        $this->getLayout()->getBlock('head')->setTitle($this->__('My Orders'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

}