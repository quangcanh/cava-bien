<?php

class Cavabien_Theme_SearchController extends Mage_Core_Controller_Front_Action
{
    public function resultAction()
    {
        $resultArray = array();
        $query = Mage::helper('catalogsearch')->getQuery();
        /* @var $query Mage_CatalogSearch_Model_Query */
        $request = $this->getRequest();


        $q = $request->getParam('q');
        $query->setQueryText($q);
        $query->setStoreId(Mage::app()->getStore()->getId());
        if ($query->getQueryText() != '') {
            if ($query->getId()) {
                $query->setPopularity($query->getPopularity() + 1);
            } else {
                $query->setPopularity(1);
            }

            if ($query->getRedirect()) {
                $query->save();
                $this->getResponse()->setRedirect($query->getRedirect());

                return;
            } else {
                $query->prepare();
            }
            if ($request->getParam('cat') != "" && $request->getParam('cat') != 0) {
                $categoryFilter = Mage::getModel('catalog/category')->load($request->getParam('cat'));
                //Mage::log($categoryFilter);
                $resultCollection = $query->getResultCollection()->addCategoryFilter($categoryFilter);
            } else {
                $resultCollection = $query->getResultCollection();
            }

            foreach ($resultCollection as $result) {
                $product = Mage::getModel('catalog/product')->load($result->getId());
                if ($product->getStatus() == 1) {
                    $resultArray[$product->getId()] = array(
                        'name' => $product->getName(),
                        'url' => $product->getProductUrl(),
                        'img' => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(50, 70),
                        //'desc' => $product->getDescription(),
                        'saleable' => $product->isSaleable(),
                        'price' => (string)Mage::helper('checkout')->convertPrice($product->getFinalPrice()),
                        'addtocartURL' => Mage::helper('checkout/cart')->getAddUrl($product)

                    );
                }
            }
        }

        $response = $this->getResponse();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(Mage::helper('core')->jsonEncode($resultArray));
    }
}