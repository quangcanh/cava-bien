<?php

class Cavabien_Theme_MyaccountController extends Mage_Core_Controller_Front_Action
{

    public function preDispatch()
    {
        parent::preDispatch();
        //$action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    public function notificationAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function deletenotificationAction()
    {
        $id = $this->getRequest()->getParam('id');
        $email = $this->getRequest()->getParam('email');
        $_resource = Mage::getSingleton('core/resource');
        $_tableName = $_resource->getTableName('cavabien_notify/notify');
        $sql = "DELETE FROM ".$_tableName." WHERE product_id='".$id."' and customer_email='".$email."'";
        $connection = $_resource->getConnection('core_write');
        $connection->query($sql);
        $url = Mage::getBaseUrl().'cavabien/myaccount/notification';
        $this->_redirectUrl($url);
    }

    public function loveitAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
 
}