<?php

/**
 * Include CartController.php
 */
require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

/**
 * Class Cavabien_Theme_AddmoreController
 */
class Cavabien_Theme_CartController extends Mage_Checkout_CartController
{
    /**
     * Add multiple product into cart
     */

    public function addMultipleAction()
    {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();

        $optionsAttributeSelected = array();
        $superAttributes = $params['super_attribute'];
        foreach ($superAttributes as $fakeAttributeId => $optionValueId) {
            $attributeId = explode('_', $fakeAttributeId);
            $optionsAttributeSelected[$attributeId[1]][$attributeId[0]] = $optionValueId;
        }

        $qtyEntered = array();
        foreach ($params['qty'] as $fakeGroupName => $qty) {
            $groupName = explode('_', $fakeGroupName);
            $qtyEntered[$groupName[1]] = $qty;
        }

        $addToCartProducts = array();
        foreach ($optionsAttributeSelected as $group => $attributeGroup) {
            $addToCartProducts[] = array(
                'product' => $params['product'],
                'super_attribute' => $attributeGroup,
                'qty' => $qtyEntered[$group],
            );
        }

        try {
            foreach ($addToCartProducts as $item) {
                if (isset($item['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $item['qty'] = $filter->filter($item['qty']);
                }

                $product = Mage::getModel('catalog/product')->load($item['product']);
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    $this->_goBack();

                    return;
                }

                $cart->addProduct($product, $item);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }
            }

            $cart->save();
            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent(
                'checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__(
                        '%s was added to your shopping cart.', Mage::helper('core')
                            ->escapeHtml($product->getName())
                    );
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }


    public function addGroupToCartAction()
    {
        $products =  explode(',', $this->getRequest()->getParam('products'));
        $cart = Mage::getModel('checkout/cart');
        $cart->init();
        /* @var $pModel Mage_Catalog_Model_Product */
        foreach ($products as $product_id) {
            if ($product_id == '') continue;
            //$pModel->unsetData();
            $pModel = Mage::getModel('catalog/product')->load($product_id);
            //$pModel;
            if ($pModel->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                try {
                    $cart->addProduct($pModel, array('qty' => '1'));
                } catch (Exception $e) {
                    continue;
                }
            }
        }
        $cart->save();
        if ($this->getRequest()->isXmlHttpRequest()) {
            exit('1');
        }

        $this->_redirect('checkout/cart');
    }


}