<?php
 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$content = <<<EOD
<p><strong>Our Guarantee</strong></p>
<p>We are here to help! We guarantee to provide you the best possible online shopping experience.</p>
<div class="clearfix">
<div class="left live-chat">
<p><strong>Live Chat</strong><em class="pink-color"> Online Now!</em></p>
<p>Got questions? We've got answers!</p>
</div>
<div class="right white-to-us">
<p><strong><a href="mailto:hello@cava-bien.com">Write to Us</a></strong></p>
<p>We respond within 24 hours!</p>
</div>
</div>
<div>
<p class="call-us a-center"><span class="black-phone-icon"><strong>Call Us</strong><em class="pink-color"> Available Now!</em></span></p>
<p class="a-center">Want to talk someone over the phone? <strong>0800 123 4567</strong></p>
</div>
EOD;
$staticBlock = array(
    'title' => 'Product Detail Tab Contact Us',
    'identifier' => 'product_detail_tab_contact_us',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(1)
);
$block = Mage::getModel('cms/block')->load('product_detail_tab_contact_us');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$installer->endSetup();