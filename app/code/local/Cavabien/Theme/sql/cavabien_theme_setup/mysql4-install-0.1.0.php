<?php
 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', "back_to_stock_from", array(
    'type'       => 'varchar',
    'input'      => 'date',
    'label'      => 'Back To Stock From',
    'sort_order' => 1000,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "back_to_stock_to", array(
    'type'       => 'varchar',
    'input'      => 'date',
    'label'      => 'Back To Stock To',
    'sort_order' => 1001,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "coming_soon", array(
    'type'       => 'int',
    'input'      => 'boolean',
    'label'      => 'Coming Soon',
    'sort_order' => 1000,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "new_arrival", array(
    'type'       => 'datetime',
    'input'      => 'date',
    'label'      => 'New Arrivals',
    'sort_order' => 1000,
    'required'   => false,
    'user_defined' => false,
    'used_for_sort_by' => true,
    'backend' => 'cavabien_theme/entity_attribute_backend_newarrival',
    'visible'      => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "top_rate", array(
    'type'       => 'int',
    'input'      => 'text',
    'label'      => 'Top Rate',
    'required'   => false,
    'user_defined' => false,
    'used_for_sort_by' => true,
    'visible'      => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "bestseller", array(
    'type'       => 'int',
    'input'      => 'text',
    'label'      => 'Bestseller',
    'required'   => false,
    'user_defined' => false,
    'used_for_sort_by' => true,
    'visible'      => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->addAttribute('catalog_product', "facebook_like", array(
    'type'       => 'int',
    'input'      => 'text',
    'label'      => 'Facebook Like',
    'required'   => false,
    'user_defined' => false,
    'used_for_sort_by' => true,
    'visible'      => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));

$installer->endSetup();