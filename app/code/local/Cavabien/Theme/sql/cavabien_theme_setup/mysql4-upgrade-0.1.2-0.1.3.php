<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');

$attributeSPTheme = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', "stylist_pick_theme");
if(!$attributeSPTheme->getId()){
    $objCatalogEavSetup->addAttribute('catalog_product', 'stylist_pick_theme', array(
        'group' => 'General',
        'type' => 'text',
        'backend' => '',
        'frontend' => '',
        'label' => 'Stylist Pick Theme',
        'note' => 'For Stylist Pick Products',
        'input' => 'select',
        'source' => 'cavabien_special/styleoption',
        'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => true, //use search
        'comparable' => false,
        'visible_on_front' => true,
        'used_in_product_listing'=> '1',
        'apply_to' => 'grouped',
        'unique' => true,
    ));
}
$installer->endSetup();