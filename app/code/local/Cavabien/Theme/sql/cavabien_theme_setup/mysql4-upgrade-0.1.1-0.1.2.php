<?php

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// Set attribute global
$entity_type = 3;
$attributeCodes = array("url_key", "custom_layout_update", "page_layout");
$data = array ('is_global' => 1);

foreach ($attributeCodes as $attrCode) {
    $attr = Mage::getModel('eav/entity_attribute')->loadByCode($entity_type, $attrCode);
    $catalog_attr = Mage::getModel('catalog/resource_eav_attribute')->load((int) $attr->getId());
    $catalog_attr->addData($data)->save();
}

//Create Category
$customLayout = <<<EOD
<reference name="category.products">
   <block type="catalog/product_list" name="product_list" template="catalog/product/shopthelook/list.phtml" />
</reference>
EOD;

$categoryUrlkey = "shop-the-look";
$categoryName = "Shop The Look";

$storeId = Mage::app()->getStore()->getStoreId();
$store = Mage::getModel('core/store')->load($storeId);
$group = Mage::getModel('core/store_group')->load($store->getGroupId());

$exist = Mage::getModel('catalog/category')->getCollection()
    ->addFieldToFilter('url_key', $categoryUrlkey);
if (!count($exist)) {
    $cat = Mage::getModel('catalog/category');
    $cat->setPath('1/' . $group->getRootCategoryId())
        ->setName($categoryName)
        ->setUrlKey($categoryUrlkey)
        ->setStoreId($storeId)
        ->setIsActive(1)
        ->setIsAnchor(1)
        ->setPageLayout('one_column')
        ->setCustomLayoutUpdate($customLayout)
        ->save();
}
$installer->endSetup();