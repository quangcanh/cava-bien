<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$objCatalogEavSetup->addAttribute('catalog_product', "back_in_stock", array(
    'type'       => 'int',
    'input'      => 'boolean',
    'label'      => 'Back in stock',
    'sort_order' => 1000,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped', //array('grouped')
));
$installer->endSetup();