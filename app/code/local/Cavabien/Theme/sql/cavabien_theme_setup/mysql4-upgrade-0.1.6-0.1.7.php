<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'image'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'item_size'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'age'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'height'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'bra'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'shoe'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'bust'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'waist'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'hip'      //column name
);

$installer->getConnection()->dropColumn(
    $this->getTable('review/review_detail'), //table name
    'dress'      //column name
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'image',      //column name
    'varchar(255)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'item_size',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'age',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'height',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'bra',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'shoe',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'bust',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'waist',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'hip',      //column name
    'int(11)'  //datatype definition
);

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'dress',      //column name
    'int(11)'  //datatype definition
);

$installer->endSetup();