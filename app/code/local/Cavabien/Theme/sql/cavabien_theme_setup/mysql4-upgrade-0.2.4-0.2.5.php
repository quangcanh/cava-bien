<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$giftcard_banner_content = <<<EOD
<div class="giftcard-banner-img">
    <img src="{{skin url='images/banner/giftcard-banner.jpg'}}" alt="Gift Card Banner" />
    <p>
        <span class="gift-icon"></span>
        Gift certificates are the simplest, speediest way to send something special! They can either be emailed to the lucky recipient or printed out at home
        (but we do not mail out physical cards). Both options include a personal note in a lovely layout. For more info, see out <a href="#">Gift Certificate FAQ</a>
    </p>
</div>
EOD;
$giftcard_banner_staticBlock = array(
    'title' => 'Gift Card Certificate Banner',
    'identifier' => 'giftcard_certificate_banner',
    'content' => $giftcard_banner_content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('giftcard_certificate_banner');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($giftcard_banner_staticBlock)->save();
} else {
    $block->setContent($giftcard_banner_content)->save();
}

$installer->endSetup();