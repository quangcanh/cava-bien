<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// Size Chart Content
$chartContent = <<<EOD
<div class="product-tabs-container clearfix">
    <ul class="product-tabs">
        <li id="size_clothing_tab">
            <a href="javascript:void(0)">Clothing</a>
        </li>
        <li id="size_bottoms_tab">
            <a href="javascript:void(0)">Bottoms</a>
        </li>
        <li id="size_shoes_tab">
            <a href="javascript:void(0)">Shoes</a>
        </li>
        <li id="size_intimates_tab">
            <a href="javascript:void(0)">Intimates</a>
        </li>
        <li id="size_accessories_tab">
            <a href="javascript:void(0)">Accessories</a>
        </li>
        <li id="size_sport_tab">
            <a href="javascript:void(0)">Sport</a>
        </li>
        <li id="size_swim_tab">
            <a href="javascript:void(0)">Swim</a>
        </li>
    </ul>
    <div id="size_clothing_tab_contents" class="tabs-content">{{block type="cms/block" block_id="size_chart_clothing_size" }}</div>
    <div id="size_bottoms_tab_contents" class="tabs-content">{{block type="cms/block" block_id="bottoms_size_chart" }}</div>
    <div id="size_shoes_tab_contents" class="tabs-content">{{block type="cms/block" block_id="size_chart_shoes_size" }}</div>
    <div id="size_intimates_tab_contents" class="tabs-content">{{block type="cms/block" block_id="intimates_size_chart" }}</div>
    <div id="size_accessories_tab_contents" class="tabs-content">{{block type="cms/block" block_id="accessories_size_chart" }}</div>
    <div id="size_sport_tab_contents" class="tabs-content">{{block type="cms/block" block_id="sport_size_chart" }}</div>
    <div id="size_swim_tab_contents" class="tabs-content">{{block type="cms/block" block_id="swim_size_chart" }}</div>
</div>
EOD;

// Clothing Tab Content
$clothingContent = <<<EOD
<div class="left-content">
    <img src="{{skin url='images/size-chart-popup-img.jpg'}} " alt="Clothing Size" />
    <a href="javascript:void(0);">
        <p class="need-help">Need A Little Help?</p>
    </a>
</div>
<div class="right-content">
    <p class="text-content">
        All conversions are approximate. Fits may vary by style or personal preference;
        sizes may vary by manufacturer.
    </p>
    <div class="chart">
        <ul class="product-tabs measure">
            <li id="measure_inches_tab">
                <a href="javascript:void(0);">Inches</a>
            </li>
            <li id="measure_centimeters_tab">
                <a href="javascript:void(0);">Centimeters</a>
            </li>
        </ul>
        <div id="measure_inches_tab_contents">
            <div class="chart-static">
                <ul class="chart-column">
                    <li>
                        <p class="title">Size</p>
                        <ul class="chart-list">
                            <li>XXS</li>
                            <li>XS</li>
                            <li>XS</li>
                            <li>S</li>
                            <li>S</li>
                            <li>M</li>
                            <li>M</li>
                            <li>L</li>
                            <li>L</li>
                            <li>XL</li>
                            <li>XL</li>
                            <li>XXL</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">Bust</p>
                        <ul class="chart-list">
                            <li>80</li>
                            <li>82.5</li>
                            <li>85</li>
                            <li>87.5</li>
                            <li>90</li>
                            <li>92.5</li>
                            <li>95</li>
                            <li>99</li>
                            <li>103</li>
                            <li>107</li>
                            <li>103</li>
                            <li>107</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">Waist</p>
                        <ul class="chart-list">
                            <li>61</li>
                            <li>64</li>
                            <li>66</li>
                            <li>68.5</li>
                            <li>71</li>
                            <li>73.5</li>
                            <li>76</li>
                            <li>80</li>
                            <li>84</li>
                            <li>88</li>
                            <li>84</li>
                            <li>88</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">Hip</p>
                        <ul class="chart-list">
                            <li>86</li>
                            <li>89</li>
                            <li>91.5</li>
                            <li>94</li>
                            <li>97</li>
                            <li>99</li>
                            <li>101.5</li>
                            <li>105.5</li>
                            <li>109</li>
                            <li>113</li>
                            <li>109</li>
                            <li>113</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="chart-country">
                <ul class="chart-column">
                    <li>
                        <p class="title">U.S</p>
                        <ul class="chart-list">
                            <li>2</li>
                            <li>4</li>
                            <li>6</li>
                            <li>8</li>
                            <li>10</li>
                            <li>12</li>
                            <li>14</li>
                            <li>16</li>
                            <li>18</li>
                            <li>20</li>
                            <li>22</li>
                            <li>24</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">U.K.</p>
                        <ul class="chart-list">
                            <li>26</li>
                            <li>28</li>
                            <li>30</li>
                            <li>32</li>
                            <li>34</li>
                            <li>36</li>
                            <li>38</li>
                            <li>40</li>
                            <li>42</li>
                            <li>44</li>
                            <li>46</li>
                            <li>48</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">Australia</p>
                        <ul class="chart-list">
                            <li>30</li>
                            <li>32</li>
                            <li>34</li>
                            <li>36</li>
                            <li>38</li>
                            <li>40</li>
                            <li>42</li>
                            <li>44</li>
                            <li>46</li>
                            <li>48</li>
                            <li>50</li>
                            <li>52</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title">Germany</p>
                        <ul class="chart-list">
                            <li>34</li>
                            <li>36</li>
                            <li>38</li>
                            <li>40</li>
                            <li>42</li>
                            <li>44</li>
                            <li>46</li>
                            <li>48</li>
                            <li>50</li>
                            <li>52</li>
                            <li>54</li>
                            <li>56</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div id="measure_centimeters_tab_contents">
            Centimeters Tab Content
        </div>
    </div>
    <p class="text-content">
        Still not sure what you need? Our <span>Very</span> Personal Stylists can help
        you find the perfect size.
    </p>
    <div class="quick-contact">
        <a class="pink-phone-icon">CUSTOMER CARE: 0800 123 4567</a>
        <a class="pink-chat-icon" href="#">Live Chat</a>
        <span>EMAIL: stylist@cava-bien.com</span>
    </div>
</div>
EOD;

// Bottoms Tab Content
$bottomsContent = <<<EOD
<p>Bottoms Content</p>
EOD;

// Shoes Tab Content
$shoesContent = <<<EOD
<p>Shoes Content</p>
EOD;

// Intimates Tab Content
$intimatesContent = <<<EOD
<p>Intimates Content</p>
EOD;

// Accessories Tab Content
$accessoriesContent = <<<EOD
<p>Accessories Content</p>
EOD;

// Sport Tab Content
$sportContent = <<<EOD
<p>Sport Content</p>
EOD;

// Swim Tab Content
$swimContent = <<<EOD
<p>Swim Content</p>
EOD;

$chartBlock = array(
    'title' => 'Size Chart Size',
    'identifier' => 'size_chart_size',
    'content' => $chartContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('size_chart_size');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($chartBlock)->save();
} else {
    $block->setContent($chartContent)->save();
}

$clothingBlock = array(
    'title' => 'Size Chart Clothing Size',
    'identifier' => 'size_chart_clothing_size',
    'content' => $clothingContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('size_chart_clothing_size');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($clothingBlock)->save();
} else {
    $block->setContent($clothingContent)->save();
}

$bottomsBlock = array(
    'title' => 'Bottoms Size Chart',
    'identifier' => 'bottoms_size_chart',
    'content' => $bottomsContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('bottoms_size_chart');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($bottomsBlock)->save();
} else {
    $block->setContent($bottomsContent)->save();
}

$shoesBlock = array(
    'title' => 'Size Chart Shoes Size',
    'identifier' => 'size_chart_shoes_size',
    'content' => $shoesContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('size_chart_shoes_size');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($shoesBlock)->save();
} else {
    $block->setContent($shoesContent)->save();
}

$intimatesBlock = array(
    'title' => 'Intimates Size Chart',
    'identifier' => 'intimates_size_chart',
    'content' => $intimatesContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('intimates_size_chart');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($intimatesBlock)->save();
} else {
    $block->setContent($intimatesContent)->save();
}

$accessoriesBlock = array(
    'title' => 'Accessories Size Chart',
    'identifier' => 'accessories_size_chart',
    'content' => $accessoriesContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('accessories_size_chart');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($accessoriesBlock)->save();
} else {
    $block->setContent($accessoriesContent)->save();
}

$sportBlock = array(
    'title' => 'Sport Size Chart',
    'identifier' => 'sport_size_chart',
    'content' => $sportContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('sport_size_chart');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($sportBlock)->save();
} else {
    $block->setContent($sportContent)->save();
}

$swimBlock = array(
    'title' => 'Swim Size Chart',
    'identifier' => 'swim_size_chart',
    'content' => $swimContent,
    'is_active' => 1,
    'stores' => array(0)
);

$block = Mage::getModel('cms/block')->load('swim_size_chart');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($swimBlock)->save();
} else {
    $block->setContent($swimContent)->save();
}

$installer->endSetup();