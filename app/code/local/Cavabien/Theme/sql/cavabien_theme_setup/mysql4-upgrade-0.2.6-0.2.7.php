<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$feature_content = <<<EOD
<div class="giftcard-feature">
    <p class="title">Gift Card Features</p>
    <p>Gift Certificate that can be e-mailed to an e-mail address of your choice</p>
    <p>E-mail within minutes</p>
    <p>Choose any denomination up to &pound;1,000</p>
    <p>Available in multiple designs</p>
    <p>Redeemable towards thousands of items online at <a href="www.cava-bien.com">www.cava-bien.com</a></p>
    <p class="bottom">Gift Certificates are subject to our <a class="term-popup-link" href="javascript:void(0)">Terms and Conditions.</a></p>
</div>
EOD;
$feature_staticBlock = array(
    'title' => 'Gift Card Feature',
    'identifier' => 'giftcard_feature',
    'content' => $feature_content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('giftcard_feature');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($feature_staticBlock)->save();
} else {
    $block->setContent($feature_content)->save();
}

$installer->endSetup();