<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Add new column to the 'review' table
$installer->getConnection()->addColumn(
    $this->getTable('review/review'), //table name
    'rating_percent',      //column name
    'FLOAT(11)'  //datatype definition
);

$installer->endSetup();