<?php
/**
 * Created by PhpStorm.
 * User: Sangnv
 * Date: 9/7/14
 * Time: 6:18 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$setup = Mage::getResourceModel('customer/setup', 'core_setup');
$setup->addAttribute('customer', 'saved_tokens_json', array(
    'input' => '',
    'type' => 'text',
    'label' => '',
    'visible' => '0',
    'required' => '0',
    'user_defined' => '0',
    'backend' => 'cavabien_createnewcard/backend_savedtokens',
));
$installer->endSetup();