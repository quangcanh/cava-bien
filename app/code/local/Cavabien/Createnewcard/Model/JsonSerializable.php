<?php
interface Cavabien_Createnewcard_Model_JsonSerializable
{
    public function jsonSerialize();
    public function getJsonData(array $rawData = null);
}