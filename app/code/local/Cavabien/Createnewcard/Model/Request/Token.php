<?php
class Cavabien_Createnewcard_Model_Request_Token extends Cavabien_Createnewcard_Model_JsonSerializableAbstract
{
    /**
     * Call create new customer token API
     *
     * @param Varien_Object $billing
     * @param Varien_Object $infoInstance
     *
     */
    public function createNewToken( Varien_Object $billing,Varien_Object $infoInstance)
    {
        // Empty Varien_Object's data

        $this->unsetData();

        $customerParam = Mage::getModel('cavabien_createnewcard/field_customer');

        $customerParam->setTitle($billing->getPrefix())
            ->setFirstName($billing->getFirstname())
            ->setLastName($billing->getLastname())
            ->setCompanyName($billing->getCompany())
            ->setJobDescription($billing->getJobDescription())
            ->setStreet1($billing->getStreet1())
            ->setStreet2($billing->getStreet2())
            ->setCity($billing->getCity())
            ->setState($billing->getRegion())
            ->setPostalCode($billing->getPostcode())
            ->setCountry(strtolower($billing->getCountryModel()->getIso2Code()))
            ->setEmail($billing->getEmail())
            ->setPhone($billing->getTelephone())
            ->setMobile($billing->getMobile())
            ->setComments('')
            ->setFax($billing->getFax())
            ->setUrl('');
        $cardDetails = Mage::getModel('cavabien_createnewcard/field_cardDetails');
        $customerId = Mage::getSingleton('customer/session')->getId();
        $cardDetails->setName($infoInstance->getCcOwner())
            ->setNumber($infoInstance->getCcNumber())
            ->setExpiryMonth($infoInstance->getCcExpMonth())
            ->setExpiryYear($infoInstance->getCcExpYear())
            ->setCVN($infoInstance->getCcCid())
            ->setStartMonth($infoInstance->getStartMonth())
            ->setStartYear($infoInstance->getStartYear())
            ->setIssueNumber($infoInstance->getIssueNumber());
        $customerParam->setCardDetails($cardDetails);
        $this->setCustomer($customerParam);
            $customerInfo = array(
                'FirstName' =>$billing->getFirstname(),
                'LastName' =>$billing->getLastname(),
                'Street1' =>$billing->getStreet1(),
                'Street2'=>$billing->getStreet2(),
                'City'  =>$billing->getCity(),
                'State' =>$billing->getRegion(),
                'Country'=>strtolower($billing->getCountryModel()->getIso2Code()),
                'PostalCode' =>$billing->getPostcode(),
                'Phone'  =>$billing->getTelephone()
            );

            $tokenInfo = array(
                'Token' => $customerId,
                'Card' => $infoInstance->getCcNumber(),
                'Owner' => $infoInstance->getCcOwner(),
                'ExpMonth' => $infoInstance->getCcExpMonth(),
                'ExpYear' => $infoInstance->getCcExpYear(),
                'Type' => $infoInstance->getCcType(),
                'Address' => Mage::getModel('cavabien_createnewcard/field_customer')->setData($customerInfo),
            );

            Mage::helper('cavabien_createnewcard/customer')->addToken($tokenInfo);
            return $this;
    }

    /**
     * Update current token
     *
     * @param Varien_Object $billing
     * @param Varien_Object $infoInstance
     *
     */
    public function updateToken(Varien_Object $billing, Varien_Object $infoInstance)
    {
        $this->unsetData();

        $customerParam = Mage::getModel('cavabien_createnewcard/field_customer');
        $customerParam->setTitle($billing->getPrefix())
            ->setFirstName($billing->getFirstname())
            ->setLastName($billing->getLastname())
            ->setCompanyName($billing->getCompany())
            ->setJobDescription($billing->getJobDescription())
            ->setStreet1($billing->getStreet1())
            ->setStreet2($billing->getStreet2())
            ->setCity($billing->getCity())
            ->setState($billing->getRegion())
            ->setPostalCode($billing->getPostcode())
            ->setCountry(strtolower($billing->getCountryModel()->getIso2Code()))
            ->setEmail($billing->getEmail())
            ->setPhone($billing->getTelephone())
            ->setMobile($billing->getMobile())
            ->setComments('')
            ->setFax($billing->getFax())
            ->setUrl('');
        $cardDetails = Mage::getModel('cavabien_createnewcard/field_cardDetails');

        $customerId = Mage::getSingleton('customer/session')->getId();

        $cardDetails->setName($infoInstance->getCcOwner())
            ->setNumber('444433XXXXXX1111') // Required dummy card number for update to work
            ->setExpiryMonth($infoInstance->getCcExpMonth())
            ->setExpiryYear($infoInstance->getCcExpYear())
            ->setCVN($infoInstance->getCcCid())
            ->setStartMonth($infoInstance->getStartMonth())
            ->setStartYear($infoInstance->getStartYear())
            ->setIssueNumber($infoInstance->getIssueNumber());

        $customerParam->setCardDetails($cardDetails);
        $this->setCustomer($customerParam);
        $customerInfo = array(
            'FirstName' =>$billing->getFirstname(),
            'LastName' =>$billing->getLastname(),
            'Street1' =>$billing->getStreet1(),
            'Street2'=>$billing->getStreet2(),
            'City'  =>$billing->getCity(),
            'State' =>$billing->getRegion(),
            'Country'=>strtolower($billing->getCountryModel()->getIso2Code()),
            'PostalCode' =>$billing->getPostcode(),
            'Phone'  =>$billing->getTelephone()
        );
        $tokenInfo = array(
            'Token' => $customerId,
            'Owner' => $infoInstance->getCcOwner(),
            'ExpMonth' => $infoInstance->getCcExpMonth(),
            'ExpYear' => $infoInstance->getCcExpYear(),
            'Type' => $infoInstance->getCcType(),
            'Address' => Mage::getModel('cavabien_createnewcard/field_customer')->setData($customerInfo),
        );
            Mage::helper('cavabien_createnewcard/customer')->updateToken($infoInstance->getSavedToken(), $tokenInfo);
            return $this;
    }
    /**
     * Get card type name by card number
     * @param $num Card number
     * @return string Card type name
     */
    public function checkCardType($num)
    {
        if (preg_match('/^(4026|417500|4508|4844|4913|4917)/', $num)) {
            return 'VE';
        }
        if (preg_match('/^4/', $num)) {
            return 'VI';
        }
        if (preg_match('/^(34|37)/', $num)) {
            return 'AE';
        }
        if (preg_match('/^(5[1-5])/', $num)) {
            return 'MC';
        }
        if (preg_match('/^(2131|1800)/', $num)) {
                return 'JCB';
            }
        if (preg_match('/^36/', $num)) {
            return 'DC';
        }
        if (preg_match('/^(5018|5020|5038|5893|6304|6759|6761|6762|6763)/', $num)) {
            return 'ME';
        }

        return 'Unknown';
    }

}