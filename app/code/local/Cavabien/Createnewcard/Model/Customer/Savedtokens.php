<?php

/**
 * Class Cavabien_Createnewcard_Model_Customer_Savedtokens
 */
class Cavabien_Createnewcard_Model_Customer_Savedtokens extends Cavabien_Createnewcard_Model_JsonSerializableAbstract
{
    protected function _construct()
    {
        $this->setLastId(0);
        $this->setTokens(array());
    }

    /**
     * @param $json string|array
     * @return $this
     */
    public function decodeJSON($json)
    {
        if(is_string($json)) {
            $json = json_decode($json, true);
        }
        $this->addData($json);
        $tokens = $this->getTokens();
        if(is_array($tokens)) {
            foreach($tokens as $id => $token) {
//                print_r($token);
//                die;
                $tokenModel = Mage::getModel('cavabien_createnewcard/customer_token')->addData($token);
//                /* @var Cavabien_Createnewcard_Model_Customer_Token $tokenModel */
                if($address = $tokenModel->getAddress()) {
                    $tokenModel->setAddress(Mage::getModel('cavabien_createnewcard/field_customer')->addData($address));
                }
                $tokens[$id] = $tokenModel;
            }

            $this->setTokens($tokens);
        }

        return $this;
    }

    /**
     * @param $id
     * @return Cavabien_Createnewcard_Model_Customer_Token
     */
    public function getTokenById($id)
    {
        if(($tokens = $this->getTokens()) && isset($tokens[$id]) && $tokens[$id] instanceof Cavabien_Createnewcard_Model_Customer_Token) {
            return $tokens[$id];
        } else {
            Mage::throwException(Mage::helper('cavabien_createnewcard')->__('Customer token does not exist.'));
        }
    }

    public function addToken($info)
    {
        $this->setLastId($this->getLastId() + 1);
        $tokens = $this->getTokens();
        $tokens[$this->getLastId()] = Mage::getModel('cavabien_createnewcard/customer_token')->addData($info)->setActive(1);
        $this->setTokens($tokens);
    }
}