<?php
class Cavabien_Createnewcard_MycardsController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * List all active tokens of current logged in customer
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Credit Cards'));

//        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
//            if (Mage::helper('ewayrapid')->isSavedMethodEnabled()) {
//                $block->setRefererUrl($this->_getRefererUrl());
//            } else {
//                $block->setRefererUrl(Mage::getUrl('customer/account/'));
//            }
//        }
        $this->renderLayout();
    }

    /**
     * Display create new token screen. Do nothing, just forward to editAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Display edit form for both create new/edit token
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');

        $tokenId = $this->getRequest()->getParam('token_id');
        if (is_numeric($tokenId)) {
            Mage::register('current_token', Mage::helper('cavabien_createnewcard/customer')->getTokenById($tokenId)->setTokenId($tokenId));
            $this->getLayout()->getBlock('head')->setTitle($this->__('Edit Credit Card'));
        } else {
            $this->getLayout()->getBlock('head')->setTitle($this->__('Add New Credit Card'));
        }
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('creditcard/mycards');
        }

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

    /**
     * Handle edit form saving
     */
    public function saveAction()
    {
        $request = $this->getRequest();

        $apiRequest = Mage::getModel('cavabien_createnewcard/request_token');
        try {
//            if (!$request->isPost() || !$request->getParam('address') || !$request->getParam('payment')) {
//                Mage::throwException($this->__('Invalid request'));
//            }

            $tokenId = $request->getParam('token_id');
            if (is_numeric($tokenId)) {
                list($billingAddress, $infoInstance) = $this->_generateApiParams($request);

                $infoInstance->setSavedToken($tokenId);

                $apiRequest->updateToken($billingAddress, $infoInstance);

                if ($request->getParam('is_default')) {
                    Mage::helper('cavabien_createnewcard/customer')->setDefaultToken($tokenId);
                }
                $this->_getSession()->addSuccess($this->__('Your Credit Card has been saved successfully.'));
                $this->_redirect('*/*/');
            } else if (!$tokenId) {
                list($billingAddress, $infoInstance) = $this->_generateApiParams($request);
                $apiRequest->createNewToken($billingAddress, $infoInstance);
                if ($request->getParam('is_default')) {
                    Mage::helper('cavabien_createnewcard/customer')->setDefaultToken(Mage::helper('cavabien_createnewcard/customer')->getLastTokenId());
                }
                $this->_getSession()->addSuccess($this->__('Your Credit Card has been saved successfully.'));
                $this->_redirect('*/*/');
            } else {
                Mage::throwException($this->__('Invalid token id'));
            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $token = Mage::getModel('cavabien_createnewcard/customer_token');
            $customerInfo = ($apiRequest->getCustomer() ? $apiRequest->getCustomer() : Mage::getModel('cavabien_createnewcard/field_customer'));
            $token->setOwner($infoInstance->getCcOwner())
                ->setExpMonth($infoInstance->getCcExpMonth())
                ->setExpYear($infoInstance->getCcExpYear())
                ->setAddress($customerInfo);
            if (is_numeric($tokenId)) {
                $oldToken = Mage::helper('cavabien_createnewcard/customer')->getTokenById($tokenId);
                $token->setToken($oldToken->getToken())
                    ->setCard($oldToken->getCard())
                    ->setTokenId($tokenId);
            }

            $this->_getSession()->setTokenInfo($token);
            $this->_getSession()->addError($e->getMessage());
            $params = is_numeric($tokenId) ? array('token_id' => $tokenId) : array();
            $this->_redirect('*/*/edit', $params);
        }
    }

    /**
     * Generate params to post to eWAY gateway to create new token.
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @return array
     */
    protected function _generateApiParams($request)
    {
        $billingAddress = Mage::getModel('customer/address');
        $billingAddress->addData($request->getParam('address'));
        $errors = $billingAddress->validate();
        if ($errors !== true && is_array($errors)) {
            Mage::throwException(implode('<br/>', $errors));
        }
        $infoInstance = new Varien_Object($request->getParam('payment'));
        return array($billingAddress, $infoInstance);
    }

    /**
     * Make current token inactive
     */
    public function deleteAction()
    {
        try {
            $tokenId = $this->getRequest()->getParam('token_id');
            if (is_numeric($tokenId)) {
                Mage::helper('cavabien_createnewcard/customer')->deleteToken($tokenId);
                $this->_getSession()->addSuccess($this->__('Your Credit Card has been deleted successfully.'));
                $this->_redirect('*/*/');
            } else {
                Mage::throwException($this->__('Invalid token id'));
            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/');
        }
    }


}