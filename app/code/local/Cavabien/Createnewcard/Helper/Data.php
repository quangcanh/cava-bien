<?php
class Cavabien_Createnewcard_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_ccTypeNames = null;
    private $_isSaveMethodEnabled = null;

    public function isBackendOrder()
    {
        return Mage::app()->getStore()->isAdmin();
    }

    public function serializeInfoInstance(&$info)
    {
        $fieldsToSerialize = array('is_new_token', 'is_update_token', 'saved_token');
        $data = array();
        foreach($fieldsToSerialize as $field) {
            $data[$field] = $info->getData($field);
        }

        $info->setAdditionalData(json_encode($data));
    }

    public function unserializeInfoInstace(&$info)
    {
        $data = json_decode($info->getAdditionalData(), true);
        $info->addData($data);
    }

    public function getCcTypeName($type)
    {
        if(is_null($this->_ccTypeNames)) {
            $this->_ccTypeNames = Mage::getSingleton('payment/config')->getCcTypes();
        }
        return (isset($this->_ccTypeNames[$type]) ? $this->_ccTypeNames[$type] : 'Unknown');
    }
}