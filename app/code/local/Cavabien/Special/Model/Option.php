<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 08/07/2014
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
class Cavabien_Special_Model_Option extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $result = array();
            $arrayData = array(
                1 => array(
                    'label' => 'Large',
                    'value' => 1
                ),

                2 => array(
                    'label' => 'Small',
                    'value' => 2
                )
            );
            foreach($arrayData as $data){
                $result[] = array(
                    'label' => $data['label'],
                    'value' => $data['value']
                );
            }

            $this->_options = $result;

        }
        return $this->_options;
    }
}