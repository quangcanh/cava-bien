<?php

class Cavabien_Special_Model_Styleoption extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $result = array();
            $arrayData = array(
                1 => array(
                    'label' => '-- Please Select --',
                    'value' => ''
                ),

                2 => array(
                    'label' => 'Monday',
                    'value' => 'monday'
                ),

                3 => array(
                    'label' => 'Tuesday',
                    'value' => 'tuesday'
                ),

                4 => array(
                    'label' => 'Wednesday',
                    'value' => 'wednesday'
                ),

                5 => array(
                    'label' => 'Thursday',
                    'value' => 'thursday'
                ),

                6 => array(
                    'label' => 'Friday',
                    'value' => 'friday'
                ),

                7 => array(
                    'label' => 'Saturday',
                    'value' => 'saturday'
                ),

                8 => array(
                    'label' => 'Sunday',
                    'value' => 'sunday'
                )
            );
            foreach($arrayData as $data){
                $result[] = array(
                    'label' => $data['label'],
                    'value' => $data['value']
                );
            }

            $this->_options = $result;

        }
        return $this->_options;
    }
}