<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create category level 2
$cate_featured = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'featured-product'
    ))->getFirstItem();

$subCategories = array(
    0 => array(
        'url_key' => 'fp-women-clothing',
        'name'=> 'Women Clothing'
    ),
    2 => array(
        'url_key' => 'fp-fashion-jewellery',
        'name'=> 'Fashion Jewellery'
    ),
    3 => array(
        'url_key' => 'fp-sports',
        'name'=> 'Sports'
    ),
    4 => array(
        'url_key' => 'fp-accessories',
        'name'=> 'Accessories'
    ),
    5 => array(
        'url_key' => 'fp-hair-beauty',
        'name'=> 'Hair & Beauty'
    )
);

if($cate_featured) {
    foreach($subCategories as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_featured->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

$installer->endSetup();