<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$customLayoutFeatured = <<<EOD
<reference name="category.products">
   <block type="cavabien_special/catalog_product_list" name="product_list" template="catalog/product/featured/list.phtml">
        <block type="cavabien_special/catalog_product_list" name="sneak_product_list" template="catalog/product/featured/sneakpeak/list.phtml" />
        <block type="multipledeals/list_sidedeals" name="deal.of.day" template="page/html/deal.of.day.phtml" />
        <block type="cavabien_special/catalog_product_list" name="sub.menu.featured" template="webandpeople/custommenu/featured_menu.phtml" />
   </block>
</reference>
EOD;

$featuredCatUrlKey = "featured-product";

$featuredCatId = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', $featuredCatUrlKey)
    ->getFirstItem()
    ->getId();
if($featuredCatId) {
    $featuredCat = Mage::getModel('catalog/category')->load($featuredCatId);
    $featuredCat->setCustomLayoutUpdate($customLayoutFeatured)->save();
}

$installer->endSetup();