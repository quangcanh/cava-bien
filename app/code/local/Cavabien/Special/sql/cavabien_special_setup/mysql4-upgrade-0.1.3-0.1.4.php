<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create category level 4

//Create sub categories in Women Dresses
$cate_women_dresses = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-women-dresses'
    ))->getFirstItem();

$subCategoriesWD = array(
    0 => array(
        'url_key' => 'fp-mid-calf',
        'name'=> 'Mid-calf'
    ),
    2 => array(
        'url_key' => 'fp-ankle-length',
        'name'=> 'Ankle-length'
    ),
    3 => array(
        'url_key' => 'fp-mini',
        'name'=> 'Mini'
    ),
    4 => array(
        'url_key' => 'fp-knee-length',
        'name'=> 'Knee-length'
    ),
    5 => array(
        'url_key' => 'fp-floor-length',
        'name'=> 'Floor-length'
    ),
    6 => array(
        'url_key' => 'fp-irregular',
        'name'=> 'Irregular'
    )
);

if($cate_women_dresses) {
    foreach($subCategoriesWD as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_women_dresses->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Jumpsuits & Rompers
$cate_jumpsuits_rompers = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-jumpsuits-rompers'
    ))->getFirstItem();

$subCategoriesJR = array(
    0 => array(
        'url_key' => 'fp-batwing',
        'name'=> 'Batwing'
    ),
    2 => array(
        'url_key' => 'fp-shoulderless',
        'name'=> 'Shoulderless'
    ),
    3 => array(
        'url_key' => 'fp-single-shoulder',
        'name'=> 'Single-shoulder'
    ),
    4 => array(
        'url_key' => 'fp-spaghetti',
        'name'=> 'Spaghetti'
    ),
    5 => array(
        'url_key' => 'fp-lantern',
        'name'=> 'Lantern'
    ),
    6 => array(
        'url_key' => 'fp-sleeveless',
        'name'=> 'Sleeveless'
    )
);

if($cate_jumpsuits_rompers) {
    foreach($subCategoriesJR as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_jumpsuits_rompers->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Skirts Fashion
$cate_skirts_fashion = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-skirts-fashion'
    ))->getFirstItem();

$subCategoriesSF = array(
    0 => array(
        'url_key' => 'fp-a-line',
        'name'=> 'A-Line'
    ),
    2 => array(
        'url_key' => 'fp-beach',
        'name'=> 'Beach'
    ),
    3 => array(
        'url_key' => 'fp-straight',
        'name'=> 'Straight'
    ),
    4 => array(
        'url_key' => 'fp-sheath',
        'name'=> 'Sheath'
    ),
    5 => array(
        'url_key' => 'fp-asymmetrical',
        'name'=> 'Asymmetrical'
    ),
    6 => array(
        'url_key' => 'fp-pencil',
        'name'=> 'Pencil'
    ),
    7 => array(
        'url_key' => 'fp-pleated',
        'name'=> 'Pleated'
    )
);

if($cate_skirts_fashion) {
    foreach($subCategoriesSF as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_skirts_fashion->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Pants, Shorts & Leggings
$cate_pants_shorts_leggings = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-pants-shorts-leggings'
    ))->getFirstItem();

$subCategoriesPSL = array(
    0 => array(
        'url_key' => 'fp-candy-color',
        'name'=> 'Candy Color'
    ),
    2 => array(
        'url_key' => 'fp-starry-sky',
        'name'=> 'Starry Sky'
    ),
    3 => array(
        'url_key' => 'fp-animal',
        'name'=> 'Animal'
    ),
    4 => array(
        'url_key' => 'fp-leopard',
        'name'=> 'Leopard'
    ),
    5 => array(
        'url_key' => 'fp-dots',
        'name'=> 'Dots'
    ),
    6 => array(
        'url_key' => 'fp-floral',
        'name'=> 'Floral'
    )
);

if($cate_pants_shorts_leggings) {
    foreach($subCategoriesPSL as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_pants_shorts_leggings->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Jeans & Denim Fashion
$cate_jeans_denim = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-jeans-denim'
    ))->getFirstItem();

$subCategoriesJD = array(
    0 => array(
        'url_key' => 'fp-ripped',
        'name'=> 'Ripped'
    ),
    2 => array(
        'url_key' => 'fp-sequins',
        'name'=> 'Sequins'
    ),
    3 => array(
        'url_key' => 'fp-rivets',
        'name'=> 'Rivets'
    ),
    4 => array(
        'url_key' => 'fp-frayed',
        'name'=> 'Frayed'
    ),
    5 => array(
        'url_key' => 'fp-plaid',
        'name'=> 'Plaid'
    ),
    6 => array(
        'url_key' => 'fp-leopard-2',
        'name'=> 'Leopard'
    ),
    7 => array(
        'url_key' => 'fp-stripes',
        'name'=> 'Stripes'
    ),
    8 => array(
        'url_key' => 'fp-high-waist',
        'name'=> 'High Waist'
    ),
    9 => array(
        'url_key' => 'fp-low-waist',
        'name'=> 'Low Waist'
    )
);

if($cate_jeans_denim) {
    foreach($subCategoriesJD as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_jeans_denim->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Blouses & Shirts
$cate_blouses_shirts = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-blouses-shirts'
    ))->getFirstItem();

$subCategoriesBS = array(
    0 => array(
        'url_key' => 'fp-ruffle-collar',
        'name'=> 'Ruffle Collar'
    ),
    2 => array(
        'url_key' => 'fp-leather',
        'name'=> 'Leather'
    ),
    3 => array(
        'url_key' => 'fp-necktie',
        'name'=> 'Necktie'
    ),
    4 => array(
        'url_key' => 'fp-candy-colors',
        'name'=> 'Candy Colors'
    ),
    5 => array(
        'url_key' => 'fp-cape-style',
        'name'=> 'Cape Style'
    ),
    6 => array(
        'url_key' => 'fp-gauze',
        'name'=> 'Gauze'
    )
);

if($cate_blouses_shirts) {
    foreach($subCategoriesBS as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_blouses_shirts->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Vintage Jewellery
$cate_vintage_jewellery = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-vintage-jewellery'
    ))->getFirstItem();

$subCategoriesVJ = array(
    0 => array(
        'url_key' => 'fp-pearls',
        'name'=> 'Pearls'
    ),
    2 => array(
        'url_key' => 'fp-resin-plastic',
        'name'=> 'Resin or Plastic'
    ),
    3 => array(
        'url_key' => 'fp-enamel',
        'name'=> 'Enamel'
    )
);

if($cate_vintage_jewellery) {
    foreach($subCategoriesVJ as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_vintage_jewellery->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Bracelets & Bangles
$cate_bracelets_bangles = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-bracelets-bangles'
    ))->getFirstItem();

$subCategoriesBB = array(
    0 => array(
        'url_key' => 'fp-bangles',
        'name'=> 'Bangles'
    ),
    2 => array(
        'url_key' => 'fp-chain-link-bracelets',
        'name'=> 'Chain & Link Bracelets'
    ),
    3 => array(
        'url_key' => 'fp-wrap-bracelets',
        'name'=> 'Wrap Bracelets'
    ),
    4 => array(
        'url_key' => 'fp-strand-bracelets',
        'name'=> 'Strand Bracelets'
    ),
    5 => array(
        'url_key' => 'fp-cuff-bracelets',
        'name'=> 'Cuff Bracelets'
    ),
    6 => array(
        'url_key' => 'fp-hologram-bracelets',
        'name'=> 'Hologram Bracelets'
    )
);

if($cate_bracelets_bangles) {
    foreach($subCategoriesBB as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_bracelets_bangles->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Necklaces & Pendants
$cate_necklaces_pendants = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-necklaces-pendants'
    ))->getFirstItem();

$subCategoriesNP = array(
    0 => array(
        'url_key' => 'fp-pendant-necklaces',
        'name'=> 'Pendant Necklaces'
    ),
    2 => array(
        'url_key' => 'fp-chain-necklaces',
        'name'=> 'Chain Necklaces'
    ),
    3 => array(
        'url_key' => 'fp-choker-necklaces',
        'name'=> 'Choker Necklaces'
    ),
    4 => array(
        'url_key' => 'fp-pendants',
        'name'=> 'Pendants'
    ),
    5 => array(
        'url_key' => 'fp-torques',
        'name'=> 'Torques'
    ),
    6 => array(
        'url_key' => 'fp-power-necklaces',
        'name'=> 'Power Necklaces'
    )
);

if($cate_necklaces_pendants) {
    foreach($subCategoriesNP as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_necklaces_pendants->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Charms & Beads
$cate_charms_beads = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-charms-beads'
    ))->getFirstItem();

$subCategoriesCB = array(
    0 => array(
        'url_key' => 'fp-stud-earrings',
        'name'=> 'Stud Earrings'
    ),
    2 => array(
        'url_key' => 'fp-drop-earrings',
        'name'=> 'Drop Earrings'
    ),
    3 => array(
        'url_key' => 'fp-hoop-earrings',
        'name'=> 'Hoop Earrings'
    ),
    4 => array(
        'url_key' => 'fp-clip-earrings',
        'name'=> 'Clip Earrings'
    )
);

if($cate_charms_beads) {
    foreach($subCategoriesCB as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_charms_beads->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Brooches
$cate_brooches = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-brooches'
    ))->getFirstItem();

$subCategoriesB = array(
    0 => array(
        'url_key' => 'fp-pearls-b',
        'name'=> 'Pearls'
    ),
    2 => array(
        'url_key' => 'fp-resin-plastic-b',
        'name'=> 'Resin or Plastic'
    )
);

if($cate_brooches) {
    foreach($subCategoriesB as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_brooches->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Soccers
$cate_soccers = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-soccers'
    ))->getFirstItem();

$subCategoriesS = array(
    0 => array(
        'url_key' => 'fp-sports-jerseys',
        'name'=> 'Sports Jerseys'
    ),
    2 => array(
        'url_key' => 'fp-soccer-shoes',
        'name'=> 'Soccer Shoes'
    ),
    3 => array(
        'url_key' => 'fp-socks',
        'name'=> 'Socks'
    ),
    4 => array(
        'url_key' => 'fp-shorts',
        'name'=> 'Shorts'
    )
);

if($cate_soccers) {
    foreach($subCategoriesS as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_soccers->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Cycling
$cate_cycling = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-cycling'
    ))->getFirstItem();

$subCategoriesCLY = array(
    0 => array(
        'url_key' => 'fp-vests',
        'name'=> 'Vests'
    ),
    2 => array(
        'url_key' => 'fp-socks-cl',
        'name'=> 'Socks'
    ),
    3 => array(
        'url_key' => 'fp-legwarmers',
        'name'=> 'Legwarmers'
    ),
    4 => array(
        'url_key' => 'fp-racing-gloves',
        'name'=> 'Racing Gloves'
    )
);

if($cate_cycling) {
    foreach($subCategoriesCLY as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_cycling->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Basketball
$cate_basketball = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-basketball'
    ))->getFirstItem();

$subCategoriesBKB = array(
    0 => array(
        'url_key' => 'fp-basketball-shoes',
        'name'=> 'Basketball Shoes'
    ),
    2 => array(
        'url_key' => 'fp-shorts-bk',
        'name'=> 'Shorts'
    ),
    3 => array(
        'url_key' => 'fp-socks-bk',
        'name'=> 'Socks'
    )
);

if($cate_basketball) {
    foreach($subCategoriesBKB as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_basketball->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Makeup
$cate_makeup = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-makeup'
    ))->getFirstItem();

$subCategoriesMKU = array(
    0 => array(
        'url_key' => 'fp-eyes-mku',
        'name'=> 'Eyes'
    ),
    2 => array(
        'url_key' => 'fp-face-mku',
        'name'=> 'Face'
    ),
    3 => array(
        'url_key' => 'fp-lips-mku',
        'name'=> 'Lips'
    )
);

if($cate_makeup) {
    foreach($subCategoriesMKU as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_makeup->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Hair Extensions & Wigs
$cate_hair_extensions_wigs = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-hair-extensions-wigs'
    ))->getFirstItem();

$subCategoriesHEW = array(
    0 => array(
        'url_key' => 'fp-synthetic-hair',
        'name'=> 'Synthetic Hair'
    ),
    2 => array(
        'url_key' => 'fp-human hair',
        'name'=> 'Human Hair'
    )
);

if($cate_hair_extensions_wigs) {
    foreach($subCategoriesHEW as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_hair_extensions_wigs->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

$installer->endSetup();