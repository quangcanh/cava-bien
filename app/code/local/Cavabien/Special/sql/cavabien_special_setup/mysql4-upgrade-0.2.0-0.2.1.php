<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// Change Custom Layout for Featured Product Page
$urlKeyFeatured = 'featured-product';
$customLayoutFeatured = <<<EOD
<reference name="category.products">
   <block type="cavabien_special/catalog_product_list" name="product_list" template="catalog/product/featured/list.phtml">
        <block type="cavabien_special/catalog_product_list" name="sneak_product_list" template="catalog/product/featured/sneakpeak/list.phtml" />
        <block type="multipledeals/list_sidedeals" name="deal.of.day" template="page/html/deal.of.day.phtml" />
        <block type="cavabien_special/catalog_product_list" name="sub.menu.featured" template="webandpeople/custommenu/featured_menu.phtml" />
        <block type="cavabien_theme/Topblock" name="feature.promotion" as="featurePromotion"
                               template="homepage/feature.promotion.phtml">
            <block type="cms/block" name="promotion.block.1">
                <action method="setBlockId">
                    <block_id>promotion_block_1</block_id>
                </action>
            </block>
            <block type="cms/block" name="promotion.block.2">
                <action method="setBlockId">
                    <block_id>promotion_block_2</block_id>
                </action>
            </block>
            <block type="cms/block" name="promotion.block.3">
                <action method="setBlockId">
                    <block_id>promotion_block_3</block_id>
                </action>
            </block>
        </block>
   </block>
</reference>
EOD;


$featuredCategory= Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('url_key', $urlKeyFeatured)->getFirstItem();
if ($featuredCategory->getId()) {
    $featuredCategory->setCustomLayoutUpdate($customLayoutFeatured);
    $featuredCategory->save();
}

// Change Custom Layout for Shop The Look Page
$urlKeyShopLook = 'shop-the-look';
$customLayoutShopLook = <<<EOD
<reference name="category.products">
   <block type="catalog/product_list" name="product_list" template="catalog/product/shopthelook/list.phtml">
        <block type="cavabien_theme/Topblock" name="feature.promotion" as="featurePromotion"
                               template="homepage/feature.promotion.phtml">
            <block type="cms/block" name="promotion.block.1">
                <action method="setBlockId">
                    <block_id>promotion_block_1</block_id>
                </action>
            </block>
            <block type="cms/block" name="promotion.block.2">
                <action method="setBlockId">
                    <block_id>promotion_block_2</block_id>
                </action>
            </block>
            <block type="cms/block" name="promotion.block.3">
                <action method="setBlockId">
                    <block_id>promotion_block_3</block_id>
                </action>
            </block>
        </block>
   </block>
</reference>
EOD;


$shopLookCategory= Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', $urlKeyShopLook)->getFirstItem();
if ($shopLookCategory->getId()) {
    $shopLookCategory->setCustomLayoutUpdate($customLayoutShopLook);
    $shopLookCategory->save();
}

$installer->endSetup();