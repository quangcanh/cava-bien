<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$attributeSubtitle = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', "subtitle");
if(!$attributeSubtitle->getId()){
    $objCatalogEavSetup->addAttribute('catalog_product', 'subtitle', array(
        'group' => 'General',
        'type' => 'text',
        'backend' => '',
        'frontend' => '',
        'label' => 'Subtitle',
        'input' => 'text',
        'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => true, //use search
        'comparable' => false,
        'visible_on_front' => true,
        'used_in_product_listing'=> '1',
        'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
        'unique' => false,
    ));
}

$attributeFrame = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', "product_frame");
if(!$attributeFrame->getId()){
    $objCatalogEavSetup->addAttribute('catalog_product', 'product_frame', array(
        'group' => 'General',
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
        'label' => 'Product Frame',
        'input' => 'select',
        'source' => 'cavabien_special/option',
        'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => true, //use search
        'comparable' => false,
        'visible_on_front' => true,
        'used_in_product_listing'=> '1',
        'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
        'unique' => false,

    ));
}
$installer->endSetup();