<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('newsletter/subscriber'), 'subscriber_phone');
$installer->getConnection()->addColumn($installer->getTable('newsletter/subscriber'), 'subscriber_phone', "varchar(20)");

$installer->endSetup();