<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$config = new Mage_Core_Model_Config();
$config->saveConfig('gomage_navigation/filter_settings/applied_values', 1, 'default', 0);

$installer->updateAttribute('catalog_product', 'color', 'is_filterable_in_search', '1');
$installer->updateAttribute('catalog_product', 'price', 'is_filterable_in_search', '1');

$installer->endSetup();