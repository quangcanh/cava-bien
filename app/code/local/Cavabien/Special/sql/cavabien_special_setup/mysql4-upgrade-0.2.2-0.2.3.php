<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//add new attribute
$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$objCatalogEavSetup->addAttribute('catalog_product', 'stylist_pick_day', array(
    'group' => 'General',
    'input'         => 'select',
    'type'          => 'varchar',
    'label'         => 'Day of product Stylist Pick',
    'sort_order' => 1000,
    'required'   => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'apply_to' => 'simple,configurable', //array('grouped')
    'source' => 'cavabien_special/styleoption',
));
$objCatalogEavSetup->addAttribute('catalog_product', 'stylist_pick_order', array(
    'input'         => 'text',
    'type'          => 'int',
    'label'         => 'Order of product Stylist Pick',
    'sort_order' => 1000,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
$objCatalogEavSetup->addAttributeToSet('catalog_product', 'Default', 'General', 'stylist_pick_order');
$objCatalogEavSetup->addAttributeToSet('catalog_product', 'Default', 'General', 'stylist_pick_day');
$installer->endSetup();