<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

// Set url_key attribute global
$entity_type = 3;
$attributeCodes = array("url_key", "custom_layout_update", "page_layout");
$data = array ('is_global' => 1);

foreach ($attributeCodes as $attrCode) {
    $attr = Mage::getModel('eav/entity_attribute')->loadByCode($entity_type, $attrCode);
    $catalog_attr = Mage::getModel('catalog/resource_eav_attribute')->load((int) $attr->getId());
    $catalog_attr->addData($data)->save();
}

$customLayoutFeatured = <<<EOD
<reference name="category.products">
   <block type="cavabien_special/catalog_product_list" name="product_list" template="catalog/product/featured/list.phtml">
        <block type="cavabien_special/catalog_product_list" name="sneak_product_list" template="catalog/product/featured/sneakpeak/list.phtml" />
        <block type="multipledeals/list_sidedeals" name="deal.of.day" template="page/html/deal.of.day.phtml" />
        <block type="cavabien_special/catalog_product_list" name="sub.menu.featured" template="webandpeople/custommenu/featured_menu.phtml" />
   </block>
</reference>
EOD;

//Create category level 1
$categories = array(
    0 => array(
        'url_key' => 'featured-product',
        'name'=> 'Featured Product'
    ),
    1 => array(
        'url_key' => 'sneak-peek-week',
        'name'=> 'Sneak Peek Week'
    )
);

foreach($categories as $cate => $data) {
    $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
    if (!$catExist->getId()) {
        $cat = Mage::getModel('catalog/category');
        $cat->setPath('1/2')
            ->setName($data['name'])
            ->setUrlKey($data['url_key'])
            ->setIsActive(1)
            ->setIsAnchor(1);
        if($data['url_key'] == "featured-product") {
            $cat->setPageLayout('one_column')
                ->setCustomLayoutUpdate($customLayoutFeatured);
        }
        $cat->save();
        unset($cat);
    }
}

$installer->endSetup();