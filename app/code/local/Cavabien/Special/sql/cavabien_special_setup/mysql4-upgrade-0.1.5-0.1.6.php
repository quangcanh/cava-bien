<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

Mage::register('isSecureArea', 1);

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// Delete category
$arrUrlKey = array('featured', 'features-products');

foreach ($arrUrlKey as $urlKey) {
    $categoryId = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('url_key', $urlKey)
        ->getFirstItem()
        ->getId();
    if($categoryId) {
        Mage::getModel("catalog/category")->load($categoryId)->delete();
    }
    unset($categoryId);
}

//Set feature category in top slider on homepage
$featuredCatUrlKey = "featured-product";

$featuredCatId = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', $featuredCatUrlKey)
    ->getFirstItem()
    ->getId();

if($featuredCatId) {
    $config = new Mage_Core_Model_Config();
    $config->saveConfig('homepage/header/featured_product_menu_id', (int) $featuredCatId, 'default', 0);
}

$installer->endSetup();