<?php
/**
 * Created by JetBrains PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create category level 3

//Create sub categories in Women Clothing
$cate_women_clothing = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-women-clothing'
    ))->getFirstItem();

$subCategoriesWC = array(
    0 => array(
        'url_key' => 'fp-women-dresses',
        'name'=> 'Women Dresses'
    ),
    2 => array(
        'url_key' => 'fp-jumpsuits-rompers',
        'name'=> 'Jumpsuits & Rompers'
    ),
    3 => array(
        'url_key' => 'fp-skirts-fashion',
        'name'=> 'Skirts Fashion'
    ),
    4 => array(
        'url_key' => 'fp-pants-shorts-leggings',
        'name'=> 'Pants, Shorts & Leggings'
    ),
    5 => array(
        'url_key' => 'fp-jeans-denim',
        'name'=> 'Jeans & Denim Fashion'
    ),
    6 => array(
        'url_key' => 'fp-blouses-shirts',
        'name'=> 'Blouses & Shirts'
    )
);

if($cate_women_clothing) {
    foreach($subCategoriesWC as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_women_clothing->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Fashion Jewellery
$cate_fashion_jewellery = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-fashion-jewellery'
    ))->getFirstItem();

$subCategoriesFJ = array(
    0 => array(
        'url_key' => 'fp-vintage-jewellery',
        'name'=> 'Vintage Jewellery'
    ),
    2 => array(
        'url_key' => 'fp-bracelets-bangles',
        'name'=> 'Bracelets & Bangles'
    ),
    3 => array(
        'url_key' => 'fp-necklaces-pendants',
        'name'=> 'Necklaces & Pendants'
    ),
    4 => array(
        'url_key' => 'fp-crystal-jewellery',
        'name'=> 'Crystal Jewellery'
    ),
    5 => array(
        'url_key' => 'fp-charms-beads',
        'name'=> 'Charms & Beads'
    ),
    6 => array(
        'url_key' => 'fp-brooches',
        'name'=> 'Brooches'
    )
);

if($cate_fashion_jewellery) {
    foreach($subCategoriesFJ as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_fashion_jewellery->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Sports
$cate_sports = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-sports'
    ))->getFirstItem();

$subCategoriesS = array(
    0 => array(
        'url_key' => 'fp-soccers',
        'name'=> 'Soccers'
    ),
    2 => array(
        'url_key' => 'fp-cycling',
        'name'=> 'Cycling'
    ),
    3 => array(
        'url_key' => 'fp-basketball',
        'name'=> 'Basketball'
    )
);

if($cate_sports) {
    foreach($subCategoriesS as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_sports->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Accessories
$cate_accessories = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-accessories'
    ))->getFirstItem();

$subCategoriesA = array(
    0 => array(
        'url_key' => 'fp-hats-caps',
        'name'=> 'Hats & Caps'
    ),
    2 => array(
        'url_key' => 'fp-scarves',
        'name'=> 'Scarves'
    ),
    3 => array(
        'url_key' => 'fp-belts-cummerbunds',
        'name'=> 'Belts & Cummerbunds'
    ),
    4 => array(
        'url_key' => 'fp-hair-accessories',
        'name'=> 'Hair Accessories'
    )
);

if($cate_accessories) {
    foreach($subCategoriesA as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_accessories->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

//Create sub categories in Hair & Beauty
$cate_hair_beauty = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('url_key', array(
        'equal' => 'fp-hair-beauty'
    ))->getFirstItem();

$subCategoriesHB = array(
    0 => array(
        'url_key' => 'fp-makeup',
        'name'=> 'Makeup'
    ),
    2 => array(
        'url_key' => 'fp-hair-extensions-wigs',
        'name'=> 'Hair Extensions & Wigs'
    )
);

if($cate_hair_beauty) {
    foreach($subCategoriesHB as $cate => $data) {
        $catExist = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array(
                'equal' => $data['url_key']
            ))->getFirstItem();
        if (!$catExist->getId()) {
            $cat = Mage::getModel('catalog/category');
            $cat->setPath($cate_hair_beauty->getPath())
                ->setName($data['name'])
                ->setUrlKey($data['url_key'])
                ->setIsActive(1)
                ->setIsAnchor(1)
                ->save();
            unset($cat);
        }
    }
}

$installer->endSetup();