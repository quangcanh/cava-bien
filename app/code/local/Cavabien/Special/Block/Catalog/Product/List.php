<?php

class Cavabien_Special_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List {

    protected $_productCollection;

    public $attributes;

    /**
     * Retrieve category url key
     * @param $categoryUrlKey
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getProductByOtherCategory($categoryUrlKey, $type = "")
    {
        $category1 = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key',$categoryUrlKey)
            ->getFirstItem();
        $category = Mage::getModel('catalog/category')->load($category1->getId());
        $_products= Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            //->addAttributeToFilter('stylist_pick_day',array('neq' => ''))
            ->addAttributeToFilter('status', 1)
            ->addCategoryFilter($category);
        if($type=='special') {
            $_products->addAttributeToFilter(trim($category->getDiscountOffer()), array('notnull'=>'1'));
        }
        $_products->addAttributeToFilter('visibility', 4)
            ->setOrder('stylist_pick_order', 'ASC');

        return $_products;
    }

    /**
     * Method:          Get price of grouped product
     * Function desc:   Get price of the grouped product by calculator
     * @param $groupedProduct
     * @param bool $incTax
     * @return int price value
     */
    public function getGroupedProductPrice($groupedProduct, $incTax = true)
    {
        $_taxHelper = $this->helper('tax');
        $aProductIds = $groupedProduct->getTypeInstance()->getChildrenIds($groupedProduct->getId());
        $prices = array();
        foreach ($aProductIds as $ids) {
            foreach ($ids as $id) {
                $aProduct = Mage::getModel('catalog/product')->load($id);
                if ($incTax) {
                    $prices[] = $_taxHelper->getPrice($aProduct, $aProduct->getPriceModel()->getFinalPrice(null, $aProduct, true), true);
                } else {
                    $prices[] = $aProduct->getPriceModel()->getFinalPrice(null, $aProduct, true);
                }
            }
        }
        $productPrice = 0;
        foreach ($prices as $price) {
            $productPrice += $price;
        }
        //asort($prices);array_shift($prices);
        return $productPrice;
    }


    /**
     * Function desc:   Get price of the grouped product by calculator
     * @return object special offer categories
     */
    public function getSpecialOfferCategory($catID = 0)
    {
        $categoryCollection = array();  // final collection of categories
        $categories_lv2 = array();      // Parent Category Collection
        $categories_lv3_Ids = array();  // Child category ids
        $categoryIds = array();         // All category ids

        if($catID > 0) {
            $_category = Mage::getModel('catalog/category')->load($catID);
            $_categories = array($_category);
        } else {
            if($catID == -1) {
                $_categories = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('parent_id', 2)
                    ->setPageSize(1)
                ;
            } else {
                $_categories = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('*')
                    ->setOrder('sort_order','DESC')
                    ->addAttributeToFilter('parent_id', 2);
            }
        }

        $attributes = array();
        // Get all special offer products 

        foreach ($_categories as $_category) {
            if ($_category->getDiscountOffer()) {
                $attributes[] = trim($_category->getDiscountOffer());
            }
        }

        if($catID == 0) {
            $this->attributes = $attributes;
        }
        $_products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*');

        $whereCond = "";
        if (count($attributes)) {
            foreach($attributes as $attrib){
                $_products->addAttributeToFilter($attrib, array('notnull' => '1'), 'left');
                $whereCond .= '(at_'.$attrib. '.value IS NOT NULL) or ';
            }
        }
        $whereCond .= '(0 = 1)';
        $_products->getSelect()->orwhere($whereCond);

        //echo (string)$_products->getSelect();

        // Push all category id into array
        foreach ($_products as $_product) {
            if($_product->getStatus() == 1){
                $catIds = $_product->getCategoryIds();

                foreach ($catIds as $catId) {
                    array_push($categoryIds, $catId);
                }
            }

        }

        // Filter duplicate ids
        $categoryIds = array_unique($categoryIds);

        // Init parent and child categories collection
        foreach ($categoryIds as $categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);

            $accept = (int) $category->getLevel() <= 3 && $category->getIncludeInMenu() == 1 && $category->getIsActive() == 1;

            if($accept) {
                if($category->getLevel() == 2) array_push($categories_lv2, $category);
                if($category->getLevel() == 3) array_push($categories_lv3_Ids, $category->getId());
            }
        }

        // Sort all categories and push into final category collection
        foreach ($categories_lv2 as $cat_lv2) {
            $categoryArray = array();                           // array of parent and child category id
            $childRealCate = $cat_lv2->getChildrenCategories(); // all child category of each parent
            $childRealCateIds = array();                        // all child category ids of each parent

            foreach ($childRealCate as $cat) {
                array_push($childRealCateIds, $cat->getId());
            }

            // Filter and push child category into array
            $childActiveCateIds = array_intersect($childRealCateIds, $categories_lv3_Ids);

            $childActiveCate = array();
            foreach ($childActiveCateIds as $id) {
                $cate_lv3 = Mage::getModel('catalog/category')->load($id);
                array_push($childActiveCate, $cate_lv3);
            }


            // Push data into array
            array_push($categoryArray, $cat_lv2);
            array_push($categoryArray, $childActiveCate);

            // Push data into category collection
            array_push($categoryCollection, $categoryArray);

        }
        if($catID == 0)
            return array_reverse($categoryCollection);
        return $categoryCollection;

    }

    /**
     * Function desc:   Get Collection of main categories
     * @return array main categories
     */
    public function getMainCategories()
    {
        $blockClassName = Mage::getConfig()->getBlockClassName('custommenu/navigation');
        $block = new $blockClassName();
        $categories = $block->getStoreCategories();
        if (is_object($categories))
            return $categories;
        else return null;
    }

    public function getMainCategoriesIds()
    {
        $categoryIds = array();
        $categories = self::getMainCategories();
        foreach ($categories as $category) {
            array_push($categoryIds, $category->getId());
            foreach ($category->getChildren() as $subCategory) {
                array_push($categoryIds, $subCategory->getId());
            }
        }

        if(count($categoryIds) > 0) {
            return $categoryIds;
        } else return null;

    }

    public function getCurrentCategory()
    {
        $category_id = Mage::registry('cat_id');
        if($category_id) {
            $category = Mage::getModel('catalog/category')->load($category_id);
            return $category;
        }
        else return null;
    }

    public function getCurrentSubCategory()
    {
        $sub_category_id = Mage::registry('sub_cat_id');
        if($sub_category_id) {
            $sub_category = Mage::getModel('catalog/category')->load($sub_category_id);
            return $sub_category;
        }
        else return null;
    }

    public function getBestsellerByCategory($categoryId, $quantity)
    {
        // get today and last 7 days time
        $today = time();
        $last = $today - (60*60*24*100);

        $from = date("Y-m-d", $last);
        $to = date("Y-m-d", $today);

        if ($categoryId == 0) {
            return null;
        }

        $storeId = (int)Mage::app()->getStore()->getId();
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $resourceCollection = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
            ->addCategoryFilter($category)
            ->setOrder('ordered_qty', 'desc')
            ->setPageSize($quantity);

        if (count($resourceCollection) > 0):
            return $resourceCollection;
        endif;

        return null;
    }

    /**
     * Retrieve quantity ordered
     *
     * @return integer
     */
    public function getOrderedQuantity($product_id)
    {
        $quantity = 0;
        // get today and last 7 days time
        $today = time();
        $last = $today - (60*60*24*100);

        $from = date("Y-m-d", $last);
        $to = date("Y-m-d", $today);



        $product = Mage::getModel('catalog/product')->load($product_id);
        if($product->getTypeId() == "configurable") {

            // Get children products (all associated children products data)
            $childProducts = Mage::getModel('catalog/product_type_configurable')
                ->getUsedProducts(null,$product);
            foreach ($childProducts as $childProduct) {
                $report = Mage::getResourceModel('reports/product_sold_collection')
                    ->addAttributeToSelect('*')
                    ->addOrderedQty($from, $to)
                    ->addFieldToFilter('entity_id',$childProduct->getId())
                    ->getFirstItem();
                $quantity += intval($report->getOrderedQty());
            }
        }

        if($product->getTypeId() == "simple") {
            $report = Mage::getResourceModel('reports/product_sold_collection')
                ->addOrderedQty($from, $to)
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id',$product_id)
                ->getFirstItem();
            $quantity = intval($report->getOrderedQty());
        }

        return $quantity;
    }

    public function getBestsellingProduct()
    {
        // store ID
        $storeId    = Mage::app()->getStore()->getId();

        // get today and last 7 days time
        $today = time();
        $last = $today - (60*60*24*7);

        $from = date("Y-m-d", $last);
        $to = date("Y-m-d", $today);

        $data = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
            ->addOrderedQty($from, $to)
            ->setOrder('ordered_qty', 'desc')
            ->getFirstItem();

        $product = Mage::getModel('catalog/product')->load($data->getId());

        return $product;
    }

    public function getMostLovedProduct()
    {
        $product = null;

        $from = date('Y-m-01 H:i:s'); //First Day of Month
        $to = date('Y-m-t H:i:s'); //Last Day of Month

        $loveIt = Mage::getModel('cavabien_loveit/loveit')->getCollection();
        $loveIt->addFieldToFilter('loved_at_time', array('gteq' => $from));
        $loveIt->addFieldToFilter('loved_at_time', array('lteq' => $to));
        $loveIt->getSelect()->group('product_id')->columns('COUNT(product_id) as c')->order('c DESC')->limit(1);

        $productId = $loveIt->getFirstItem()->getData('product_id');
        if($productId) {
            $product = Mage::getModel('catalog/product')->load($productId);
        }

        return $product;
    }

    public function getProductSubtitle($productId)
    {
        $subtitle = "";
        if($productId) {
            $subtitle = Mage::getModel('catalog/product')->load($productId)->getSubtitle();
        }
        return $subtitle;

    }

    public function getLink($categoryId)
    {
        $_link = "#";
        if($categoryId) {
            $_link = Mage::getModel('catalog/category')->load($categoryId)->getShopMoreLink();
        }
        return $_link;
    }

    public function getFrame($productId)
    {
        $frame = 0;
        if($productId) {
            $frame = Mage::getModel('catalog/product')->load($productId)->getProductFrame();
        }
        return $frame;
    }

    public function getConfigPath($categoryId)
    {
        $_config_path = "";
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $cateUrlKey = $category->getUrlKey();

        if($cateUrlKey) {
            switch($cateUrlKey) {
                case 'fp-women-clothing':
                    $_config_path = "featuredpage/first_theme/";
                    break;
                case 'fp-fashion-jewellery':
                    $_config_path = "featuredpage/second_theme/";
                    break;
                case 'fp-sports':
                    $_config_path = "featuredpage/third_theme/";
                    break;
                case 'fp-accessories':
                    $_config_path = "featuredpage/fourth_theme/";
                    break;
                case 'fp-hair-beauty':
                    $_config_path = "featuredpage/fifth_theme/";
                    break;
                default:
                    break;
            }
        }

        return $_config_path;
    }

    public function getColourArea($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."colour_area";
        $_colour = Mage::getStoreConfig($_config_path);

        return $_colour;
    }

    public function getTextFont($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."text_font";
        $_text_font = Mage::getStoreConfig($_config_path);

        return $_text_font;
    }

    public function getTextBackground($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."text_background";
        $_text_background = Mage::getStoreConfig($_config_path);

        return $_text_background;
    }

    public function getTextColour($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."text_colour";
        $_text_colour = Mage::getStoreConfig($_config_path);

        return $_text_colour;
    }

    public function getImageWidth($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."image_width";
        $image_width = Mage::getStoreConfig($_config_path);

        return $image_width;
    }

    public function getImageHeight($categoryId)
    {
        $_config_path = $this->getConfigPath(($categoryId))."image_height";
        $image_height = Mage::getStoreConfig($_config_path);

        return $image_height;
    }

    public function getStylistProduct() {

        $product_id = Mage::registry('stylist_product_id');
        $product = null;
        if($product_id) {
            $product = Mage::getModel('catalog/product')->load($product_id);
        }
        return $product;
    }

}