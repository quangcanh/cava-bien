<?php

class Cavabien_Special_Block_Custommenu_Navigation extends Mage_Core_Block_Template
{
    public function getMainCategories()
    {
        $blockClassName = Mage::getConfig()->getBlockClassName('custommenu/navigation');
        $block = new $blockClassName();
        $categories = $block->getStoreCategories();
        if (is_object($categories))
            return $categories;
        else return null;
    }
}