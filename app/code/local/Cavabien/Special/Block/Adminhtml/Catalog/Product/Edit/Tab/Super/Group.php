<?php
/**
 * Created by Nguyễn Đình Tiệp ( noname9x99 ).
 * Project : sea-cavabien
 * Website : http://sositviet.com
 * File : Group.php
 */
class Cavabien_Special_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Group extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Group {
    /**
     * @overide function
     */
    public function _prepareColumns(){
        parent::_prepareColumns();
        $this->addColumn('sort_stylist', array(
            'header'    => Mage::helper('catalog')->__('Sort Stylist Pick'),
            'name'      => 'sort_stylist',
            'type'      => 'number',
            'validate_class' => 'validate-number',
            'index'     => 'sort_stylist',
            'width'     => '1',
            'editable'  => true,
            'edit_only' => !$this->_getProduct()->getId()
        ));
        return parent::_prepareColumns();
    }
}