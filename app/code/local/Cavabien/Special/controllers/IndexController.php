<?php

class Cavabien_Special_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        /* get category id from best seller */
        $category_id = $this->getRequest()->getParam('cat_id');
        if($category_id != null || $category_id != "") {
            Mage::register('cat_id', $category_id);
        }

        /* get sub category id from best seller */
        $sub_category_id = $this->getRequest()->getParam('sub_cat_id');
        if($sub_category_id != null || $sub_category_id != "") {
            Mage::register('sub_cat_id', $sub_category_id);
        }

        $this->loadLayout();
        $this->renderLayout();
    }

}