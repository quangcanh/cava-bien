<?php

class Cavabien_Special_ReviewController extends Mage_Core_Controller_Front_Action
{
    public function popupAction()
    {
        $this->loadLayout('best_seller_review_popup');
        $this->renderLayout();
    }
}