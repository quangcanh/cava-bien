<?php

class Cavabien_Special_Helper_GiftCard_Catalog_Product_Configuration extends Enterprise_GiftCard_Helper_Catalog_Product_Configuration {

    /**
     * Get gift card option list
     *
     * @return array
     */
    public function getGiftcardOptions(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
    {
        $result = array();
        $value = $this->prepareCustomOption($item, 'giftcard_sender_name');
        if ($value) {
            $email = $this->prepareCustomOption($item, 'giftcard_sender_email');
            if ($email) {
                $value = "{$value} &lt;{$email}&gt;";
            }
            $result[] = array(
                'label' => $this->__('Gift Card Sender'),
                'value' => $value
            );
        }

        $value = $this->prepareCustomOption($item, 'giftcard_recipient_name');
        if ($value) {
            $email = $this->prepareCustomOption($item, 'giftcard_recipient_email');
            if ($email) {
                $value = "{$value} &lt;{$email}&gt;";
            }
            $result[] = array(
                'label' => $this->__('Gift Card Recipient'),
                'value' => $value
            );
        }

        $value = $this->prepareCustomOption($item, 'giftcard_delivery_date');
        if($value) {
            $result[] = array(
                'label' => $this->__('Gift Card Delivery Date'),
                'value' => $value
            );
        }

        $value = $this->prepareCustomOption($item, 'giftcard_message');
        if ($value) {
            $result[] = array(
                'label' => $this->__('Gift Card Message'),
                'value' => $value
            );
        }

        return $result;
    }

}