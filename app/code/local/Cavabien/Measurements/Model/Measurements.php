<?php

class Cavabien_Measurements_Model_Measurements extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('cavabien_measurements/measurements');
    }
}