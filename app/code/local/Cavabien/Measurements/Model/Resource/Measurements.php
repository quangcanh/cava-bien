<?php

class Cavabien_Measurements_Model_Resource_Measurements extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_measurements/measurements', 'measurement_id');
    }
}