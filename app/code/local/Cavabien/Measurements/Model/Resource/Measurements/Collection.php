<?php

/**
 * Class Cavabien_Measurements_Model_Resource_Measurements_Collection
 */
class Cavabien_Measurements_Model_Resource_Measurements_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_measurements/measurements');
    }
}