<?php

class Cavabien_Measurements_Block_Adminhtml_Measurements_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('measurement_id');
        $this->setDefaultSort('measurement_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('cavabien_measurements/measurements')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('measurement_id', array(
            'header' => Mage::helper('cavabien_measurements')->__('ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'measurement_id',
        ));

        $this->addColumn('product_id', array(
            'header' => Mage::helper('cavabien_measurements')->__('Product ID'),
            'align' => 'left',
            'index' => 'product_id',
            'width' => '50px',
        ));


        $this->addColumn('customer_name', array(
            'header' => Mage::helper('cavabien_measurements')->__('Customer Name'),
            'width' => '150px',
            'index' => 'customer_name',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('cavabien_measurements')->__('Customer Email'),
            'width' => '150px',
            'index' => 'customer_email',
        ));


        $this->addColumn('customer_phone', array(
            'header' => Mage::helper('cavabien_measurements')->__('Customer Phone'),
            'width' => '150px',
            'index' => 'customer_phone',
        ));

        $this->addColumn('measurement_subject', array(
            'header' => Mage::helper('cavabien_measurements')->__('Subject'),
            'width' => '150px',
            'index' => 'measurement_subject',
        ));

        $this->addColumn('measurement_inquiry', array(
            'header' => Mage::helper('cavabien_measurements')->__('Inquiry'),
            'width' => '150px',
            'index' => 'measurement_inquiry',
        ));

        $this->addColumn('measurement_file', array(
            'header' => Mage::helper('cavabien_measurements')->__('File attach'),
            'width' => '150px',
            'index' => 'measurement_file',
        ));

        return parent::_prepareColumns();
    }


    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}