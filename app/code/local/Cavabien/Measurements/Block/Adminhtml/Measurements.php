<?php

class Cavabien_Measurements_Block_Adminhtml_Measurements extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_controller = 'adminhtml_measurements';
        $this->_blockGroup = 'cavabien_measurements';
        $this->_headerText = Mage::helper('cavabien_measurements')->__('Measurements Manager');
        $this->_addButtonLabel = Mage::helper('cavabien_measurements')->__('Add News');
        parent::__construct();
        $this->_removeButton('add');
    }
}