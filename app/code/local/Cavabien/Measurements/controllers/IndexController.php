<?php

class Cavabien_Measurements_IndexController extends Mage_Core_Controller_Front_Action
{
    public function addMeasurementAction(){
        $post = $this->getRequest()->getPost();
        $url = $this->getRequest()->getPost('product_url');
        if ( $post ) {

            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {

                /**************************************************************/
                $fileName = '';
                $error = '';
                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    try {
                        $fileName       = $_FILES['file']['name'];
                        $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
                        $fileNamewoe    = rtrim($fileName, $fileExt);
                        $fileName       = $fileNamewoe . time() . '.' . $fileExt;

                        $uploader       = new Varien_File_Uploader('file');
                        //$uploader->setAllowedExtensions(array('doc', 'docx','pdf','png','jpg','txt','xls','xlsx'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'measurements';
                        if(!is_dir($path)){
                            mkdir($path, 0777, true);
                        }
                        $uploader->save($path . DS, $fileName );
                    } catch (Exception $e) {
                        $error = true;
                    }
                }

                $name = $this->getRequest()->getPost('name');
                $email = $this->getRequest()->getPost('email');
                $subject = $this->getRequest()->getPost('subj');
                $phone = $this->getRequest()->getPost('phone');
                $product_id = $this->getRequest()->getPost('product_id');
                $inquiry = $this->getRequest()->getPost('inquiry');

                if(isset($email)&&($email!='')&& isset($inquiry)&&($inquiry!=''))
                {

                    $measurement = Mage::getModel('cavabien_measurements/measurements');
                    $measurement->setData('product_id', $product_id);
                    $measurement->setData('customer_name', $name);
                    $measurement->setData('customer_email', $email);
                    $measurement->setData('customer_phone', $phone);
                    $measurement->setData('measurement_subject', $subject);
                    $measurement->setData('measurement_inquiry', $inquiry);
                    $measurement->setData('measurement_file', $fileName);
                    $measurement->save();
                    $translate->setTranslateInline(true);

                }else{
                    $error = true;
                }

                if($error!=true){
                    $this->_redirectUrl($url);
                    Mage::getSingleton('core/session')->addSuccess('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                    return;
                }else{
                    $this->_redirectUrl($url);
                    Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
                }


            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                $this->_redirectUrl($url);
                Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
                return;
            }

        } else {
            $this->_redirectUrl($url);
        }
    }
}