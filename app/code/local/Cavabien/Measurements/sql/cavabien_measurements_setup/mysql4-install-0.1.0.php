<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_measurements/measurements')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_measurements/measurements')} (
      `measurement_id` int(10) unsigned NOT NULL auto_increment,
      `product_id` int(10) unsigned NOT NULL,
      `customer_name` varchar(255),
      `customer_email` varchar(255),
      `customer_phone` varchar(255),
      `measurement_subject` varchar(255),
      `measurement_inquiry` varchar(1000),
      `measurement_file` varchar(255),
      PRIMARY KEY  (`measurement_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->endSetup();