<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 03/07/2014
 * Time: 09:36
 */
class Cavabien_Config_Helper_Data extends Mage_Core_Helper_Abstract
{
    const NEW_ARRIVAL_CAT   = 'category/general/newarrival';
    const BACK_IN_STOCK_CAT = 'category/general/backinstock';
    const COMING_SOON_CAT   = 'category/general/comingsoon';
    const TOPRATE_CAT       = 'category/general/toprate';
    const ALMOST_GONE_CAT   = 'category/general/almostgone';

    public function get_the_fb_like( $url = '', $type = null ){
        $pageURL = Mage::getBaseDir();

        $url = ($url == '' ) ? $pageURL : $url; // setting a value in $url variable

        $params = 'select comment_count, share_count, like_count from link_stat where url = "'.$url.'"';
        $component = urlencode( $params );
        $url = 'http://graph.facebook.com/fql?q='.$component;
        $fbLIkeAndSahre = json_decode($this->file_get_contents_curl($url));
        $getFbStatus = $fbLIkeAndSahre->data['0'];
        if ($type && $type === 'share_count') {
            return $getFbStatus->share_count;
        }
        return $getFbStatus->like_count;
    }

    public function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    public function getPathProduct($url){
        $pathUrl = null;
        $baseUrl = Mage::getBaseUrl();
        if(strpos($url,$baseUrl) !== false){
            $pathUrl = substr($url,strlen($baseUrl));
        }
        return $pathUrl;
    }

    public function getTitle()
    {
        $currentCategory = Mage::registry('current_category');
        switch($currentCategory->getId()) {
            case Mage::getStoreConfig(self::NEW_ARRIVAL_CAT):
                return $this->__('See Newest In');
                break;
            case Mage::getStoreConfig(self::BACK_IN_STOCK_CAT):
                return $this->__('See Back In Stock In');
                break;
            case Mage::getStoreConfig(self::COMING_SOON_CAT):
                return $this->__('See Comming soon In');
                break;
            case Mage::getStoreConfig(self::TOPRATE_CAT):
                return $this->__('See Top Rate In');
                break;
            case Mage::getStoreConfig(self::ALMOST_GONE_CAT):
                return $this->__('See Almost Gone In');
                break;
            default:
                return '';
                break;
        }
    }
}