<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Delete Attribute facebook_like
try{
    $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'love_it');
    if($attributeModel->getId()){
        $installer->updateAttribute('catalog_product', 'love_it', 'is_visible', '0');
    }

}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();