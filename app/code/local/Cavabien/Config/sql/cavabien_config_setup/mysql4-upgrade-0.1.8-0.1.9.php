<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Delete Attribute facebook_like
try{
    $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'facebook_like');
    if($attributeModel->getId()){
        $installer->updateAttribute('catalog_product',$attributeModel->getId(),array(
            'source_model'       => ''
        ));
    }

}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();