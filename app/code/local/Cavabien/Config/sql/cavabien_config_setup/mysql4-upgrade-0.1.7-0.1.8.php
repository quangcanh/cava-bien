<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Update Backend Type for attribute
try{
    $attrArr = array('back_to_stock_from','back_to_stock_to');
    foreach($attrArr as $attributeCode){
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
        if($attributeModel->getId()){
            $installer->updateAttribute('catalog_product',$attributeModel->getId(),array(
                'backend_type'       => 'datetime'
            ));
        }
    }

}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();