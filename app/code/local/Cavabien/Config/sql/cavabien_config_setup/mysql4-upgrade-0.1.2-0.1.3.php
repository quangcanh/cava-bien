<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Config "Show reset link " for attribute .
try{
    $attArr = array('color','size','clothing_size','shoes_size','bra_size','length','sleeve','price','designer');
    foreach($attArr as $attributeCode){
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attributeCode);
        if($attributeId = $attributeModel->getId()){
            $model = Mage::getModel('gomage_navigation/attribute')->load($attributeId);
            if($model->getId()){
                $model->setFilterReset(1)->setShowCheckbox(1)->save();
            }
        }
    }
}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();