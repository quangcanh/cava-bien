<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create Attribute and config Use In Layered Navigation.
try{
    $attArr = array('color','size','clothing_size','shoes_size','bra_size','length','sleeve','price','designer');
    $i = 0;
    foreach($attArr as $attributeCode){
        $i++;
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
        if($attributeModel->getId()){
            if($attributeCode == 'size'){
                $attributeModel->setData('is_filterable',1)->setData('is_filterable_in_search',1);
            }
            $attributeModel->setData('position',$i);
            $attributeModel->save();
        }
    }
}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();