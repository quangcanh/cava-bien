<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create Attribute and config Use In Layered Navigation.
$attArr = array(
    'designer'=>array(
        'label'   => 'Designer',
        'options' => array(
            0 => 'Alice + Olivia',
            1 => 'Anna sui',
            2 => 'Antonio Berardi',
            3 => 'Aubin Wills',
            4 => 'Chalayan',
            5 => 'Chinti & Parker',
            6 => 'Chloe'
        )
    ),
    'shoes_size' => array(
        'label' => 'Shoes Size',
        'options' => array(
            0   => '3',
            1   => '3.5',
            2   => '4',
            3   => '4.5',
            4   => '5',
            5   => '5.5',
            6   => '6',
            7   => '6.5',
            8   => '7',
            9   => '7.5',
            10   =>  '8',
            11   =>  '9',
            12  => '9.5',
            13  =>  '10',
            14 => 'O/S'
        )
    ),
    'bra_size'   => array(
        'label' => 'Bra Size',
        'options' => array(
            0  => 'A-DD',
            1  => 'E-GG',
            2   => 'H-H',
            3   => '30A',
            4   => '30B',
            5   => '30C',
            6   => '30D',
            7   => '32A',
            8   => '32B',
            9   => '32C',
            10   => '32D',
            11   => '34A',
            12   => '34B',
            13   => '34C',
            14   => '36A',
            15   => '36B',
            16   => '36C',
            17   => '36D',
            18   => '38A',
            19   => '38B',
            20   => '38C',
            21   => '38D',

        )
    ),
    'length'     => array(
        'label' => 'Length',
        'options'   => array(
            0 => 'Short',
            1  => 'Long',
            2  => 'Maxi',
            3  => 'Mini',
            4   => 'Mid',
            5  => 'Others'
        )
    ),
    'sleeve'     => array(
        'label' => 'Sleeve',
        'options'   => array(
            0 => 'Sleeveless',
            1 => 'Short Sleeve',
            2 => '3/4 Sleeve',
            3 => 'Long Sleeve',
            4 => 'Cap Sleeve',
            5 => 'Elbow Length',
        )
    )
);
foreach($attArr as $attributeCode => $val){
    $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
    if(!$attributeModel->getId()){
        $installer->addAttribute('catalog_product', $attributeCode , array(
            'group' => 'General',
            'type' => 'int',
            'backend' => '',
            'frontend' => '',
            'label' => $val['label'],
            'input' => 'select',
            'class' => '',
            'source' => 'eav/entity_attribute_source_table',
            'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'searchable' => true, //use search
            'filterable' => true, //show sidebar in listing
            'is_filterable_in_search' => '1',  // show sidebar in search page
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
            'unique' => false,
            'is_configurable' => true,
            'option' => array('values' => $val['options']),
        ));
    }
}

$installer->endSetup();