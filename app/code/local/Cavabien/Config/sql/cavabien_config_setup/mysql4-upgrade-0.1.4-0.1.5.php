<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create static block category_top_block_clothing & category_top_block_clothing_top .
try{
    $clothingContent = <<<EOD
    <ul class ="navigation-top-link">
        <li>
            <a href="#">Luggage & Bags</a>
        </li>
        <li>
            <a href="#">Shoes</a>
        </li>
        <li>
            <a href="#">Jewelry & Watches</a>
        </li>
        <li>
            <a href="#">Beauty & Health</a>
        </li>
        <li>
            <a href="#">Women's Apparel & Accessories</a>
        </li>
    </ul>
    <div class ="top-banner">
        <div class="top-banner-slide left">
            <ul class="slide-banner">
                <li>
                    <a href="#"><img src="{{skin url='images/banner/banner1.jpg'}}"/></a>
                </li>
                <li>
                    <a href="#"><img src="{{skin url='images/banner/banner2.jpg'}}"/></a>
                </li>
                <li>
                    <a href="#"><img src="{{skin url='images/banner/banner3.jpg'}}"/></a>
                </li>
            </ul>
        </div>
        <div class="top-banner-slide right">
            <a href="#"></a>
            <a href="#"></a>
         </div>
    </div>
EOD;
    $topContent      = <<<EOD
    <div class="top-banner category-top">
        <div class = "left">
            <div id = "top-pager">
                <a data-slide-index="0" href="javascript:void(0)">
                    <h3>Maxi Dresses</h3>
                    <span>Ideal for evening and daywear explore our gorgeous collection</span>
                </a>

                <a data-slide-index="1" href="javascript:void(0)">
                    <h3>Leggings</h3>
                    <span>In various styles, lengths and with build in shapewear</span>
                </a>

                <a data-slide-index="2" href="javascript:void(0)">
                    <h3>Mark Heyes</h3>
                    <span>Celebrity stylist Mark Heyes has an eye for detail and he's got some key summer outifit ideas</span>
                </a>
            </div>
            <ul class = "top-cate-banner">
                <li>
                    <a href="#"><img src="{{skin url='images/banner/top-slide1.jpg'}}"/></a>
                </li>
                <li>
                    <a href="#"><img src="{{skin url='images/banner/top-slide2.jpg'}}"/></a>
                </li>
                <li>
                    <a href="#"><img src="{{skin url='images/banner/top-slide3.jpg'}}"/></a>
                </li>
            </ul>
        </div>
        <div class = "right">
            <img src="{{skin url='images/banner/top-banner.jpg'}}" />
        </div>
    </div>
EOD;


    $blockArr = array(
        array(
            'identifier' => 'category_top_block_clothing',
            'title'      => 'Category Top Block Clothing',
            'content'    => $clothingContent
        ),
        array(
            'identifier' => 'category_top_block_clothing_top',
            'title'      => 'Category Top Block Clothing Top',
            'content'    => $topContent
        ),
    );
    foreach($blockArr as $block){
        $blockModel = Mage::getModel('cms/block')->load($block['identifier']);
        if(is_object($blockModel)){
            if(!$blockModel->getId()){
                $blockModel->setTitle($block['title'])
                           ->setIdentifier($block['identifier'])
                           ->setStores(array(0))
                           ->setIsActive(1)
                           ->setContent($block['content'])->save();
            }else{
                $blockModel->setContent($block['content'])->save();
            }
        }

    }

}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();