<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//create static block for size chart.
try{
    $attributeCode = array('size','bra_size','clothing_size','shoes_size');
    foreach($attributeCode as $attr){
        $block = Mage::getModel('cms/block')->load('size_chart_'.$attr);
        if(!$block->getId()){
            $block->setTitle('Size Chart '.ucwords(str_replace('_'," ",$attr)))
                  ->setIdentifier('size_chart_'.$attr)
                  ->setStores(array(0))
                  ->setIsActive(1)
                  ->setContent("This is size chart")
                  ->save();
        }
    }
}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();