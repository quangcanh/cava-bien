<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create Attribute facebook_like for product
try{
    $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'facebook_like');
    if(!$attributeModel->getId()){
        $installer->addAttribute('catalog_product', 'facebook_like' , array(
            'group' => 'General',
            'type' => 'int',
            'backend' => '',
            'frontend' => '',
            'label' => 'Facebook Like',
            'input' => 'text',
            'class' => '',
            'source' => 'eav/entity_attribute_source_table',
            'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'searchable' => false, //use search
            'filterable' => false, //show sidebar in listing
            'is_filterable_in_search' => false,  // show sidebar in search page
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
            'unique' => false,
        ));
    }
}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();