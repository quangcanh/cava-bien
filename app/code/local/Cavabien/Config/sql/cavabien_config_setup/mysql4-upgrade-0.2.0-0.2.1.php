<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Config Show Products per Page on Grid Allowed Values
try{
    $conf = Mage::app()->getConfig();
    $conf->saveConfig('catalog/frontend/grid_per_page_values',"20,30,50,100");
    $conf->saveConfig('catalog/frontend/grid_per_page',"20");
    $conf->saveConfig('catalog/frontend/list_allow_all',1);

}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();