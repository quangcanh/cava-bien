<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 24/06/2014
 * Time: 09:40
 */ 
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Create static block navigation_layer_adv & navigation_layer_gift .
try{
    $navigation_layer_adv = <<<EOD
<img src="{{skin url='images/layer-navi-adv.jpg'}}" />
EOD;
    $navigation_layer_gift = <<<EOD
<a href="{{store direct_url='#'}}">Gift Certificates</a>
EOD;


     $blockArray = array(
         'navigation_layer_adv' => array(
             'title'    => 'Layer Navigation Adv',
             'content'  => $navigation_layer_adv
         ),
         'navigation_layer_gift' => array(
             'title'    => 'Layer Navigation Gift',
             'content'  => $navigation_layer_gift
         ));
     foreach($blockArray as $urlKey => $block){
         $blockModel = Mage::getModel('cms/block')->load($urlKey);
         if(is_object($blockModel)){
             if(!$blockModel->getId()){
                $blockModel->setTitle($block['title'])
                           ->setIdentifier($urlKey)
                           ->setContent($block['content'])
                           ->setIsActive(1)
                           ->setStores(array(0))->save();
             }
         }
     }
}catch (Exception $e){
    echo $e->getMessage();
}

$installer->endSetup();