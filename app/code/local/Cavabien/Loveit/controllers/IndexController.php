<?php

/**
 * Class Cavabien_LoveIt_IndexController
 */
class Cavabien_Loveit_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Ajax save loveit
     */
    public function ajaxSaveLoveitAction()
    {
        $request = $this->getRequest();
        $productId = $request->getParam('productId') ? $request->getParam('productId') : null;
        $customerId = $request->getParam('customerId') ? $request->getParam('customerId') : -1;
        $result = array();

        /** @var $loveit Cavabien_LoveIt_Model_LoveIt */
        $loveit = Mage::getModel('cavabien_loveit/loveit');
        $product = Mage::getModel('catalog/product');
        // If customer not logged in and customer have loved this product before
        if ($customerId != -1 && Mage::helper('cavabien_loveit')->isLoveitExist($productId, $customerId)) {
            $result['success'] = false;
        } else {
            $loveitData = array(
                'product_id' => $productId,
                'customer_id' => $customerId,
                'loved_at_time' => Mage::getModel('core/date')->timestamp(time()),
            );
            try {
                $loveit->setData($loveitData)->save();
                $product->load($productId);
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $loveitNumber = intval($product->getLoveIt()) + 1;
                $product->setData('love_it', $loveitNumber)->getResource()->saveAttribute($product, 'love_it');
                $result['success'] = true;
                $result['number'] = number_format($loveitNumber);
            } catch (Exception $e) {
                $result['success'] = false;
            }
        }

        $response = $this->getResponse();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Ajax unlove
     */
    public function ajaxUnLoveitAction()
    {
        $request = $this->getRequest();
        $productId = $request->getParam('productId') ? $request->getParam('productId') : null;
        $customerId = $request->getParam('customerId') ? $request->getParam('customerId') : -1;
        $result = array();

        /** @var $loveit Cavabien_LoveIt_Model_LoveIt */
        $loveit = Mage::getModel('cavabien_loveit/loveit');
        $product = Mage::getModel('catalog/product');
        // If customer not logged in and customer have loved this product before
        if ($customerId != -1 && !Mage::helper('cavabien_loveit')->isLoveitExist($productId, $customerId)) {
            $result['success'] = false;
        } else {
            $sql = "DELETE FROM cavabien_loveit WHERE customer_id='" . $customerId . "' AND product_id='" . $productId . "'";
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $connection->query($sql);
            try {
                $product->load($productId);
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $loveitNumber = intval($product->getLoveIt()) - 1;
                $product->setData('love_it', $loveitNumber)->getResource()->saveAttribute($product, 'love_it');


                $result['success'] = number_format($loveitNumber);
                $result['number'] = number_format($loveitNumber);
            } catch (Exception $e) {
                $result['success'] = false;
            }
        }
        $response = $this->getResponse();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(Mage::helper('core')->jsonEncode($result));
    }
}