<?php

/**
 * Class Cavabien_LoveIt_Helper_Data
 */
class Cavabien_Loveit_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Checkout loveit record exist.
     * Using in case customerId != -1
     *
     * @param $productId
     * @param $customerId
     *
     * @return bool
     */
    public function isLoveitExist($productId, $customerId)
    {
        $sampleRecord = Mage::getModel('cavabien_loveit/loveit')->getCollection()
                             ->addFieldToFilter('product_id',
                array(
                    'eq'  => $productId,
                ))
                             ->addFieldToFilter('customer_id',
                array(
                    'eq'  => $customerId,
                )
            )->getFirstItem();

        return $sampleRecord->getId() ? true : false;
    }
}