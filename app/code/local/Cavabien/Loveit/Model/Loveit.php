<?php

/**
 * Class Cavabien_LoveIt_Model_LoveIt
 */
class Cavabien_Loveit_Model_Loveit extends Mage_Core_Model_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_loveit/loveit');
    }
}