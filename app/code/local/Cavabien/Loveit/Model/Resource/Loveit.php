<?php

/**
 * Class Cavabien_LoveIt_Model_Resource_LoveIt
 */
class Cavabien_Loveit_Model_Resource_Loveit extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_loveit/loveit', 'loveit_id');
    }
}
 