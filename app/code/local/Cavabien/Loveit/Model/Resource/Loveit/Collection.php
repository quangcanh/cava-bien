<?php

/**
 * Class Cavabien_LoveIt_Model_Resource_LoveIt_Collection
 */
class Cavabien_Loveit_Model_Resource_Loveit_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_loveit/loveit');
    }
}
