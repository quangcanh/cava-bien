<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('cavabien_loveit/loveit')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_loveit/loveit')} (
      `loveit_id` INT(10) unsigned NOT NULL auto_increment,
      `customer_id` INT(10) NOT NULL,
      `product_id` INT(10) unsigned NOT NULL,
      `loved_at_time` TIMESTAMP NOT NULL,
      PRIMARY KEY  (`loveit_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('cavabien_loveit/loveit')} ADD INDEX (`customer_id`, `product_id`);
ALTER TABLE {$this->getTable('cavabien_loveit/loveit')} ADD INDEX (`loved_at_time`);
");

$installer->endSetup();