<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 26/06/2014
 * Time: 10:13
 */
class Cavabien_Ajax_IndexController extends Mage_Core_Controller_Front_Action
{
    public function quickViewAction(){
        $productId = $this->getRequest()->getParam('product_id');
        $viewHelper = Mage::helper('catalog/product_view');
        $params = new Varien_Object();
        $params->setCategoryId(false);
        $params->setSpecifyOptions(false);
        try{
            $viewHelper->prepareAndRender($productId, $this, $params);
        }catch (Exception $e){
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }

    public function galleryAction(){
        $productId = $this->getRequest()->getParam('product_id');
        $customerId = $this->getRequest()->getParam('customer_id');
        Mage::register('gallery_customer_id',$customerId);
        $viewHelper = Mage::helper('catalog/product_view');
        $params = new Varien_Object();
        $params->setCategoryId(false);
        $params->setSpecifyOptions(false);
        try{
            $viewHelper->prepareAndRender($productId, $this, $params);
        }catch (Exception $e){
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }

    /*
     * Update Facebook Like Number
     *
     * */

    public function updateLikeNumberAction(){
        $param = $this->getRequest()->getParam('linkedLike');
        $likedNumber = Mage::helper('cavabien_config')->get_the_fb_like($param);
        $vPath = Mage::helper('cavabien_config')->getPathProduct($param);
        $oRewrite = Mage::getModel('core/url_rewrite')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->loadByRequestPath($vPath);
        $iProductId = $oRewrite->getProductId();
        $productModel = Mage::getModel('catalog/product')->load($iProductId);
        if($productModel->getId()){
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $productModel->setFacebookLike((int)$likedNumber)->save();
        }
    }
}