<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 26/08/2014
 * Time: 11:18
 */ 
class Cavabien_Geoip_Helper_Data extends Mage_Core_Helper_Abstract {
    /**
     * Load GeoIP binary data file
     *
     * @return string
     */
    public function loadGeoIp()
    {
        // Load geoip.inc
        include_once(Mage::getBaseDir().'/media/geoip/geoip.inc');

        // Open Geo IP binary data file
        $geoIp = geoip_open(Mage::getBaseDir().'/media/geoip/GeoIP.dat',GEOIP_STANDARD);

        return $geoIp;
    }

    public function isEnabled()
    {
//         $ipConfig = trim(Mage::getStoreConfig('homepage/header/fix_ip_for_language'));
        return true;
    }

    /**
     * Get IP Address
     *
     * @return string
     */
    public function getIpAddress()
    {
        //$ipConfig = trim(Mage::getStoreConfig('homepage/header/fix_ip_for_language'));
        return $_SERVER['REMOTE_ADDR'];
     }

    public function getCountryCodeByIp($result = '')
    {
        // load GeoIP binary data file
        $geoIp = Mage::helper('cavabien_geoip')->loadGeoIp();

        $ipAddress = Mage::helper('cavabien_geoip')->getIpAddress();

        // get country code from ip address
        $countryCode = geoip_country_code_by_addr($geoIp, $ipAddress);
         if($countryCode == '') {
            return $result;
        }
        return $countryCode;
    }

}