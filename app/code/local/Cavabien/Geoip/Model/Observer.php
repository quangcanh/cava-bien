<?php
class Cavabien_Geoip_Model_Observer
{
    public function controllerFrontInitBefore($observer)
    {
        //CHECK SESSION => If isn't first change
         if (!Mage::getSingleton('core/session')->getChangeLanguage()) {
             if (Mage::helper('cavabien_geoip')->isEnabled()) {
                $allStores = Mage::app()->getStores();
                $storeCodeArr = array();
                foreach ($allStores as $_eachStoreId => $val) {
                    $_storeCode = Mage::app()->getStore($_eachStoreId)->getCode();
                     $storeCodeArr[] = $_storeCode;
                }
                // get country code from ip address
                  $countryCode = Mage::helper('cavabien_geoip')->getCountryCodeByIp();
                if ($countryCode && in_array(strtolower($countryCode), $storeCodeArr)) {
                    Mage::app()->setCurrentStore(strtolower($countryCode));
                }else{
                    Mage::app()->setCurrentStore(strtolower('en'));
                }
             }
             //SET SESSION =>True after first change
             Mage::getSingleton('core/session')->setChangeLanguage(1);
        }

    }
}