<?php
/**
 * Created by PhpStorm.
 * User: My PC
 * Date: 26/08/2014
 * Time: 16:56
 */ 
class Cavabien_Geoip_Model_Core_Store extends Mage_Core_Model_Store {
    public function getDefaultCurrencyCode()
    {
            $result = $this->getConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_DEFAULT);
            return $this->getCurrencyCodeByIp($result);
    }

    /**
     * Get Currency code by IP Address
     *
     * @return string
     */
    public function getCurrencyCodeByIp($result = '')
    {
        // load GeoIP binary data file
        $geoIp = Mage::helper('cavabien_geoip')->loadGeoIp();

        $ipAddress = Mage::helper('cavabien_geoip')->getIpAddress();

        // get country code from ip address
        $countryCode = geoip_country_code_by_addr($geoIp, $ipAddress);
         if($countryCode == '') {
            return $result;
        }

        // get currency code from country code
        $currencyCode = geoip_currency_code_by_country_code($geoIp, $countryCode);

        // close the geo database
        geoip_close($geoIp);

        // if currencyCode is not present in allowedCurrencies
        // then return the default currency code
        $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();

        if(!in_array($currencyCode, $allowedCurrencies)) {
            return $result;
        }
         return $currencyCode;
    }
}