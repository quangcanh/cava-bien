<?php

class Cavabien_Stylegallery_Block_Adminhtml_Template_Grid_Renderer_Enable extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    }
    protected function _getValue(Varien_Object $row)
    {
        $val = $row->getData($this->getColumn()->getIndex());
        $val = str_replace("no_selection", "", $val);
        if($val==1){
            $out = "Yes";
        }elseif($val==0){
            $out = "No";
        }
        return $out;
    }
}