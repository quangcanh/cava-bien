<?php

class Cavabien_Stylegallery_Block_Adminhtml_Stylegallery extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_controller = 'adminhtml_stylegallery';
        $this->_blockGroup = 'cavabien_stylegallery';
        $this->_headerText = Mage::helper('cavabien_stylegallery')->__('Style Gallery Manager');
        $this->_addButtonLabel = Mage::helper('cavabien_stylegallery')->__('Add News');
        parent::__construct();
        $this->_removeButton('add');
    }
}