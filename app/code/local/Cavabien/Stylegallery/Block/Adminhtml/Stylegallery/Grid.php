<?php

class Cavabien_Stylegallery_Block_Adminhtml_Stylegallery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('gallery_id');
        $this->setDefaultSort('gallery_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('cavabien_stylegallery/stylegallery')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('gallery_id', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'gallery_id',
        ));

        $this->addColumn('product_id', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('Product ID'),
            'align' => 'left',
            'index' => 'product_id',
            'width' => '50px',
        ));


        $this->addColumn('customer_id', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('Customer Id'),
            'width' => '150px',
            'index' => 'customer_id',
        ));

        $this->addColumn('add_time', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('Time added'),
            'width' => '150px',
            'index' => 'add_time',
        ));


        $this->addColumn('enable', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('Enable'),
            'width' => '150px',
            'index' => 'enable',
            'renderer' => 'Cavabien_Stylegallery_Block_Adminhtml_Template_Grid_Renderer_Enable'
        ));

        $this->addColumn('image', array(
            'header' => Mage::helper('cavabien_stylegallery')->__('Image'),
            'width' => '150px',
            'index' => 'image',
            'renderer' => 'Cavabien_Stylegallery_Block_Adminhtml_Template_Grid_Renderer_Image'
        ));

        return parent::_prepareColumns();
    }


    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}