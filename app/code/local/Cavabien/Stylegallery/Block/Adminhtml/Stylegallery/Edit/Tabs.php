<?php

class Cavabien_Stylegallery_Block_Adminhtml_Stylegallery_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct() {
        parent::__construct();
        $this->setId('stylegallery_tabs');
        $this->setDestElementId('edit_form'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('cavabien_stylegallery')->__('Style Gallery Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('cavabien_stylegallery')->__('Style Gallery Information'),
            'title' => Mage::helper('cavabien_stylegallery')->__('Style Gallery Information'),
            'content' => $this->getLayout()->createBlock('cavabien_stylegallery/adminhtml_stylegallery_edit_tab_form')->toHtml(),
        ));

//        $this->addTab('form_section1', array(
//            'label' => Mage::helper('cavabien_measurements')->__('Content'),
//            'title' => Mage::helper('cavabien_measurements')->__('Content'),
//            'content' => $this->getLayout()->createBlock('cavabien_measurements/adminhtml_measurements_edit_tab_content')->toHtml(),
//        ));

        return parent::_beforeToHtml();
    }
}