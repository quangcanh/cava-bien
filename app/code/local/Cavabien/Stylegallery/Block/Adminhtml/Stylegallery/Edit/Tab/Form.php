<?php

class Cavabien_Stylegallery_Block_Adminhtml_Stylegallery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {

        if (Mage::registry('stylegallery_data')) {
            $data = Mage::registry('stylegallery_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('stylegallery_stylegallery', array('legend' => Mage::helper('cavabien_stylegallery')->__('style gallery information')));

        $fieldset->addField('enable', 'select', array(
            'label' => Mage::helper('cavabien_stylegallery')->__('Enable'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'enable',
            'onclick' => "",
            'onchange' => "",
            'value'  => '1',
            'values' => array('-1'=>'Please Select..','1' => 'Yes','0' => 'No'),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));

        $fieldset->addField('image', 'link', array(
            'label'     => Mage::helper('cavabien_stylegallery')->__('Image'),
            'style'   => "",
            'href' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'stylegallery/'.$data['image'],
            'value'  => 'Magento Blog',
            'after_element_html' => ''
        ));

        $form->setValues($data);

        return parent::_prepareForm();
    }
}