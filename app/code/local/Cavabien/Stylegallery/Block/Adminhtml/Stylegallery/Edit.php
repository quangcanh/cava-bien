<?php

class Cavabien_Stylegallery_Block_Adminhtml_Stylegallery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'cavabien_stylegallery';
        $this->_controller = 'adminhtml_stylegallery';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', Mage::helper('cavabien_stylegallery')->__('Save Style Gallery'));
        $this->_updateButton('delete', 'label', Mage::helper('cavabien_stylegallery')->__('Delete'));
        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('cavabien_stylegallery')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    /*
     * This function is responsible for Including TincyMCE in Head.
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
    }


    public function getHeaderText() {
        if (Mage::registry('stylegallery_data') && Mage::registry('stylegallery_data')->getGalleryId()) {
            return Mage::helper('cavabien_stylegallery')->__('Edit Style Gallery');
        } else {
            return Mage::helper('cavabien_stylegallery')->__('New Style Gallery');
        }
    }
}