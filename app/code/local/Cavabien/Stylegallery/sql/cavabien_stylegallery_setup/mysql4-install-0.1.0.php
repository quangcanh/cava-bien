<?php
/**
 * Created by JetBrains PhpStorm.
 * User: LongPD
 * Date: 7/7/14
 * Time: 10:17 AM
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_stylegallery/stylegallery')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_stylegallery/stylegallery')} (
      `gallery_id` int(10) unsigned NOT NULL auto_increment,
      `product_id` int(10) unsigned,
      `customer_id` int(10) unsigned,
      `image` varchar(255),
      `add_time` TIMESTAMP,
      `enable` int(10) unsigned,
      PRIMARY KEY  (`gallery_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_stylegallery/stylegallerylook')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_stylegallery/stylegallerylook')} (
      `gallery_look_id` int(10) unsigned NOT NULL auto_increment,
      `product_main_id` int(10) unsigned,
      `customer_id` int(10) unsigned,
      `product_sub_id` varchar(255),
      `product_sub_status` varchar(255),
      PRIMARY KEY  (`gallery_look_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->endSetup();