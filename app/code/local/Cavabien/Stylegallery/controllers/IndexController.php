<?php
class Cavabien_Stylegallery_IndexController extends Mage_Core_Controller_Front_Action
{
    public function sharephotoAction(){
        $post = $this->getRequest()->getPost();
        $url = $this->getRequest()->getPost('product_url');

        if ( $post ) {

            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                /**************************************************************/
                $fileName = '';
                $error = '';
                if (isset($_FILES['avt']['name']) && $_FILES['avt']['name'] != '') {
                    try {
                        $fileName       = $_FILES['avt']['name'];
                        $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
                        $fileNamewoe    = rtrim($fileName, $fileExt);
                        $fileName       = $fileNamewoe . time() . '.' . $fileExt;

                        $uploader       = new Varien_File_Uploader('avt');
                        $uploader->setAllowedExtensions(array('png','jpg','gif'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'stylegallery';
                        if(!is_dir($path)){
                            mkdir($path, 0777, true);
                        }
                        $uploader->save($path . DS, $fileName );
                    } catch (Exception $e) {
                        $error = true;
                    }
                }elseif(isset($post['fb_avt'])&&$post['fb_avt']!=''){
                    $arrFbavt = explode(" ",$post['fb_avt']);
                    $filePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA)
                        .DS
                        .'stylegallery'
                        .DS
                        .basename($arrFbavt[0]);

                    $directory = dirname($filePath);

                    if (!file_exists($directory) || !is_dir($directory)) {
                        if (!@mkdir($directory, 0777, true))
                            return null;
                    }

                    if(!file_exists($filePath) ||
                        (file_exists($filePath) && (time() - filemtime($filePath) >= 3600))){
                        $client = new Zend_Http_Client($arrFbavt[0]);
                        $client->setStream();
                        $response = $client->request('GET');
                        stream_copy_to_stream($response->getStream(), fopen($filePath, 'w'));
                        $fileName = basename($arrFbavt[0]);
                        $imageObj = new Varien_Image($filePath);
                        $imageObj->constrainOnly(true);
                        $imageObj->keepAspectRatio(true);
                        $imageObj->keepFrame(false);
                        $imageObj->resize(150, 150);
                        $imageObj->save($filePath);
                    }
                }elseif(isset($post['instagram_avt'])&&$post['instagram_avt']!=''){
                    $filePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA)
                        .DS
                        .'stylegallery'
                        .DS
                        .basename($post['instagram_avt']);

                    $directory = dirname($filePath);

                    if (!file_exists($directory) || !is_dir($directory)) {
                        if (!@mkdir($directory, 0777, true))
                            return null;
                    }

                    if(!file_exists($filePath) ||
                        (file_exists($filePath) && (time() - filemtime($filePath) >= 3600))){
                        $client = new Zend_Http_Client($post['instagram_avt']);
                        $client->setStream();
                        $response = $client->request('GET');
                        stream_copy_to_stream($response->getStream(), fopen($filePath, 'w'));
                        $fileName = basename($post['instagram_avt']);
                        $imageObj = new Varien_Image($filePath);
                        $imageObj->constrainOnly(true);
                        $imageObj->keepAspectRatio(true);
                        $imageObj->keepFrame(false);
                        $imageObj->resize(150, 150);
                        $imageObj->save($filePath);

                    }
                }elseif(isset($post['flickr_avt'])&&$post['flickr_avt']!=''){
                    $filePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA)
                        .DS
                        .'stylegallery'
                        .DS
                        .basename($post['flickr_avt']);

                    $directory = dirname($filePath);

                    if (!file_exists($directory) || !is_dir($directory)) {
                        if (!@mkdir($directory, 0777, true))
                            return null;
                    }

                    if(!file_exists($filePath) ||
                        (file_exists($filePath) && (time() - filemtime($filePath) >= 3600))){
                        $client = new Zend_Http_Client($post['flickr_avt']);
                        $client->setStream();
                        $response = $client->request('GET');
                        stream_copy_to_stream($response->getStream(), fopen($filePath, 'w'));
                        $fileName = basename($post['flickr_avt']);
                        $imageObj = new Varien_Image($filePath);
                        $imageObj->constrainOnly(true);
                        $imageObj->keepAspectRatio(true);
                        $imageObj->keepFrame(false);
                        $imageObj->resize(150, 150);
                        $imageObj->save($filePath);

                    }
                }

                $customerId = $post['customer_id'];
                $productMainId = $post['product_main_id'];

                $arrSubProduct = array();
                $arrSubProductKey = array();
                $arrSubProductCollection = array();

                foreach($post as $k => $v){ ;
                    if(strstr($k,'select') != false){
                        $arrSubProduct[str_replace('select','',$k)] = $v;
                        array_push($arrSubProductKey,str_replace('select','',$k));
                    }
                }
                if(isset($customerId)&&($customerId!='')&& isset($productMainId)&&($productMainId!=''))
                {
                    $colectionfull =  Mage::getModel('cavabien_stylegallery/stylegallerylook')->getCollection()
                        ->addFieldToFilter('product_main_id', array('eq' => $productMainId));
                    foreach($colectionfull as $coll){
                        array_push($arrSubProductCollection,$coll->getData('product_sub_id'));
                    }
                    $arrdels = array_diff( $arrSubProductCollection, $arrSubProductKey) ;
                    foreach($arrdels as $del){
                        $colectionDel =  Mage::getModel('cavabien_stylegallery/stylegallerylook')->getCollection()
                            ->addFieldToFilter('product_main_id', array('eq' => $productMainId))
                            ->addFieldToFilter('product_sub_id', array('eq' => $del));
                        $stylegallerylookModel = Mage::getModel('cavabien_stylegallery/stylegallerylook');
                        $data = $colectionDel->getData();
                        $stylegallerylookModel->setGalleryLookId($data[0]['gallery_look_id'])->delete();
                    }
                    foreach($arrSubProduct as $subId=>$status){
                        $colection =  Mage::getModel('cavabien_stylegallery/stylegallerylook')->getCollection()
                            ->addFieldToFilter('product_main_id', array('eq' => $productMainId))
                            ->addFieldToFilter('customer_id', array('eq' => $customerId))
                            ->addFieldToFilter('product_sub_id', array('eq' => $subId));
                        if(count($colection)<1){
                            $stylegallerylook = Mage::getModel('cavabien_stylegallery/stylegallerylook');
                            $arrDataLook = array(
                                'product_main_id' => $productMainId,
                                'customer_id' => $customerId,
                                'product_sub_id' => $subId,
                                'product_sub_status' => $status
                            );
                            $stylegallerylook->setData($arrDataLook)->save();
                        }else{
                            $stylegallerylookModel = Mage::getModel('cavabien_stylegallery/stylegallerylook');
                            $data = $colection->getData();
                            $stylegallerylookModel->load($data[0]['gallery_look_id'])->setProductSubStatus($status)->save();
                        }
                    }
                    $stylegallery = Mage::getModel('cavabien_stylegallery/stylegallery');
                    $colectionGallery =  Mage::getModel('cavabien_stylegallery/stylegallery')->getCollection()
                        ->addFieldToFilter('product_id', array('eq' => $productMainId))
                        ->addFieldToFilter('customer_id', array('eq' => $customerId));

                    if(count($colectionGallery)<1&&$fileName!=''){
                        $arrData = array(
                            'product_id' => $productMainId,
                            'customer_id' => $customerId,
                            'image' => $fileName,
                            'add_time' => Mage::getModel('core/date')->timestamp(time()),
                            'enable' => 0
                        );
                        $stylegallery->setData($arrData)->save();
                    }
                    $translate->setTranslateInline(true);
                }else{
                    $error = true;
                }
                if($error!=true){
                    $this->_redirectUrl($url);
                    Mage::getSingleton('core/session')->setUploadSuccess('true');
                    return;
                }else{
                    $this->_redirectUrl($url);
                    Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
                }


            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                $this->_redirectUrl($url);
                Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
                return;
            }

        } else {
            $this->_redirectUrl($url);
        }
    }

    public function searchAction(){
        $keyword = $this->getRequest()->getPost('keyword');
        $products = Mage::helper("cavabien_stylegallery")->getSearchProduct($keyword);
        $_coreHelper = Mage::helper('core');
        $result = array();
        $items = array();
        $result['all_items'] = "";
        $result['keyword'] = $keyword;
        $result['countitem'] = count($products);
        if(count($products))
        {
            foreach($products as $productId)
            {
                $product = Mage::getModel("catalog/product")->load($productId);
                $img = Mage::helper('catalog/image')->init($product, 'image')->resize(100,142);
                $img = $img->__toString();
                $price = $_coreHelper->currency($product->getPrice(),true,false);
                $product_url = $product->getProductUrl();
                $items[] = array("name"=>$product->getName(),"id"=>$product->getId(),"image"=>$img,"price"=>$price,"url"=>$product_url);

            }
            $productsSearch = $items;

            if($productsSearch!=''){
                $result['all_items'] = "<ul id='list-items-search-result' class='bx-items-search-result'>";
                $count = 0;
                $result['all_items'] .= '<li>';
                foreach($productsSearch as $p){
                    $result['all_items'] .= "<a><img name='select".$p['id']."' title='".$p['name']."' src='".$p['image']."'</a>";
                    $count++;if($count % 2 == 0){
                         $result['all_items'] .= "</li><li>";
                    }
                }
                $result['all_items'].= "</ul>";
                $result['all_items'].= <<<EOD
             <script type='text/javascript'>
              jQuery('.bx-items-search-result').owlCarousel({
                     items : 5,
                     itemsCustom : false,
                     itemsDesktop : [1199,4],
                     itemsDesktopSmall : [980,4],
                     itemsTablet: [768,3],
                     itemsTabletSmall: false,
                     itemsMobile : [479,1],
                     pagination : false,
                     navigation: true,
                     slideSpeed : 400,
                     paginationSpeed : 800,
                     rewindSpeed : 1000
                 });
                 jQuery('.bx-items-search-result li a').each(function(){
                 var e = jQuery(this);
                 e.click(function(){
                     jQuery('#upload-items-empty').hide();
                     jQuery('#upload-items').show();
                     var exists = false;
                     jQuery('div.upload-add-item div.item').each(function(){
                         if(jQuery(this).find('select').attr('id')== e.find('img').attr('name')){
                             exists = true;
                         }
                     })
                     if(exists==false){
                         var content = "<div class='item'>"+e.html()+ "<a class='close' id='item-" + e.find('img').attr('name') +"' onclick='removeUploadItem(\"" + e.find('img').attr('name') + "\")' href='javascript:void(0)'><span><i class='fa fa-times-circle' title=''></i></span></a><select name='select"+e.find('img').attr('name')+"' id='"+e.find('img').attr('name')+"'><option value='0'>Pictured</option><option value='1'>Similar</option></select></div>";
                         var owlSlider = jQuery("#upload-items");
                         owlSlider.data('owlCarousel').addItem(content);
                         jQuery('select#'+e.find('img').attr('name')).switchify();
                     }else{
                         alert('This item has been exists.');
                     }

                    })
                 });
              </script>
EOD;
            }
        }else{
            $result['all_items'] = "<ul id='list-items-search-result' class='bx-items-search-result'>Your search returns no results.</ul>";
        }

        $result['success'] = true;
        $response = $this->getResponse();
        $response->setBody(Mage::helper('core')->jsonEncode($result));
    }

}