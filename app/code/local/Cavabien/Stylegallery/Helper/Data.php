<?php

class Cavabien_Stylegallery_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getSearchProduct($keyword)
    {
        $result = array();

        //search by name
        $products = $this->searchProductByAttribute($keyword,"name");
        $product_count = count($products);

        if($product_count)
        {
            foreach($products as $productId)
            {
                $result[] = $productId;
            }
        }

        $result_final = array();
        if(count($result)) {
            foreach($result as $item)
                $result_final[$item] = $item;
        }

        return $result_final;
    }

    public function searchProductByAttribute($keyword,$att)
    {
        $result = array();

        $storeId    = Mage::app()->getStore()->getId();
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addFieldToFilter("status",1)
            ->addFieldToFilter($att,array('like'=>'%'. $keyword.'%'))
            ->setCurPage(1)
            ->setOrder('name','ASC');

        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInSiteFilterToCollection($products);
        $products->load();

        if(count($products))
        {
            foreach($products as $product)
            {
                $result[] = $product->getId();
            }
        }
        return $result;
    }
}