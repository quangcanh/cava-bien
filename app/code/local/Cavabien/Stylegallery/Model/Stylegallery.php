<?php

class Cavabien_Stylegallery_Model_Stylegallery extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('cavabien_stylegallery/stylegallery');
    }
}