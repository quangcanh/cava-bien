<?php

/**
 * Class Cavabien_Stylegallery_Model_Resource_Stylegallerylook_Collection
 */
class Cavabien_Stylegallery_Model_Resource_Stylegallerylook_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_stylegallery/stylegallerylook');
    }
}