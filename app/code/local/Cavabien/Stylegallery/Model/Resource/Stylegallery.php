<?php

class Cavabien_Stylegallery_Model_Resource_Stylegallery extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_stylegallery/stylegallery', 'gallery_id');
    }
}