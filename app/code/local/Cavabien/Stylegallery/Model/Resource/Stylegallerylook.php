<?php

class Cavabien_Stylegallery_Model_Resource_Stylegallerylook extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_stylegallery/stylegallerylook', 'gallery_look_id');
    }
}