<?php

/**
 * Class Cavabien_Rewardsocial_Index
 */
class Cavabien_Rewardsocial_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Record points when customer share via social service
     * TODO: update message content
     */
    public function processPointAction()
    {
        $result = array();
        $request = $this->getRequest();
        $productId = $request->getParam('productId');
        $customerId = $request->getParam('customerId');
        $service = $request->getParam('service');
        $storeId = Mage::app()->getStore()->getId();

        if ($customerId == '') {
            $result['success'] = true;
            $result['message'] = $this->__('Thanks for sharing this product. Logged in our store to get reward points from sharing.');
        } else {
            /** @var $helper Cavabien_Rewardsocial_Helper_Data */
            $helper = Mage::helper('cavabien_rewardsocial');
            $serviceToOrderIdMapping = Cavabien_Rewardsocial_Model_SocialService::getMappingServiceOrderId();
            if (!$helper->isStillAbleToShare($customerId, $service, $storeId)) {
                $result['success'] = false;
                $result['type'] = 'warning';
                $result['message'] = $this->__('You have shared max %d times per day.', $helper->getMaxTimeOfSharingPerday());
            } else {
                try {
                    $rewardPoints = intval($helper->getSharingRewardPoints($productId, $service));
                    /** @var $observer Rewardpoints_Model_Observer */
                    $observer = Mage::getModel('rewardpoints/observer');
                    $observer->recordPoints($rewardPoints, $customerId, $serviceToOrderIdMapping[$service], true, false, false, $storeId);
                    if (Mage::getStoreConfig('rewardpoints/default/flatstats', $storeId)) {
                        $observer->processRecordFlat($customerId, $storeId, false);
                    }
                    $result['success'] = true;
                    $result['message'] = $this->__('You have just earned %d points for sharing this product.', $rewardPoints);
                } catch (Exception $e) {
                    $result['success'] = false;
                    $result['message'] = $this->__('Have a problem when calculate reward points. Please try again.');
                    $result['type'] = 'error';
                }
            }
        }

        $response = $this->getResponse();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(Mage::helper('core')->jsonEncode($result));
    }
}