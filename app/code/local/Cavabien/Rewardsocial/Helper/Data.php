<?php

/**
 * Class Cavabien_Rewardsocial_Helper_Data
 */
class Cavabien_Rewardsocial_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Return reward points configurations per product
     *
     * @param $productId
     * @param $service
     *
     * @return mixed|null
     */
    public function getConfigRewardPointsPerProduct($productId, $service)
    {
        $suffixAttribute = '_share_reward_points';
        $product = Mage::getModel('catalog/product')->load($productId);
        if (!$product) {
            return null;
        }

        return $product->getData($service . $suffixAttribute);
    }

    /**
     * Return reward points global configurations
     *
     * @param $service
     *
     * @return mixed
     */
    public function getConfigRewardPointsGlobal($service)
    {
        $prefixPathConfiguration = 'social_reward/points/';
        $rewardPoints = Mage::getStoreConfig($prefixPathConfiguration . $service);

        return $rewardPoints;
    }

    /**
     * Return max social share per day configuration
     *
     * @return mixed
     */
    public function getMaxTimeOfSharingPerday()
    {
        return Mage::getStoreConfig('social_reward/max_share_perday/max_share');
    }

    /**
     * Get social share time per customer until now.
     *
     * @param $service
     * @param $customerId
     * @param $storeId
     *
     * @return bool|int
     */
    public function getTimesOfShared($service, $customerId, $storeId)
    {
        date_default_timezone_set('UTC');
        $serviceToOrderIdMapping = Cavabien_Rewardsocial_Model_SocialService::getMappingServiceOrderId();
        $beginOfDayTimestamp = mktime(0, 0, 0);
        $endOfDayTimestamp = mktime(23, 59, 59);
        if (!array_key_exists($service, $serviceToOrderIdMapping)) {
            return false;
        }
        $orderId = $serviceToOrderIdMapping[$service];
        $coreResource = Mage::getSingleton('core/resource');
        $reader = $coreResource->getConnection('core_read');
        $tableName = $coreResource->getTableName('rewardpoints/rewardpoints_account');
        $sql = "SELECT COUNT(*) FROM {$tableName}
                WHERE customer_id = {$customerId}
                AND store_id = {$storeId}
                AND order_id = {$orderId}
                AND sharing_at_time <= {$endOfDayTimestamp}
                AND sharing_at_time >= {$beginOfDayTimestamp}
                ";

        return intval($reader->fetchOne($sql));
    }

    /**
     * Return true if customer still able to share and get reward points, otherwise.
     *
     * @param $service
     * @param $customerId
     * @param $storeId
     *
     * @return bool
     */
    public function isStillAbleToShare($customerId, $service, $storeId)
    {
        $timeOfShared = $this->getTimesOfShared($service, $customerId, $storeId);
        // Service invalid
        if ($timeOfShared === false) {
            return false;
        }

        $maxTimesOfSharingPerday = intval($this->getMaxTimeOfSharingPerday());

        // If time of shared less than max times of sharing perday, customer still able to share and get reward points.
        return $timeOfShared < $maxTimesOfSharingPerday ? true : false;
    }

    /**
     * Get final reward points configuration per product and service.
     *
     * @param $productId
     * @param $service
     *
     * @return mixed|null
     */
    public function getSharingRewardPoints($productId, $service)
    {
        $productRewardPointsConfiguration = $this->getConfigRewardPointsPerProduct($productId, $service);
        $rewardPointsGlobalConfiguration = $this->getConfigRewardPointsGlobal($service);

        return $productRewardPointsConfiguration ? $productRewardPointsConfiguration : $rewardPointsGlobalConfiguration;
    }
}