<?php

/**
 * Add sharing time into table 'rewardpoints_account'.
 * This field using to limit max share per day per customer.
 */

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$tableName = $this->getTable('rewardpoints/rewardpoints_account');
$connection = $installer->getConnection();
$connection->addColumn(
           $tableName, 'sharing_at_time', array(
                   'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
                   'nullable' => true,
                   'default'  => null,
                   'comment'  => 'Sharing time',
               )
);

$connection->addIndex($tableName, 'sharing_at_time', 'sharing_at_time', Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX);

$installer->endSetup();