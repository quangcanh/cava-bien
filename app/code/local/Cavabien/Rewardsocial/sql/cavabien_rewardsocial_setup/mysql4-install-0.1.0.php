<?php

/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$attributes = array(
    'email_share_reward_points'     => 'Email Share Reward Points',
    'facebook_share_reward_points'  => 'Facebook Share Reward Points',
    'twitter_share_reward_points'   => 'Twitter Share Reward Points',
    'pinterest_share_reward_points' => 'Pinterest Share Reward Points',
);

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeGroup = Mage::getResourceModel('eav/entity_attribute_set_collection')
                      ->addFieldToFilter('attribute_set_name', array('eq' => 'Default'))
                      ->addFieldToFilter('entity_type_id', array('eq' => $entityTypeId))
                      ->getFirstItem();

if (!$attributeGroup->getAttributeSetId()) {
    $installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Default');
    $attributeGroup = Mage::getResourceModel('eav/entity_attribute_set_collection')
                          ->addFieldToFilter('attribute_set_name', array('eq' => 'Default'))
                          ->addFieldToFilter('entity_type_id', array('eq' => $entityTypeId))
                          ->getFirstItem();
}

$installer->startSetup();

// Get default attributeSetId
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);

// New attribute group name
$attributeGroupName = 'Cavabien Reward Social';

// Adding new attribute group 'Cavabien Reward Social'
$installer->addAttributeGroup($entityTypeId, $attributeSetId, $attributeGroupName, 31);
$cavabienRewardSocialAttributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $attributeGroupName);

foreach ($attributes as $attrCode => $attrName) {
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                         ->setCodeFilter($attrCode)
                         ->getFirstItem();

    if (!$attributeInfo->getAttributeId() && $attributeGroup->getAttributeSetId()) {
        $attrData = array(
            'group'                      => $attributeGroupName,
            'type'                       => 'int',
            'input'                      => 'text',
            'label'                      => $attrName,
            'backend'                    => '',
            'default'                    => '0',
            'visible'                    => 1,
            'required'                   => 0,
            'user_defined'               => 0,
            'searchable'                 => 0,
            'filterable'                 => 0,
            'comparable'                 => 0,
            'visible_on_front'           => 0,
            'visible_in_advanced_search' => 0,
            'is_html_allowed_on_front'   => 0,
            'used_in_product_listing'    => false,
            'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        );

        // Add new attribute
        $installer->addAttribute($entityTypeId, $attrCode, $attrData);

        // Get attributeId
        $attrId = $installer->getAttributeId($entityTypeId, $attrCode);

        // Add each attribute to group
        $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $cavabienRewardSocialAttributeGroupId, $attrId);
    }
}

$installer->endSetup();