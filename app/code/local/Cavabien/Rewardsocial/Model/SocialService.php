<?php

/**
 * Class Cavabien_Rewardsocial_Model_SocialService
 */
class Cavabien_Rewardsocial_Model_SocialService
{
    /**
     * Facebook service
     */
    const SERVICE_FACEBOOK = 'facebook';
    /**
     * Twitter service
     */
    const SERVICE_TWITTER = 'twitter';
    /**
     * Email service
     */
    const SERVICE_EMAIL = 'email';
    /**
     * Pinterest service
     */
    const SERVICE_PINTEREST = 'pinterest';

    /**
     * Return array mapping service => order_id (column in table 'rewardpoints_account')
     */
    public static function getMappingServiceOrderId()
    {
        return array(
            self::SERVICE_EMAIL     => Rewardpoints_Model_Stats::TYPE_POINTS_GP,
            self::SERVICE_FACEBOOK  => Rewardpoints_Model_Stats::TYPE_POINTS_FB,
            self::SERVICE_PINTEREST => Rewardpoints_Model_Stats::TYPE_POINTS_TT,
            self::SERVICE_PINTEREST => Rewardpoints_Model_Stats::TYPE_POINTS_PIN,
        );
    }
}