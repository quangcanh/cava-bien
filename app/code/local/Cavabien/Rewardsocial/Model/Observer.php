<?php

/**
 * Class Cavabien_Rewardsocial_Model_Observer
 */
class Cavabien_Rewardsocial_Model_Observer
{
    /**
     * Shortcut to get Request
     */
    public function _getRequest()
    {
        return Mage::app()->getRequest();
    }
}