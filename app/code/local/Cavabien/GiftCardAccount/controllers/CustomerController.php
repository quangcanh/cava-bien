<?php
require_once'Enterprise'.DS.'GiftCardAccount'.DS.'controllers'.DS.'CustomerController.php';
class Cavabien_GiftCardAccount_CustomerController extends Enterprise_GiftCardAccount_CustomerController
{
    /**
     * Redeem gift card
     *
     */
    public function indexAction()
    {

        $data = $this->getRequest()->getPost();
        if (isset($data['giftcard_code'])) {
            $code = $data['giftcard_code'];
            try {
                if (!Mage::helper('enterprise_customerbalance')->isEnabled()) {
                    Mage::throwException($this->__('Redemption functionality is disabled.'));
                }
                Mage::getModel('enterprise_giftcardaccount/giftcardaccount')->loadByCode($code)
                    ->setIsRedeemed(true)->redeem();
                Mage::getSingleton('customer/session')->addSuccess(
                    $this->__('Gift Card "%s" was redeemed.', Mage::helper('core')->escapeHtml($code))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addException($e, $this->__('Cannot redeem Gift Card.'));
            }
            $this->_redirect('storecredit/info/index/');
            return;
        }
//        $this->loadLayout();
//        $this->_initLayoutMessages('customer/session');
//        $this->loadLayoutUpdates();
//        $headBlock = $this->getLayout()->getBlock('head');
//        if ($headBlock) {
//            $headBlock->setTitle($this->__('Gift Card'));
//        }
//        $this->renderLayout();
    }
}
