<?php
class Cavabien_Catalogrequest_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
    * Checkout email record exist.
    * @param $email
    * @return bool
    */
    public function isEmailExist($email)
    {
         $result = Mage::getModel('cavabien_catalogrequest/catalogrequest')->getCollection()
                 ->addFieldToFilter('email',array('eq'=>$email))
                 ->getFirstItem();
         return $result->getId() ? true : false;
    }
}