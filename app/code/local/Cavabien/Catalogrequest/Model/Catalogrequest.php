<?php

class Cavabien_Catalogrequest_Model_Catalogrequest extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cavabien_catalogrequest/catalogrequest');
    }
    
    public function addCountry()
    {
        $this->full_country = Zend_Locale::getTranslation($this->country, 'Country');
    }

}