<?php

class Cavabien_Catalogrequest_Model_Resource_Catalogrequest extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {    
        // Note that the catalogrequest_id refers to the key field in your database table.
        $this->_init('cavabien_catalogrequest/catalogrequest', 'catalogrequest_id');
    }
}