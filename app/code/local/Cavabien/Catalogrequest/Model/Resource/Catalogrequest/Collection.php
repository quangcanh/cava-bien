<?php

class Cavabien_Catalogrequest_Model_Resource_Catalogrequest_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cavabien_catalogrequest/catalogrequest', 'cavabien_catalogrequest/catalogrequest' );
    }
}