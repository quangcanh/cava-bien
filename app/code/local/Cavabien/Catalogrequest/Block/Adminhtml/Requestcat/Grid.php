<?php
class Cavabien_Catalogrequest_Block_Adminhtml_Requestcat_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel(
            'cavabien_catalogrequest/catalogrequest_collection'
        );
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'cavabien_catalogrequest_admin/catalogrequest/edit', 
            array(
                'id' => $row->getId()
            )
        );
    }

    protected function _prepareColumns()
  {
      $this->addColumn('catalogrequest_id', array(
          'header'    => Mage::helper('cavabien_catalogrequest')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'catalogrequest_id',
      ));

      $this->addColumn('fname', array(
          'header'    => Mage::helper('cavabien_catalogrequest')->__('First Name'),
          'align'     =>'left',
          'index'     => 'fname',
      ));
      
      $this->addColumn('suffix', array(
          'header'    => Mage::helper('cavabien_catalogrequest')->__('Suffix'),
          'align'     =>'left',
          'index'     => 'suffix',
      ));

      $this->addColumn('lname', array(
          'header'    => Mage::helper('cavabien_catalogrequest')->__('Last Name'),
          'align'     =>'left',
          'index'     => 'lname',
      ));
      
      $this->addColumn('email', array(
          'header'    => Mage::helper('cavabien_catalogrequest')->__('Email'),
          'align'     =>'left',
          'index'     => 'email',
      ));
      
      $this->addColumn('time_added', array(
        'header'    => Mage::helper('cavabien_catalogrequest')->__('Created At'),
        'align'     => 'left',
        'index'     => 'time_added',
        'type'      => 'datetime',
        'width'     => '160px',
      ));
         
      $this->addColumn('edit',
                    array(
                          'header' => Mage::helper('cavabien_catalogrequest')->__('Edit'),
                          'width' => '50',
                          'type' => 'action',
                          'getter' => 'getId',
                          'actions' => array(
                                 array(
                                      'caption' => Mage::helper('cavabien_catalogrequest')->__('Edit'),
                                      'url' => array('base'=> '*/*/edit'),
                                      'field' => 'id'
                                    )),
                          'filter' => false,
                          'sortable' => false,
                          'index' => 'stores',
                          'is_system' => true,
                    ));

          $this->addColumn('delete',
                    array(
                          'header' => Mage::helper('cavabien_catalogrequest')->__('Delete'),
                          'width' => '50',
                          'type' => 'action',
                          'getter' => 'getId',
                          'actions' => array(
                                 array(
                                      'caption' => Mage::helper('cavabien_catalogrequest')->__('Delete'),
                                      'url' => array('base'=> '*/*/delete'),
                                      'confirm' => Mage::helper('cavabien_catalogrequest')->__('Are you sure?'),
                                      'field' => 'id'
                                    )),
                          'filter' => false,
                          'sortable' => false,
                          'index' => 'stores',
                          'is_system' => true,
                    ));
		
	$this->addExportType('*/*/exportCsv', Mage::helper('cavabien_catalogrequest')->__('CSV'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('catalogreqest_id');
        $this->getMassactionBlock()->setFormFieldName('catalogreqest_id');//tra ve action
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('cavabien_catalogrequest')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),     
            'confirm' => Mage::helper('cavabien_catalogrequest')->__('Are you sure delete multi request?')
        ));
        return $this;
    }
    
    protected function _getHelper()
    {
        return Mage::helper('cavabien_catalogrequest');
    }
}