<?php
class Cavabien_Catalogrequest_Block_Adminhtml_Requestcat_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if (Mage::registry('current_request')) {
            $data = Mage::registry('current_request')->getData();
            //$catalogrequest_id = $data['catalogrequest_id'][0];
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/edit', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post'
        ));
  
        $form->setUseContainer(true);
 
        $this->setForm($form);

        $fieldset = $form->addFieldset('request_form', array(
            'legend' => Mage::helper('cavabien_catalogrequest')->__('Request Information')
        ));
        
        $fieldset->addField('fname', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('First Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'fname',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is first name'),
        ));
        
         $fieldset->addField('suffix', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Suffix'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'suffix',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is Suffix'),
        ));
         
        $fieldset->addField('lname', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Last Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'lname',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is last name'),
        ));
          
        $fieldset->addField('company_name', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Company Name'),
//            'class' => 'required-entry',
//            'required' => true,
            'name' => 'company_name',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is company name'),
        ));
        
        $fieldset->addField('address1', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Address 1'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'address1',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is address 1'),
        ));
        
        $fieldset->addField('address2', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Address 2'),
//            'class' => 'required-entry',
//            'required' => true,
            'name' => 'address2',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is address 2'),
        ));
        
        $fieldset->addField('city', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('City'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'city',
            'note' => Mage::helper('cavabien_catalogrequest')->__('City'),
        ));
        
        $fieldset->addField('county', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('County'),
//            'class' => 'required-entry',
//            'required' => true,
            'name' => 'county',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is county'),
        ));
          
        $fieldset->addField('zip', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Zip Code'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'zip',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is zip code'),
        ));
            
        $fieldset->addField('country', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Country'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'country',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is country'),
        ));
        
        $fieldset->addField('email', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Email'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'email',
            'note' => Mage::helper('cavabien_catalogrequest')->__('Email'),
        ));
          
        $fieldset->addField('phone', 'text', array(
            'label' => Mage::helper('cavabien_catalogrequest')->__('Phone'),
//            'class' => 'required-entry',
//            'required' => true,
            'name' => 'phone',
            'note' => Mage::helper('cavabien_catalogrequest')->__('This is phone'),
        ));
   
 
        $form->setValues($data);
 
        return parent::_prepareForm();
    }    
}	