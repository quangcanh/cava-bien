<?php
class Cavabien_Catalogrequest_Block_Adminhtml_Requestcat_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'cavabien_catalogrequest';
        $this->_controller = 'adminhtml_requestcat';
        $this->_mode = 'edit';
        
        $newOrEdit = $this->getRequest()->getParam('id')
            ? $this->__('Edit') 
            : $this->__('Add new');
        $this->_headerText =  $newOrEdit . ' ' . $this->__('request from customer');
    }
}