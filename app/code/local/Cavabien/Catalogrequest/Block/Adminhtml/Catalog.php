<?php
class Cavabien_Catalogrequest_Block_Adminhtml_Catalog extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        parent::_construct();
        $this->_blockGroup = 'cavabien_catalogrequest'; 
        $this->_controller = 'adminhtml_requestcat';
        $this->_headerText = Mage::helper('cavabien_catalogrequest')
            ->__('request Directory');
    }
    
    public function getCreateUrl()
    {
        return $this->getUrl(
            'cavabien_catalogrequest_admin/catalogrequest/edit'
        );
    }
}