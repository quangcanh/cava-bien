<?php
class Cavabien_Catalogrequest_Block_Formrequest extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
         return parent::_prepareLayout();
    }
 
    public function getFormrequest()  
    {
        if (!$this->hasData('customrequest')) {
            $this->setData('customrequest', Mage::registry('customrequest'));
        }
        return $this->getData('customrequest');
     
    }
}
