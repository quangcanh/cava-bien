<?php 
class Cavabien_Catalogrequest_Adminhtml_CatalogrequestController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
      $catalogBlock = $this->getLayout()->createBlock('cavabien_catalogrequest/adminhtml_catalog'); 
      $this->loadLayout()->_addContent($catalogBlock)->renderLayout();       
    }
    
    public function editAction()
    {
        $catRequest = Mage::getModel('cavabien_catalogrequest/catalogrequest');
        if ($requestId = $this->getRequest()->getParam('id', false)) {
            $catRequest->load($requestId);
        
            if ($catRequest->getId() < 1) {
                $this->_getSession()->addError(
                    $this->__('This request no longer exists.')
                );
                return $this->_redirect(
                    'cavabien_catalogrequest_admin/catalogrequest/index'
                );
            }
        }
        
        if ($postData = $this->getRequest()->getPost()) {

            try {
                 $catRequest->addData($postData);
                 $catRequest->save();
                
                $this->_getSession()->addSuccess(
                    $this->__('The request has been saved.')
                );
                
                 return $this->_redirect(
                    'cavabien_catalogrequest_admin/catalogrequest/index'
                );
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
            
        }
        
        
        Mage::register('current_request', $catRequest);
        
        $requestEditBlock = $this->getLayout()->createBlock(
            'cavabien_catalogrequest/adminhtml_requestcat_edit'
        );

        // add the form container as the only item on this page
        $this->loadLayout()->_addContent($requestEditBlock)->renderLayout();
    }
    
     public function deleteAction()
    {
        $catRequest = Mage::getModel('cavabien_catalogrequest/catalogrequest');

        if ($requestId = $this->getRequest()->getParam('id', false)) {
            $catRequest->load($requestId)->delete();
        }
        
        if ($catRequest->getId() < 1) {
            $this->_getSession()->addError(
                $this->__('This request no longer exists.')
            );
            return $this->_redirect(
                'cavabien_catalogrequest_admin/catalogrequest/index'
            );
        }

        try {   
               
            $this->_getSession()->addSuccess(
                $this->__('The request has been deleted.')
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect(
            'cavabien_catalogrequest_admin/catalogrequest/index'
        );
    }
    
    public function massDeleteAction()
    {
        $requestIds = $this->getRequest()->getParam('catalogreqest_id');       // Nhận từ setFormFieldName('catalogreqest_id') của _prepareMassaction()
        if(!is_array($requestIds)) {
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cavabien_catalogrequest')->__('Please select request.'));
        } else {
        try {
                $catRequest = Mage::getModel('cavabien_catalogrequest/catalogrequest');
                foreach ($requestIds as $requestId) {
                $catRequest->load($requestId)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('cavabien_catalogrequest')->__(
                'Total of %d record(s) were deleted.', count($requestIds)
                   )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
     public function exportCsvAction()
    {
        $fileName   = 'catalogrequest.csv';
        $catalogrequests = Mage::getSingleton('cavabien_catalogrequest/catalogrequest')->getCollection();
        $catalogrequests->getSelect()->where('status = ?',1);
        foreach($catalogrequests as $catalogrequest){
            $catalogrequest->setStatus(1)
                           ->addCountry();
        }
        $catalogrequests->save();
        $data = $catalogrequests->toArray();
        // print_r($data);
        $output = "";
        foreach($data['items'] as $line){
            $row = implode('","', $line);
            $output .= '"' . $row . '"' . "\015\012";
        }
        $this->_sendUploadResponse($fileName, $output);
    }
    
//    public function exportXmlAction()
//    {
//        $fileName   = 'catalogrequest.xml';
//        $content    = $this->getLayout()->createBlock('sm_catalogrequest/adminhtml_catalogrequest_grid')
//            ->getXml();
//
//        $this->_sendUploadResponse($fileName, $content);
//    }
    
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

}    