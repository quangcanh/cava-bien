<?php 
class Cavabien_Catalogrequest_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if($this->getRequest()->isPost()){
            $request = Mage::getModel('cavabien_catalogrequest/catalogrequest');
            $data = $this->getRequest()->getPost();
            $data['time_added'] = now();
            $data['country'] = $data['country_id'];
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $data['fname'] = $data['fname'];
            $data['suffix'] = $data['suffix'];
            $data['lname'] = $data['lname'];
            $data['company_name'] = $data['company_name'];
            $data['address1'] = str_replace(",", " ",$data['address1']);
            $data['address2'] = str_replace(",", " ",$data['address2']);
            $data['city'] = $data['city'];
            $data['zip'] = $data['zip'];
            if(isset($data['is_business_address'])){
                $data['is_business_address'] = $data['is_business_address'];
            }else{
                $data['is_business_address'] = 0;
            }
            $data['email'] = $data['email'];
            $data['phone'] = $data['phone'];
           
            $email = $data['email'];
             
            $helper = Mage::helper('cavabien_catalogrequest');
            $result = $helper->isEmailExist($email);
            if($result == 1)
            {
                MAGE::getSingleton('core/session')->addError($this->__('Sorry, this email already exist'));
                Mage::register('customrequest_data',$data);
                $this->loadLayout();   
                $this->getLayout()->getBlock('formrequest')->setFormAction( Mage::getUrl('*/*/') );
                $this->getLayout()->getBlock('head')->setTitle('Catalog Request');
                $this->renderLayout();
            }
             else 
            {
                  try {
                        $request->setData($data)->save();
                        $this->_redirect('*/*/success');
                        return;
                        die;
                    } catch(Exception $e){
                        MAGE::getSingleton('core/session')->addError($this->__('Sorry, we\'ve had some trouble saving your request, please call and request a catalog 
        (800-222-2222) or email (service@example.com) to request a catalog'));
                        $this->_redirect('*/*/');
                        return;
                    }
                   }
            }
        //return;
        $this->loadLayout();   
        $this->getLayout()->getBlock('formrequest')->setFormAction( Mage::getUrl('*/*/') );
        $this->getLayout()->getBlock('head')->setTitle('Catalog Request');
        $this->renderLayout();
    }
    
    
    public function successAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle('Catalog Request');
        $this->renderLayout();
    }
    
}