<?php

class Cavabien_Notify_Block_Adminhtml_Reviewnotify extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_controller = 'adminhtml_reviewnotify';
        $this->_blockGroup = 'cavabien_notify';
        $this->_headerText = Mage::helper('cavabien_notify')->__('Review Notify Manager');
        $this->_addButtonLabel = Mage::helper('cavabien_notify')->__('Add News');
        parent::__construct();
        $this->_removeButton('add');
    }
}