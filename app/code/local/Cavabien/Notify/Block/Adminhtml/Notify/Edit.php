<?php

class Cavabien_Measurements_Block_Adminhtml_Measurements_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'cavabien_measurements';
        $this->_controller = 'adminhtml_measurements';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', Mage::helper('cavabien_measurements')->__('Save Measurements'));
        $this->_updateButton('delete', 'label', Mage::helper('cavabien_measurements')->__('Delete'));
        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('cavabien_measurements')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    /*
     * This function is responsible for Including TincyMCE in Head.
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
    }


    public function getHeaderText() {
        if (Mage::registry('measurements_data') && Mage::registry('measurements_data')->getMeasurementId()) {
            return Mage::helper('cavabien_measurements')->__('Edit Measurements "%s"', $this->htmlEscape(Mage::registry('measurements_data')->getMeasurementSubject()));
        } else {
            return Mage::helper('cavabien_measurements')->__('New Measurements');
        }
    }
}