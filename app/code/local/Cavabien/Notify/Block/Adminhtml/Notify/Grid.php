<?php

class Cavabien_Notify_Block_Adminhtml_Notify_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('notify_id');
        $this->setDefaultSort('notify_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('cavabien_notify/notify')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('notify_id', array(
            'header' => Mage::helper('cavabien_notify')->__('ID'),
            'align' => 'right',
            'width' => '10px',
            'index' => 'notify_id',
        ));

        $this->addColumn('product_id', array(
            'header' => Mage::helper('cavabien_notify')->__('Product ID'),
            'align' => 'left',
            'index' => 'product_id',
            'width' => '50px',
        ));


        $this->addColumn('product_option', array(
            'header' => Mage::helper('cavabien_notify')->__('Product Options'),
            'width' => '150px',
            'index' => 'product_option',
        ));

        $this->addColumn('product_qty', array(
            'header' => Mage::helper('cavabien_notify')->__('Product Qty'),
            'width' => '150px',
            'index' => 'product_qty',
        ));


        $this->addColumn('customer_name', array(
            'header' => Mage::helper('cavabien_notify')->__('Customer Name'),
            'width' => '150px',
            'index' => 'customer_name',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('cavabien_notify')->__('Customer Email'),
            'width' => '150px',
            'index' => 'customer_email',
        ));

        return parent::_prepareColumns();
    }


//    public function getRowUrl($row) {
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
//    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('notify_id');
        $this->getMassactionBlock()->setFormFieldName('notify_id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));
        return $this;
    }
}