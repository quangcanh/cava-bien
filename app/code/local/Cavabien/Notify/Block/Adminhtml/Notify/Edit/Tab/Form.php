<?php

class Cavabien_Measurements_Block_Adminhtml_Measurements_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {

        if (Mage::registry('measurements_data')) {
            $data = Mage::registry('measurements_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('measurements_measurements', array('legend' => Mage::helper('cavabien_measurements')->__('measurements information')));

        $fieldset->addField('measurement_subject', 'text', array(
            'label' => Mage::helper('cavabien_measurements')->__('Measurements Subject'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'measurement_subject',
        ));

        $fieldset->addField('measurement_inquiry', 'textarea', array(
            'label' => Mage::helper('cavabien_measurements')->__('Measurements Inquiry'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'measurement_inquiry',
        ));

        $fieldset->addField('measurement_file', 'link', array(
            'label'     => Mage::helper('cavabien_measurements')->__('File attach'),
            'style'   => "",
            'href' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'measurements/'.$data['measurement_file'],
            'value'  => 'Magento Blog',
            'after_element_html' => ''
        ));

        $form->setValues($data);

        return parent::_prepareForm();
    }
}