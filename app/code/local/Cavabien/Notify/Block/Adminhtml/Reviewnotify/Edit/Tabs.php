<?php

class Cavabien_Measurements_Block_Adminhtml_Measurements_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct() {
        parent::__construct();
        $this->setId('measurements_tabs');
        $this->setDestElementId('edit_form'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('cavabien_measurements')->__('Measurements Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('cavabien_measurements')->__('Measurements Information'),
            'title' => Mage::helper('cavabien_measurements')->__('Measurements Information'),
            'content' => $this->getLayout()->createBlock('cavabien_measurements/adminhtml_measurements_edit_tab_form')->toHtml(),
        ));

//        $this->addTab('form_section1', array(
//            'label' => Mage::helper('cavabien_measurements')->__('Content'),
//            'title' => Mage::helper('cavabien_measurements')->__('Content'),
//            'content' => $this->getLayout()->createBlock('cavabien_measurements/adminhtml_measurements_edit_tab_content')->toHtml(),
//        ));

        return parent::_beforeToHtml();
    }
}