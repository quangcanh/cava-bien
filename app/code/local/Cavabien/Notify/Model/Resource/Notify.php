<?php

class Cavabien_Notify_Model_Resource_Notify extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_notify/notify', 'notify_id');
    }
}