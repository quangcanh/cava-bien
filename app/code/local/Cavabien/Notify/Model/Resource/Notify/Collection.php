<?php

/**
 * Class Cavabien_Notify_Model_Resource_Notify_Collection
 */
class Cavabien_Notify_Model_Resource_Notify_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_notify/notify');
    }
}