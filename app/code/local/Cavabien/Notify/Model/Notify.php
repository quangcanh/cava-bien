<?php

class Cavabien_Notify_Model_Notify extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('cavabien_notify/notify');
    }
}