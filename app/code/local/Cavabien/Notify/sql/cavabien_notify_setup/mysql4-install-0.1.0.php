<?php
/**
 * Created by JetBrains PhpStorm.
 * User: LongPD
 * Date: 7/1/14
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_notify/notify')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_notify/notify')} (
      `notify_id` int(10) unsigned NOT NULL auto_increment,
      `product_id` int(10) unsigned NOT NULL,
      `product_option` varchar(255),
      `product_qty` int(10),
      `customer_email` varchar(255),
      `customer_name` varchar(255),
      PRIMARY KEY  (`notify_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->endSetup();