<?php

class Cavabien_Notify_IndexController extends Mage_Core_Controller_Front_Action
{
    public function notifymeAction(){
        $post = $this->getRequest()->getPost();
        $url = $post['product_url'];
        if ($post) {
            if (isset($post['super_attribute'])) {
                $options = $post['super_attribute'];
            } else {
                $fapData = Mage::helper('core')->jsonDecode($this->getRequest()->getParam('sm_notify_super_attribute'));
                $options = $fapData[0]['super_attribute'];
            }
            $productId = $post['productId'];
            $qty = $post['qty'];
            $cusName = $post['cus-name'];
            $cusEmail = $post['cus-email'];

            $opt = '';
            foreach($options as $k=>$v){
                $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $k);
                $frontLabel = $attribute->getFrontendLabel();
                $optionText = $attribute->getSource()->getOptionText($v);
                $opt.=  $frontLabel.':'.$optionText.'. ';
            }
            $notifyModel = Mage::getModel('cavabien_notify/notify');
            $notifyData = array(
                'product_id'    => $productId,
                'product_option'   => $opt,
                'product_qty' => $qty,
                'customer_email' => $cusEmail,
                'customer_name' => $cusName,
            );
            $notifyModel->setData($notifyData)->save();
            $this->_redirectUrl($url);
            Mage::getSingleton('core/session')->addSuccess('Your notify has been sent. We will email you on item arrival.');
        }else{
            $this->_redirectUrl($url);
            Mage::getSingleton('core/session')->addError('Error. Please try again.');
        }

    }

    public function reviewNotifyAction(){
        $post = $this->getRequest()->getPost();
        $url = $post['product_url'];
        if ($post) {
            $productId = $post['productId'];
            $cusName = $post['cus-name'];
            $cusEmail = $post['cus-email'];

            $notifyModel = Mage::getModel('cavabien_notify/reviewnotify');
            $notifyData = array(
                'product_id'    => $productId,
                'customer_email' => $cusEmail,
                'customer_name' => $cusName,
            );
            $notifyModel->setData($notifyData)->save();
            $this->_redirectUrl($url);
            Mage::getSingleton('core/session')->addSuccess('Your notify has been sent. We will email you on item arrival.');
        }else{
            $this->_redirectUrl($url);
            Mage::getSingleton('core/session')->addError('Error. Please try again.');
        }

    }

    public function ajaxSelectOptionAction(){
        $post = $this->getRequest()->getPost();
        //$url = $post['product_url'];
        if ($post) {
            $productId = $post['productId'];
            $attributeId = $post['attributeId'];
            $optionId = $post['productOption'];
            $optionColorId = $post['optColor'];
            $optionSizeId = $post['optSize'];
            $_product = Mage::getModel('catalog/product')->load($productId);
            if($_product->getTypeId() == "configurable"){
                $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($_product);
                $simple_collection = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
                $stockColor = "outstock";
                $stockSize = "outstock";
                foreach($simple_collection as $simple_product){
                    if($attributeId==92){
                        $stockSize = "instock";
                        if($optionSizeId!=''){
                            if($simple_product->getColor()==$optionId &&$simple_product->getSize()==$optionSizeId){
                                $result['priceNew'] = $this->getLayout()->createBlock("catalog/product_view")->setProductLct($simple_product)->setTemplate('catalog/product/priceselectoption.phtml')->toHtml();
                                $result['totalCost'] = $this->getLayout()->createBlock("catalog/product_view")->setProductLct($simple_product)->setTemplate('catalog/product/totalcost.phtml')->toHtml();
                                if($inStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getIsInStock()){
                                    $stockColor = "instock";
                                }
                            }
                        }else{
                            if($simple_product->getColor()==$optionId && $inStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getIsInStock()){
                                $stockColor = "instock";
                            }
                        }
                        if($simple_product->getColor()==$optionId){
                            $subProduct = Mage::getModel('catalog/product')->load($simple_product->getId());
                        }
                    }elseif($attributeId==194){
                        $stockColor = "instock";
                        if($optionColorId!=''){
                            if($simple_product->getSize()==$optionId &&$simple_product->getColor()==$optionColorId){
                                $result['priceNew'] = $this->getLayout()->createBlock("catalog/product_view")->setProductLct($simple_product)->setTemplate('catalog/product/priceselectoption.phtml')->toHtml();
                                $result['totalCost'] = $this->getLayout()->createBlock("catalog/product_view")->setProductLct($simple_product)->setTemplate('catalog/product/totalcost.phtml')->toHtml();
                                if($inStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getIsInStock()){
                                    $stockSize = "instock";
                                }
                            }
                        }else{
                            if($simple_product->getSize()==$optionId && $inStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getIsInStock()){
                                $stockSize = "instock";
                            }
                        }
                        $subProduct = '';
                    }

                }
            }
            if($subProduct!=''){
                $media = $this->getLayout()->createBlock('amconf/catalog_product_view_media','media-sub')->setTemplate('catalog/product/view/media.phtml')->setProduct($subProduct)->toHtml();
                $result['mediasub']= $media;

            }

            $result['stockColor']= $stockColor;
            $result['stockSize']= $stockSize;
            $response = $this->getResponse();
            $response->setHeader('Content-type', 'application/json');
            $response->setBody(Mage::helper('core')->jsonEncode($result));
//            $this->_redirectUrl($url);
//            Mage::getSingleton('core/session')->addSuccess('Your notify has been sent. We will email you on item arrival.');
        }else{
//            $this->_redirectUrl($url);
//            Mage::getSingleton('core/session')->addError('Error. Please try again.');
        }

    }
}