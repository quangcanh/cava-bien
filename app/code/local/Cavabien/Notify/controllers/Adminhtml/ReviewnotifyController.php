<?php

class Cavabien_Notify_Adminhtml_ReviewnotifyController extends Mage_Adminhtml_Controller_action
{
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction(){
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('measurements/adminhtml_measurements_edit'))
            ->_addLeft($this->getLayout()->createBlock('measurements/adminhtml_measurements_edit_tabs'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $notifyIds = $this->getRequest()->getParam('reviewnotify_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
        if(!is_array($notifyIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cavabien_notify')->__('Please select review notify.'));
        } else {
            try {
                $notifyModel = Mage::getModel('cavabien_notify/reviewnotify');
                foreach ($notifyIds as $notifyId) {
                    $notifyModel->load($notifyId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('tax')->__(
                        'Total of %d record(s) were deleted.', count($notifyIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('cavabien_notify/reviewnotify');

                $model->setReviewnotifyId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function saveAction() {

        if ($data = $this->getRequest()->getPost())
        {
            $model = Mage::getModel('cavabien_notify/reviewnotify');
            $id = $this->getRequest()->getParam('id');
            foreach ($data as $key => $value)
            {
                if (is_array($value))
                {
                    $data[$key] = implode(',',$this->getRequest()->getParam($key));
                }
            }

            if ($id) {
                $model->load($id);
            }
            $model->setData($data);

            Mage::getSingleton('adminhtml/session')->setFormData($data);
            try {
                if ($id) {
                    $model->setId($id);
                }

                $model->save();

                if (!$model->getMeasurementId()) {
                    Mage::throwException(Mage::helper('cavabien_notify')->__('Error saving news details'));
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cavabien_notify')->__('Details was successfully saved.'));

                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // The following line decides if it is a "save" or "save and continue"
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/');
                }

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                if ($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/');
                }
            }

            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cavabien_notify')->__('No data found to save'));
        $this->_redirect('*/*/');
    }


    public function editAction() {

        $id = $this->getRequest()->getParam('id', null);

        $model = Mage::getModel('cavabien_notify/reviewnotify');
        if ($id) {
            $model->load((int) $id);
            if ($model->getId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $model->setData($data)->setId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cavabien_notify')->__('Review notify does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('reviewnotify_data', $model);

        $this->_title($this->__('Review Notify'))->_title($this->__('Edit Review Notify'));
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('cavabien_measurements/adminhtml_measurements_edit'))
            ->_addLeft($this->getLayout()->createBlock('cavabien_measurements/adminhtml_measurements_edit_tabs'));
        $this->renderLayout();
    }
}