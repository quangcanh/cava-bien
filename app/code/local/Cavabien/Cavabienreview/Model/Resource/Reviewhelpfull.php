<?php

class Cavabien_Cavabienreview_Model_Resource_Reviewhelpfull extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('cavabien_cavabienreview/reviewhelpfull', 'reviewhelpfull_id');
    }
}