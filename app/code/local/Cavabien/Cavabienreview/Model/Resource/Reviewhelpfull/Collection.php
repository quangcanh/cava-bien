<?php

/**
 * Class Cavabien_Cavabienreview_Model_Resource_Reviewhelpfull_Collection
 */
class Cavabien_Cavabienreview_Model_Resource_Reviewhelpfull_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cavabien_cavabienreview/reviewhelpfull');
    }
}