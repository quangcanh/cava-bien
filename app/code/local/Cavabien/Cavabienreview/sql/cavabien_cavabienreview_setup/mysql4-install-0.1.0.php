<?php
/**
 * Created by JetBrains PhpStorm.
 * User: LongPD
 * Date: 8/4/14
 * Time: 9:52 AM
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run(
    "
DROP TABLE IF EXISTS {$this->getTable('cavabien_cavabienreview/reviewhelpfull')};
CREATE TABLE IF NOT EXISTS {$this->getTable('cavabien_cavabienreview/reviewhelpfull')} (
      `reviewhelpfull_id` int(10) unsigned NOT NULL auto_increment,
      `review_id` int(10),
      `customer_id` int(10),
      `review_status` varchar(255),
      `is_new_visitor` varchar(255),
      PRIMARY KEY  (`reviewhelpfull_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->endSetup();