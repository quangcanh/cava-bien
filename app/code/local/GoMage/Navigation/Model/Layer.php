<?php

/**
 * GoMage Advanced Navigation Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2010-2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use
 * @version      Release: 4.2
 * @since        Class available since Release 1.0
 */
class GoMage_Navigation_Model_Layer extends Mage_Catalog_Model_Layer
{

    const FILTER_TYPE_DEFAULT = 0;
    const FILTER_TYPE_IMAGE = 1;
    const FILTER_TYPE_DROPDOWN = 2;
    const FILTER_TYPE_INPUT = 3;
    const FILTER_TYPE_SLIDER = 4;
    const FILTER_TYPE_SLIDER_INPUT = 5;
    const FILTER_TYPE_PLAIN = 6;
    const FILTER_TYPE_FOLDING = 7;
    const FILTER_TYPE_DEFAULT_PRO = 8;
    const FILTER_TYPE_DEFAULT_INBLOCK = 9;
    const FILTER_TYPE_INPUT_SLIDER = 10;
    const FILTER_TYPE_ACCORDION = 11;

    const FILTER_TYPE_SIZE = 12;
    const FILTER_TYPE_BRA_SIZE = 13;

    public function prepareProductCollection($collection)
    {

        parent::prepareProductCollection($collection);

        $collection->getSelect()->group('e.entity_id');

        return $this;

    }

    protected function _prepareAttributeCollection($collection)
    {

        $collection = parent::_prepareAttributeCollection($collection);

        $collection->addIsFilterableFilter();

        $tableAlias = 'gomage_nav_attr';

        $collection->getSelect()->joinLeft(
            array($tableAlias => Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute')),
            "`main_table`.`attribute_id` = `{$tableAlias}`.`attribute_id`",
            array('filter_type',
                'inblock_type',
                'round_to',
                'show_currency',
                'image_align',
                'image_width',
                'image_height',
                'show_minimized',
                'show_image_name',
                'visible_options',
                'show_help',
                'show_checkbox',
                'popup_width',
                'popup_height',
                'filter_reset',
                'is_ajax',
                'inblock_height',
                'max_inblock_height',
                'filter_button',
                'category_ids_filter',
                'range_options',
                'range_manual',
                'range_auto',
                'attribute_location')
        );

        $tableAliasStore = 'gomage_nav_attr_store';

        $collection->getSelect()->joinLeft(
            array($tableAliasStore => Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute_store')),
            "`main_table`.`attribute_id` = `{$tableAliasStore}`.`attribute_id` and `{$tableAliasStore}`.store_id = " . Mage::app()->getStore()->getStoreId(),
            array('popup_text')
        );

        $connection = Mage::getSingleton('core/resource')->getConnection('read');
        $table = Mage::getSingleton('core/resource')->getTableName('gomage_navigation_attribute_option');

        foreach ($collection as $attribute) {

            $option_images = array();

            $attribute_id = $attribute->getId();

            $_option_images = $connection->fetchAll("SELECT `option_id`, `filename` FROM {$table} WHERE `attribute_id` = {$attribute_id};");

            foreach ($_option_images as $imageInfo) {

                $option_images[$imageInfo['option_id']] = $imageInfo['filename'];

            }

            $attribute->setOptionImages($option_images);


        }


        return $collection;
    }

    /**
     * Retrieve current layer product collection
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */

    public function getProductCollection()
    {
        if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
            $params = Mage::app()->getRequest()->getParams();
            $collection = $this->getCurrentCategory()->getProductCollection();
            switch ($params['pro_status']) {
                case SM_Filter_Model_Layer::NEW_ARRIVAL:
                    $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=1', array('qty', 'is_in_stock'));
                    $collection->addAttributeToFilter('news_from_date', array('lteq' => date('Y/m/d')))
                        ->addAttributeToFilter('news_from_date', array('gteq' => date('Y/m/d', strtotime('-7 days'))));
                    /*$timeNow = date(Varien_Date::DATETIME_PHP_FORMAT, Mage::getModel('core/date')->gmtTimestamp());
                    //10080 = 7 ngày
                    $collection->getSelect()->where("
                    TIMESTAMPDIFF(MINUTE, `e`.`created_at`, '" . $timeNow . "') BETWEEN 0 AND 10080
                ");*/
                    break;
                case SM_Filter_Model_Layer::BACK_IN_STOCK:
                    $todayStartOfDayDate = Mage::app()->getLocale()->date()
                        ->setTime('00:00:00')
                        ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                    $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=0', array('qty', 'is_in_stock'));
                    //$collection->addAttributeToFilter('back_in_stock', array('eq' => 1));
                    $collection->addAttributeToFilter('back_to_stock_from', array('gteq' => date('Y/m/d', strtotime('-7 days'))))
                        ->addAttributeToFilter('back_to_stock_from', array('lteq' => date('Y/m/d')));
                    break;
                case SM_Filter_Model_Layer::COMING_SOON:
                    $collection->addAttributeToFilter('coming_soon', 1);
                    $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=0', array('qty', 'is_in_stock'));
                    break;
                case SM_Filter_Model_Layer::TOP_RATED:
                    $minProductRate = Mage::getStoreConfig(SM_Filter_Model_Layer::SM_FILTER_TOP_RATED_PRODUCT_RATING);
                    $collection->getSelect()->join(array('rv' => Mage::getSingleton('core/resource')->getTableName('review/review_aggregate')), '`e`.`entity_id` = `rv`.`entity_pk_value` AND `rv`.`store_id` = ' . Mage::app()->getStore()->getId() . ' AND rv.rating_summary >= ' . $minProductRate, array('rating_summary'));
                    break;
                case SM_Filter_Model_Layer::ALMOST_GONE:

                    $collections = Mage::getModel('catalog/product')->getCollection();
                    $collections->addAttributeToFilter('type_id', 'configurable');
                    $collections->addFieldToFilter('visibility', '4');
                    $modelConfig = Mage::getModel('catalog/product_type_configurable');
                    $idsConfig = array();
                    $minQty = Mage::getStoreConfig(Cavabien_Theme_Helper_Conf_Data::PRODUCT_LEFT);
                    foreach ($collections as $col) {
                        $childProducts = $modelConfig->getUsedProducts(null, $col);
                        if (!empty($childProducts)) {
                            $flag = true;
                            foreach ($childProducts as $child) {
                                if ($child->getStockItem()->getQty() == 0 || $child->getStockItem()->getQty() > $minQty || $child->getStatus() != 1) {
                                    $flag = false;
                                }
                            }
                            if ($flag) {
                                $idsConfig [] = $col->getId();
                            }
                        }
                    }
                    $idsConfig = implode(',', $idsConfig) ? implode(',', $idsConfig) : "''";
                    $collection->getSelect()->join(array('ci' => Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_item')), '`ci`.`product_id` = `e`.`entity_id` AND `ci`.`stock_id`=1 AND `ci`.`is_in_stock`=1 AND (`ci`.`qty` > 0 AND `ci`.`qty` < ' . $minQty . ') OR `e`.`entity_id` IN (' . $idsConfig . ')', array('qty', 'is_in_stock'));
                    break;
                default:
                    break;
            }

            $this->prepareProductCollection($collection);
            $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
        }
        return $collection;
    }

    /**
     * @param $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    /*protected function _getNewArrivalProductCollection($collection)
    {
        $todayStartOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection
            ->addAttributeToFilter('news_from_date', array('or' => array(
                0 => array('date' => true, 'to' => $todayEndOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('news_to_date', array('or' => array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'news_from_date', 'is' => new Zend_Db_Expr('not null')),
                    array('attribute' => 'news_to_date', 'is' => new Zend_Db_Expr('not null'))
                )
            )
            ->addAttributeToSort('news_from_date', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
        return $collection;
    }

    /**
     * @param $collection  Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    /*protected function _getBackInStockProductCollection($collection)
    {
        $todayStartOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection
            ->addAttributeToFilter('back_to_stock_from', array('or' => array(
                0 => array('date' => true, 'to' => $todayEndOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('back_to_stock_to', array('or' => array(
                0 => array('date' => true, 'from' => $todayStartOfDayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'back_to_stock_from', 'is' => new Zend_Db_Expr('not null')),
                    array('attribute' => 'back_to_stock_to', 'is' => new Zend_Db_Expr('not null'))
                )
            )
            ->addAttributeToSort('back_to_stock_from', 'desc');
        return $collection;
    }*/
}