<?php
 /**
 * GoMage Advanced Navigation Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2010-2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use
 * @version      Release: 4.2
 * @since        Class available since Release 1.0
 */
	
class GoMage_Navigation_Model_Resource_Eav_Mysql4_Product_Collection extends Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection{
		
	public function getSelectCountSql()
    {    	
        $select = parent::getSelectCountSql();
        $select->reset(Zend_Db_Select::GROUP);
        
        return $select;
    }
    
	public function getSearchedEntityIds(){
		return false;		
	}

    public function sortByNewArrivals($dir){
        $this->addAttributeToSelect('*')->addAttributeToSort('created_at', $dir);
    }
    public function sortByTopRated($dir){
        $table = $this->getTable('review/review_aggregate');
        $this->getSelect()->joinLeft(array('t2'=>$table), 't2.entity_pk_value = e.entity_id',array('rating_summary' => new Zend_Db_Expr('SUM(rating_summary)')))
            ->group('e.entity_id')->order("rating_summary $dir");
    }
    public function sortByMostLoved($dir){
        $table = $this->getTable('cavabien_loveit/loveit');
        $this->getSelect()->joinLeft(array('t2'=>$table), 't2.product_id = e.entity_id',"COUNT(t2.loveit_id) AS loveit")
            ->group('e.entity_id')->order("loveit $dir");
    }
    public function sortByBestsellers($dir){
        $table = $this->getTable('sales/order_item');
        $this->getSelect()->joinLeft(array('t2' => $table),"e.entity_id = t2.product_id","SUM(t2.qty_ordered) AS ordered_qty")
             ->group("e.entity_id")->order("ordered_qty $dir");
    }
    public function sortByFaebookLikes($dir){
        $this->addAttributeToSelect('*')->addAttributeToSort('facebook_like', $dir);
    }
}