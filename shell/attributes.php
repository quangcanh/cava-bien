<?php
require_once 'abstract.php';
class Mage_Shell_Attribute extends Mage_Shell_Abstract
{
    public function createStyleAttributes($installer) {
        try {
            // Create attribute for sub category of 'Clothing'
            $optionsDress = array(
                'Bandeau Dress', 'BodyCon Dresses', 'Maxi Dresses', 'Mini Dresses', 'One Shoulder', 'Peplum Dresses', 'Prom Dresses', 'Shift Dresses', 'Skater Dresses', 'Tunic Dresses', 'Denim Dresses'
            );
            $optionsOuterWear = array(
                'Jackets', 'Coats', 'Blazers & Vests', 'Denim Coats'
            );
            $optionsTop = array(
                'Blouses', 'Bodysuits', 'Bustiers & Halters', 'Camis & Tanks', 'Sweaters', 'Tees & Tunics', 'Activewear', 'T-Shirts', 'Denim Blouses', 'Sleeveless', 'Shirts'
            );
            $optionsJumpsuits = array(
                'Culottes', 'Denim Dungarees', 'Denim Jumpsuits', 'Dungaree Shorts'
            );
            $optionsBottom = array(
                'Skirts', 'Trousers', 'Shorts', 'Leggings', 'Jeggings', 'Denim Skirts', 'Jeans & Denims', 'Denim Shorts'
            );
            $optionsKnitwear = array(
                'Cardigans', 'Cover-Up & Kaftans', 'Crop Tops', 'Jumpers', 'Ponchos', 'T-shirts & Vests'
            );

            // Create attribute for sub category of 'Intimates'
            $optionsBras = array(
                'Bras', 'Bra & Brief Sets', 'Knickers', 'Shorties', 'Thermals', 'Panties'
            );
            $optionsSocks = array(
                'Tights', 'Ankle Socks', 'Knee High Socks', 'Trainer Socks', 'Hold-Ups'
            );
            $optionsSwim = array(
                'Bikinis', 'Tankinis', 'Swimsuits', 'Coverups & Sarongs', 'Swim Shorts', 'Trunks'
            );
            $optionsLingerie = array(
                'Basques & Bodysuits', 'Camisoles & Slips', 'Corsets & Teddies', 'Suspenders & Garters'
            );
            $optionsNightWear = array(
                'Robes', 'Nightwear Set', 'Pajamas', 'Sleepshirts & Nighties', 'Loungewear', 'Onesies', 'Chemises'
            );
            $optionsShapewear = array(
                'Shop all Shapewear', 'Waist & Tummy', 'Bottom & Thighs', 'About Our Shapewear', 'Whole Body Shapewear'
            );

            // Create attribute for sub category of 'Shoes'
            $optionsDressShoes = array(
                'Heels', 'Pumps', 'Wedding Shoes', 'Flats', 'Evening', 'Business'
            );
            $optionsBoots = array(
                'Shoe Boots', 'Ankle Boots', 'Knee Boots', 'Mid-Calf Boots', 'Over-the-Knee', 'Snow Boots'
            );
            $optionsAthletic = array(
                'Fitness', 'Dance & Ballet', 'Walking & Running', 'Trainers & Sneakers', 'Health & Wellness', 'Tennis & Cycling'
            );
            $optionsCasual = array(
                'Dress Sandals', 'Mules & Clogs', 'Flats & Wedges', 'Gladiator', 'Flip-Flops & Slippers', 'Corporate Casual'
            );
            $optionsShopByTrend = array(
                'Sneaker Wedges', 'Aquatic', 'Black & White', 'Ankle Straps', 'Coral'
            );
            $optionsFeaturedProducts = array(
                '£70 & Under Heels and Pumps', '£50 & Under Flats and Loafers', '£100 & Under Boots', 'Colin Stuart', 'Boho Bags', 'New Arrivals', 'Beach Getaway Essentials', 'The Shoe Edit', 'Flip-Flop Hurray!'
            );

            // Create attribute for sub category of 'Bags'
            $optionsHandbags = array(
                'Evening & Clutches', 'Totes', 'Shoulder', 'Cross-Body Bags', 'Satchel', 'Backpacks', 'Messenger Bags', 'Hobos', 'Sports and Duffel', 'Clutches', 'Mini Bags'
            );
            $optionsBagAccessories = array(
                'Cosmetic Pouches', 'Wallets & Accessories', 'Beach Bags'
            );
            $optionsLuggage = array(
                'Laptop', 'Business'
            );
            $optionsWallets = array(
                'Standard Wallets', 'Clutch Wallets', 'Solid PU', 'Genuine Leather', 'Zipper'
            );
            $optionsBackPacks = array(
                'Softback', 'Solid', 'Canvas', 'Nylon'
            );
            $optionsMessengerBags = array(
                'Solid', 'PU', 'Letter', 'Genuine Leather', 'Plaid'
            );

            // Create attribute for sub category of 'Jewellery & Watches'
            $optionsJewellery = array(
                'Earrings', 'Necklaces & Pendants', 'Bracelets & Bangles', 'Body Jewelry', 'Charms & Beads', 'Brooches', 'Jewellery Sets', 'Rings'
            );
            $optionsJewelleryAccessories = array(
                'Tiaras & Side Tiaras', 'Hair Combs', 'Hair Clips & Claws', 'Head Bands', 'Headpieces', 'Hair Sticks & Pins', 'Hair Slides'
            );
            $optionsWatchesByStyle = array(
                'Chronograph Watch', 'Digital Watches', 'Leather Watches', 'Vintage Watches', 'Silicone Watches', 'Ceramic Watches'
            );
            $optionsJewelleryByStyle = array(
                'Fashion Jewellery', 'Crystal Jewellery', 'Vintage Jewellery', 'Wedding Jewellery', 'Bridesmaid Jewellery'
            );
            $optionsAccessoriesByStyle = array(
                'Fashion Accessories', 'Crystal Accessories', 'Vintage Accessories', 'Wedding Accessories', 'Bridesmaid Accessories'
            );
            $optionsWatches = array(
                'Sport Watches', 'Bracelet Watches', 'Dress Watches', 'Charm Watches', 'Vintage Watches', 'Fashion & Casual Watches', 'Pocket & Fob Watch'
            );

            // Create attribute for sub category of 'Hair & Beauty'
            $optionsWeftsExtension = array(
                'Remy Indian Hair', 'Virgin Brazilian Hair', 'Virgin Malaysian Hair', 'Virgin Peruvian Hair', 'Virgin Indian Hair', 'Virgin Cambodia Hair', 'Virgin Mongolia Hair', 'European Virgin Hair', 'FUSION / PREBONDED'
            );
            $optionsFusionPrebonded = array(
                'Nail Tip (U TIP)', 'Stick Tip (I Tip)', '50S Per Set'
            );
            $optionsClipInExtensions = array(
                'Human Clip In Hair', 'Synthetic Clip In Hair', 'Blended Clip In Hair', 'Feather Clip In Hair'
            );
            $optionsHairPiecesBurns = array(
                'Hair Bangs', 'Ponytails', 'Chignons', 'Toupees', 'Hair Braids', 'Hair Buns'
            );
            $optionsWigs = array(
                'Lace Front Wigs', 'Full Lace Wigs', 'Lace Frontal', 'Half Wigs', 'Cosplay Wigs', 'Party Wigs', 'Costume Wigs'
            );
            $optionsLaceClosures = array(
                'Virgin Brazilian Lace Closure', 'Virgin Malaysian Lace Closure', 'Virgin Peruvian Lace Closure', 'Virgin Indian Lace Closure', 'Synthetic Lace Closure'
            );

            // Create attribute for sub category of 'Accessories'
            $optionsFashionAccessories = array(
                'Sunglasses', 'Umbrellas', 'Gloves & Mittens', 'Fashion Scarves', 'Wraps & Pashminas', 'Wallets', 'Belts & Cummerbunds', 'Bridal Veils', 'Hats & Caps'
            );
            $optionsBeachAccessories = array(
                'Tanning & Suncare', 'Beach Towels', 'Swimming Accessories'
            );
            $optionsLingerieAccessories = array(
                'Gel pads & cups', 'Adhesive covering', 'Caps & clasps', 'Bra straps & extenders'
            );
            $optionsShoeAccessories = array(
                'Insoles'
            );
            $optionsHairAccessories = array(
                'Bandanas', 'Hair Bands', 'Hair Combs', 'Hair Ribbons', 'Hair Flowers', 'Hair Grips & Pins'
            );
            $optionsTechAccessories = array(

            );

            // Create attribute for sub category of 'Sport & Leisure'
            $optionsSportClothing = array(
                'Sports Outerwear', 'Sports Swimwear', 'Sports Underwear', 'Cycling Wear', 'Baselayers & Leggings', 'Running Wear', 'Fitness Wear', 'Tennis Wear', 'Yoga & Pilates Wear', 'SPORT ACCESSORIES'
            );
            $optionsSportAccessories = array(
                'Sports Bags', 'Sports Gloves', 'Sports Socks', 'Sport Hats & Scarves', 'Sports Eyewear', 'Swimming Accessories'
            );
            $optionsSportShoes = array(
                'Running Shoes', 'Trail & Walking Shoes', 'Cross Trainers', 'Tennis Shoes', 'Toning Shoes', 'Water Shoes', 'Snow Boots', 'Sports Sandals'
            );

            $attributes = array(
                'dresses' => array('label' => 'DRESSES', 'options' => $optionsDress),
                'outerwear' => array('label' => 'OUTERWEAR', 'options' => $optionsOuterWear),
                'top_shirt_blouses' => array('label' => 'TOPS, SHIRTS & BLOUSES', 'options' => $optionsTop),
                'jumpsuits_playsuits' => array('label' => 'JUMPSUITS & PLAYSUITS', 'options' => $optionsJumpsuits),
                'bottom' => array('label' => 'BOTTOM', 'options' => $optionsBottom),
                'knitwear' => array('label' => 'KNITWEAR', 'options' => $optionsKnitwear),

                'bras_underwear' => array('label' => 'BRAS & UNDERWEAR', 'options' => $optionsBras),
                'socks_hosiery' => array('label' => 'SOCKS & HOSIERY', 'options' => $optionsSocks),
                'swim_beachwear' => array('label' => 'SWIM & BEACHWEAR', 'options' => $optionsSwim),
                'lingerie' => array('label' => 'LINGERIE', 'options' => $optionsLingerie),
                'nightwear' => array('label' => 'NIGHTWEAR', 'options' => $optionsNightwear),
                'shapewear' => array('label' => 'SHAPEWEAR', 'options' => $optionsShapewear),

                'dresses_shoes' => array('label' => 'DRESS SHOES', 'options' => $optionsDressShoes),
                'boots_booties' => array('label' => 'BOOTS & BOOTIES', 'options' => $optionsBoots),
                'athletic_outdoor' => array('label' => 'ATHLETIC & OUTDOOR', 'options' => $optionsAthletic),
                'casuals_sandals' => array('label' => 'CASUAL & SANDALS', 'options' => $optionsCasual),
                'shop_by_trend' => array('label' => 'SHOP BY TREND', 'options' => $optionsShopByTrend),
                'featured_products' => array('label' => 'FEATURED PRODUCTS', 'options' => $optionsFeaturedProducts),

                'handbags' => array('label' => 'HANDBAGS', 'options' => $optionsHandbags),
                'bag_accessories' => array('label' => 'BAG ACCESSORIES', 'options' => $optionsBagAccessories),
                'luggage' => array('label' => 'LUGGAGE', 'options' => $optionsLuggage),
                'wallets' => array('label' => 'WALLETS', 'options' => $optionsWallets),
                'backpacks' => array('label' => 'BACKPACKS', 'options' => $optionsBackPacks),
                'messenger_bags' => array('label' => 'MESSENGER BAGS', 'options' => $optionsMessengerBags),

                'jewellery' => array('label' => 'JEWELLERY', 'options' => $optionsJewellery),
                'jewellery_access' => array('label' => 'JEWELLED ACCESSORIES', 'options' => $optionsJewelleryAccessories),
                'watches_style' => array('label' => 'WATCHES BY STYLE', 'options' => $optionsWatchesByStyle),
                'jewellery_style' => array('label' => 'JEWELLERY BY STYLE', 'options' => $optionsJewelleryByStyle),
                'accessories_style' => array('label' => 'ACCESSORIES BY STYLE', 'options' => $optionsAccessoriesByStyle),
                'watches' => array('label' => 'WATCHES', 'options' => $optionsWatches),

                'wefts_extensions' => array('label' => 'WEFTS EXTENSIONS', 'options' => $optionsWeftsExtension),
                'fusion_prebonded' => array('label' => 'FUSION / PREBONDED', 'options' => $optionsFusionPrebonded),
                'clip_in_extensions' => array('label' => 'CLIP IN EXTENSIONS', 'options' => $optionsClipInExtensions),
                'hair_pieces_burns' => array('label' => 'HAIR PIECES & BURNS', 'options' => $optionsHairPiecesBurns),
                'wigs' => array('label' => 'WIGS', 'options' => $optionsWigs),
                'lace_closures' => array('label' => 'LACE CLOSURES', 'options' => $optionsLaceClosures),

                'fashion_access' => array('label' => 'FASHION ACCESSORIES', 'options' => $optionsFashionAccessories),
                'beach_access' => array('label' => 'BEACH ACCESSORIES', 'options' => $optionsBeachAccessories),
                'lingerie_access' => array('label' => 'LINGERIE ACCESSORIES', 'options' => $optionsLingerieAccessories),
                'shoe_access' => array('label' => 'SHOE ACCESSORIES', 'options' => $optionsShoeAccessories),
                'hair_access' => array('label' => 'HAIR ACCESSORIES', 'options' => $optionsHairAccessories),
                'tech_access' => array('label' => 'TECH ACCESSORIES', 'options' => $optionsTechAccessories),

                'sport_clothing' => array('label' => 'SPORT CLOTHING', 'options' => $optionsSportClothing),
                'sport_access' => array('label' => 'SPORT ACCESSORIES', 'options' => $optionsSportAccessories),
                'sport_shoes' => array('label' => 'SPORT SHOES', 'options' => $optionsSportShoes)
            );

            foreach ($attributes as $code => $data) {
                $code = 'styles_' . $code;
                $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, array(
                    'input' => 'multiselect',
                    'type' => 'varchar',
                    'label' => $data['label'],
                    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                    'backend' => 'eav/entity_attribute_backend_array',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => true,
                    'comparable' => true,
                    'default' => NULL,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'visible_in_advanced_search' => true,
                    'is_html_allowed_on_front' => '0',
                    'filterable_in_search' => true,
                    'is_configurable' => true,
                    'apply_to' => 'simple,grouped,configurable',
                    'option' => array(
                        'values' => $data['options']
                    )
                ));
                $this->updateAttribute($code);
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function createAttributesForCat($installer) {
        try {
            $optionsDiscountOffer = array(
                'Buy One Get One Free', 'Buy 1, Get 1 50% Off', 'Buy 2 or More For £25', '0% - 20% Steal Deals', '20% - 40% Vintage Style', '40% - 60% Fab Deals', '60% - 80% Not to Miss', '80% - 100% High End', 'Last Chance to Buy'
            );
            $optionsStylistPicks = array(
                'Lace & Crochet', 'Get Graphic', 'Summer Tops & Shorts', 'Sexy To A Tee', 'Holiday Dresses'
            );
            $attributes = array(
                'clothing_discount_offer' => array('label' => 'Clothing - Discount & Offer', 'options' => $optionsDiscountOffer),
                'intimates_discount_offer' => array('label' => 'Intimates - Discount & Offer', 'options' => $optionsDiscountOffer),
                'shoes_discount_offer' => array('label' => 'Shoes - Discount & Offer', 'options' => $optionsDiscountOffer),
                'bags_discount_offer' => array('label' => 'Bags - Discount & Offer', 'options' => $optionsDiscountOffer),
                'jewellery_discount_offer' => array('label' => 'Jewellery - Discount & Offer', 'options' => $optionsDiscountOffer),
                'hair_discount_offer' => array('label' => 'Hair - Discount & Offer', 'options' => $optionsDiscountOffer),
                'accessories_discount_offer' => array('label' => 'Accessories - Discount & Offer', 'options' => $optionsDiscountOffer),
                'sport_discount_offer' => array('label' => 'Sport - Discount & Offer', 'options' => $optionsDiscountOffer),

                'clothing_stylist_picks' => array('label' => 'Clothing - Stylist Picks', 'options' => $optionsStylistPicks),
                'intimates_stylist_picks' => array('label' => 'Intimates - Stylist Picks', 'options' => $optionsStylistPicks),
                'shoes_stylist_picks' => array('label' => 'Shoes - Stylist Picks', 'options' => $optionsStylistPicks),
                'bags_stylist_picks' => array('label' => 'Bags - Stylist Picks', 'options' => $optionsStylistPicks),
                'jewellery_stylist_picks' => array('label' => 'Jewellery - Stylist Picks', 'options' => $optionsStylistPicks),
                'hair_stylist_picks' => array('label' => 'Hair - Stylist Picks', 'options' => $optionsStylistPicks),
                'accessories_stylist_picks' => array('label' => 'Accessories - Stylist Picks', 'options' => $optionsStylistPicks),
                'sport_stylist_picks' => array('label' => 'Sport - Stylist Picks', 'options' => $optionsStylistPicks),
            );
            foreach ($attributes as $code => $data) {
                $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $code, array(
                    'input' => 'multiselect',
                    'type' => 'varchar',
                    'label' => $data['label'],
                    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                    'backend' => 'eav/entity_attribute_backend_array',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => true,
                    'comparable' => true,
                    'default' => NULL,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'visible_in_advanced_search' => true,
                    'is_html_allowed_on_front' => '0',
                    'filterable_in_search' => true,
                    'is_configurable' => true,
                    'apply_to' => 'simple,grouped,configurable',
                    'option' => array(
                        'values' => $data['options']
                    )
                ));
                $this->updateAttribute($code);
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function createAttributeSets($installer) {
        try {
            $categoryNames = array(
                'Clothing' => array(
                    'clothing_discount_offer' => 10,
                    'clothing_stylist_picks' => 20,
                    'styles_dresses' => 30,
                    'styles_outerwear' => 40,
                    'styles_top_shirt_blouses' => 50,
                    'styles_jumpsuits_playsuits' => 60,
                    'styles_bottom' => 70,
                    'styles_knitwear' => 80
                ),
                'Intimates' => array(
                    'intimates_discount_offer' => 10,
                    'intimates_stylist_picks' => 20,
                    'styles_bras_underwear' => 30,
                    'styles_socks_hosiery' => 40,
                    'styles_swim_beachwear' => 50,
                    'styles_lingerie' => 60,
                    'styles_nightwear' => 70,
                    'styles_shapewear' => 80
                ),
                'Shoes' => array(
                    'shoes_discount_offer' => 10,
                    'shoes_stylist_picks' => 20,
                    'styles_dresses_shoes' => 30,
                    'styles_boots_booties' => 40,
                    'styles_athletic_outdoor' => 50,
                    'styles_casuals_sandals' => 60,
                    'styles_shop_by_trend' => 70,
                    'styles_featured_products' => 80
                ),
                'Bags' => array(
                    'bags_discount_offer' => 10,
                    'bags_stylist_picks' => 20,
                    'styles_handbags' => 30,
                    'styles_bag_accessories' => 40,
                    'styles_luggage' => 50,
                    'styles_wallets' => 60,
                    'styles_backpacks' => 70,
                    'styles_messenger_bags' => 80
                ),
                'Jewellery & Watches' => array(
                    'jewellery_discount_offer' => 10,
                    'jewellery_stylist_picks' => 20,
                    'styles_jewellery' => 30,
                    'styles_jewellery_access' => 40,
                    'styles_watches_style' => 50,
                    'styles_jewellery_style' => 60,
                    'styles_accessories_style' => 70,
                    'styles_watches' => 80
                ),
                'Hair & Beauty' => array(
                    'hair_discount_offer' => 10,
                    'hair_stylist_picks' => 20,
                    'styles_wefts_extensions' => 30,
                    'styles_fusion_prebonded' => 40,
                    'styles_clip_in_extensions' => 50,
                    'styles_hair_pieces_burns' => 60,
                    'styles_wigs' => 70,
                    'styles_lace_closures' => 80
                ),
                'Accessories' => array(
                    'accessories_discount_offer' => 10,
                    'accessories_stylist_picks' => 20,
                    'styles_fashion_access' => 30,
                    'styles_beach_access' => 40,
                    'styles_lingerie_access' => 50,
                    'styles_shoe_access' => 60,
                    'styles_hair_access' => 70,
                    'styles_tech_access' => 80
                ),
                'Sport & Leisure' => array(
                    'sport_discount_offer' => 10,
                    'sport_stylist_picks' => 20,
                    'styles_sport_clothing' => 30,
                    'styles_sport_access' => 40,
                    'styles_sport_shoes' => 50
                ),
            );

            // Create new attribute sets for categories
            foreach ($categoryNames as $name => $attrs) {
                $cloneSetId = 4; //id of default attribute set
                $entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();
                $attributeSet = Mage::getModel('eav/entity_attribute_set');
                $attributeSet->setEntityTypeId($entityTypeId);
                $attributeSet->setAttributeSetName($name);
                $attributeSet->validate();
                $attributeSet->save();
                $attributeSet->initFromSkeleton($cloneSetId);
                $attributeSet->save();

                //Create new Group
                $attributeGroup = Mage::getModel('eav/entity_attribute_group');
                $attributeGroup->setAttributeGroupName('Attributes for Category')
                    ->setAttributeSetId($attributeSet->getId())
                    ->setSortOrder(40);
                $attributeGroup->save();

                //Add attributes for new group
                foreach ($attrs as $code => $sort) {
                    $installer->addAttributeToGroup('catalog_product', $attributeSet->getId(), $attributeGroup->getId(), $code, $sort);
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function removeAll($installer) {
        $attributes = array(
            'dresses' => array('label' => 'DRESSES', 'options' => $optionsDress),
            'outerwear' => array('label' => 'OUTERWEAR', 'options' => $optionsOuterWear),
            'top_shirt_blouses' => array('label' => 'TOPS, SHIRTS & BLOUSES', 'options' => $optionsTop),
            'jumpsuits_playsuits' => array('label' => 'JUMPSUITS & PLAYSUITS', 'options' => $optionsJumpsuits),
            'bottom' => array('label' => 'BOTTOM', 'options' => $optionsBottom),
            'knitwear' => array('label' => 'KNITWEAR', 'options' => $optionsKnitwear),

            'bras_underwear' => array('label' => 'BRAS & UNDERWEAR', 'options' => $optionsBras),
            'socks_hosiery' => array('label' => 'SOCKS & HOSIERY', 'options' => $optionsSocks),
            'swim_beachwear' => array('label' => 'SWIM & BEACHWEAR', 'options' => $optionsSwim),
            'lingerie' => array('label' => 'LINGERIE', 'options' => $optionsLingerie),
            'nightwear' => array('label' => 'NIGHTWEAR', 'options' => $optionsNightwear),
            'shapewear' => array('label' => 'SHAPEWEAR', 'options' => $optionsShapewear),

            'dresses_shoes' => array('label' => 'DRESS SHOES', 'options' => $optionsDressShoes),
            'boots_booties' => array('label' => 'BOOTS & BOOTIES', 'options' => $optionsBoots),
            'athletic_outdoor' => array('label' => 'ATHLETIC & OUTDOOR', 'options' => $optionsAthletic),
            'casuals_sandals' => array('label' => 'CASUAL & SANDALS', 'options' => $optionsCasual),
            'shop_by_trend' => array('label' => 'SHOP BY TREND', 'options' => $optionsShopByTrend),
            'featured_products' => array('label' => 'FEATURED PRODUCTS', 'options' => $optionsFeaturedProducts),

            'handbags' => array('label' => 'HANDBAGS', 'options' => $optionsHandbags),
            'bag_accessories' => array('label' => 'BAG ACCESSORIES', 'options' => $optionsBagAccessories),
            'luggage' => array('label' => 'LUGGAGE', 'options' => $optionsLuggage),
            'wallets' => array('label' => 'WALLETS', 'options' => $optionsWallets),
            'backpacks' => array('label' => 'BACKPACKS', 'options' => $optionsBackPacks),
            'messenger_bags' => array('label' => 'MESSENGER BAGS', 'options' => $optionsMessengerBags),

            'jewellery' => array('label' => 'JEWELLERY', 'options' => $optionsJewellery),
            'jewellery_access' => array('label' => 'JEWELLED ACCESSORIES', 'options' => $optionsJewelleryAccessories),
            'watches_style' => array('label' => 'WATCHES BY STYLE', 'options' => $optionsWatchesByStyle),
            'jewellery_style' => array('label' => 'JEWELLERY BY STYLE', 'options' => $optionsJewelleryByStyle),
            'accessories_style' => array('label' => 'ACCESSORIES BY STYLE', 'options' => $optionsAccessoriesByStyle),
            'watches' => array('label' => 'WATCHES', 'options' => $optionsWatches),

            'wefts_extensions' => array('label' => 'WEFTS EXTENSIONS', 'options' => $optionsWeftsExtension),
            'fusion_prebonded' => array('label' => 'FUSION / PREBONDED', 'options' => $optionsFusionPrebonded),
            'clip_in_extensions' => array('label' => 'CLIP IN EXTENSIONS', 'options' => $optionsClipInExtensions),
            'hair_pieces_burns' => array('label' => 'HAIR PIECES & BURNS', 'options' => $optionsHairPiecesBurns),
            'wigs' => array('label' => 'WIGS', 'options' => $optionsWigs),
            'lace_closures' => array('label' => 'LACE CLOSURES', 'options' => $optionsLaceClosures),

            'fashion_access' => array('label' => 'FASHION ACCESSORIES', 'options' => $optionsFashionAccessories),
            'beach_access' => array('label' => 'BEACH ACCESSORIES', 'options' => $optionsBeachAccessories),
            'lingerie_access' => array('label' => 'LINGERIE ACCESSORIES', 'options' => $optionsLingerieAccessories),
            'shoe_access' => array('label' => 'SHOE ACCESSORIES', 'options' => $optionsShoeAccessories),
            'hair_access' => array('label' => 'HAIR ACCESSORIES', 'options' => $optionsHairAccessories),
            'tech_access' => array('label' => 'TECH ACCESSORIES', 'options' => $optionsTechAccessories),

            'sport_clothing' => array('label' => 'SPORT CLOTHING', 'options' => $optionsSportClothing),
            'sport_access' => array('label' => 'SPORT ACCESSORIES', 'options' => $optionsSportAccessories),
            'sport_shoes' => array('label' => 'SPORT SHOES', 'options' => $optionsSportShoes)
        );
        foreach ($attributes as $code => $data) {
            $code = 'styles_' . $code;
            $installer->removeAttribute('catalog_product', $code);
        }

        $attributes = array(
            'clothing_discount_offer' => array('label' => 'Clothing - Discount & Offer', 'options' => $optionsDiscountOffer),
            'intimates_discount_offer' => array('label' => 'Intimates - Discount & Offer', 'options' => $optionsDiscountOffer),
            'shoes_discount_offer' => array('label' => 'Shoes - Discount & Offer', 'options' => $optionsDiscountOffer),
            'bags_discount_offer' => array('label' => 'Bags - Discount & Offer', 'options' => $optionsDiscountOffer),
            'jewellery_discount_offer' => array('label' => 'Jewellery - Discount & Offer', 'options' => $optionsDiscountOffer),
            'hair_discount_offer' => array('label' => 'Hair - Discount & Offer', 'options' => $optionsDiscountOffer),
            'accessories_discount_offer' => array('label' => 'Accessories - Discount & Offer', 'options' => $optionsDiscountOffer),
            'sport_discount_offer' => array('label' => 'Sport - Discount & Offer', 'options' => $optionsDiscountOffer),

            'clothing_stylist_picks' => array('label' => 'Clothing - Stylist Picks', 'options' => $optionsStylistPicks),
            'intimates_stylist_picks' => array('label' => 'Intimates - Stylist Picks', 'options' => $optionsStylistPicks),
            'shoes_stylist_picks' => array('label' => 'Shoes - Stylist Picks', 'options' => $optionsStylistPicks),
            'bags_stylist_picks' => array('label' => 'Bags - Stylist Picks', 'options' => $optionsStylistPicks),
            'jewellery_stylist_picks' => array('label' => 'Jewellery - Stylist Picks', 'options' => $optionsStylistPicks),
            'hair_stylist_picks' => array('label' => 'Hair - Stylist Picks', 'options' => $optionsStylistPicks),
            'accessories_stylist_picks' => array('label' => 'Accessories - Stylist Picks', 'options' => $optionsStylistPicks),
            'sport_stylist_picks' => array('label' => 'Sport - Stylist Picks', 'options' => $optionsStylistPicks),
        );
        foreach ($attributes as $code => $data) {
            $installer->removeAttribute('catalog_product', $code);
        }
        for ($i = 94; $i < 102; $i++) {
            try {
                Mage::getModel("eav/entity_attribute_set")->load($i)->delete();
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
        echo 'Done';
    }

    public function updateAttribute($code) {
        $data = array(
            'filter_type' => '0',
            'inblock_type' => '1',
            'round_to' => '',
            'show_currency' => '0',
            'image_align' => '0',
            'image_width' => '',
            'image_height' => '',
            'show_minimized' => '0',
            'show_image_name' => '0',
            'show_checkbox' => '1',
            'visible_options' => '',
            'show_help' => '0',
            'popup_width' => '',
            'popup_height' => '',
            'filter_reset' => '1',
            'is_ajax' => '1',
            'inblock_height' => '',
            'max_inblock_height' => '',
            'filter_button' => '0',
            'category_ids_filter' => '',
            'range_options' => '0',
            'range_manual' => '',
            'range_auto' => '',
            'attribute_location' => '0');
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $code);
        $attributeModel->addData($data);
        $event = new Varien_Object();
        $event->setAttribute($attributeModel);
        Mage::getModel('gomage_navigation/observer')->saveAttribute($event);
    }

    public function run()
    {
        $installer = new Mage_Catalog_Model_Resource_Setup('core_setup');
        $this->createStyleAttributes($installer);
        $this->createAttributesForCat($installer);
        $this->createAttributeSets($installer);
//        $this->removeAll($installer);
    }

    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f attributes.php

  -h            Short alias for help
  help          This help
USAGE;
    }
}

$shell = new Mage_Shell_Attribute();
$shell->run();